Imports WPM_EDCRYPT
Public Class frm_MF_copassword

    Private gTrans As New clsTransactionFunctions
    Private gEncrypt As New WPMED

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            If gTrans.gVerifyCompanyUser(gEncrypt.gEncryptString(txtCoPass.Text), Me) = "Load Successful." Then
                MsgBox(gTrans.gVerifyCompanyUser(gEncrypt.gEncryptString(txtCoPass.Text), Me), MsgBoxStyle.Information, Me.Text)
                gOpenCompany = 1
                Try
                    frm_MF_Company.txtcolegal.Text = gLegalName
                    frm_MF_Company.txtcotaxid.Text = gTaxId
                    frm_MF_Company.txtcostadd.Text = gStAdd
                    frm_MF_Company.txtcocityadd.Text = gCityAdd
                    frm_MF_Company.txtcozipadd.Text = gZipAdd
                    frm_MF_Company.txtcophone.Text = gPhone
                    frm_MF_Company.txtcofax.Text = gFax
                    frm_MF_Company.txtcoemail.Text = gEmail
                    frm_MF_Company.txtcosite.Text = gWebSite
                    frm_MF_Company.cboconame.Enabled = False
                    frm_MF_Company.Refresh()
                    If gClassingStatus = 1 Then
                        frmMain.LblClassActivator.Visible = True
                        frmMain.ClassToolStripMenuItem.Visible = True
                    Else
                        frmMain.LblClassActivator.Visible = False
                        frmMain.ClassToolStripMenuItem.Visible = False
                    End If
                Catch ex As Exception
                End Try
                Me.Close()
            Else
                MsgBox(gTrans.gVerifyCompanyUser(gEncrypt.gEncryptString(txtCoPass.Text), Me), MsgBoxStyle.Information, Me.Text)
                gOpenCompany = 0
                txtCoPass.Text = ""
            End If
        Catch ex As Exception
            MsgBox("Invalid password." + vbCr + "Try Again...", MsgBoxStyle.Critical, Me.Text)
            txtCoPass.Select()
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        DiscardGlobalVariables()
        Me.Close()
    End Sub

    Private Sub DiscardGlobalVariables()
        txtCoPass.Text = ""
        gCompanyName = ""
    End Sub
    Private Sub frm_MF_copassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gOpenCompany = 0
        txtCoPass.Text = ""
        txtCoPass.Select()
    End Sub

    Private Sub txtCoPass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCoPass.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            Try
                If gTrans.gVerifyCompanyUser(gEncrypt.gEncryptString(txtCoPass.Text), Me) = "Load Successful." Then
                    MsgBox(gTrans.gVerifyCompanyUser(gEncrypt.gEncryptString(txtCoPass.Text), Me), MsgBoxStyle.Information, Me.Text)
                    gOpenCompany = 1
                    Try
                        frm_MF_Company.txtcolegal.Text = gLegalName
                        frm_MF_Company.txtcotaxid.Text = gTaxId
                        frm_MF_Company.txtcostadd.Text = gStAdd
                        frm_MF_Company.txtcocityadd.Text = gCityAdd
                        frm_MF_Company.txtcozipadd.Text = gZipAdd
                        frm_MF_Company.txtcophone.Text = gPhone
                        frm_MF_Company.txtcofax.Text = gFax
                        frm_MF_Company.txtcoemail.Text = gEmail
                        frm_MF_Company.txtcosite.Text = gWebSite
                        frm_MF_Company.Refresh()
                        If gClassingStatus = 1 Then
                            frmMain.LblClassActivator.Visible = True
                            frmMain.ClassToolStripMenuItem.Visible = True
                        Else
                            frmMain.LblClassActivator.Visible = False
                            frmMain.ClassToolStripMenuItem.Visible = False
                        End If
                    Catch ex As Exception
                    End Try
                    Me.Close()
                Else
                    MsgBox(gTrans.gVerifyCompanyUser(gEncrypt.gEncryptString(txtCoPass.Text), Me), MsgBoxStyle.Information, Me.Text)
                    gOpenCompany = 0
                    txtCoPass.Text = ""
                End If
            Catch ex As Exception
                MsgBox("Try Again...", MsgBoxStyle.Critical, Me.Text)
            End Try
        End If
    End Sub

 
End Class