Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_vend_masterVendorAddress
    Private gCon As New Clsappconfiguration
    Private sKeyAddress As String
    Private sKeySupplier As String

    Public Property KeyAddress() As String
        Get
            Return sKeyAddress
        End Get
        Set(ByVal value As String)
            sKeyAddress = value
        End Set
    End Property

    Public Property KeySupplier() As String
        Get
            Return sKeySupplier
        End Get
        Set(ByVal value As String)
            sKeySupplier = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frm_vend_masterVendorAddress_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadAddress()
    End Sub

    Private Sub loadAddress()
        Dim sSQLCmd As String = "SELECT * FROM mAddress WHERE fxKeyAddress = '" & sKeyAddress & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtStreet.Text = rd.Item("fcStreet")
                    txtCity.Text = rd.Item("fcCity")
                    txtPostal.Text = rd.Item("fnPostal")
                    txtNotes.Text = rd.Item("fcNote")
                    txtAddress.Text = rd.Item("fcAddress")
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Address")
        End Try
    End Sub

    Private Sub addressUpdate()
        Dim sSQLCmd As String = "SPU_AddressUpdate "
        sSQLCmd &= "@fxKeyAddress ='" & sKeyAddress & "'"
        sSQLCmd &= ",@fxKeyID ='" & sKeySupplier & "'"
        sSQLCmd &= ",@fcAddress ='" & txtAddress.Text & "'"
        sSQLCmd &= ",@fcStreet ='" & txtStreet.Text & "'"
        sSQLCmd &= ",@fcCity ='" & txtCity.Text & "'"
        sSQLCmd &= ",@fnPostal =" & txtPostal.Text
        sSQLCmd &= ",@fcNote ='" & txtNotes.Text & "'"
        sSQLCmd &= ",@fbSupplier = 1 , @fbCustomer = 0 , @fbOther = 0 "

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated", "Update Address")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Address")
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        addressUpdate()
        'frm_vend_masterVendorAddEdit.loadSupplier()
        'If Trim(frm_vend_masterVendorAddEdit.txtAddress.Text) <> Trim(combineAddress()) Then
        '    frm_vend_masterVendorAddEdit.txtAddress.Text = combineAddress()
        'End If
        Me.Close()
    End Sub

    Private Function combineAddress() As String
        combineAddress = Trim(txtStreet.Text) & " " & Trim(txtCity.Text) & vbCrLf & _
        Trim(txtPostal.Text) & vbCrLf & txtNotes.Text
    End Function

    Private Sub txtPostal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPostal.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

End Class