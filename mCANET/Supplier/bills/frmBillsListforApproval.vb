Imports Microsoft.ApplicationBlocks.Data
Imports System.data.SqlClient

Public Class frmBillsListforApproval
    Private gCon As New Clsappconfiguration

    Private Sub frmBillsListforApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sSqlCmd As String = "CAS_t_Company_DateStart_Get '" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    dteFrm.Value = rd.Item(0)
                End While
            End Using
        Catch ex As Exception
            ShowErrorMessage("frmBillsListforApproval_Load", Me.Name, ex.Message)
        End Try
        Call loadbillslist()
    End Sub

    Private Sub chkViewAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkViewAll.CheckedChanged
        Call loadbillslist()
    End Sub

    Private Sub btnApproved_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        Dim isReviewed As Boolean = False

        Dim xForm As New frmPostingVerification
        xForm.xParent = Me
        xForm.Mode = "Approving"
        xForm.ShowDialog()

        If chkReview.Checked = True Then
            Call loadreviewbills()
            dteUntil.Enabled = False
            dteFrm.Enabled = False
        Else
            loadbillslist()
            dteUntil.Enabled = True
            dteFrm.Enabled = True
        End If
    End Sub

    Private Sub dteFrm_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteFrm.ValueChanged
        Call loadbillslist()
    End Sub

    Private Sub dteUntil_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteUntil.ValueChanged
        Call loadbillslist()
    End Sub

    Private Sub btnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click
        Call selectall()
    End Sub

    Private Sub grdBillsApproval_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdBillsApproval.CellDoubleClick
        With grdBillsApproval
            frm_vend_EnterBills.SupplierBill = False
            frm_vend_EnterBills.Mode = 2
            frm_vend_EnterBills.KeyBill = .Item(0, .CurrentRow.Index).Value.ToString
            frm_vend_EnterBills.ShowDialog()
        End With
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            gKeyBills = Me.grdBillsApproval.CurrentRow.Cells(0).Value.ToString
            frmAccountsPayableVoucher.GetbillID() = gKeyBills
            Dim xForm As New frmaccntvoucherparameters
            xForm.ShowDialog()
        Catch ex As Exception
            MessageBox.Show("Please Select first the bill you want to view.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub btnReview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReview.Click
        Dim xForm As New frmPostingVerification
        xForm.xParent = Me
        xForm.Mode = "Review"
        xForm.ShowDialog()
        If chkReview.Checked = True Then
            Call loadreviewbills()
            dteUntil.Enabled = False
            dteFrm.Enabled = False
        Else
            loadbillslist()
            dteUntil.Enabled = True
            dteFrm.Enabled = True
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub chkReview_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkReview.CheckedChanged
        If chkReview.Checked = True Then
            Call loadreviewbills()
            dteUntil.Enabled = False
            dteFrm.Enabled = False
        Else
            loadbillslist()
            dteUntil.Enabled = True
            dteFrm.Enabled = True
        End If
    End Sub

    Private Sub loadbillslist()

        grdBillsApproval.Columns.Clear()
        With grdBillsApproval
            Dim cReview As New DataGridViewCheckBoxColumn
            Dim dtBill As DataTable = m_GetBillsforApproval(Microsoft.VisualBasic.FormatDateTime(dteFrm.Value, DateFormat.ShortDate), _
                                                            Microsoft.VisualBasic.FormatDateTime(dteUntil.Value, DateFormat.ShortDate), view_all()).Tables(0)
            .DataSource = dtBill.DefaultView
            .Columns(0).Visible = False
            .Columns(2).ReadOnly = True
            .Columns(3).ReadOnly = True
            .Columns(4).ReadOnly = True
            .Columns(5).ReadOnly = True
            .Columns(6).ReadOnly = True
        End With
      
    End Sub

    Private Sub loadreviewbills()
        grdBillsApproval.Columns.Clear()
        With grdBillsApproval
            Dim cReview As New DataGridViewCheckBoxColumn
            Dim dtBill As DataTable = m_GetBillsforApprovalReviewed().Tables(0)
            .DataSource = dtBill.DefaultView
            .Columns(0).Visible = False
            .Columns(1).Width = 50
            .Columns(2).Width = 80
            .Columns(3).Width = 233
            .Columns(4).Width = 95
            .Columns(5).Width = 70
            .Columns(6).Width = 70
            .Columns(7).Width = 55
        End With
    End Sub

    Private Function view_all() As String
        Dim result As String = ""
        If chkViewAll.Checked = True Then
            dteFrm.Enabled = False
            dteUntil.Enabled = False
            result = "True"
        ElseIf chkViewAll.Checked = False Then
            dteFrm.Enabled = True
            dteUntil.Enabled = True
            result = "False"
        End If
        Return result
    End Function

    Private Sub selectall()
        Dim xCnt As Integer
        With grdBillsApproval
            For xCnt = 0 To .Rows.Count - 1
                If .Rows(xCnt).Cells(2).Value IsNot Nothing Then
                    .Rows(xCnt).Cells(1).Value = True
                End If
            Next
        End With
    End Sub

    Private Sub grdBillsApproval_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdBillsApproval.CurrentCellDirtyStateChanged
        If grdBillsApproval.IsCurrentRowDirty = True Then
            grdBillsApproval.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    Private Sub btnPostAsPettyCash_Click(sender As System.Object, e As System.EventArgs) Handles btnPostAsPettyCash.Click
        Dim isReviewed As Boolean = False

        Dim xForm As New frmPostingVerification
        xForm.xParent = Me
        xForm.Mode = "Petty Cash"
        xForm.ShowDialog()

        If chkReview.Checked = True Then
            Call loadreviewbills()

            dteUntil.Enabled = False
            dteFrm.Enabled = False
        Else
            loadbillslist()
            dteUntil.Enabled = True
            dteFrm.Enabled = True
        End If

    End Sub

    Private Sub PostAsPettyCash()

    End Sub
End Class