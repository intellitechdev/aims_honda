<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBillsListforApproval
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBillsListforApproval))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkReview = New System.Windows.Forms.CheckBox()
        Me.chkViewAll = New System.Windows.Forms.CheckBox()
        Me.dteUntil = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dteFrm = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grdBillsApproval = New System.Windows.Forms.DataGridView()
        Me.btnSelectAll = New System.Windows.Forms.Button()
        Me.btnApproved = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnReview = New System.Windows.Forms.Button()
        Me.btnPostAsPettyCash = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdBillsApproval, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.chkReview)
        Me.GroupBox1.Controls.Add(Me.chkViewAll)
        Me.GroupBox1.Controls.Add(Me.dteUntil)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dteFrm)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 1)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(897, 43)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'chkReview
        '
        Me.chkReview.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkReview.AutoSize = True
        Me.chkReview.Location = New System.Drawing.Point(641, 17)
        Me.chkReview.Name = "chkReview"
        Me.chkReview.Size = New System.Drawing.Size(163, 17)
        Me.chkReview.TabIndex = 7
        Me.chkReview.Text = "View all reviewed Items"
        Me.chkReview.UseVisualStyleBackColor = True
        '
        'chkViewAll
        '
        Me.chkViewAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkViewAll.AutoSize = True
        Me.chkViewAll.Location = New System.Drawing.Point(456, 16)
        Me.chkViewAll.Name = "chkViewAll"
        Me.chkViewAll.Size = New System.Drawing.Size(175, 17)
        Me.chkViewAll.TabIndex = 6
        Me.chkViewAll.Text = " View all bills for Approval"
        Me.chkViewAll.UseVisualStyleBackColor = True
        '
        'dteUntil
        '
        Me.dteUntil.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.dteUntil.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteUntil.Location = New System.Drawing.Point(331, 13)
        Me.dteUntil.Name = "dteUntil"
        Me.dteUntil.Size = New System.Drawing.Size(109, 21)
        Me.dteUntil.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(299, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "until"
        '
        'dteFrm
        '
        Me.dteFrm.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.dteFrm.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteFrm.Location = New System.Drawing.Point(181, 13)
        Me.dteFrm.Name = "dteFrm"
        Me.dteFrm.Size = New System.Drawing.Size(109, 21)
        Me.dteFrm.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(174, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "List of Bills for Approval from"
        '
        'grdBillsApproval
        '
        Me.grdBillsApproval.AllowUserToAddRows = False
        Me.grdBillsApproval.AllowUserToDeleteRows = False
        Me.grdBillsApproval.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdBillsApproval.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdBillsApproval.BackgroundColor = System.Drawing.Color.White
        Me.grdBillsApproval.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdBillsApproval.GridColor = System.Drawing.Color.Black
        Me.grdBillsApproval.Location = New System.Drawing.Point(13, 48)
        Me.grdBillsApproval.Name = "grdBillsApproval"
        Me.grdBillsApproval.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdBillsApproval.Size = New System.Drawing.Size(895, 349)
        Me.grdBillsApproval.TabIndex = 1
        '
        'btnSelectAll
        '
        Me.btnSelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelectAll.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSelectAll.Location = New System.Drawing.Point(13, 402)
        Me.btnSelectAll.Name = "btnSelectAll"
        Me.btnSelectAll.Size = New System.Drawing.Size(86, 31)
        Me.btnSelectAll.TabIndex = 2
        Me.btnSelectAll.Text = "Select All"
        Me.btnSelectAll.UseVisualStyleBackColor = True
        '
        'btnApproved
        '
        Me.btnApproved.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApproved.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApproved.Image = Global.CSAcctg.My.Resources.Resources.post
        Me.btnApproved.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnApproved.Location = New System.Drawing.Point(655, 402)
        Me.btnApproved.Name = "btnApproved"
        Me.btnApproved.Size = New System.Drawing.Size(163, 31)
        Me.btnApproved.TabIndex = 3
        Me.btnApproved.Text = "Post Selected Bills"
        Me.btnApproved.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnApproved.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(822, 402)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 31)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrint.Location = New System.Drawing.Point(105, 402)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(127, 31)
        Me.btnPrint.TabIndex = 5
        Me.btnPrint.Text = "Print Preview"
        Me.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnReview
        '
        Me.btnReview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReview.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnReview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnReview.Location = New System.Drawing.Point(238, 402)
        Me.btnReview.Name = "btnReview"
        Me.btnReview.Size = New System.Drawing.Size(165, 31)
        Me.btnReview.TabIndex = 6
        Me.btnReview.Text = "Save Reviewed Bills"
        Me.btnReview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnReview.UseVisualStyleBackColor = True
        '
        'btnPostAsPettyCash
        '
        Me.btnPostAsPettyCash.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPostAsPettyCash.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPostAsPettyCash.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPostAsPettyCash.Location = New System.Drawing.Point(512, 402)
        Me.btnPostAsPettyCash.Name = "btnPostAsPettyCash"
        Me.btnPostAsPettyCash.Size = New System.Drawing.Size(139, 31)
        Me.btnPostAsPettyCash.TabIndex = 7
        Me.btnPostAsPettyCash.Text = "Close Bills Entry"
        Me.btnPostAsPettyCash.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPostAsPettyCash.UseVisualStyleBackColor = True
        '
        'frmBillsListforApproval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(921, 439)
        Me.Controls.Add(Me.btnPostAsPettyCash)
        Me.Controls.Add(Me.btnReview)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnApproved)
        Me.Controls.Add(Me.btnSelectAll)
        Me.Controls.Add(Me.grdBillsApproval)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(837, 463)
        Me.Name = "frmBillsListforApproval"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "List of Bills for Approval"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdBillsApproval, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dteUntil As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dteFrm As System.Windows.Forms.DateTimePicker
    Friend WithEvents grdBillsApproval As System.Windows.Forms.DataGridView
    Friend WithEvents btnSelectAll As System.Windows.Forms.Button
    Friend WithEvents btnApproved As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnReview As System.Windows.Forms.Button
    Friend WithEvents chkReview As System.Windows.Forms.CheckBox
    Friend WithEvents chkViewAll As System.Windows.Forms.CheckBox
    Friend WithEvents btnPostAsPettyCash As System.Windows.Forms.Button
End Class
