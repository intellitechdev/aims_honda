Public Class frmaccntvoucherparameters

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        gRvwd = txtRvwd.Text
        gApprvd = txtApprvd.Text

        frmAccountsPayableVoucher.MdiParent = frmMain
        frmAccountsPayableVoucher.Show()

        Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtRvwd.Text = ""
        txtApprvd.Text = ""
        Me.Close()
    End Sub

    Private Sub frmaccntvoucherparameters_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub frmaccntvoucherparameters_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub

    Private Sub frmaccntvoucherparameters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtRvwd.Text = gUserName
        txtApprvd.Text = gUserName
    End Sub
End Class