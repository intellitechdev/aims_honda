Public Class frmRecurringEntries

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If cboPeriod.SelectedItem <> "" Then
            gRecurringPeriod = cboPeriod.SelectedItem
            gRecurringStartDate = dteStart.Value
            gRecurringEndDate = dteEnd.Value
            Me.Close()
        Else
            MsgBox("selection of period is required....", MsgBoxStyle.Information, Me.Text)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class