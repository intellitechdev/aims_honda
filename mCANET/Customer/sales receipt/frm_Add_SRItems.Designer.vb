<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Add_SRItems
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Add_SRItems))
        Me.TxtOrdered = New System.Windows.Forms.TextBox
        Me.txtRate = New System.Windows.Forms.TextBox
        Me.lblAmout = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.vDGV = New System.Windows.Forms.DataGridView
        Me.cboMItem = New MTGCComboBox
        Me.BtnEdt = New System.Windows.Forms.Button
        CType(Me.vDGV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TxtOrdered
        '
        Me.TxtOrdered.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtOrdered.Location = New System.Drawing.Point(93, 37)
        Me.TxtOrdered.Name = "TxtOrdered"
        Me.TxtOrdered.Size = New System.Drawing.Size(100, 22)
        Me.TxtOrdered.TabIndex = 3
        Me.TxtOrdered.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRate
        '
        Me.txtRate.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRate.Location = New System.Drawing.Point(93, 65)
        Me.txtRate.Name = "txtRate"
        Me.txtRate.Size = New System.Drawing.Size(100, 22)
        Me.txtRate.TabIndex = 5
        Me.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAmout
        '
        Me.lblAmout.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmout.Location = New System.Drawing.Point(96, 98)
        Me.lblAmout.Name = "lblAmout"
        Me.lblAmout.Size = New System.Drawing.Size(97, 19)
        Me.lblAmout.TabIndex = 8
        Me.lblAmout.Text = "0.00"
        Me.lblAmout.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Item:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(2, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Ordered Qty:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(2, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Rate:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Amount:"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel1.Location = New System.Drawing.Point(76, 95)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(142, 2)
        Me.Panel1.TabIndex = 7
        '
        'btnAdd
        '
        Me.btnAdd.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdd.Location = New System.Drawing.Point(228, 53)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(85, 28)
        Me.btnAdd.TabIndex = 9
        Me.btnAdd.Text = "Add Item"
        Me.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(228, 87)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 28)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'vDGV
        '
        Me.vDGV.AllowUserToAddRows = False
        Me.vDGV.AllowUserToDeleteRows = False
        Me.vDGV.AllowUserToOrderColumns = True
        Me.vDGV.AllowUserToResizeColumns = False
        Me.vDGV.AllowUserToResizeRows = False
        Me.vDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.vDGV.Enabled = False
        Me.vDGV.Location = New System.Drawing.Point(30, 238)
        Me.vDGV.Name = "vDGV"
        Me.vDGV.Size = New System.Drawing.Size(240, 150)
        Me.vDGV.TabIndex = 193
        '
        'cboMItem
        '
        Me.cboMItem.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboMItem.ArrowColor = System.Drawing.Color.Black
        Me.cboMItem.BindedControl = CType(resources.GetObject("cboMItem.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboMItem.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboMItem.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboMItem.ColumnNum = 3
        Me.cboMItem.ColumnWidth = "400; 0; 0"
        Me.cboMItem.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboMItem.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboMItem.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboMItem.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboMItem.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboMItem.DisplayMember = "Text"
        Me.cboMItem.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboMItem.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cboMItem.DropDownForeColor = System.Drawing.Color.Black
        Me.cboMItem.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.cboMItem.DropDownWidth = 620
        Me.cboMItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMItem.GridLineColor = System.Drawing.Color.LightGray
        Me.cboMItem.GridLineHorizontal = False
        Me.cboMItem.GridLineVertical = False
        Me.cboMItem.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboMItem.Location = New System.Drawing.Point(93, 8)
        Me.cboMItem.ManagingFastMouseMoving = True
        Me.cboMItem.ManagingFastMouseMovingInterval = 30
        Me.cboMItem.Name = "cboMItem"
        Me.cboMItem.SelectedItem = Nothing
        Me.cboMItem.SelectedValue = Nothing
        Me.cboMItem.Size = New System.Drawing.Size(220, 23)
        Me.cboMItem.TabIndex = 1
        '
        'BtnEdt
        '
        Me.BtnEdt.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.BtnEdt.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnEdt.Location = New System.Drawing.Point(228, 53)
        Me.BtnEdt.Name = "BtnEdt"
        Me.BtnEdt.Size = New System.Drawing.Size(85, 28)
        Me.BtnEdt.TabIndex = 10
        Me.BtnEdt.Text = "Edit Item"
        Me.BtnEdt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnEdt.UseVisualStyleBackColor = True
        Me.BtnEdt.Visible = False
        '
        'frm_Add_SRItems
        '
        Me.AcceptButton = Me.btnAdd
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(320, 118)
        Me.Controls.Add(Me.cboMItem)
        Me.Controls.Add(Me.vDGV)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.BtnEdt)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblAmout)
        Me.Controls.Add(Me.txtRate)
        Me.Controls.Add(Me.TxtOrdered)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_Add_SRItems"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "New Item"
        CType(Me.vDGV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TxtOrdered As System.Windows.Forms.TextBox
    Friend WithEvents txtRate As System.Windows.Forms.TextBox
    Friend WithEvents lblAmout As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents vDGV As System.Windows.Forms.DataGridView
    Friend WithEvents cboMItem As MTGCComboBox
    Friend WithEvents BtnEdt As System.Windows.Forms.Button
End Class
