Imports Microsoft.ApplicationBlocks.Data
Imports System.data.SqlClient
Public Class frm_Cust_CreateSalesReciept_COOP_

    '                   ########################################################
    '                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
    '                   ### Date Created: Aug 26, 2009                       ###
    '                   ### Website: http://www.ionflux.site50.net           ###
    '                   ########################################################


#Region "Declaration"
    Private gCon As New Clsappconfiguration
    Private dPercentage As Long = 0
    Private sKeyTax As String
    Public sKeySR As String
    Private dTotalInitial As Long = 0
    Private dTaxAmntInitial As Long = 0
    Private sCoAddress As String
    Private sKeyCustomer As String
    Dim checkDate As New clsRestrictedDate
    Private sKeyPaymentMethod As String
    Private ClassRefNo As String = ""
#End Region
#Region "Subs & Functions"
    Private Sub loadcomboboxs()
        Call m_loadCustomer(cboCustomerName, cboCustomerID)
        Call m_loadPaymentMethod(cboPaymentMethodName, cboPaymentMethodID)
        Call m_loadTax(cboTaxName, cboTaxID)
    End Sub
    Private Sub loaddiscounts()
        Call m_GetSalesDiscount(cboSD, Me)
        Call m_GetRetailerDiscount(cboRD, Me)
    End Sub
    Private Sub computeTax()
        Dim dAmt As Decimal = 0

        If sKeyTax <> "" Then
            Dim dtTax As DataSet = m_GetTax(sKeyTax)
            dPercentage = dtTax.Tables(0).Rows(0)("fdTaxRate")
        Else
            dPercentage = 0
        End If
        lblPercentage.Text = Format(CDec(dPercentage), "0.00")

        'With Me.LstSRItem
        '    Dim xCnt As Integer = 0
        '    Dim xTotal As Decimal = 0
        '    For xCnt = 0 To .RowCount - 1
        '        If .Item("cDescription", xCnt).Value IsNot Nothing Then
        '            dAmt = CDec(.Item("cAmount", xCnt).Value)
        '            xTotal += dAmt
        '        End If
        '    Next
        '    lblTaxAmt.Text = Format(CDec(xTotal * (CDec(lblPercentage.Text) / 100)), "##,##0.00")
        'End With
    End Sub
    Private Sub computesalesdiscount()
        Try
            If lblTotal.Text <> "0.00" Then
                lblSaleDiscount.Text = Format(CDec(lblTotal.Text * (CDec(m_GetSalesDiscountPercentage(cboSD.SelectedItem, Me)) / 100)), "##,##0.00")
            Else
                lblSaleDiscount.Text = "0.00"
            End If
        Catch ex As Exception
            lblSaleDiscount.Text = "0.00"
        End Try

    End Sub
    Private Sub calculatedueamount()
        lblBalance.Text = CDec(IIf(lblTotal.Text = "", 0, lblTotal.Text)) - _
                        (CDec(IIf(lblSaleDiscount.Text = "", 0, lblSaleDiscount.Text)) + _
                        CDec(IIf(lblRetDiscount.Text = "", 0, lblRetDiscount.Text))) '+ CDec(lblPayments.Text))
        lblBalance.Text = Format(CDec(lblBalance.Text), "#,##0.00")
    End Sub
    Private Sub computeretailersdiscount()
        Try
            If lblTotal.Text <> "0.00" Then
                lblRetDiscount.Text = Format(CDec(CDec(lblTotal.Text) - _
                        CDec(lblSaleDiscount.Text)) * _
                        (CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)) / 100), "##,##0.00")
            Else
                lblRetDiscount.Text = "0.00"
            End If
        Catch ex As Exception
            lblRetDiscount.Text = "0.00"
        End Try
    End Sub
    Private Function defaultInvoiceNo() As String
        Dim sDefault As String = "1"
        Dim sInvc As String = ""
        Dim sInvcNum As String = ""
        Dim sSCLCmd As String = "SELECT fcSaleNo FROM tSalesReceipt where fxKeyCompany ='" & gCompanyID()
        sSCLCmd &= "' AND LEN(fcSaleNo) < 10"
        sSCLCmd &= " ORDER BY CAST(fcSaleNo AS INTEGER)"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    sInvc = rd.Item(0).ToString
                End While
            End Using

            If sInvc = "" Or sInvc = "0" Then
                sInvcNum = sDefault
            ElseIf sInvc <> "" Or sInvc <> "0" Then
                sInvcNum = CInt(sInvc) + 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sInvcNum
    End Function
    Private Sub refreshForm()
        LstSRItem.Items.Clear()
        Dim dtComp As DataRow = gCompanyInfo().Tables(0).Rows(0)
        sCoAddress = dtComp("co_name").ToString + _
                         vbCrLf + dtComp("co_street").ToString + _
                        " " + dtComp("co_city").ToString + _
                        vbCrLf + dtComp("co_zip").ToString

        If sKeySR = "" Then
            sKeySR = Guid.NewGuid.ToString
        End If

        lblTotal.Text = Format(dTotalInitial, "0.00")
        lblTaxAmt.Text = Format(dTaxAmntInitial, "0.00")

    End Sub
    Private Sub IsDateRestricted()
        If checkDate.checkIfRestricted(dteSale.Value) Then
            btnSave.Enabled = False 'true if date is NOT within range of period restricted
            btnAccountability.Enabled = False
        Else
            btnSave.Enabled = True 'false if date is within range of period restricted
            btnAccountability.Enabled = True
        End If
    End Sub
    Private Function defaultSRNo() As String
        Dim sDefault As String = "1"
        Dim sSRNoFetch As String = ""
        Dim sSRNo() As String = {""}
        Dim sSRNosTemp As String = ""
        Dim i As Integer = 0

        Dim sSCLCmd As String = "SELECT fcSaleNo FROM tSalesReceipt ORDER BY fcSaleNo"
        Try
            Using rd = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSCLCmd)
                While rd.Read
                    If IsNumeric(rd.Item("fcSaleNo")) Then
                        sSRNoFetch = rd.Item("fcSaleNo")
                        sSRNosTemp &= sSRNoFetch & " "
                    End If
                End While
            End Using

            sSRNo = sSRNosTemp.Split
            If UBound(sSRNo) > 0 Then
                For i = 0 To UBound(sSRNo)
                    If CStr(i + 1) <> sSRNo(i) Then
                        If i > 0 Then
                            sDefault = CStr(CInt(sSRNo(i - 1) + 1))
                        End If
                        Exit For
                    End If
                Next

            End If
            Return sDefault

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Sales Receipt No.")
            Return sDefault
        End Try
    End Function
    Private Sub loadRecpItems()
        Dim sSQLCmd As String = "usp_t_salesReceiptItem_list "
        sSQLCmd &= "@fxKeySR = '" & sKeySR & "'"
        Try
            Using dtsSet As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While dtsSet.Read
                    With LstSRItem.Items.Add(dtsSet.Item("fxKeySRItem").ToString)
                        .SubItems.Add(dtsSet.Item("Item").ToString)
                        .SubItems.Add(dtsSet.Item("Qty").ToString)
                        '.SubItems.Add(dtsSet.Item("Order").ToString)
                        .SubItems.Add(Format(CDec(dtsSet.Item("Rate").ToString), "##,##0.00##"))
                        .SubItems.Add(Format(CDec(dtsSet.Item("Amount").ToString), "##,##0.00##"))
                    End With
                End While
                'MsgBox(.Rows(0).Cells("cItem").Value.ToString, MsgBoxStyle.Information)
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Sales Receipt Items")
        End Try

    End Sub
    Private Sub loadSR()
        Dim dtSR As DataSet = m_GetSR(sKeySR)

        Try
            Using rd As DataTableReader = dtSR.CreateDataReader
                If rd.Read Then
                    loadRecpItems()
                    'total() 'get the total

                    If IsDBNull(rd.Item("fxKeyCustomer")) Then
                        MsgBox("Customer Field is empty. Please indicate the name " & _
                                "of the Customer.", MsgBoxStyle.Information, "Notice")
                        cboCustomerName.Focus()
                    End If

                    sKeyCustomer = rd.Item("fxKeyCustomer").ToString
                    sKeyPaymentMethod = rd.Item("fxKeyPaymentMethod").ToString
                    sKeyTax = rd.Item("fxKeyTax").ToString
                    txtSaleNo.Text = rd.Item("fcSaleNo")
                    dteSale.Value = rd.Item("fdDateTransact")
                    txtCheckNo.Text = IIf(IsDBNull(rd.Item("fcCheckNo")), "", rd.Item("fcCheckNo"))
                    dPercentage = rd.Item("fdTaxRate")
                    lblPercentage.Text = "(" & Format(dPercentage, "0.00") & "%)"
                    dTotalInitial = rd.Item("fdTotal")
                    dTaxAmntInitial = rd.Item("fdTaxAmount")
                    txtMemo.Text = IIf(IsDBNull(rd.Item("fcMemo")), "", rd.Item("fcMemo"))
                    lblSaleDiscount.Text = Format(CDec(rd.Item("fdSDAmt")), "##,##0.00")
                    lblRetDiscount.Text = Format(CDec(rd.Item("fdRDAmt")), "##,##0.00")
                    cboSD.SelectedItem = rd.Item("fcSDCode")
                    cboRD.SelectedItem = rd.Item("fcRDCode")


                    lblBalance.Text = Format(CDec(rd.Item("fdBalanceDue")), "##,##0.00")
                    If lblBalance.Text = "0.00" Then
                        calculatedueamount()
                    End If
                    ClassRefNo = rd.Item("fxClassRefNo").ToString
                    TxtClassRefno.Text = ClassRefNo
                    txtClassName.Text = GetClassName(ClassRefNo)
                Else
                    sKeyCustomer = ""
                    sKeyPaymentMethod = ""
                    sKeyTax = ""
                    txtSaleNo.Text = defaultSRNo()
                    dteSale.Value = Now
                    txtCheckNo.Text = ""
                    dPercentage = 0
                    lblPercentage.Text = "(" & Format(dPercentage, "0.00") & "%)"
                    dTotalInitial = 0
                    dTaxAmntInitial = 0
                    txtMemo.Text = ""

                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Sales Receipt")
        End Try
    End Sub
    Private Sub loadsalesreceipts()
        Call loadSR()
        Call m_selectedCboValue(cboCustomerName, cboCustomerID, sKeyCustomer)
        Call m_selectedCboValue(cboTaxName, cboTaxID, sKeyTax)
        Call m_selectedCboValue(cboPaymentMethodName, cboPaymentMethodID, sKeyPaymentMethod)
    End Sub
    Private Function updateSRItem() As Boolean
        Dim iRow As Integer
        Dim sSQLCmd As String
        Dim sKeySRItem As String
        Dim sItem As String = ""
        Dim bClosed As Boolean = False
        Dim sDescription As String = ""
        Dim dQuantity As Long = 0
        Dim dRate As Decimal
        Dim dAmount As Decimal
        Dim sKeyTaxCode As String = ""
        Try
            If LstSRItem.Items.Count > 0 Then
                For Each SRItem As ListViewItem In LstSRItem.Items
                    With SRItem
                        sKeySRItem = SRItem.Text
                        sItem = SRItem.SubItems(1).Text
                        sItem = m_GetItemByName(sItem)
                        sDescription = SRItem.SubItems(1).Text
                        If SRItem.SubItems(2).Text Is Nothing Or SRItem.SubItems(2).Text = "" Then
                            dQuantity = 0.0
                        Else
                            'MsgBox(SRItem.SubItems(2).Text)
                            dQuantity = SRItem.SubItems(2).Text
                        End If
                        If SRItem.SubItems(3).Text Is Nothing Or SRItem.SubItems(3).Text = "" Then
                            dRate = 0.0
                        Else
                            dRate = SRItem.SubItems(3).Text
                        End If
                        If SRItem.SubItems(4).Text Is Nothing Or SRItem.SubItems(4).Text = "" Then
                            dAmount = 0
                        Else
                            dAmount = SRItem.SubItems(4).Text
                        End If

                    End With

                    sSQLCmd = "usp_t_salesReceiptItem_update "
                    sSQLCmd &= "@fxKeySRItem = '" & sKeySRItem & "'"
                    sSQLCmd &= ",@fxKeySR = '" & sKeySR & "'"
                    sSQLCmd &= ",@fxKeyItem = '" & sItem & "'"
                    sSQLCmd &= ",@fcSRDescription = '" & sDescription & "'"
                    sSQLCmd &= ",@fdQuantity ='" & dQuantity & "' "
                    sSQLCmd &= ",@fdRate ='" & dRate & "' "
                    sSQLCmd &= ",@fdAmount ='" & dAmount & "' "
                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSQLCmd)
                    Catch ex As Exception
                    End Try


                Next
            End If
            Return True
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit Sales Receipt Item")
            Return False
        End Try
    End Function
    Private Sub updateSR()
        Dim msgWarning As DialogResult
        Dim sWarning As String
        Dim dtAddress As DataTable
        For Each DeletedItem As ListViewItem In LstDelete.Items
            Dim sSqlDeleteCmd As String = "DELETE FROM dbo.tSalesReceipt_item WHERE fxKeySRItem ='" & DeletedItem.Text & "'"
            Try
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlDeleteCmd)
            Catch ex As Exception
            End Try
        Next
        LstDelete.Items.Clear()
        Dim sSQLCmd As String = "usp_t_salesReceipt_update "
        sSQLCmd &= "@fxKeySR = '" & sKeySR & "'"
        sSQLCmd &= ",@fcSaleNo = '" & txtSaleNo.Text & "'"
        sSQLCmd &= ",@fxKeyCustomer = '" & sKeyCustomer & "'"
        sSQLCmd &= ",@fdDateTransact = '" & Microsoft.VisualBasic.FormatDateTime(dteSale.Value, DateFormat.ShortDate) & "'"
        sSQLCmd &= ",@fcBillTo =''"
        sSQLCmd &= ",@fdSalesPeriodFrom = ''"
        sSQLCmd &= ",@fdSalesPeriodTo = ''"

        dtAddress = m_GetCustomer(sKeyCustomer).Tables(0)
        Dim pxPayAccount As String = ""
        Dim pxTaxAccount As String = ""
        sSQLCmd &= ",@fxKeyPayAcnt =" & IIf(gKeyPayAccount = "", "NULL", "'" & gKeyPayAccount & "'") & " "
        sSQLCmd &= ",@fxKeyTaxAcnt =" & IIf(gKeyTaxAccount = "", "NULL", "'" & gKeyTaxAccount & "'") & " "
        sSQLCmd &= ",@fxKeySalesAcnt =" & IIf(gKeySalesAccount = "", "NULL", "'" & gKeySalesAccount & "'") & " "
        sSQLCmd &= ",@fdTaxPercent ='" & gTaxPercent & "' "
        sSQLCmd &= ",@fxOutPutTaxAcnt =" & IIf(gKeyOutputTax = "", "NULL", "'" & gKeyOutputTax & "'") & " "
        sSQLCmd &= ",@fdOutputTaxpercent ='" & gOutputTaxPercent & "' "
        sSQLCmd &= ",@fxKeyCOSAcnt=" & IIf(gKeyCOSAccount = "", "NULL", "'" & gKeyCOSAccount & "'") & " "
        sSQLCmd &= ",@fcCheckNo = '" & txtCheckNo.Text & "'"
        sSQLCmd &= ",@fcCustomerMessage = ' '"
        sSQLCmd &= ",@fcMemo = '" & txtMemo.Text & "'"
        sSQLCmd &= ",@fdTaxAmount ='" & CDec(lblTaxAmt.Text) & "' "
        sSQLCmd &= ",@fdTotal ='" & CDec(lblTotal.Text) & "' "
        sSQLCmd &= ",@fbPrint ='0' "
        sSQLCmd &= ",@fbEmail ='0' "
        sSQLCmd &= ",@fcSDCode ='" & cboSD.SelectedItem & "' "
        sSQLCmd &= ",@fcRDCode ='" & cboRD.SelectedItem & "' "
        sSQLCmd &= ",@fdSDAmt ='" & CDec(lblSaleDiscount.Text) & "' "
        sSQLCmd &= ",@fdRDAmt ='" & CDec(lblRetDiscount.Text) & "' "
        If LblSalesDiscountAmount.Text = "" Or LblSalesDiscountAmount.Text Is Nothing Or LblSalesDiscountAmount.Text = "" Then
            sSQLCmd &= ",@fdSDAccountAmt='0.00' "
        Else
            sSQLCmd &= ",@fdSDAccountAmt='" & LblSalesDiscountAmount.Text & "' "
        End If
        sSQLCmd &= ",@fdBalanceDue='" & CDec(lblBalance.Text) & "' "
        sSQLCmd &= ",@fxKeyCompany ='" & gCompanyID() & "' "

        'if sKeySalesTaxCode <> "" Then
        sSQLCmd &= ",@fxKeySalesTaxCode = null"
        'End If
        If sKeyTax <> "" Then
            sSQLCmd &= ",@fxKeyTax = '" & sKeyTax & "'"
        End If
        If sKeyPaymentMethod <> "" Then
            sSQLCmd &= ",@fxKeyPaymentMethod = '" & sKeyPaymentMethod & "'"
        End If

        sSQLCmd &= ", @fxKeyClassRefNo='" & ClassRefNo & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            gKeySR = sKeySR
            If updateSRItem() Then
                MessageBox.Show("Record successfully Updated.", "Add/Edit S.Receipt.")
                frm_cust_masterCustomer.loadGrdCustomer2("Sales Receipts")
                frm_item_availability.loadDetails("Sales Receipts")
            End If
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Sales Receipt")
        End Try
    End Sub
    Private Sub EditSRItem()
        If LstSRItem.SelectedItems.Count = 1 Then
            With frm_Add_SRItems
                .Mode = "Edit Mode"
                .sKeySRItem = LstSRItem.SelectedItems.Item(0).Text
                .nItemName = LstSRItem.SelectedItems.Item(0).SubItems(1).Text
                .nQty = LstSRItem.SelectedItems.Item(0).SubItems(2).Text
                .nfxKeyItem = LstSRItem.SelectedItems.Item(0).Text
                .nRate = LstSRItem.SelectedItems.Item(0).SubItems(3).Text
                .nAmount = LstSRItem.SelectedItems.Item(0).SubItems(4).Text
            End With
            frm_Add_SRItems.btnAdd.Visible = False
            frm_Add_SRItems.BtnEdt.Visible = True
            frm_Add_SRItems.ShowDialog()
            If frm_Add_SRItems.Result = "Ready to add" Then
                Dim xitem As ListViewItem
                xitem = LstSRItem.FindItemWithText(LstSRItem.SelectedItems.Item(0).Text)
                LstSRItem.Items.Remove(xitem)
                With LstSRItem.Items.Add(frm_Add_SRItems.nfxKeyItem)
                    .SubItems.Add(frm_Add_SRItems.nItemName)
                    .SubItems.Add(frm_Add_SRItems.nQty)
                    .SubItems.Add(frm_Add_SRItems.nRate)
                    .SubItems.Add(frm_Add_SRItems.nAmount)
                End With
            End If
        Else
            MsgBox("Cannot edit multiple item, please select only one.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Multiple selected item detected")
        End If
    End Sub
    Private Sub AddSRItem()
        frm_Add_SRItems.Mode = "Add Mode"
        frm_Add_SRItems.btnAdd.Visible = True
        frm_Add_SRItems.BtnEdt.Visible = False
        frm_Add_SRItems.ShowDialog()
        If frm_Add_SRItems.Result = "Ready to add" Then
            With LstSRItem.Items.Add(frm_Add_SRItems.nfxKeyItem)
                .SubItems.Add(frm_Add_SRItems.nItemName)
                .SubItems.Add(frm_Add_SRItems.nQty)
                .SubItems.Add(frm_Add_SRItems.nRate)
                .SubItems.Add(frm_Add_SRItems.nAmount)
            End With
        End If
    End Sub
    Private Sub DelSRItem()
        Dim xdel As String
        If LstSRItem.SelectedItems.Count > 0 Then
            xdel = MsgBox("Are your sure you want to delete the seleted item/s", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm deletion")
            If xdel = vbYes Then
                For Each item As ListViewItem In LstSRItem.SelectedItems
                    Dim xItem As ListViewItem
                    Dim xDeletedItem As String
                    xItem = LstSRItem.FindItemWithText(item.Text)
                    If xItem.Text <> Nothing Then
                        xDeletedItem = xItem.Text
                        LstDelete.Items.Add(xDeletedItem)
                        LstSRItem.Items.Remove(xItem)
                    End If
                Next
            End If
        Else
            MsgBox("No selected item found.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
        End If
    End Sub
#End Region

    Private Sub frm_Cust_CreateSalesReciept_COOP__Load(ByVal sender As System.Object, _
                                                        ByVal e As System.EventArgs) Handles MyBase.Load
        loadcomboboxs()
        loaddiscounts()
        If sKeySR = "" Or sKeySR = Nothing Then
            sKeySR = Guid.NewGuid.ToString
            txtSaleNo.Text = defaultInvoiceNo()
            refreshForm()
            IsDateRestricted()
        Else
            refreshForm()
            gEditEvent = 1
            loadsalesreceipts()
            IsDateRestricted()
        End If

        If frmMain.LblClassActivator.Visible = True Then
            Label9.Visible = True
            txtClassName.Visible = True
            txtClassName.Enabled = True
            BtnBrowse.Enabled = True
            BtnBrowse.Visible = True
        Else
            Label9.Visible = False
            txtClassName.Visible = False
            txtClassName.Enabled = False
            BtnBrowse.Enabled = False
            BtnBrowse.Visible = False
        End If


    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Private Sub cboSD_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSD.SelectedIndexChanged
        If cboSD.SelectedIndex = 1 Then
            frmSalesDiscount.ShowDialog()
        ElseIf cboSD.SelectedIndex > 2 Then
            lblSDpercent.Text = CStr(Format(CDec(m_GetSalesDiscountPercentage(cboSD.SelectedItem, Me)), "##0.0#")) + "%"
            Call computesalesdiscount()
        ElseIf cboSD.SelectedIndex = 0 Then
            lblSaleDiscount.Text = "0.00"
            lblSDpercent.Text = "(0.0%)"
            Call calculatedueamount()
        End If
        computeretailersdiscount()
        LblSalesDiscountAmount.Text = ComputeSalesDiscountAmountForGL(CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")), Format(CDec(dPercentage), "0.00"), lblSaleDiscount.Text)

    End Sub
    Private Sub cboRD_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRD.SelectedIndexChanged
        If cboRD.SelectedIndex = 1 Then
            frmRetailersDiscount.ShowDialog()
        ElseIf cboRD.SelectedIndex > 2 Then
            lblRDPercent.Text = CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")) + "%"
            Call computeretailersdiscount()
        ElseIf cboRD.SelectedIndex = 0 Then
            lblRetDiscount.Text = "0.00"
            lblRDPercent.Text = "(0.0%)"
            Call calculatedueamount()
        End If
        LblSalesDiscountAmount.Text = ComputeSalesDiscountAmountForGL(CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")), Format(CDec(dPercentage), "0.00"), lblSaleDiscount.Text)

    End Sub
    Private Sub cboTaxName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTaxName.SelectedIndexChanged
        cboTaxID.SelectedIndex = cboTaxName.SelectedIndex
        sKeyTax = cboTaxID.SelectedItem.ToString

        Call computeTax()
        computesalesdiscount()
        computeretailersdiscount()

        If cboTaxName.SelectedIndex = 1 Then
            frm_MF_taxAddEdit.Text = "New Sales Tax"
            frm_MF_taxAddEdit.KeyTax = Guid.NewGuid.ToString
            frm_MF_taxAddEdit.Show()
        End If
        LblSalesDiscountAmount.Text = ComputeSalesDiscountAmountForGL(CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")), Format(CDec(dPercentage), "0.00"), lblSaleDiscount.Text)

    End Sub
    Private Sub BtnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowse.Click
        FrmBrowseClass.Mode = "BROWSE MODE"
        FrmBrowseClass.ShowDialog()
        txtClassName.Text = FrmBrowseClass.Data1
        TxtClassRefno.Text = FrmBrowseClass.Data2
    End Sub
    Private Sub BtnAddItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddItem.Click
        AddSRItem()
    End Sub
    Private Sub BtnEditItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEditItem.Click
        EditSRItem()
    End Sub
    Private Sub btnSelDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelDel.Click
        DelSRItem()
    End Sub
    Private Sub btnAccountability_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccountability.Click
        frmSRAccountability.ShowDialog()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If validateAccountability("SR") = "Kulang" Then
            frmSRAccountability.ShowDialog()
            Exit Sub
        Else
            If cboCustomerID.Text = "" Then
                MessageBox.Show("Customer field cannot be empty!", "Data Validation")
            Else
                updateSR()
            End If
        End If
    End Sub
    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        sKeyCustomer = cboCustomerID.SelectedItem

        If cboCustomerName.SelectedIndex = 1 Then
            frm_cust_masterCustomerAddEdit.MdiParent = Me.MdiParent
            frm_cust_masterCustomerAddEdit.KeyCustomer = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.KeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.IsNew = True
            frm_cust_masterCustomerAddEdit.Text = "Add Customer"
            frm_cust_masterCustomerAddEdit.Show()
        End If

        If sKeyCustomer <> "" Then
            Dim dtCustomer As DataTable = m_GetCustomer(sKeyCustomer).Tables(0)
            cboPaymentMethodID.SelectedItem = dtCustomer.Rows(0)("fxKeyPaymentMethod").ToString
            cboPaymentMethodName.SelectedIndex = cboPaymentMethodID.SelectedIndex
        End If
    End Sub
    Private Sub LstSRItem_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles LstSRItem.DoubleClick
        EditSRItem()
    End Sub
    Private Sub AddSalesRecieptItemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddSalesRecieptItemToolStripMenuItem.Click
        AddSRItem()
    End Sub
    Private Sub EditSalesRecieptItemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditSalesRecieptItemToolStripMenuItem.Click
        EditSRItem()
    End Sub
    Private Sub DeleteItemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteItemToolStripMenuItem.Click
        DelSRItem()
    End Sub
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        frmSalesReceipt.ShowDialog()
    End Sub
End Class