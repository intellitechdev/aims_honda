Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmSRAccountability

    Private gcon As New Clsappconfiguration

    Private Sub frmSRAccountability_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Load Combobox item
        Call m_DisplayAccountsAll(cboPayAcnt)
        Call m_DisplayAccountsAll(cboTaxAcnt)
        Call m_DisplayAccountsAll(cboSalesAcnt)
        Call m_DisplayAccountsAll(cboOutputTax)
        Call m_DisplayAccountsAll(cboCOS)
        Call m_DisplayAccountsAll(cboMerch)
        Call m_DisplayAccountsAll(cboSalesDiscount)

        'load current selected items
        Call loadaccountabilitydetails()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If gKeySR = "" Then
            Call updateInvoiceAccountability()
            txtPercent.Text = "0.00"
            txtPer1.Text = "0.00"
        Else
            Call updateInvoiceAccountability()
            Call updateinvoice()
        End If
        Me.Dispose()
        Me.Close()
        validateAccountability("SR")
    End Sub
    
    Private Sub loadaccountabilitydetails()

        Try
            If gKeySR <> "" Then
                Dim sSQLCmd As String = "usp_t_SRAccountabilityLoad "
                sSQLCmd &= " @fxKeySR='" & gKeySR & "' "
                sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "

                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                    While rd.Read
                        cboPayAcnt.SelectedItem = rd.Item(0).ToString
                        cboTaxAcnt.SelectedItem = rd.Item(1).ToString
                        cboSalesAcnt.SelectedItem = rd.Item(2).ToString
                        cboOutputTax.SelectedItem = rd.Item(3).ToString
                        txtPercent.Text = rd.Item(4)
                        txtPer1.Text = rd.Item(5)
                        cboCOS.SelectedItem = rd.Item(6).ToString
                        cboMerch.SelectedItem = rd.Item(7).ToString
                        cboSalesDiscount.SelectedItem = rd.Item(8).ToString
                    End While
                End Using
            Else
                If gKeyPayAccount <> Nothing Or gKeyPayAccount <> "" Then
                    cboPayAcnt.SelectedItem = GetAccountability(gKeyPayAccount)
                End If
                If gKeyTaxAccount <> Nothing Or gKeyTaxAccount <> "" Then
                    cboTaxAcnt.SelectedItem = GetAccountability(gKeyTaxAccount)
                End If
                If gKeySalesAccount <> Nothing Or gKeySalesAccount <> "" Then
                    cboSalesAcnt.SelectedItem = GetAccountability(gKeySalesAccount)
                End If
                txtPercent.Text = gTaxPercent
                If gKeyOutputTax <> Nothing Or gKeyOutputTax <> "" Then
                    cboOutputTax.SelectedItem = GetAccountability(gKeyOutputTax)
                End If
                txtPer1.Text = gOutputTaxPercent
                If gKeyCOSAccount <> Nothing Or gKeyCOSAccount <> "" Then
                    cboCOS.SelectedItem = GetAccountability(gKeyCOSAccount)
                End If
                If gKeyMerchAcnt <> Nothing Or gKeyMerchAcnt <> "" Then
                    cboMerch.SelectedItem = GetAccountability(gKeyMerchAcnt)
                End If
                If gKeyDiscount <> Nothing Or gKeyDiscount <> "" Then
                    cboSalesDiscount.SelectedItem = GetAccountability(gKeyDiscount)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub updateInvoiceAccountability()
        Dim pxPayAccount As String = ""
        Dim pxTaxAccount As String = ""
        Dim pxSalesAccount As String = ""
        Dim pxOutTaxAcnt As String = ""
        Dim pxCOSAcnt As String = ""
        Dim pxMerchAcnt As String = ""
        Dim pxSalesDiscount As String = ""

        If cboSalesAcnt.SelectedItem <> " " Or cboSalesAcnt.SelectedItem <> "" _
                    Or cboSalesAcnt.SelectedItem <> Nothing Then
            pxSalesAccount = getAccountID(cboSalesAcnt.SelectedItem)
        End If

        If cboPayAcnt.SelectedItem <> " " Or cboPayAcnt.SelectedItem <> "" _
                    Or cboPayAcnt.SelectedItem <> Nothing Then
            pxPayAccount = getAccountID(cboPayAcnt.SelectedItem)
        End If

        If cboTaxAcnt.SelectedItem <> " " Or cboTaxAcnt.SelectedItem <> "" _
                    Or cboTaxAcnt.SelectedItem <> Nothing Then
            pxTaxAccount = getAccountID(cboTaxAcnt.SelectedItem)
        End If

        If cboOutputTax.SelectedItem <> " " Or cboOutputTax.SelectedItem <> "" _
                    Or cboOutputTax.SelectedItem <> Nothing Then
            pxOutTaxAcnt = getAccountID(cboOutputTax.SelectedItem)
        End If

        If cboCOS.SelectedItem <> " " Or cboCOS.SelectedItem <> "" _
                    Or cboCOS.SelectedItem <> Nothing Then
            pxCOSAcnt = getAccountID(cboCOS.SelectedItem)
        End If

        If cboMerch.SelectedItem <> " " Or cboMerch.SelectedItem <> "" _
                    Or cboMerch.SelectedItem <> Nothing Then
            pxMerchAcnt = getAccountID(cboMerch.SelectedItem)
        End If

        If cboSalesDiscount.SelectedItem <> " " Or cboSalesDiscount.SelectedItem <> "" _
                    Or cboSalesDiscount.SelectedItem <> Nothing Then
            pxSalesDiscount = getAccountID(cboSalesDiscount.SelectedItem)
        End If

        gKeyPayAccount = pxPayAccount
        gKeyTaxAccount = pxTaxAccount
        gKeySalesAccount = pxSalesAccount
        gTaxPercent = CDec(txtPercent.Text)
        gKeyOutputTax = pxOutTaxAcnt
        gOutputTaxPercent = CDec(txtPer1.Text)
        gKeyCOSAccount = pxCOSAcnt
        gKeyMerchAcnt = pxMerchAcnt
        gKeyDiscount = pxSalesDiscount
    End Sub

    Private Sub updateinvoice()
        Try
            Dim sSQLCmd As String = "usp_t_SRAccountability_update "
            sSQLCmd &= " @fxKeyPayAcnt=" & IIf(gKeyPayAccount = "", "NULL", "'" & gKeyPayAccount & "'") & " "
            sSQLCmd &= ",@fxKeyTaxAcnt=" & IIf(gKeyTaxAccount = "", "NULL", "'" & gKeyTaxAccount & "'") & " "
            sSQLCmd &= ",@fxKeySalesAcnt=" & IIf(gKeySalesAccount = "", "NULL", "'" & gKeySalesAccount & "'") & " "
            sSQLCmd &= ",@fxOutputTaxAcnt=" & IIf(gKeyOutputTax = "", "NULL", "'" & gKeyOutputTax & "'") & " "
            sSQLCmd &= ",@fxKeyCOSAcnt=" & IIf(gKeyCOSAccount = "", "NULL", "'" & gKeyCOSAccount & "'") & " "
            sSQLCmd &= ",@fdOutputTaxPercent='" & gOutputTaxPercent & "' "
            sSQLCmd &= ",@fdTaxPercent='" & gTaxPercent & "' "
            sSQLCmd &= ",@fxKeySR=" & IIf(gKeySR = "", "NULL", "'" & gKeySR & "'") & " "
            sSQLCmd &= ",@fxKeyCompany=" & IIf(gCompanyID() = "", "NULL", "'" & gCompanyID() & "'") & " "
            sSQLCmd &= ",@fxKeyMerchAcnt=" & IIf(gKeyMerchAcnt = "", "NULL", "'" & gKeyMerchAcnt & "'") & " "
            sSQLCmd &= " ,@fxKeySalesDiscount =" & IIf(gKeyDiscount = "", "NULL", "'" & gKeyDiscount & "'") & " "
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
        End Try
        Me.Close()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        validateAccountability("SR")
        txtPercent.Text = "0.00"
        txtPer1.Text = "0.00"
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub cboOutputTax_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOutputTax.SelectedValueChanged
        If cboOutputTax.SelectedItem = " " Then
            txtPer1.Text = "0.00"
            txtPer1.Enabled = False
        Else
            txtPer1.Text = "12.00"
            txtPer1.Enabled = True
        End If
    End Sub
End Class