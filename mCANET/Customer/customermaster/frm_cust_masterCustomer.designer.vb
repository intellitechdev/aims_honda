<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_masterCustomer
    Inherits System.Windows.Forms.Form


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cust_masterCustomer))
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ts_NewCustomer = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_salesOrder = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Invoice = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_salesReceipt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Charges = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_rcvPayment = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_creditMemo = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Print = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_Info = New System.Windows.Forms.ToolStripButton
        Me.ts_Export = New System.Windows.Forms.ToolStripLabel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.tabCustomernTransaction = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.grdCustomer1 = New System.Windows.Forms.DataGridView
        Me.cboView = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.grdTransaction = New System.Windows.Forms.DataGridView
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lbldteRange = New System.Windows.Forms.Label
        Me.PanelCustomerChild = New System.Windows.Forms.Panel
        Me.spltCustomerInfo = New System.Windows.Forms.SplitContainer
        Me.btnPrintCustomerMaster = New System.Windows.Forms.Button
        Me.lnkEstimates = New System.Windows.Forms.LinkLabel
        Me.lnkOpeningBal = New System.Windows.Forms.LinkLabel
        Me.lnkReport = New System.Windows.Forms.LinkLabel
        Me.btnEditNotes = New System.Windows.Forms.Button
        Me.txtPriceLvl = New System.Windows.Forms.TextBox
        Me.txtEmail = New System.Windows.Forms.TextBox
        Me.txtFax = New System.Windows.Forms.TextBox
        Me.txtPhone2 = New System.Windows.Forms.TextBox
        Me.txtPhone = New System.Windows.Forms.TextBox
        Me.txtTerms = New System.Windows.Forms.TextBox
        Me.txtContact = New System.Windows.Forms.TextBox
        Me.txtBillingAdd = New System.Windows.Forms.TextBox
        Me.txtCompName = New System.Windows.Forms.TextBox
        Me.txtCustomerType = New System.Windows.Forms.TextBox
        Me.txtCustomerName = New System.Windows.Forms.TextBox
        Me.txtNotes = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.btnEditCustomer = New System.Windows.Forms.Button
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lbldteRange2 = New System.Windows.Forms.Label
        Me.grdCustomer3 = New System.Windows.Forms.DataGridView
        Me.dteRange2 = New System.Windows.Forms.ComboBox
        Me.cboFilter = New System.Windows.Forms.ComboBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.cboShow = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.grdCustomer2 = New System.Windows.Forms.DataGridView
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dteRange = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboFilterBy = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ToolStrip1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.tabCustomernTransaction.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.grdCustomer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdTransaction, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelCustomerChild.SuspendLayout()
        Me.spltCustomerInfo.Panel1.SuspendLayout()
        Me.spltCustomerInfo.Panel2.SuspendLayout()
        Me.spltCustomerInfo.SuspendLayout()
        CType(Me.grdCustomer3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdCustomer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_NewCustomer, Me.ToolStripButton2, Me.ts_Print, Me.ToolStripSeparator2, Me.ToolStripSeparator3, Me.ts_Info, Me.ts_Export})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1248, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_NewCustomer
        '
        Me.ts_NewCustomer.Image = CType(resources.GetObject("ts_NewCustomer.Image"), System.Drawing.Image)
        Me.ts_NewCustomer.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_NewCustomer.Name = "ts_NewCustomer"
        Me.ts_NewCustomer.Size = New System.Drawing.Size(111, 22)
        Me.ts_NewCustomer.Text = "New Customer"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_salesOrder, Me.ts_Invoice, Me.ToolStripSeparator1, Me.ts_salesReceipt, Me.ts_Charges, Me.ts_rcvPayment, Me.ts_creditMemo})
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(133, 22)
        Me.ToolStripButton2.Text = "New Transaction"
        '
        'ts_salesOrder
        '
        Me.ts_salesOrder.Name = "ts_salesOrder"
        Me.ts_salesOrder.Size = New System.Drawing.Size(223, 22)
        Me.ts_salesOrder.Text = "Sales Orders"
        '
        'ts_Invoice
        '
        Me.ts_Invoice.Name = "ts_Invoice"
        Me.ts_Invoice.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.ts_Invoice.Size = New System.Drawing.Size(223, 22)
        Me.ts_Invoice.Text = "Invoices"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(220, 6)
        '
        'ts_salesReceipt
        '
        Me.ts_salesReceipt.Name = "ts_salesReceipt"
        Me.ts_salesReceipt.Size = New System.Drawing.Size(223, 22)
        Me.ts_salesReceipt.Text = "Sales Receipts"
        '
        'ts_Charges
        '
        Me.ts_Charges.Name = "ts_Charges"
        Me.ts_Charges.Size = New System.Drawing.Size(223, 22)
        Me.ts_Charges.Text = "Statement Charges"
        Me.ts_Charges.Visible = False
        '
        'ts_rcvPayment
        '
        Me.ts_rcvPayment.Name = "ts_rcvPayment"
        Me.ts_rcvPayment.Size = New System.Drawing.Size(223, 22)
        Me.ts_rcvPayment.Text = "Receive Payments"
        '
        'ts_creditMemo
        '
        Me.ts_creditMemo.Name = "ts_creditMemo"
        Me.ts_creditMemo.Size = New System.Drawing.Size(223, 22)
        Me.ts_creditMemo.Text = "Credit Memos / Refunds"
        '
        'ts_Print
        '
        Me.ts_Print.Image = CType(resources.GetObject("ts_Print.Image"), System.Drawing.Image)
        Me.ts_Print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Print.Name = "ts_Print"
        Me.ts_Print.Size = New System.Drawing.Size(49, 22)
        Me.ts_Print.Text = "Print"
        Me.ts_Print.Visible = False
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator2.Visible = False
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator3.Visible = False
        '
        'ts_Info
        '
        Me.ts_Info.Image = CType(resources.GetObject("ts_Info.Image"), System.Drawing.Image)
        Me.ts_Info.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Info.Name = "ts_Info"
        Me.ts_Info.Size = New System.Drawing.Size(151, 22)
        Me.ts_Info.Text = "Customer && Jobs Info"
        Me.ts_Info.Visible = False
        '
        'ts_Export
        '
        Me.ts_Export.Image = CType(resources.GetObject("ts_Export.Image"), System.Drawing.Image)
        Me.ts_Export.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Export.Name = "ts_Export"
        Me.ts_Export.Size = New System.Drawing.Size(60, 22)
        Me.ts_Export.Text = "Export"
        Me.ts_Export.Visible = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 25)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.tabCustomernTransaction)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.lbldteRange)
        Me.SplitContainer1.Panel2.Controls.Add(Me.PanelCustomerChild)
        Me.SplitContainer1.Panel2.Controls.Add(Me.grdCustomer2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dteRange)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel2.Controls.Add(Me.cboFilterBy)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Size = New System.Drawing.Size(1248, 531)
        Me.SplitContainer1.SplitterDistance = 392
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 1
        '
        'tabCustomernTransaction
        '
        Me.tabCustomernTransaction.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabCustomernTransaction.Controls.Add(Me.TabPage1)
        Me.tabCustomernTransaction.Controls.Add(Me.TabPage2)
        Me.tabCustomernTransaction.Location = New System.Drawing.Point(0, 0)
        Me.tabCustomernTransaction.Name = "tabCustomernTransaction"
        Me.tabCustomernTransaction.SelectedIndex = 0
        Me.tabCustomernTransaction.Size = New System.Drawing.Size(389, 517)
        Me.tabCustomernTransaction.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.TabPage1.Controls.Add(Me.grdCustomer1)
        Me.TabPage1.Controls.Add(Me.cboView)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(381, 491)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Customer"
        '
        'grdCustomer1
        '
        Me.grdCustomer1.AllowUserToAddRows = False
        Me.grdCustomer1.AllowUserToDeleteRows = False
        Me.grdCustomer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdCustomer1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdCustomer1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdCustomer1.GridColor = System.Drawing.Color.Black
        Me.grdCustomer1.Location = New System.Drawing.Point(-1, 46)
        Me.grdCustomer1.MultiSelect = False
        Me.grdCustomer1.Name = "grdCustomer1"
        Me.grdCustomer1.ReadOnly = True
        Me.grdCustomer1.RowTemplate.Height = 24
        Me.grdCustomer1.Size = New System.Drawing.Size(377, 442)
        Me.grdCustomer1.TabIndex = 3
        '
        'cboView
        '
        Me.cboView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboView.FormattingEnabled = True
        Me.cboView.Location = New System.Drawing.Point(45, 9)
        Me.cboView.Name = "cboView"
        Me.cboView.Size = New System.Drawing.Size(202, 21)
        Me.cboView.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "View"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.TabPage2.Controls.Add(Me.grdTransaction)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(381, 491)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Transaction"
        '
        'grdTransaction
        '
        Me.grdTransaction.AllowUserToAddRows = False
        Me.grdTransaction.AllowUserToDeleteRows = False
        Me.grdTransaction.AllowUserToResizeColumns = False
        Me.grdTransaction.AllowUserToResizeRows = False
        Me.grdTransaction.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTransaction.BackgroundColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.grdTransaction.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdTransaction.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.grdTransaction.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdTransaction.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.grdTransaction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdTransaction.ColumnHeadersVisible = False
        Me.grdTransaction.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column3})
        Me.grdTransaction.Location = New System.Drawing.Point(-1, 0)
        Me.grdTransaction.MultiSelect = False
        Me.grdTransaction.Name = "grdTransaction"
        Me.grdTransaction.ReadOnly = True
        Me.grdTransaction.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.grdTransaction.RowHeadersVisible = False
        Me.grdTransaction.RowTemplate.Height = 24
        Me.grdTransaction.Size = New System.Drawing.Size(299, 341)
        Me.grdTransaction.TabIndex = 0
        '
        'Column3
        '
        Me.Column3.HeaderText = ""
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'lbldteRange
        '
        Me.lbldteRange.AutoSize = True
        Me.lbldteRange.Location = New System.Drawing.Point(478, 13)
        Me.lbldteRange.Name = "lbldteRange"
        Me.lbldteRange.Size = New System.Drawing.Size(0, 13)
        Me.lbldteRange.TabIndex = 5
        Me.lbldteRange.Visible = False
        '
        'PanelCustomerChild
        '
        Me.PanelCustomerChild.Controls.Add(Me.spltCustomerInfo)
        Me.PanelCustomerChild.Location = New System.Drawing.Point(80, 35)
        Me.PanelCustomerChild.Name = "PanelCustomerChild"
        Me.PanelCustomerChild.Size = New System.Drawing.Size(762, 573)
        Me.PanelCustomerChild.TabIndex = 0
        '
        'spltCustomerInfo
        '
        Me.spltCustomerInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.spltCustomerInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spltCustomerInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spltCustomerInfo.Location = New System.Drawing.Point(0, 0)
        Me.spltCustomerInfo.Name = "spltCustomerInfo"
        Me.spltCustomerInfo.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'spltCustomerInfo.Panel1
        '
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.btnPrintCustomerMaster)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.lnkEstimates)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.lnkOpeningBal)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.lnkReport)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.btnEditNotes)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtPriceLvl)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtEmail)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtFax)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtPhone2)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtPhone)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtTerms)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtContact)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtBillingAdd)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtCompName)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtCustomerType)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtCustomerName)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.txtNotes)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label20)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label11)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label19)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.btnEditCustomer)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label17)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label16)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label15)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label14)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label13)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label12)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label4)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label10)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label9)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label8)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label7)
        Me.spltCustomerInfo.Panel1.Controls.Add(Me.Label6)
        Me.spltCustomerInfo.Panel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'spltCustomerInfo.Panel2
        '
        Me.spltCustomerInfo.Panel2.Controls.Add(Me.lbldteRange2)
        Me.spltCustomerInfo.Panel2.Controls.Add(Me.grdCustomer3)
        Me.spltCustomerInfo.Panel2.Controls.Add(Me.dteRange2)
        Me.spltCustomerInfo.Panel2.Controls.Add(Me.cboFilter)
        Me.spltCustomerInfo.Panel2.Controls.Add(Me.Label22)
        Me.spltCustomerInfo.Panel2.Controls.Add(Me.Label21)
        Me.spltCustomerInfo.Panel2.Controls.Add(Me.cboShow)
        Me.spltCustomerInfo.Panel2.Controls.Add(Me.Label5)
        Me.spltCustomerInfo.Size = New System.Drawing.Size(762, 573)
        Me.spltCustomerInfo.SplitterDistance = 303
        Me.spltCustomerInfo.TabIndex = 0
        '
        'btnPrintCustomerMaster
        '
        Me.btnPrintCustomerMaster.Location = New System.Drawing.Point(540, 245)
        Me.btnPrintCustomerMaster.Name = "btnPrintCustomerMaster"
        Me.btnPrintCustomerMaster.Size = New System.Drawing.Size(101, 23)
        Me.btnPrintCustomerMaster.TabIndex = 35
        Me.btnPrintCustomerMaster.Text = "Print Preview"
        Me.btnPrintCustomerMaster.UseVisualStyleBackColor = True
        '
        'lnkEstimates
        '
        Me.lnkEstimates.AutoSize = True
        Me.lnkEstimates.Location = New System.Drawing.Point(621, 255)
        Me.lnkEstimates.Name = "lnkEstimates"
        Me.lnkEstimates.Size = New System.Drawing.Size(97, 13)
        Me.lnkEstimates.TabIndex = 34
        Me.lnkEstimates.TabStop = True
        Me.lnkEstimates.Text = "Show Estimates"
        Me.lnkEstimates.Visible = False
        '
        'lnkOpeningBal
        '
        Me.lnkOpeningBal.AutoSize = True
        Me.lnkOpeningBal.Location = New System.Drawing.Point(383, 255)
        Me.lnkOpeningBal.Name = "lnkOpeningBal"
        Me.lnkOpeningBal.Size = New System.Drawing.Size(103, 13)
        Me.lnkOpeningBal.TabIndex = 33
        Me.lnkOpeningBal.TabStop = True
        Me.lnkOpeningBal.Text = "Opening Balance"
        Me.lnkOpeningBal.Visible = False
        '
        'lnkReport
        '
        Me.lnkReport.AutoSize = True
        Me.lnkReport.Location = New System.Drawing.Point(282, 255)
        Me.lnkReport.Name = "lnkReport"
        Me.lnkReport.Size = New System.Drawing.Size(81, 13)
        Me.lnkReport.TabIndex = 32
        Me.lnkReport.TabStop = True
        Me.lnkReport.Text = "Quick Report"
        Me.lnkReport.Visible = False
        '
        'btnEditNotes
        '
        Me.btnEditNotes.Location = New System.Drawing.Point(125, 245)
        Me.btnEditNotes.Name = "btnEditNotes"
        Me.btnEditNotes.Size = New System.Drawing.Size(103, 23)
        Me.btnEditNotes.TabIndex = 31
        Me.btnEditNotes.Text = "Edit Notes"
        Me.btnEditNotes.UseVisualStyleBackColor = True
        '
        'txtPriceLvl
        '
        Me.txtPriceLvl.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtPriceLvl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPriceLvl.Location = New System.Drawing.Point(498, 173)
        Me.txtPriceLvl.Name = "txtPriceLvl"
        Me.txtPriceLvl.ReadOnly = True
        Me.txtPriceLvl.Size = New System.Drawing.Size(245, 21)
        Me.txtPriceLvl.TabIndex = 29
        '
        'txtEmail
        '
        Me.txtEmail.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Location = New System.Drawing.Point(498, 127)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.ReadOnly = True
        Me.txtEmail.Size = New System.Drawing.Size(245, 21)
        Me.txtEmail.TabIndex = 28
        '
        'txtFax
        '
        Me.txtFax.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.Location = New System.Drawing.Point(498, 104)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.ReadOnly = True
        Me.txtFax.Size = New System.Drawing.Size(245, 21)
        Me.txtFax.TabIndex = 27
        '
        'txtPhone2
        '
        Me.txtPhone2.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtPhone2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone2.Location = New System.Drawing.Point(498, 81)
        Me.txtPhone2.Name = "txtPhone2"
        Me.txtPhone2.ReadOnly = True
        Me.txtPhone2.Size = New System.Drawing.Size(245, 21)
        Me.txtPhone2.TabIndex = 26
        '
        'txtPhone
        '
        Me.txtPhone.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.Location = New System.Drawing.Point(498, 58)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.ReadOnly = True
        Me.txtPhone.Size = New System.Drawing.Size(245, 21)
        Me.txtPhone.TabIndex = 25
        '
        'txtTerms
        '
        Me.txtTerms.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtTerms.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTerms.Location = New System.Drawing.Point(498, 150)
        Me.txtTerms.Name = "txtTerms"
        Me.txtTerms.ReadOnly = True
        Me.txtTerms.Size = New System.Drawing.Size(245, 21)
        Me.txtTerms.TabIndex = 24
        '
        'txtContact
        '
        Me.txtContact.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContact.Location = New System.Drawing.Point(498, 35)
        Me.txtContact.Name = "txtContact"
        Me.txtContact.ReadOnly = True
        Me.txtContact.Size = New System.Drawing.Size(245, 21)
        Me.txtContact.TabIndex = 23
        '
        'txtBillingAdd
        '
        Me.txtBillingAdd.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtBillingAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBillingAdd.Location = New System.Drawing.Point(126, 126)
        Me.txtBillingAdd.Multiline = True
        Me.txtBillingAdd.Name = "txtBillingAdd"
        Me.txtBillingAdd.ReadOnly = True
        Me.txtBillingAdd.Size = New System.Drawing.Size(245, 85)
        Me.txtBillingAdd.TabIndex = 22
        '
        'txtCompName
        '
        Me.txtCompName.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtCompName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompName.Location = New System.Drawing.Point(126, 81)
        Me.txtCompName.Multiline = True
        Me.txtCompName.Name = "txtCompName"
        Me.txtCompName.ReadOnly = True
        Me.txtCompName.Size = New System.Drawing.Size(245, 43)
        Me.txtCompName.TabIndex = 21
        '
        'txtCustomerType
        '
        Me.txtCustomerType.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtCustomerType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustomerType.Location = New System.Drawing.Point(126, 58)
        Me.txtCustomerType.Name = "txtCustomerType"
        Me.txtCustomerType.ReadOnly = True
        Me.txtCustomerType.Size = New System.Drawing.Size(245, 21)
        Me.txtCustomerType.TabIndex = 20
        '
        'txtCustomerName
        '
        Me.txtCustomerName.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtCustomerName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustomerName.Location = New System.Drawing.Point(126, 35)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.ReadOnly = True
        Me.txtCustomerName.Size = New System.Drawing.Size(245, 21)
        Me.txtCustomerName.TabIndex = 19
        '
        'txtNotes
        '
        Me.txtNotes.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtNotes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotes.Location = New System.Drawing.Point(126, 213)
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.ReadOnly = True
        Me.txtNotes.Size = New System.Drawing.Size(618, 21)
        Me.txtNotes.TabIndex = 18
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(15, 216)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(39, 13)
        Me.Label20.TabIndex = 17
        Me.Label20.Text = "Notes"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(405, 150)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(43, 13)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Terms"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(14, 229)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(735, 13)
        Me.Label19.TabIndex = 16
        Me.Label19.Text = "_________________________________________________________________________________" & _
            "_______________________"
        '
        'btnEditCustomer
        '
        Me.btnEditCustomer.Location = New System.Drawing.Point(16, 245)
        Me.btnEditCustomer.Name = "btnEditCustomer"
        Me.btnEditCustomer.Size = New System.Drawing.Size(101, 23)
        Me.btnEditCustomer.TabIndex = 15
        Me.btnEditCustomer.Text = "Edit Customer"
        Me.btnEditCustomer.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(405, 176)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(66, 13)
        Me.Label17.TabIndex = 13
        Me.Label17.Text = "Price level"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(405, 38)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(51, 13)
        Me.Label16.TabIndex = 12
        Me.Label16.Text = "Contact"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(405, 130)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(38, 13)
        Me.Label15.TabIndex = 11
        Me.Label15.Text = "Email"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(405, 107)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(27, 13)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "Fax"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(405, 84)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(65, 13)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "Alt. Phone"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(405, 61)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(42, 13)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Phone"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(133, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Customer Information"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(14, 19)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(735, 13)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "_________________________________________________________________________________" & _
            "_______________________"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(15, 130)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(91, 13)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Billing Address"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 84)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(99, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Company Name"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 61)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Customer Type"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 38)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Customer Name"
        '
        'lbldteRange2
        '
        Me.lbldteRange2.AutoSize = True
        Me.lbldteRange2.Location = New System.Drawing.Point(590, 12)
        Me.lbldteRange2.Name = "lbldteRange2"
        Me.lbldteRange2.Size = New System.Drawing.Size(51, 13)
        Me.lbldteRange2.TabIndex = 9
        Me.lbldteRange2.Text = "1/1/1900"
        '
        'grdCustomer3
        '
        Me.grdCustomer3.AllowUserToAddRows = False
        Me.grdCustomer3.AllowUserToDeleteRows = False
        Me.grdCustomer3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdCustomer3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdCustomer3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdCustomer3.GridColor = System.Drawing.Color.Black
        Me.grdCustomer3.Location = New System.Drawing.Point(-2, 36)
        Me.grdCustomer3.Name = "grdCustomer3"
        Me.grdCustomer3.RowTemplate.Height = 24
        Me.grdCustomer3.Size = New System.Drawing.Size(747, 225)
        Me.grdCustomer3.TabIndex = 8
        '
        'dteRange2
        '
        Me.dteRange2.DropDownHeight = 200
        Me.dteRange2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.dteRange2.DropDownWidth = 200
        Me.dteRange2.FormattingEnabled = True
        Me.dteRange2.IntegralHeight = False
        Me.dteRange2.Location = New System.Drawing.Point(429, 9)
        Me.dteRange2.Name = "dteRange2"
        Me.dteRange2.Size = New System.Drawing.Size(139, 21)
        Me.dteRange2.TabIndex = 7
        '
        'cboFilter
        '
        Me.cboFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilter.DropDownWidth = 150
        Me.cboFilter.FormattingEnabled = True
        Me.cboFilter.Location = New System.Drawing.Point(241, 9)
        Me.cboFilter.Name = "cboFilter"
        Me.cboFilter.Size = New System.Drawing.Size(146, 21)
        Me.cboFilter.TabIndex = 6
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(393, 12)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(30, 13)
        Me.Label22.TabIndex = 5
        Me.Label22.Text = "Date"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(189, 12)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(46, 13)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Filter By"
        '
        'cboShow
        '
        Me.cboShow.FormattingEnabled = True
        Me.cboShow.Location = New System.Drawing.Point(61, 9)
        Me.cboShow.MaxDropDownItems = 5
        Me.cboShow.Name = "cboShow"
        Me.cboShow.Size = New System.Drawing.Size(122, 21)
        Me.cboShow.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(13, 12)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(33, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Show"
        '
        'grdCustomer2
        '
        Me.grdCustomer2.AllowUserToAddRows = False
        Me.grdCustomer2.AllowUserToDeleteRows = False
        Me.grdCustomer2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdCustomer2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdCustomer2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column4, Me.Column6, Me.Column7, Me.Column5, Me.Column8, Me.Column16})
        Me.grdCustomer2.GridColor = System.Drawing.Color.Black
        Me.grdCustomer2.Location = New System.Drawing.Point(-2, 47)
        Me.grdCustomer2.Name = "grdCustomer2"
        Me.grdCustomer2.RowTemplate.Height = 24
        Me.grdCustomer2.Size = New System.Drawing.Size(504, 338)
        Me.grdCustomer2.TabIndex = 4
        '
        'Column4
        '
        Me.Column4.HeaderText = "Customer"
        Me.Column4.Name = "Column4"
        '
        'Column6
        '
        Me.Column6.HeaderText = "Num"
        Me.Column6.Name = "Column6"
        '
        'Column7
        '
        Me.Column7.HeaderText = "Date"
        Me.Column7.Name = "Column7"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Pay Method"
        Me.Column5.Name = "Column5"
        '
        'Column8
        '
        Me.Column8.HeaderText = "Amount"
        Me.Column8.Name = "Column8"
        '
        'Column16
        '
        Me.Column16.HeaderText = "Unapplied Amount"
        Me.Column16.Name = "Column16"
        '
        'dteRange
        '
        Me.dteRange.DropDownHeight = 200
        Me.dteRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.dteRange.DropDownWidth = 200
        Me.dteRange.FormattingEnabled = True
        Me.dteRange.IntegralHeight = False
        Me.dteRange.Location = New System.Drawing.Point(329, 10)
        Me.dteRange.Name = "dteRange"
        Me.dteRange.Size = New System.Drawing.Size(140, 21)
        Me.dteRange.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(271, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Date"
        '
        'cboFilterBy
        '
        Me.cboFilterBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterBy.DropDownWidth = 200
        Me.cboFilterBy.FormattingEnabled = True
        Me.cboFilterBy.Location = New System.Drawing.Point(78, 10)
        Me.cboFilterBy.Name = "cboFilterBy"
        Me.cboFilterBy.Size = New System.Drawing.Size(140, 21)
        Me.cboFilterBy.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Filter By"
        '
        'frm_cust_masterCustomer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(1248, 556)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_cust_masterCustomer"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Customer Master"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.tabCustomernTransaction.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.grdCustomer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grdTransaction, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelCustomerChild.ResumeLayout(False)
        Me.spltCustomerInfo.Panel1.ResumeLayout(False)
        Me.spltCustomerInfo.Panel1.PerformLayout()
        Me.spltCustomerInfo.Panel2.ResumeLayout(False)
        Me.spltCustomerInfo.Panel2.PerformLayout()
        Me.spltCustomerInfo.ResumeLayout(False)
        CType(Me.grdCustomer3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdCustomer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_salesOrder As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Invoice As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_salesReceipt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Charges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_rcvPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_creditMemo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents tabCustomernTransaction As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grdTransaction As System.Windows.Forms.DataGridView
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents grdCustomer2 As System.Windows.Forms.DataGridView
    Friend WithEvents dteRange As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboFilterBy As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents cboView As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PanelCustomerChild As System.Windows.Forms.Panel
    Friend WithEvents spltCustomerInfo As System.Windows.Forms.SplitContainer
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents btnEditCustomer As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtNotes As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtCompName As System.Windows.Forms.TextBox
    Friend WithEvents txtCustomerType As System.Windows.Forms.TextBox
    Friend WithEvents txtCustomerName As System.Windows.Forms.TextBox
    Friend WithEvents txtTerms As System.Windows.Forms.TextBox
    Friend WithEvents txtBillingAdd As System.Windows.Forms.TextBox
    Friend WithEvents txtPriceLvl As System.Windows.Forms.TextBox
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents txtFax As System.Windows.Forms.TextBox
    Friend WithEvents txtPhone2 As System.Windows.Forms.TextBox
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Friend WithEvents btnEditNotes As System.Windows.Forms.Button
    Friend WithEvents lnkOpeningBal As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkReport As System.Windows.Forms.LinkLabel
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboShow As System.Windows.Forms.ComboBox
    Friend WithEvents grdCustomer3 As System.Windows.Forms.DataGridView
    Friend WithEvents dteRange2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboFilter As System.Windows.Forms.ComboBox
    Friend WithEvents lbldteRange2 As System.Windows.Forms.Label
    Friend WithEvents txtContact As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lnkEstimates As System.Windows.Forms.LinkLabel
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ts_Info As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Export As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ts_Print As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents grdCustomer1 As System.Windows.Forms.DataGridView
    Friend WithEvents ts_NewCustomer As System.Windows.Forms.ToolStripButton
    Friend WithEvents lbldteRange As System.Windows.Forms.Label
    Friend WithEvents btnPrintCustomerMaster As System.Windows.Forms.Button
End Class
