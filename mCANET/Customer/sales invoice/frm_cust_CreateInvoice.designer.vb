<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_CreateInvoice
    Inherits System.Windows.Forms.Form


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cust_CreateInvoice))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ts_Previous = New System.Windows.Forms.ToolStripButton
        Me.ts_Next = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_Preview = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Print = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_printShippingLbl = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_PrintEnvelope = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_Find = New System.Windows.Forms.ToolStripButton
        Me.ts_History = New System.Windows.Forms.ToolStripButton
        Me.ts_Journal = New System.Windows.Forms.ToolStripButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboCustomerID = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtBillingAdd = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lstClassList = New System.Windows.Forms.DateTimePicker
        Me.txtInvoiceNo = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtShippingAdd = New System.Windows.Forms.TextBox
        Me.cboShipToAddID = New System.Windows.Forms.ComboBox
        Me.grdInvoice = New System.Windows.Forms.DataGridView
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label9 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblSalesPeriod = New System.Windows.Forms.Label
        Me.dteSalesPeriodTo = New System.Windows.Forms.DateTimePicker
        Me.dteSalesPeriodFrom = New System.Windows.Forms.DateTimePicker
        Me.cboRepName = New System.Windows.Forms.ComboBox
        Me.cboTermsName = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.dteShippingDate = New System.Windows.Forms.DateTimePicker
        Me.Label14 = New System.Windows.Forms.Label
        Me.cboRepID = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.cboTermsID = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtPONo = New System.Windows.Forms.TextBox
        Me.cboViaName = New System.Windows.Forms.ComboBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtFOB = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.cboViaID = New System.Windows.Forms.ComboBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.cboTaxCodeID = New System.Windows.Forms.ComboBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.cboCustomerName = New System.Windows.Forms.ComboBox
        Me.cboTaxCodeName = New System.Windows.Forms.ComboBox
        Me.cboShipToAddName = New System.Windows.Forms.ComboBox
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.lblTaxAmt = New System.Windows.Forms.Label
        Me.lblBalance = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dteDueDate = New System.Windows.Forms.DateTimePicker
        Me.lblPaid = New System.Windows.Forms.PictureBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.dteDeliverydate = New System.Windows.Forms.DateTimePicker
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblSalesDiscountAmount = New System.Windows.Forms.Label
        Me.lblPayments = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblRetDiscount = New System.Windows.Forms.Label
        Me.lblRDPercent = New System.Windows.Forms.Label
        Me.txtRDAmt = New System.Windows.Forms.TextBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.cboRD = New System.Windows.Forms.ComboBox
        Me.lblSaleDiscount = New System.Windows.Forms.Label
        Me.lblSDpercent = New System.Windows.Forms.Label
        Me.txtSDAmt = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.cboSD = New System.Windows.Forms.ComboBox
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.cboTaxName = New System.Windows.Forms.ComboBox
        Me.cboTaxID = New System.Windows.Forms.ComboBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.btnApplyCredits = New System.Windows.Forms.Button
        Me.btnAddTimeCost = New System.Windows.Forms.Button
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.chkEmail = New System.Windows.Forms.CheckBox
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.txtCustomerMsg = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnDelRows = New System.Windows.Forms.Button
        Me.Label20 = New System.Windows.Forms.Label
        Me.cboPayAcnt = New System.Windows.Forms.ComboBox
        Me.cboTaxAcnt = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.txtPercent = New System.Windows.Forms.TextBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.btnSubTotal = New System.Windows.Forms.Button
        Me.lblSubTotal = New System.Windows.Forms.TextBox
        Me.chkCancel = New System.Windows.Forms.CheckBox
        Me.btnOk = New System.Windows.Forms.Button
        Me.btnAccountability = New System.Windows.Forms.Button
        Me.Label26 = New System.Windows.Forms.Label
        Me.txtClassName = New System.Windows.Forms.TextBox
        Me.txtClassRefno = New System.Windows.Forms.TextBox
        Me.BtnBrowse = New System.Windows.Forms.Button
        Me.ToolStrip1.SuspendLayout()
        CType(Me.grdInvoice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.lblPaid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ToolStrip1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_Previous, Me.ts_Next, Me.ToolStripSplitButton1, Me.ToolStripSeparator1, Me.ToolStripSeparator2, Me.ts_Find, Me.ts_History, Me.ts_Journal})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(837, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_Previous
        '
        Me.ts_Previous.Image = CType(resources.GetObject("ts_Previous.Image"), System.Drawing.Image)
        Me.ts_Previous.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Previous.Name = "ts_Previous"
        Me.ts_Previous.Size = New System.Drawing.Size(84, 22)
        Me.ts_Previous.Text = "Previous"
        '
        'ts_Next
        '
        Me.ts_Next.Image = CType(resources.GetObject("ts_Next.Image"), System.Drawing.Image)
        Me.ts_Next.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Next.Name = "ts_Next"
        Me.ts_Next.Size = New System.Drawing.Size(57, 22)
        Me.ts_Next.Text = "Next"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_Preview, Me.ts_Print, Me.ts_printShippingLbl, Me.ts_PrintEnvelope})
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(32, 22)
        Me.ToolStripSplitButton1.Text = "lope"
        '
        'ts_Preview
        '
        Me.ts_Preview.Name = "ts_Preview"
        Me.ts_Preview.Size = New System.Drawing.Size(215, 22)
        Me.ts_Preview.Text = "Preview"
        '
        'ts_Print
        '
        Me.ts_Print.Name = "ts_Print"
        Me.ts_Print.Size = New System.Drawing.Size(215, 22)
        Me.ts_Print.Text = "Print"
        '
        'ts_printShippingLbl
        '
        Me.ts_printShippingLbl.Name = "ts_printShippingLbl"
        Me.ts_printShippingLbl.Size = New System.Drawing.Size(215, 22)
        Me.ts_printShippingLbl.Text = "Print Shipping Label"
        '
        'ts_PrintEnvelope
        '
        Me.ts_PrintEnvelope.Name = "ts_PrintEnvelope"
        Me.ts_PrintEnvelope.Size = New System.Drawing.Size(215, 22)
        Me.ts_PrintEnvelope.Text = "Print Envelope"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator1.Visible = False
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator2.Visible = False
        '
        'ts_Find
        '
        Me.ts_Find.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ts_Find.Image = CType(resources.GetObject("ts_Find.Image"), System.Drawing.Image)
        Me.ts_Find.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Find.Name = "ts_Find"
        Me.ts_Find.Size = New System.Drawing.Size(23, 22)
        Me.ts_Find.Text = "ToolStripButton4"
        Me.ts_Find.ToolTipText = "Find"
        Me.ts_Find.Visible = False
        '
        'ts_History
        '
        Me.ts_History.Image = CType(resources.GetObject("ts_History.Image"), System.Drawing.Image)
        Me.ts_History.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_History.Name = "ts_History"
        Me.ts_History.Size = New System.Drawing.Size(74, 22)
        Me.ts_History.Text = "History"
        Me.ts_History.Visible = False
        '
        'ts_Journal
        '
        Me.ts_Journal.Image = CType(resources.GetObject("ts_Journal.Image"), System.Drawing.Image)
        Me.ts_Journal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Journal.Name = "ts_Journal"
        Me.ts_Journal.Size = New System.Drawing.Size(75, 22)
        Me.ts_Journal.Text = "Journal"
        Me.ts_Journal.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Customer:Job"
        '
        'cboCustomerID
        '
        Me.cboCustomerID.FormattingEnabled = True
        Me.cboCustomerID.Location = New System.Drawing.Point(12, 45)
        Me.cboCustomerID.Name = "cboCustomerID"
        Me.cboCustomerID.Size = New System.Drawing.Size(210, 21)
        Me.cboCustomerID.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 23)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Invoice"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.DarkBlue
        Me.Label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label4.Location = New System.Drawing.Point(10, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(288, 25)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Bill To"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBillingAdd
        '
        Me.txtBillingAdd.Location = New System.Drawing.Point(10, 135)
        Me.txtBillingAdd.Multiline = True
        Me.txtBillingAdd.Name = "txtBillingAdd"
        Me.txtBillingAdd.Size = New System.Drawing.Size(288, 55)
        Me.txtBillingAdd.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.DarkBlue
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label5.Location = New System.Drawing.Point(544, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(136, 15)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Invoice Date"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.DarkBlue
        Me.Label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label6.Location = New System.Drawing.Point(687, 32)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(140, 21)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Invoice No."
        '
        'lstClassList
        '
        Me.lstClassList.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.lstClassList.Location = New System.Drawing.Point(544, 47)
        Me.lstClassList.Name = "lstClassList"
        Me.lstClassList.Size = New System.Drawing.Size(136, 21)
        Me.lstClassList.TabIndex = 10
        '
        'txtInvoiceNo
        '
        Me.txtInvoiceNo.Location = New System.Drawing.Point(687, 48)
        Me.txtInvoiceNo.Name = "txtInvoiceNo"
        Me.txtInvoiceNo.Size = New System.Drawing.Size(140, 21)
        Me.txtInvoiceNo.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.DarkBlue
        Me.Label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label7.Location = New System.Drawing.Point(544, 113)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(282, 25)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Ship To"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShippingAdd
        '
        Me.txtShippingAdd.Location = New System.Drawing.Point(544, 135)
        Me.txtShippingAdd.Multiline = True
        Me.txtShippingAdd.Name = "txtShippingAdd"
        Me.txtShippingAdd.Size = New System.Drawing.Size(282, 52)
        Me.txtShippingAdd.TabIndex = 13
        '
        'cboShipToAddID
        '
        Me.cboShipToAddID.FormattingEnabled = True
        Me.cboShipToAddID.Location = New System.Drawing.Point(719, 114)
        Me.cboShipToAddID.Name = "cboShipToAddID"
        Me.cboShipToAddID.Size = New System.Drawing.Size(104, 21)
        Me.cboShipToAddID.TabIndex = 14
        '
        'grdInvoice
        '
        Me.grdInvoice.AllowUserToOrderColumns = True
        Me.grdInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdInvoice.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column3, Me.Column1, Me.Column7, Me.Column4, Me.Column5, Me.Column6, Me.Column8})
        Me.grdInvoice.Location = New System.Drawing.Point(12, 249)
        Me.grdInvoice.Name = "grdInvoice"
        Me.grdInvoice.RowTemplate.Height = 24
        Me.grdInvoice.Size = New System.Drawing.Size(815, 183)
        Me.grdInvoice.TabIndex = 15
        '
        'Column2
        '
        Me.Column2.HeaderText = "Item Code"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 200
        '
        'Column3
        '
        Me.Column3.HeaderText = "Description"
        Me.Column3.Name = "Column3"
        '
        'Column1
        '
        Me.Column1.HeaderText = "QTY"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 50
        '
        'Column7
        '
        Me.Column7.HeaderText = "Unit"
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 80
        '
        'Column4
        '
        Me.Column4.HeaderText = "Price Each"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Amount"
        Me.Column5.Name = "Column5"
        '
        'Column6
        '
        Me.Column6.HeaderText = "Tax"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 80
        '
        'Column8
        '
        Me.Column8.HeaderText = "Remove"
        Me.Column8.Name = "Column8"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(532, 151)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(109, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Payments Applied"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblSalesPeriod)
        Me.GroupBox1.Controls.Add(Me.dteSalesPeriodTo)
        Me.GroupBox1.Controls.Add(Me.dteSalesPeriodFrom)
        Me.GroupBox1.Controls.Add(Me.cboRepName)
        Me.GroupBox1.Controls.Add(Me.cboTermsName)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.dteShippingDate)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.cboRepID)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.cboTermsID)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtPONo)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 190)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(822, 58)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        '
        'lblSalesPeriod
        '
        Me.lblSalesPeriod.BackColor = System.Drawing.Color.DarkBlue
        Me.lblSalesPeriod.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalesPeriod.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblSalesPeriod.Location = New System.Drawing.Point(620, 13)
        Me.lblSalesPeriod.Name = "lblSalesPeriod"
        Me.lblSalesPeriod.Size = New System.Drawing.Size(195, 18)
        Me.lblSalesPeriod.TabIndex = 95
        Me.lblSalesPeriod.Text = "Sales Period"
        Me.lblSalesPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dteSalesPeriodTo
        '
        Me.dteSalesPeriodTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteSalesPeriodTo.Location = New System.Drawing.Point(718, 31)
        Me.dteSalesPeriodTo.Name = "dteSalesPeriodTo"
        Me.dteSalesPeriodTo.Size = New System.Drawing.Size(97, 21)
        Me.dteSalesPeriodTo.TabIndex = 59
        '
        'dteSalesPeriodFrom
        '
        Me.dteSalesPeriodFrom.CustomFormat = ""
        Me.dteSalesPeriodFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteSalesPeriodFrom.Location = New System.Drawing.Point(620, 31)
        Me.dteSalesPeriodFrom.Name = "dteSalesPeriodFrom"
        Me.dteSalesPeriodFrom.Size = New System.Drawing.Size(97, 21)
        Me.dteSalesPeriodFrom.TabIndex = 57
        '
        'cboRepName
        '
        Me.cboRepName.FormattingEnabled = True
        Me.cboRepName.Location = New System.Drawing.Point(297, 31)
        Me.cboRepName.Name = "cboRepName"
        Me.cboRepName.Size = New System.Drawing.Size(186, 21)
        Me.cboRepName.TabIndex = 55
        '
        'cboTermsName
        '
        Me.cboTermsName.FormattingEnabled = True
        Me.cboTermsName.Location = New System.Drawing.Point(147, 31)
        Me.cboTermsName.Name = "cboTermsName"
        Me.cboTermsName.Size = New System.Drawing.Size(145, 21)
        Me.cboTermsName.TabIndex = 54
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.DarkBlue
        Me.Label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label15.Location = New System.Drawing.Point(489, 13)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(124, 19)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Ship"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dteShippingDate
        '
        Me.dteShippingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteShippingDate.Location = New System.Drawing.Point(489, 31)
        Me.dteShippingDate.Name = "dteShippingDate"
        Me.dteShippingDate.Size = New System.Drawing.Size(124, 21)
        Me.dteShippingDate.TabIndex = 15
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.DarkBlue
        Me.Label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label14.Location = New System.Drawing.Point(296, 13)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(187, 19)
        Me.Label14.TabIndex = 12
        Me.Label14.Text = "Rep"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRepID
        '
        Me.cboRepID.FormattingEnabled = True
        Me.cboRepID.Location = New System.Drawing.Point(296, 31)
        Me.cboRepID.Name = "cboRepID"
        Me.cboRepID.Size = New System.Drawing.Size(151, 21)
        Me.cboRepID.TabIndex = 13
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.DarkBlue
        Me.Label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label13.Location = New System.Drawing.Point(146, 13)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(146, 19)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "Terms"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTermsID
        '
        Me.cboTermsID.FormattingEnabled = True
        Me.cboTermsID.Location = New System.Drawing.Point(146, 30)
        Me.cboTermsID.Name = "cboTermsID"
        Me.cboTermsID.Size = New System.Drawing.Size(145, 21)
        Me.cboTermsID.TabIndex = 11
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.DarkBlue
        Me.Label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label12.Location = New System.Drawing.Point(5, 13)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(138, 19)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "D.R. No."
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPONo
        '
        Me.txtPONo.Location = New System.Drawing.Point(5, 32)
        Me.txtPONo.Multiline = True
        Me.txtPONo.Name = "txtPONo"
        Me.txtPONo.Size = New System.Drawing.Size(137, 21)
        Me.txtPONo.TabIndex = 9
        '
        'cboViaName
        '
        Me.cboViaName.FormattingEnabled = True
        Me.cboViaName.Location = New System.Drawing.Point(305, 91)
        Me.cboViaName.Name = "cboViaName"
        Me.cboViaName.Size = New System.Drawing.Size(119, 21)
        Me.cboViaName.TabIndex = 54
        Me.cboViaName.Visible = False
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.DarkBlue
        Me.Label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label17.Location = New System.Drawing.Point(412, 73)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(122, 19)
        Me.Label17.TabIndex = 18
        Me.Label17.Text = "F.O.B."
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label17.Visible = False
        '
        'txtFOB
        '
        Me.txtFOB.Location = New System.Drawing.Point(412, 91)
        Me.txtFOB.Multiline = True
        Me.txtFOB.Name = "txtFOB"
        Me.txtFOB.Size = New System.Drawing.Size(122, 20)
        Me.txtFOB.TabIndex = 19
        Me.txtFOB.Visible = False
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.DarkBlue
        Me.Label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label16.Location = New System.Drawing.Point(304, 73)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(120, 19)
        Me.Label16.TabIndex = 16
        Me.Label16.Text = "Via"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label16.Visible = False
        '
        'cboViaID
        '
        Me.cboViaID.FormattingEnabled = True
        Me.cboViaID.Location = New System.Drawing.Point(305, 91)
        Me.cboViaID.Name = "cboViaID"
        Me.cboViaID.Size = New System.Drawing.Size(119, 21)
        Me.cboViaID.TabIndex = 17
        Me.cboViaID.Visible = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(532, 174)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(79, 13)
        Me.Label19.TabIndex = 30
        Me.Label19.Text = "Balance Due"
        '
        'cboTaxCodeID
        '
        Me.cboTaxCodeID.FormattingEnabled = True
        Me.cboTaxCodeID.Location = New System.Drawing.Point(135, 70)
        Me.cboTaxCodeID.Name = "cboTaxCodeID"
        Me.cboTaxCodeID.Size = New System.Drawing.Size(83, 21)
        Me.cboTaxCodeID.TabIndex = 38
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(13, 73)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(122, 13)
        Me.Label21.TabIndex = 37
        Me.Label21.Text = "Customer Tax Code"
        '
        'lblTotal
        '
        Me.lblTotal.Location = New System.Drawing.Point(652, 44)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(143, 13)
        Me.lblTotal.TabIndex = 50
        Me.lblTotal.Text = "0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(528, 44)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(59, 18)
        Me.Label22.TabIndex = 49
        Me.Label22.Text = "Total"
        '
        'cboCustomerName
        '
        Me.cboCustomerName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboCustomerName.FormattingEnabled = True
        Me.cboCustomerName.Location = New System.Drawing.Point(12, 45)
        Me.cboCustomerName.Name = "cboCustomerName"
        Me.cboCustomerName.Size = New System.Drawing.Size(286, 21)
        Me.cboCustomerName.TabIndex = 51
        '
        'cboTaxCodeName
        '
        Me.cboTaxCodeName.FormattingEnabled = True
        Me.cboTaxCodeName.Location = New System.Drawing.Point(135, 70)
        Me.cboTaxCodeName.Name = "cboTaxCodeName"
        Me.cboTaxCodeName.Size = New System.Drawing.Size(163, 21)
        Me.cboTaxCodeName.TabIndex = 52
        '
        'cboShipToAddName
        '
        Me.cboShipToAddName.FormattingEnabled = True
        Me.cboShipToAddName.Location = New System.Drawing.Point(606, 114)
        Me.cboShipToAddName.Name = "cboShipToAddName"
        Me.cboShipToAddName.Size = New System.Drawing.Size(220, 21)
        Me.cboShipToAddName.TabIndex = 53
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(739, 666)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(90, 28)
        Me.btnClose.TabIndex = 56
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(646, 666)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(90, 28)
        Me.btnSave.TabIndex = 55
        Me.btnSave.Text = "S&ave "
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblTaxAmt
        '
        Me.lblTaxAmt.Location = New System.Drawing.Point(652, 19)
        Me.lblTaxAmt.Name = "lblTaxAmt"
        Me.lblTaxAmt.Size = New System.Drawing.Size(143, 13)
        Me.lblTaxAmt.TabIndex = 58
        Me.lblTaxAmt.Text = "0.00"
        Me.lblTaxAmt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblBalance
        '
        Me.lblBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalance.Location = New System.Drawing.Point(652, 174)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(143, 13)
        Me.lblBalance.TabIndex = 60
        Me.lblBalance.Text = "0.00"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.DarkBlue
        Me.Label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label2.Location = New System.Drawing.Point(687, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(140, 15)
        Me.Label2.TabIndex = 61
        Me.Label2.Text = "Due Date"
        '
        'dteDueDate
        '
        Me.dteDueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteDueDate.Location = New System.Drawing.Point(687, 88)
        Me.dteDueDate.Name = "dteDueDate"
        Me.dteDueDate.Size = New System.Drawing.Size(140, 21)
        Me.dteDueDate.TabIndex = 62
        '
        'lblPaid
        '
        Me.lblPaid.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.lblPaid.Image = CType(resources.GetObject("lblPaid.Image"), System.Drawing.Image)
        Me.lblPaid.Location = New System.Drawing.Point(305, 114)
        Me.lblPaid.Name = "lblPaid"
        Me.lblPaid.Size = New System.Drawing.Size(229, 76)
        Me.lblPaid.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lblPaid.TabIndex = 63
        Me.lblPaid.TabStop = False
        Me.lblPaid.Visible = False
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.DarkBlue
        Me.Label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label10.Location = New System.Drawing.Point(544, 73)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(136, 15)
        Me.Label10.TabIndex = 64
        Me.Label10.Text = "Delivery Date"
        '
        'dteDeliverydate
        '
        Me.dteDeliverydate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteDeliverydate.Location = New System.Drawing.Point(544, 88)
        Me.dteDeliverydate.Name = "dteDeliverydate"
        Me.dteDeliverydate.Size = New System.Drawing.Size(136, 21)
        Me.dteDeliverydate.TabIndex = 65
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblSalesDiscountAmount)
        Me.GroupBox2.Controls.Add(Me.lblPayments)
        Me.GroupBox2.Controls.Add(Me.Panel2)
        Me.GroupBox2.Controls.Add(Me.lblRetDiscount)
        Me.GroupBox2.Controls.Add(Me.lblRDPercent)
        Me.GroupBox2.Controls.Add(Me.txtRDAmt)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.cboRD)
        Me.GroupBox2.Controls.Add(Me.lblSaleDiscount)
        Me.GroupBox2.Controls.Add(Me.lblSDpercent)
        Me.GroupBox2.Controls.Add(Me.txtSDAmt)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.cboSD)
        Me.GroupBox2.Controls.Add(Me.lblPercentage)
        Me.GroupBox2.Controls.Add(Me.cboTaxName)
        Me.GroupBox2.Controls.Add(Me.cboTaxID)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.btnApplyCredits)
        Me.GroupBox2.Controls.Add(Me.btnAddTimeCost)
        Me.GroupBox2.Controls.Add(Me.txtMemo)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.chkEmail)
        Me.GroupBox2.Controls.Add(Me.chkPrint)
        Me.GroupBox2.Controls.Add(Me.txtCustomerMsg)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Controls.Add(Me.lblTaxAmt)
        Me.GroupBox2.Controls.Add(Me.lblTotal)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.lblBalance)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Location = New System.Drawing.Point(10, 456)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(819, 196)
        Me.GroupBox2.TabIndex = 66
        Me.GroupBox2.TabStop = False
        '
        'lblSalesDiscountAmount
        '
        Me.lblSalesDiscountAmount.Location = New System.Drawing.Point(144, 96)
        Me.lblSalesDiscountAmount.Name = "lblSalesDiscountAmount"
        Me.lblSalesDiscountAmount.Size = New System.Drawing.Size(107, 17)
        Me.lblSalesDiscountAmount.TabIndex = 88
        Me.lblSalesDiscountAmount.Visible = False
        '
        'lblPayments
        '
        Me.lblPayments.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.lblPayments.Location = New System.Drawing.Point(652, 145)
        Me.lblPayments.Name = "lblPayments"
        Me.lblPayments.ReadOnly = True
        Me.lblPayments.Size = New System.Drawing.Size(142, 21)
        Me.lblPayments.TabIndex = 87
        Me.lblPayments.Text = "0.00"
        Me.lblPayments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel2.Location = New System.Drawing.Point(652, 190)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(142, 2)
        Me.Panel2.TabIndex = 86
        '
        'lblRetDiscount
        '
        Me.lblRetDiscount.Location = New System.Drawing.Point(652, 120)
        Me.lblRetDiscount.Name = "lblRetDiscount"
        Me.lblRetDiscount.Size = New System.Drawing.Size(143, 13)
        Me.lblRetDiscount.TabIndex = 85
        Me.lblRetDiscount.Text = "0.00"
        Me.lblRetDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblRDPercent
        '
        Me.lblRDPercent.AutoSize = True
        Me.lblRDPercent.Location = New System.Drawing.Point(469, 121)
        Me.lblRDPercent.Name = "lblRDPercent"
        Me.lblRDPercent.Size = New System.Drawing.Size(47, 13)
        Me.lblRDPercent.TabIndex = 84
        Me.lblRDPercent.Text = "(0.0%)"
        '
        'txtRDAmt
        '
        Me.txtRDAmt.Enabled = False
        Me.txtRDAmt.Location = New System.Drawing.Point(524, 117)
        Me.txtRDAmt.Name = "txtRDAmt"
        Me.txtRDAmt.Size = New System.Drawing.Size(95, 21)
        Me.txtRDAmt.TabIndex = 83
        Me.txtRDAmt.Text = "0.00"
        Me.txtRDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(256, 120)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(104, 13)
        Me.Label27.TabIndex = 82
        Me.Label27.Text = "Retailer Discount"
        '
        'cboRD
        '
        Me.cboRD.FormattingEnabled = True
        Me.cboRD.Location = New System.Drawing.Point(360, 117)
        Me.cboRD.Name = "cboRD"
        Me.cboRD.Size = New System.Drawing.Size(105, 21)
        Me.cboRD.TabIndex = 81
        '
        'lblSaleDiscount
        '
        Me.lblSaleDiscount.Location = New System.Drawing.Point(652, 95)
        Me.lblSaleDiscount.Name = "lblSaleDiscount"
        Me.lblSaleDiscount.Size = New System.Drawing.Size(143, 13)
        Me.lblSaleDiscount.TabIndex = 80
        Me.lblSaleDiscount.Text = "0.00"
        Me.lblSaleDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSDpercent
        '
        Me.lblSDpercent.AutoSize = True
        Me.lblSDpercent.Location = New System.Drawing.Point(469, 96)
        Me.lblSDpercent.Name = "lblSDpercent"
        Me.lblSDpercent.Size = New System.Drawing.Size(47, 13)
        Me.lblSDpercent.TabIndex = 79
        Me.lblSDpercent.Text = "(0.0%)"
        '
        'txtSDAmt
        '
        Me.txtSDAmt.Enabled = False
        Me.txtSDAmt.Location = New System.Drawing.Point(524, 92)
        Me.txtSDAmt.Name = "txtSDAmt"
        Me.txtSDAmt.Size = New System.Drawing.Size(95, 21)
        Me.txtSDAmt.TabIndex = 78
        Me.txtSDAmt.Text = "0.00"
        Me.txtSDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(257, 97)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(91, 13)
        Me.Label18.TabIndex = 77
        Me.Label18.Text = "Sales Discount"
        '
        'cboSD
        '
        Me.cboSD.FormattingEnabled = True
        Me.cboSD.Location = New System.Drawing.Point(360, 92)
        Me.cboSD.Name = "cboSD"
        Me.cboSD.Size = New System.Drawing.Size(105, 21)
        Me.cboSD.TabIndex = 76
        '
        'lblPercentage
        '
        Me.lblPercentage.AutoSize = True
        Me.lblPercentage.Location = New System.Drawing.Point(552, 19)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(47, 13)
        Me.lblPercentage.TabIndex = 73
        Me.lblPercentage.Text = "(0.0%)"
        '
        'cboTaxName
        '
        Me.cboTaxName.FormattingEnabled = True
        Me.cboTaxName.Location = New System.Drawing.Point(443, 15)
        Me.cboTaxName.Name = "cboTaxName"
        Me.cboTaxName.Size = New System.Drawing.Size(104, 21)
        Me.cboTaxName.TabIndex = 74
        '
        'cboTaxID
        '
        Me.cboTaxID.FormattingEnabled = True
        Me.cboTaxID.Location = New System.Drawing.Point(443, 15)
        Me.cboTaxID.Name = "cboTaxID"
        Me.cboTaxID.Size = New System.Drawing.Size(104, 21)
        Me.cboTaxID.TabIndex = 72
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(413, 17)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(59, 18)
        Me.Label25.TabIndex = 71
        Me.Label25.Text = "Tax"
        '
        'btnApplyCredits
        '
        Me.btnApplyCredits.Location = New System.Drawing.Point(124, 164)
        Me.btnApplyCredits.Name = "btnApplyCredits"
        Me.btnApplyCredits.Size = New System.Drawing.Size(142, 23)
        Me.btnApplyCredits.TabIndex = 70
        Me.btnApplyCredits.Text = "Apply Credits"
        Me.btnApplyCredits.UseVisualStyleBackColor = True
        Me.btnApplyCredits.Visible = False
        '
        'btnAddTimeCost
        '
        Me.btnAddTimeCost.Location = New System.Drawing.Point(124, 141)
        Me.btnAddTimeCost.Name = "btnAddTimeCost"
        Me.btnAddTimeCost.Size = New System.Drawing.Size(142, 23)
        Me.btnAddTimeCost.TabIndex = 69
        Me.btnAddTimeCost.Text = "Add Time Cost"
        Me.btnAddTimeCost.UseVisualStyleBackColor = True
        Me.btnAddTimeCost.Visible = False
        '
        'txtMemo
        '
        Me.txtMemo.Location = New System.Drawing.Point(84, 59)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(319, 21)
        Me.txtMemo.TabIndex = 68
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(7, 59)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(59, 18)
        Me.Label11.TabIndex = 67
        Me.Label11.Text = "Memo"
        '
        'chkEmail
        '
        Me.chkEmail.AutoSize = True
        Me.chkEmail.Location = New System.Drawing.Point(12, 168)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Size = New System.Drawing.Size(107, 17)
        Me.chkEmail.TabIndex = 65
        Me.chkEmail.Text = "To be emailed"
        Me.chkEmail.UseVisualStyleBackColor = True
        Me.chkEmail.Visible = False
        '
        'chkPrint
        '
        Me.chkPrint.AutoSize = True
        Me.chkPrint.Location = New System.Drawing.Point(12, 145)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(102, 17)
        Me.chkPrint.TabIndex = 64
        Me.chkPrint.Text = "To be printed"
        Me.chkPrint.UseVisualStyleBackColor = True
        Me.chkPrint.Visible = False
        '
        'txtCustomerMsg
        '
        Me.txtCustomerMsg.Location = New System.Drawing.Point(84, 15)
        Me.txtCustomerMsg.Multiline = True
        Me.txtCustomerMsg.Name = "txtCustomerMsg"
        Me.txtCustomerMsg.Size = New System.Drawing.Size(319, 41)
        Me.txtCustomerMsg.TabIndex = 63
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(7, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 34)
        Me.Label8.TabIndex = 62
        Me.Label8.Text = "Customer &Message"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel1.Location = New System.Drawing.Point(652, 62)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(142, 2)
        Me.Panel1.TabIndex = 61
        '
        'btnRefresh
        '
        Me.btnRefresh.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Location = New System.Drawing.Point(105, 666)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(87, 28)
        Me.btnRefresh.TabIndex = 75
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(16, 666)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 28)
        Me.btnPreview.TabIndex = 66
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnDelRows
        '
        Me.btnDelRows.Image = Global.CSAcctg.My.Resources.Resources.delete
        Me.btnDelRows.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelRows.Location = New System.Drawing.Point(12, 432)
        Me.btnDelRows.Name = "btnDelRows"
        Me.btnDelRows.Size = New System.Drawing.Size(166, 28)
        Me.btnDelRows.TabIndex = 76
        Me.btnDelRows.Text = "Delete Selected Rows"
        Me.btnDelRows.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDelRows.UseVisualStyleBackColor = True
        Me.btnDelRows.Visible = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(13, 660)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(145, 13)
        Me.Label20.TabIndex = 88
        Me.Label20.Text = "Payment Credit Account"
        Me.Label20.Visible = False
        '
        'cboPayAcnt
        '
        Me.cboPayAcnt.FormattingEnabled = True
        Me.cboPayAcnt.Location = New System.Drawing.Point(161, 657)
        Me.cboPayAcnt.Name = "cboPayAcnt"
        Me.cboPayAcnt.Size = New System.Drawing.Size(209, 21)
        Me.cboPayAcnt.TabIndex = 88
        Me.cboPayAcnt.Visible = False
        '
        'cboTaxAcnt
        '
        Me.cboTaxAcnt.FormattingEnabled = True
        Me.cboTaxAcnt.Location = New System.Drawing.Point(497, 657)
        Me.cboTaxAcnt.Name = "cboTaxAcnt"
        Me.cboTaxAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboTaxAcnt.TabIndex = 90
        Me.cboTaxAcnt.Visible = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(378, 660)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(116, 13)
        Me.Label23.TabIndex = 89
        Me.Label23.Text = "Tax Credit Account"
        Me.Label23.Visible = False
        '
        'txtPercent
        '
        Me.txtPercent.Location = New System.Drawing.Point(696, 657)
        Me.txtPercent.Name = "txtPercent"
        Me.txtPercent.Size = New System.Drawing.Size(40, 21)
        Me.txtPercent.TabIndex = 88
        Me.txtPercent.Text = "0.00"
        Me.txtPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPercent.Visible = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(739, 661)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(19, 13)
        Me.Label24.TabIndex = 88
        Me.Label24.Text = "%"
        Me.Label24.Visible = False
        '
        'btnSubTotal
        '
        Me.btnSubTotal.Location = New System.Drawing.Point(504, 437)
        Me.btnSubTotal.Name = "btnSubTotal"
        Me.btnSubTotal.Size = New System.Drawing.Size(155, 23)
        Me.btnSubTotal.TabIndex = 91
        Me.btnSubTotal.Text = "Compute Sub-Total/s"
        Me.btnSubTotal.UseVisualStyleBackColor = True
        '
        'lblSubTotal
        '
        Me.lblSubTotal.Location = New System.Drawing.Point(662, 438)
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Size = New System.Drawing.Size(142, 21)
        Me.lblSubTotal.TabIndex = 88
        Me.lblSubTotal.Text = "0.00"
        Me.lblSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkCancel
        '
        Me.chkCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkCancel.AutoSize = True
        Me.chkCancel.BackColor = System.Drawing.Color.White
        Me.chkCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCancel.ForeColor = System.Drawing.Color.Red
        Me.chkCancel.Location = New System.Drawing.Point(636, 5)
        Me.chkCancel.Name = "chkCancel"
        Me.chkCancel.Size = New System.Drawing.Size(122, 17)
        Me.chkCancel.TabIndex = 92
        Me.chkCancel.Text = "Cancel Invoice"
        Me.chkCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOk.Location = New System.Drawing.Point(763, 1)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(67, 22)
        Me.btnOk.TabIndex = 93
        Me.btnOk.Text = "&Ok..."
        Me.btnOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'btnAccountability
        '
        Me.btnAccountability.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccountability.Image = Global.CSAcctg.My.Resources.Resources.post
        Me.btnAccountability.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAccountability.Location = New System.Drawing.Point(105, 666)
        Me.btnAccountability.Name = "btnAccountability"
        Me.btnAccountability.Size = New System.Drawing.Size(146, 28)
        Me.btnAccountability.TabIndex = 94
        Me.btnAccountability.Text = "Accountability"
        Me.btnAccountability.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAccountability.UseVisualStyleBackColor = True
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(308, 48)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(43, 13)
        Me.Label26.TabIndex = 97
        Me.Label26.Text = "Class:"
        '
        'txtClassName
        '
        Me.txtClassName.BackColor = System.Drawing.SystemColors.HighlightText
        Me.txtClassName.Location = New System.Drawing.Point(346, 45)
        Me.txtClassName.Name = "txtClassName"
        Me.txtClassName.ReadOnly = True
        Me.txtClassName.Size = New System.Drawing.Size(136, 21)
        Me.txtClassName.TabIndex = 100
        '
        'txtClassRefno
        '
        Me.txtClassRefno.Location = New System.Drawing.Point(346, 21)
        Me.txtClassRefno.Name = "txtClassRefno"
        Me.txtClassRefno.Size = New System.Drawing.Size(109, 21)
        Me.txtClassRefno.TabIndex = 101
        Me.txtClassRefno.Visible = False
        '
        'BtnBrowse
        '
        Me.BtnBrowse.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBrowse.Location = New System.Drawing.Point(482, 45)
        Me.BtnBrowse.Name = "BtnBrowse"
        Me.BtnBrowse.Size = New System.Drawing.Size(56, 21)
        Me.BtnBrowse.TabIndex = 102
        Me.BtnBrowse.Text = "Browse"
        Me.BtnBrowse.UseVisualStyleBackColor = True
        Me.BtnBrowse.Visible = False
        '
        'frm_cust_CreateInvoice
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(837, 698)
        Me.ControlBox = False
        Me.Controls.Add(Me.BtnBrowse)
        Me.Controls.Add(Me.txtClassRefno)
        Me.Controls.Add(Me.txtClassName)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.cboViaName)
        Me.Controls.Add(Me.btnAccountability)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtFOB)
        Me.Controls.Add(Me.chkCancel)
        Me.Controls.Add(Me.lblSubTotal)
        Me.Controls.Add(Me.cboViaID)
        Me.Controls.Add(Me.btnSubTotal)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txtPercent)
        Me.Controls.Add(Me.cboTaxAcnt)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.cboPayAcnt)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.btnDelRows)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.dteDeliverydate)
        Me.Controls.Add(Me.lblPaid)
        Me.Controls.Add(Me.dteDueDate)
        Me.Controls.Add(Me.txtInvoiceNo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboShipToAddName)
        Me.Controls.Add(Me.cboTaxCodeName)
        Me.Controls.Add(Me.cboCustomerName)
        Me.Controls.Add(Me.cboTaxCodeID)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grdInvoice)
        Me.Controls.Add(Me.cboShipToAddID)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lstClassList)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtBillingAdd)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboCustomerID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.txtShippingAdd)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(845, 732)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(845, 688)
        Me.Name = "frm_cust_CreateInvoice"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Invoice"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.grdInvoice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.lblPaid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_Previous As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Next As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_Preview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Print As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_printShippingLbl As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_PrintEnvelope As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_Find As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_History As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Journal As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtBillingAdd As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lstClassList As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtInvoiceNo As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtShippingAdd As System.Windows.Forms.TextBox
    Friend WithEvents cboShipToAddID As System.Windows.Forms.ComboBox
    Friend WithEvents grdInvoice As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtPONo As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboTermsID As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cboRepID As System.Windows.Forms.ComboBox
    Friend WithEvents dteShippingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtFOB As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cboViaID As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents cboTaxCodeID As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents cboCustomerName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxCodeName As System.Windows.Forms.ComboBox
    Friend WithEvents cboShipToAddName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTermsName As System.Windows.Forms.ComboBox
    Friend WithEvents cboRepName As System.Windows.Forms.ComboBox
    Friend WithEvents cboViaName As System.Windows.Forms.ComboBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblTaxAmt As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dteDueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPaid As System.Windows.Forms.PictureBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dteDeliverydate As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents cboTaxName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxID As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents btnApplyCredits As System.Windows.Forms.Button
    Friend WithEvents btnAddTimeCost As System.Windows.Forms.Button
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents chkEmail As System.Windows.Forms.CheckBox
    Friend WithEvents txtCustomerMsg As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblSaleDiscount As System.Windows.Forms.Label
    Friend WithEvents lblSDpercent As System.Windows.Forms.Label
    Friend WithEvents txtSDAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cboSD As System.Windows.Forms.ComboBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblRetDiscount As System.Windows.Forms.Label
    Friend WithEvents lblRDPercent As System.Windows.Forms.Label
    Friend WithEvents txtRDAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cboRD As System.Windows.Forms.ComboBox
    Friend WithEvents btnDelRows As System.Windows.Forms.Button
    Friend WithEvents lblPayments As System.Windows.Forms.TextBox
    Friend WithEvents chkPrint As System.Windows.Forms.CheckBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cboPayAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtPercent As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents btnSubTotal As System.Windows.Forms.Button
    Friend WithEvents lblSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents chkCancel As System.Windows.Forms.CheckBox
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnAccountability As System.Windows.Forms.Button
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dteSalesPeriodFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblSalesPeriod As System.Windows.Forms.Label
    Friend WithEvents dteSalesPeriodTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtClassName As System.Windows.Forms.TextBox
    Friend WithEvents txtClassRefno As System.Windows.Forms.TextBox
    Friend WithEvents BtnBrowse As System.Windows.Forms.Button
    Friend WithEvents lblSalesDiscountAmount As System.Windows.Forms.Label
End Class
