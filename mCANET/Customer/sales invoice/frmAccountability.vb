Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmAccountability

    Private gcon As New Clsappconfiguration

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ' If validateAccountability() = "Sapat" Then
        If gInvoiceKey = "" Then
            Call updateInvoiceAccountability()
            txtPercent.Text = "0.00"
            txtPer1.Text = "0.00"
            Me.Close()
        Else
            Call updateInvoiceAccountability()
            Call updateinvoice()
        End If
        validateAccountability("Invoice")
        '  End If
    End Sub

    Private Sub frmAccountability_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim comboBox As ComboBox() = {cboSalesAcnt, cboPayAcnt, cboTaxAcnt, cboOutputTax, cboCOS, cboMerch, cboDiscount}

        For index As Integer = 0 To 6
            m_DisplayAccountsAll(comboBox(index))
        Next

        loadaccountabilitydetails()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        txtPercent.Text = "0.00"
        txtPer1.Text = "0.00"
        validateAccountability("Invoice")
        Me.Close()
    End Sub

    Public Sub loadaccountabilitydetails()
        Dim sSQLCmd As String

        Try
            If gInvoiceKey <> "" Then
                sSQLCmd = "usp_t_InvoiceAccountabilityLoad "
                sSQLCmd &= " @fxKeyInvoice='" & gInvoiceKey & "' "
                sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                    If rd.Read Then
                        cboPayAcnt.SelectedItem = rd.Item(0).ToString
                        cboTaxAcnt.SelectedItem = rd.Item(1).ToString
                        cboSalesAcnt.SelectedItem = rd.Item(2).ToString
                        cboOutputTax.SelectedItem = rd.Item(3).ToString
                        txtPercent.Text = rd.Item(4)
                        txtPer1.Text = rd.Item(5)
                        cboCOS.SelectedItem = rd.Item(6).ToString
                        cboMerch.SelectedItem = rd.Item(7).ToString
                        cboDiscount.SelectedItem = rd.Item(8).ToString
                    End If
                End Using
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub updateInvoiceAccountability()
        Dim pxPayAccount As String = ""
        Dim pxTaxAccount As String = ""
        Dim pxSalesAccount As String = ""
        Dim pxOutTaxAcnt As String = ""
        Dim pxCOSAcnt As String = ""
        Dim pxMerchAcnt As String = ""
        Dim pxDiscount As String = ""

        Dim _accountability As String() = {pxPayAccount, pxTaxAccount, pxSalesAccount, pxOutTaxAcnt, _
                                          pxCOSAcnt, pxMerchAcnt, pxDiscount}
        Dim _combobox As ComboBox() = {cboSalesAcnt, cboPayAcnt, cboTaxAcnt, cboOutputTax, cboCOS, cboMerch, cboDiscount}


        For index As Integer = 0 To _combobox.Length - 1
            If _combobox(index).Text <> " " Then
                _accountability(index) = getAccountID(_combobox(index).SelectedItem)
            End If
        Next

        gKeySalesAccount = _accountability(0)
        gKeyPayAccount = _accountability(1)
        gKeyTaxAccount = _accountability(2)
        gKeyOutputTax = _accountability(3)
        gKeyCOSAccount = _accountability(4)
        gKeyMerchAcnt = _accountability(5)
        gKeyDiscount = _accountability(6)


        gTaxPercent = CDec(txtPercent.Text)
        gOutputTaxPercent = CDec(txtPer1.Text)
    End Sub

    Private Sub updateinvoice()
        Try
            Dim sSQLCmd As String = "usp_t_InvoiceAccountability_update "
            sSQLCmd &= " @fxKeyPayAcnt=" & IIf(gKeyPayAccount = "", "NULL", "'" & gKeyPayAccount & "'") & " "
            sSQLCmd &= ",@fxKeyTaxAcnt=" & IIf(gKeyTaxAccount = "", "NULL", "'" & gKeyTaxAccount & "'") & " "
            sSQLCmd &= ",@fxKeySalesTax=" & IIf(gKeySalesAccount = "", "NULL", "'" & gKeySalesAccount & "'") & " "
            sSQLCmd &= ",@fxOutputTax=" & IIf(gKeyOutputTax = "", "NULL", "'" & gKeyOutputTax & "'") & " "
            sSQLCmd &= ",@fxKeyCOSAcnt=" & IIf(gKeyCOSAccount = "", "NULL", "'" & gKeyCOSAccount & "'") & " "
            sSQLCmd &= ",@fdOutputTaxPercent='" & gOutputTaxPercent & "' "
            sSQLCmd &= ",@fdTaxPercent='" & gTaxPercent & "' "
            sSQLCmd &= ",@fxKeyInvoice=" & IIf(gInvoiceKey = "", "NULL", "'" & gInvoiceKey & "'") & " "
            sSQLCmd &= ",@fxKeyCompany=" & IIf(gCompanyID() = "", "NULL", "'" & gCompanyID() & "'") & " "
            sSQLCmd &= ",@fxKeySalesDiscount=" & IIf(gKeyDiscount = "", "NULL", "'" & gKeyDiscount & "'") & " "
            sSQLCmd &= ",@fxKeyMerchAcnt=" & IIf(gKeyMerchAcnt = "", "NULL", "'" & gKeyMerchAcnt & "'") & " "
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
            Me.Close()
        End Try
        Me.Close()
    End Sub


    Private Sub cboOutputTax_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOutputTax.SelectedValueChanged
        If cboOutputTax.SelectedItem = " " Then
            txtPer1.Text = "0.00"
            txtPer1.Enabled = False
        Else
            txtPer1.Text = "12.00"
            txtPer1.Enabled = True
        End If
    End Sub



    Private Sub cboOutputTax_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOutputTax.SelectedIndexChanged
        If cboOutputTax.SelectedItem = " " Then
            txtPer1.Text = "0.00"
            txtPer1.Enabled = False
        Else
            txtPer1.Text = "12.00"
            txtPer1.Enabled = True
        End If
    End Sub

    Private Sub cboOutputTax_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOutputTax.TextChanged
        If cboOutputTax.SelectedItem = " " Then
            txtPer1.Text = "0.00"
            txtPer1.Enabled = False
        Else
            txtPer1.Text = "12.00"
            txtPer1.Enabled = True
        End If
    End Sub
End Class