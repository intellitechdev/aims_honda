Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Collections.Specialized

Public Class frm_cust_CreateInvoice
#Region "Declarations"

    Private gcon As New Clsappconfiguration
    Private Const cKeyInvoiceItem = 0
    Private Const cQuantity = 3
    Private Const cItem = 1
    Private Const cDescription = 2
    Private Const cUnit = 4
    Private Const cPrice = 5
    Private Const cAmount = 6
    Private Const cTax = 7
    Private bIsNew As Boolean = False
    Private sKeyInvoice As String = ""
    Private sKeyCustomer As String = ""
    Private sKeySalesTaxCode As String = ""
    Private sKeyTax As String
    Private sKeyShipAdd As String = ""
    Private sKeyTerms As String = ""
    Private sKeyRep As String = ""
    Private dPercentage As Decimal = 0
    Private bPaid As Boolean
    Private sBillAddInitial As String = ""
    Private sShipAddInitial As String = ""
    Private sKeyShipAddInitial As String = ""
    Private sKeySalesTaxCodeInitial As String
    Private dTotalInitial As Decimal = 0
    Private dTaxAmntInitial As Decimal = 0
    Private sKeyToDel() As String = {""}
    Private iDel As Integer = 0
    Private psDescriptino As String = ""
    Private psUnit As String = ""
    Private pdPrice As Decimal = 0
    Private psSalesTax As String = ""
    Private psEdit As Integer = 0
    Private sclear As Integer = 0
    Private sCoAddress As String
    Private checkDate As New clsRestrictedDate
    Private ClassRefno As String = ""
    Public Accountability_Form_Mode As String = "No Current Accountability"
#End Region
#Region "Form Events"

    Public Property IsNew() As Boolean
        Get
            Return bIsNew
        End Get
        Set(ByVal value As Boolean)
            bIsNew = value
        End Set
    End Property
    Public Property KeyInvoice() As String
        Get
            Return sKeyInvoice
        End Get
        Set(ByVal value As String)
            sKeyInvoice = value
            refreshForm()
        End Set
    End Property
    Private Function defaultInvoiceNo() As String
        Dim sDefault As String = "1"
        Dim sInvc As String = ""
        Dim sInvcNum As String = ""

        Dim sSCLCmd As String

        Try
            sSCLCmd = "SELECT TOP 1 fcInvoiceNo FROM tInvoice where fxKeyCompany ='" & gCompanyID() & "' ORDER BY fcInvoiceNo DESC"

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSCLCmd)
                If rd.Read Then
                    sInvc = rd.Item(0).ToString
                End If
                If sInvc = "" Or sInvc = "0" Then
                    sInvcNum = sDefault
                ElseIf sInvc <> "" Or sInvc <> "0" Then
                    sInvcNum = CInt(sInvc) + 1
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sInvcNum
    End Function
    Private Sub frm_cust_CreateInvoice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If frmMain.LblClassActivator.Visible = True Then
            Label26.Visible = True
            txtClassName.Visible = True
            txtClassName.Enabled = True
            BtnBrowse.Visible = True
        Else
            Label26.Visible = False
            txtClassName.Visible = False
            txtClassName.Enabled = False
            BtnBrowse.Visible = False
        End If


        Call loadcombobox()
        Call loaddiscounts()
        If sKeyInvoice = "" Or sKeyInvoice = Nothing Then
            sKeyInvoice = Guid.NewGuid.ToString
            bIsNew = True
            Me.Text = "Create Invoice"
            psEdit = 0
            txtInvoiceNo.Text = defaultInvoiceNo()
            refreshForm()
            IsDateRestricted()
        Else
            psEdit = 1
            refreshForm()
            Call getInvoice()
            IsDateRestricted()
        End If

        If validateAccountability("Invoice") = "Kulang" Then
            frmAccountability.ShowDialog()
        End If



    End Sub
    Private Sub loadform()
        Call m_LoadCustomers(cboCustomerName, Me)
    End Sub
    Private Sub loadcombobox()
        m_loadCustomer(cboCustomerName, cboCustomerID)
        m_loadSalesTaxCode(cboTaxCodeName, cboTaxCodeID)
        m_loadTax(cboTaxName, cboTaxID)
        m_loadTerms(cboTermsName, cboTermsID)
        m_loadSalesRep(cboRepName, cboRepID)
        m_loadShippingMethod(cboViaName, cboViaID)
    End Sub
    Private Sub refreshForm()
        Dim dtComp As DataRow = gCompanyInfo().Tables(0).Rows(0)
        sCoAddress = dtComp("co_name").ToString + _
                         vbCrLf + dtComp("co_street").ToString + _
                        " " + dtComp("co_city").ToString + _
                        vbCrLf + dtComp("co_zip").ToString
        Call createColumn()
    End Sub
    Private Sub createColumn()

        Dim colKeyInvoiceItem As New DataGridViewTextBoxColumn
        Dim colQuantity As New DataGridViewTextBoxColumn
        Dim colItem As New DataGridViewComboBoxColumn
        'Dim colItem As New DataGridViewTextBoxColumn
        Dim colDescription As New DataGridViewTextBoxColumn
        Dim colUnit As New DataGridViewTextBoxColumn
        Dim colPrice As New DataGridViewTextBoxColumn
        Dim colAmount As New DataGridViewTextBoxColumn
        Dim colTax As New DataGridViewComboBoxColumn
        Dim colSel As New DataGridViewCheckBoxColumn
        Dim dtItem As DataTable = m_GetItem.Tables(0)
        Dim dtTax As DataTable = m_GetSalesTaxCode.Tables(0)

        colKeyInvoiceItem.Name = "cKeyInvoiceItem"
        colQuantity.HeaderText = "Quantity"
        colQuantity.Name = "cQuantity"

        With colItem
            .Items.Clear()
            .HeaderText = "Item"
            .DataPropertyName = "fcItemName"
            .Name = "cItem"
            .DisplayMember = "fcItemName"
            .ValueMember = "fcItemName"
            .DropDownWidth = 200
            .AutoComplete = True
            .DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
            .MaxDropDownItems = 10
            .Resizable = DataGridViewTriState.True
            .DataSource = dtItem.DefaultView
        End With

        With colSel
            .HeaderText = "Select"
            .Name = "cSelect"
        End With

        With colTax
            .Items.Clear()
            .HeaderText = "Tax"
            .DataPropertyName = "fcSalesTaxCodeName"
            .DataSource = dtTax.DefaultView
            .DisplayMember = "fcSalesTaxCodeName"
            .ValueMember = "fcSalesTaxCodeName"
            .Name = "cTax"
        End With

        colDescription.HeaderText = "Description"
        colDescription.Name = "cDescription"
        colUnit.HeaderText = "Unit"
        colUnit.Name = "cUnit"
        colPrice.HeaderText = "Price"
        colPrice.Name = "cPrice"
        colAmount.HeaderText = "Amount"
        colAmount.Name = "cAmount"

        With grdInvoice
            .Columns.Clear()
            .Columns.Add(colKeyInvoiceItem)
            .Columns.Add(colItem)
            .Columns.Add(colDescription)
            .Columns.Add(colQuantity)
            .Columns.Add(colUnit)
            .Columns.Add(colPrice)
            .Columns.Add(colAmount)
            .Columns.Add(colTax)
            .Columns(cKeyInvoiceItem).Visible = False
            .Columns(cUnit).ReadOnly = True
            .Columns.Add(colSel)

            .Columns("cQuantity").Width = 70
            .Columns("cItem").Width = 200
            .Columns("cSelect").Width = 50
            .Columns("cTax").Width = 70
            .Columns("cUnit").Width = 50
            .Columns("cDescription").Width = 100
            .Columns("cPrice").Width = 100
            .Columns("cAmount").Width = 100
        End With

        With Me.grdInvoice
            Dim xCnt As Integer
            For xCnt = 0 To .Columns.Count - 1
                .Columns(xCnt).SortMode = DataGridViewColumnSortMode.NotSortable
            Next
        End With

    End Sub
    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdInvoice.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdInvoice.Rows(irow).Cells(iCol).Value)
    End Function
    Private Sub getInvoice()
        loadInvoice()

        m_selectedCboValue(cboCustomerName, cboCustomerID, sKeyCustomer)
        m_selectedCboValue(cboTaxCodeName, cboTaxCodeID, sKeySalesTaxCodeInitial)
        m_selectedCboValue(cboTaxName, cboTaxID, sKeyTax)
        m_selectedCboValue(cboTermsName, cboTermsID, sKeyTerms)
        m_selectedCboValue(cboRepName, cboRepID, sKeyRep)

        If sKeyCustomer <> "" Then
            m_loadShippingAddress(cboShipToAddName, cboShipToAddID, sKeyCustomer)
            m_selectedCboValue(cboShipToAddName, cboShipToAddID, sKeyShipAddInitial)
        End If

        txtBillingAdd.Text = sBillAddInitial
        txtShippingAdd.Text = sShipAddInitial

        lblTotal.Text = Format(dTotalInitial, "##,##0.00")
        lblTaxAmt.Text = Format(dTaxAmntInitial, "##,##0.00")

        If bPaid Then
            lblPaid.Visible = True
        Else
            lblPaid.Visible = False
        End If
    End Sub
    Private Sub getItemDetails()
        'Dim sSQLCmd As String = " select distinct t1.fxKeyInvoiceItem as [fxKeyInvoiceItem], t1.fdQuantity as [fdQuantity], "
        'sSQLCmd &= " t1.fcDescription as [fcDescription], t3.fcUnitMeasurement as [fcUnit], "
        'sSQLCmd &= " t1.fdPrice as [fdPrice], t1.fdAmount as [fdAmount], t4.fcSalesTaxCodeName as [fdTaxCode], "
        'sSQLCmd &= " t3.fcDescription+' '+isnull(t6.fcColorDescription, '')+' '+isnull(t3.fcSizeCode, '') as [fcItemName], t1.fdCount as [fdCount] "
        'sSQLCmd &= " from dbo.tInvoice_item as t1 left outer join dbo.tInvoice as t2 on t2.fxKeyInvoice = t1.fxKeyInvoice "
        'sSQLCmd &= " left outer join dbo.mItemMaster as t3 on t3.fxKeyItem = t1.fxKeyItem "
        'sSQLCmd &= " left outer join dbo.mSalesTaxCode as t4 on t4.fxKeySalesTaxCode = t1.fxKeySalesTaxCode "
        'sSQLCmd &= " left outer join dbo.mItemCode as t5 on t5.fcItemCode = t3.fcItemCode "
        'sSQLCmd &= " left outer join dbo.mItemColor as t6 on t6.fcColorCode = t3.fcColorCode"
        'sSQLCmd &= " WHERE t1.fxKeyInvoice = '" & sKeyInvoice & "'"
        'sSQLCmd &= " ORDER BY fdCount ASC "
        Dim sSQLCmd As String = "usp_t_SALESINVOICE_LoadDetails"
        sSQLCmd &= " @fxKeyInvoice = '" & sKeyInvoice & "'"
        With Me.grdInvoice
            Try
                Dim dtsTable As DataSet = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
                Dim xRow As Integer = 0
                For xRow = 0 To dtsTable.Tables(0).Rows.Count - 1
                    .Rows.Add()
                    .Item("cKeyInvoiceItem", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fxKeyInvoiceItem").ToString
                    .Item("cQuantity", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fdQuantity")
                    .Item("cDescription", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcDescription")
                    .Item("cItem", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcItemName")
                    .Item("cUnit", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcUnit")
                    .Item("cPrice", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdPrice")), "##,##0.00##")
                    .Item("cAmount", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdAmount")), "##,##0.00##")
                    .Item("cTax", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fdTaxCode")
                Next
            Catch ex As Exception
                MessageBox.Show(Err.Description, "Load Invoice Items")
            End Try
        End With
    End Sub
    Private Sub loadInvoice()
        Dim dtPO As DataSet = m_GetInvoice(sKeyInvoice)

        Try
            Using rd As DataTableReader = dtPO.CreateDataReader
                If rd.Read Then
                    Call getItemDetails()
                    sBillAddInitial = rd.Item("fcBillTo")
                    sShipAddInitial = rd.Item("fcShiptoAddress")
                    txtInvoiceNo.Text = rd.Item("fcInvoiceNo")
                    txtPONo.Text = rd.Item("fcPoNo")
                    lstClassList.Value = rd.Item("fdDateTransact")
                    dteShippingDate.Value = rd.Item("fdDateShip")
                    dteDueDate.Value = rd.Item("fdDateDue")
                    dteSalesPeriodFrom.Value = rd.Item("fdSalesPeriodFROM")
                    dteSalesPeriodTo.Value = rd.Item("fdSalesPeriodTO")
                    txtFOB.Text = rd.Item("fcFOB")
                    txtMemo.Text = rd.Item("fcMemo")
                    lblPayments.Text = rd.Item("fdPayment")
                    sKeyCustomer = rd.Item("fxKeyCustomer").ToString
                    sKeyTax = rd.Item("fxKeyTax").ToString
                    sKeySalesTaxCodeInitial = rd.Item("fxKeySalesTaxCode").ToString
                    sKeyShipAddInitial = rd.Item("fxKeyShipAdd").ToString
                    sKeyTerms = rd.Item("fxKeyTerms").ToString
                    sKeyRep = rd.Item("fxKeyRep").ToString
                    lblSaleDiscount.Text = Format(CDec(rd.Item("fdSDAmt")), "##,##0.00")
                    lblRetDiscount.Text = Format(CDec(rd.Item("fdRDAmt")), "##,##0.00")
                    dTotalInitial = Format(CDec(rd.Item("fdTotal")), "##,##0.00")
                    dTaxAmntInitial = Format(CDec(rd.Item("fdTaxAmount")), "##,##0.00")
                    dteDeliverydate.Value = rd.Item("fdDelivryDate")
                    cboSD.SelectedItem = rd.Item("fcSDCode").ToString
                    cboRD.SelectedItem = rd.Item("fcRDCode").ToString
                    lblBalance.Text = Format(CDec(rd.Item("fdBalance")), "##,##0.00")
                    bPaid = Format(CDec(rd.Item("fbPaid")), "##,##0.00")
                    txtCustomerMsg.Text = rd.Item("fcCustomerMessage").ToString
                    txtClassRefno.Text = rd.Item("fxKeyClassRefNo").ToString

                    If frmMain.LblClassActivator.Visible = True Then
                        txtClassName.Text = GetClassName(txtClassRefno.Text)
                        ClassRefno = txtClassRefno.Text
                    Else
                        txtClassName.Text = ""
                    End If

                Else
                    txtInvoiceNo.Text = defaultInvoiceNo()
                    txtPONo.Text = ""
                    lstClassList.Value = Now
                    dteShippingDate.Value = Now
                    dteDueDate.Value = Now
                    txtFOB.Text = ""
                    lblPayments.Text = "0.00"
                    sBillAddInitial = ""
                    sShipAddInitial = ""
                    sKeyCustomer = ""
                    sKeyTax = ""
                    sKeySalesTaxCode = ""
                    sKeyShipAddInitial = ""
                    sKeyTerms = ""
                    sKeyRep = ""
                    lblTotal.Text = "0.00"
                    dteDeliverydate.Value = Now
                    bPaid = False
                    dTotalInitial = "0"
                    dTaxAmntInitial = "0"
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Invoice")
        End Try
    End Sub
    Private Function loadPayment() As DataSet
        Dim sSQLCmd As String = "SELECT fxKeyPaymentItem,fdAmtDue,fdPayment "
        sSQLCmd &= "FROM tPayment_Invoice "
        sSQLCmd &= "WHERE fxKeyInvoice = '" & sKeyInvoice & "'"

        Try
            Return SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Payment")
            Return Nothing
        End Try
    End Function
    Private Sub loaddiscounts()
        Call m_GetSalesDiscount(cboSD, Me)
        Call m_GetRetailerDiscount(cboRD, Me)
    End Sub
    Private Function loadItems() As DataSet
        Dim ProductItems As DataSet = Nothing
        Try
            ProductItems = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "usp_t_getItemDescription")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        End Try

        Return ProductItems
    End Function
    Private Function bWithPayment() As Boolean
        Dim sSQLCmd As String = "SELECT * FROM tPayment_Invoice "
        sSQLCmd &= "WHERE fxKeyInvoice = '" & sKeyInvoice & "' "


        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.HasRows Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Validation")
            Return False
        End Try
    End Function

    Private Sub updatePayment()
        If lblTotal.Text < lblPayments.Text Then
            Exit Sub
        End If

        Try
            Dim dtPayment As DataSet = loadPayment()
            Using rd As DataTableReader = dtPayment.CreateDataReader
                Dim dAmtDue As Decimal
                Dim sSQLCmd As String
                While rd.Read
                    dAmtDue = rd.Item("fdPayment") + lblBalance.Text
                    sSQLCmd = "UPDATE tPayment_Invoice "
                    sSQLCmd &= "SET fdAmtDue = " & dAmtDue
                    sSQLCmd &= "WHERE fxKeyPaymentItem = '" & rd.Item("fxKeyPaymentItem").ToString & "'"

                    SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)

                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Payment")
        End Try
    End Sub
    Private Sub updateInvoice()
        ClassRefno = txtClassRefno.Text
        Dim sSQLCmd As String = " usp_t_invoice_save "
        sSQLCmd &= "@fxKeyInvoice = '" & sKeyInvoice & "'"
        sSQLCmd &= ",@fcInvoiceNo = '" & txtInvoiceNo.Text & "'"
        sSQLCmd &= ",@fxKeyCustomer = '" & sKeyCustomer & "'"
        sSQLCmd &= ",@fdDateTransact = '" & lstClassList.Value & "'"
        sSQLCmd &= ",@fdDateShip = '" & dteShippingDate.Value & "'"
        sSQLCmd &= ",@fdDateDue = '" & dteDueDate.Value & "'"
        sSQLCmd &= ",@fdSalesPeriodFROM = '" & dteSalesPeriodFrom.Value & "'"
        sSQLCmd &= ",@fdSalesPeriodTo = '" & dteSalesPeriodTo.Value & "'"
        sSQLCmd &= ",@fcBillTo ='" & txtBillingAdd.Text & "'"
        sSQLCmd &= ",@fdDelivryDate='" & dteDeliverydate.Value & "'"
        sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "
        sSQLCmd &= ",@fcShiptoAddress = '" & txtShippingAdd.Text & "'"
        sSQLCmd &= ",@fcPoNo = '" & txtPONo.Text & "'"
        sSQLCmd &= ",@fcFOB = '" & txtFOB.Text & "'"
        sSQLCmd &= ",@fcCustomerMessage = '" & txtCustomerMsg.Text & "'"
        sSQLCmd &= ",@fcMemo = '" & txtMemo.Text & "'"
        sSQLCmd &= ",@fdTaxAmount ='" & CDec(lblTaxAmt.Text) & "' "
        sSQLCmd &= ",@fdTotal ='" & CDec(lblTotal.Text) & "' "
        sSQLCmd &= ",@fcSDCode='" & cboSD.SelectedItem & "' "
        sSQLCmd &= ",@fcRDCode='" & cboRD.SelectedItem & "' "
        sSQLCmd &= ",@fdSDAmt='" & CDec(lblSaleDiscount.Text) & "' "
        sSQLCmd &= ",@fdRDAmt='" & CDec(lblRetDiscount.Text) & "' "
        sSQLCmd &= ",@fdBalance ='" & CDec(lblBalance.Text) & "' "
        sSQLCmd &= ",@fbPrint ='" & IIf(chkPrint.Checked = True, 1, 0) & "' "
        sSQLCmd &= ",@fbEmail ='" & IIf(chkEmail.Checked = True, 1, 0) & "' "
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= ",@fxKeyPayAcnt =" & IIf(gKeyPayAccount = "", "NULL", "'" & gKeyPayAccount & "'") & " "
        sSQLCmd &= ",@fxKeyTaxAcnt =" & IIf(gKeyTaxAccount = "", "NULL", "'" & gKeyTaxAccount & "'") & " "
        sSQLCmd &= ",@fxKeySalesAcnt =" & IIf(gKeySalesAccount = "", "NULL", "'" & gKeySalesAccount & "'") & " "
        sSQLCmd &= ",@fdTaxPercent ='" & gTaxPercent & "' "
        sSQLCmd &= ",@fxKeyOutputTax =" & IIf(gKeyOutputTax = "", "NULL", "'" & gKeyOutputTax & "'") & " "
        sSQLCmd &= ",@fdOutputTax ='" & gOutputTaxPercent & "' "
        sSQLCmd &= ",@fxKeyCOSAcnt=" & IIf(gKeyCOSAccount = "", "NULL", "'" & gKeyCOSAccount & "'") & " "
        sSQLCmd &= ",@fxKeySalesDiscount=" & IIf(gKeyDiscount = "", "NULL", "'" & gKeyDiscount & "'") & " "
        sSQLCmd &= ",@fxKeySDAmount='" & lblSalesDiscountAmount.Text & "' "
        sSQLCmd &= ",@fxKeyMerchAcnt=" & IIf(gKeyMerchAcnt = "", "NULL", "'" & gKeyMerchAcnt & "'") & " "

        If sKeyRep <> "" Then
            sSQLCmd &= ",@fxKeyRep = '" & sKeyRep & "'"
        End If

        sKeyTerms = m_getinvoiceterms(cboTermsName.SelectedItem, Me)

        If sKeyTerms <> "" Then
            sSQLCmd &= ",@fxKeyTerms = '" & sKeyTerms & "'"
        End If

        If sKeySalesTaxCode <> "" Then
            sSQLCmd &= ",@fxKeySalesTaxCode = '" & sKeySalesTaxCode & "'"
        End If

        If sKeyTax <> "" Then
            sSQLCmd &= ",@fxKeyTax = '" & sKeyTax & "'"
        End If
        sSQLCmd &= ", @fxKeyClassRefno = '" & ClassRefno & "'"
        Try
            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
            gInvoiceKey = sKeyInvoice
            If updateInvoiceItem() = True Then
                MessageBox.Show("Record successfully Updated.", "Add/Edit Invoice")
                frm_cust_masterCustomer.loadGrdCustomer2("Invoices")
                If bWithPayment() Then
                    updatePayment()
                End If
            Else
                MessageBox.Show("Cannot update this record!" & vbCrLf & "Please check the Invoice Items.", "Invoice")
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit Invoice")
        End Try

    End Sub
    Private Function updateInvoiceItem() As Boolean
        Dim iRow As Integer
        Dim sSQLCmd As String
        Try
            If Me.grdInvoice.Rows.Count > 0 Then
                For iRow = 0 To (grdInvoice.RowCount - 1)
                    Dim sKeyInvoiceItem As String = ""
                    Dim sItem As String = ""
                    Dim sDescription As String = ""
                    Dim dQuantity As Integer = 0
                    Dim dPrice As Decimal = 0
                    Dim dAmount As Decimal = 0
                    Dim sKeyTaxCode As String = ""
                    If grdInvoice.Rows(iRow).Cells("cDescription").Value IsNot Nothing Then

                        If mkDefaultValues(iRow, cKeyInvoiceItem) <> Nothing Then
                            sKeyInvoiceItem = grdInvoice.Rows(iRow).Cells("cKeyInvoiceItem").Value.ToString
                        Else
                            sKeyInvoiceItem = Guid.NewGuid.ToString
                            grdInvoice.Rows(iRow).Cells("cKeyInvoiceItem").Value = sKeyInvoiceItem
                        End If

                        If mkDefaultValues(iRow, cQuantity) <> Nothing Then
                            dQuantity = grdInvoice.Rows(iRow).Cells("cQuantity").Value
                        Else
                            dQuantity = 0
                        End If

                        If mkDefaultValues(iRow, cItem) <> Nothing Then
                            sItem = grdInvoice.Rows(iRow).Cells("cItem").Value.ToString
                            sItem = m_GetItemByName(sItem)
                        End If

                        If mkDefaultValues(iRow, cDescription) <> Nothing Then
                            sDescription = grdInvoice.Rows(iRow).Cells("cDescription").Value
                        End If

                        If mkDefaultValues(iRow, cPrice) <> Nothing Then
                            dPrice = Format(CDec(grdInvoice.Rows(iRow).Cells("cPrice").Value), "##,##0.0000")
                        Else
                            dPrice = 0
                        End If

                        If mkDefaultValues(iRow, cAmount) <> Nothing Then
                            dAmount = Format(CDec(grdInvoice.Rows(iRow).Cells("cAmount").Value), "##,##0.0000")
                        Else
                            dAmount = 0
                        End If

                        If mkDefaultValues(iRow, cTax) <> Nothing Then
                            sKeyTaxCode = grdInvoice.Rows(iRow).Cells("cTax").Value
                            sKeyTaxCode = m_getSalesTaxCodeByName(sKeyTaxCode)
                        End If

                        sSQLCmd = "usp_t_invoiceItem_update "
                        sSQLCmd &= "@fxKeyInvoiceItem = '" & sKeyInvoiceItem & "'"
                        sSQLCmd &= ",@fxKeyInvoice = '" & sKeyInvoice & "'"
                        sSQLCmd &= ",@fxKeyItem = '" & sItem & "'"
                        sSQLCmd &= ",@fcDescription = '" & sDescription & "'"
                        sSQLCmd &= ",@fdQuantity = '" & dQuantity & "'"
                        sSQLCmd &= ",@fdPrice = '" & dPrice & "'"
                        sSQLCmd &= ",@fdAmount = '" & dAmount & "'"
                        sSQLCmd &= ",@fxKeySalesTaxCode =" & IIf(sKeyTaxCode = "", "NULL", "'" & sKeyTaxCode & "' ") & " "
                        If sItem <> "" Then
                            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
                        End If
                    End If
                Next
                Return True
            End If
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit Invoice Item")
            Return False
        End Try

    End Function
    Private Sub updateBillAddress()
        Dim sSQLCmd As String = "UPDATE mAddress SET fcAddress = '" & txtBillingAdd.Text & "' "
        sSQLCmd &= "WHERE fxKeyID = '" & sKeyCustomer & "' AND "
        sSQLCmd &= "fbCustomer = 1 AND fcAddressName = ''"
        Try
            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Billing Address")
        End Try
    End Sub
    Private Sub updateShipAddress()
        Dim sSQLCmd1 As String
        Dim sSQLCmd2 As String

        sSQLCmd1 = "UPDATE mCustomer00Master "
        sSQLCmd1 &= "SET fxKeyShippingAddress = '" & sKeyShipAdd & "' "
        sSQLCmd1 &= "WHERE fxKeyCustomer = '" & sKeyCustomer & "'"



        sSQLCmd2 = "UPDATE mAddress "
        sSQLCmd2 &= "SET fcAddress = '" & txtShippingAdd.Text & "' "
        sSQLCmd2 &= "WHERE fxKeyAddress = '" & sKeyShipAdd & "'"


        Try
            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd1)
            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd2)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Shipping Address")
        End Try
    End Sub

    Private Function isTaxable(ByVal bCustomer As Boolean, _
    Optional ByVal iRow As Integer = 0) As Boolean
        Dim bTaxable As Boolean
        If bCustomer Then
            If sKeySalesTaxCode <> "" Then
                bTaxable = m_isTaxable(sKeySalesTaxCode)
            Else
                bTaxable = False
            End If
        Else
            Dim sKey As String
            Dim sName As String = ""

            If mkDefaultValues(iRow, cTax) <> Nothing Then
                sName = grdInvoice.Item(cTax, iRow).Value
            End If

            sKey = m_getSalesTaxCodeByName(sName).ToString
            bTaxable = m_isTaxable(sKey)
        End If
        Return bTaxable
    End Function
    Private Sub updateItem(ByVal sKey As String, ByVal nPrice As Long)
        Dim sSQLCmd As String = "UPDATE mItem00Master "
        sSQLCmd &= "SET fdPrice = " & nPrice
        sSQLCmd &= " WHERE fxKeyItem = '" & sKey & "'"
        Try
            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Item Price")
        End Try
    End Sub

    Private Function getTotal() As Decimal
        Dim iRow As Integer
        Dim dTotal As Decimal = 0
        Dim dTotalTx As Decimal = 0
        Dim dTax As Decimal = 0
        Dim dTotalNTx As Decimal = 0
        Dim sKeyItm As String
        Try
            For iRow = 0 To grdInvoice.Rows.Count - 2
                sKeyItm = grdInvoice.Item(cItem, iRow).Value.ToString
                sKeyItm = m_GetItemByName(sKeyItm)
                If sKeyCustomer <> "" Then
                    If isTaxable(1) Then
                        If isTaxable(0, iRow) Then
                            dTotalTx = dTotalTx + grdInvoice.Item("cAmount", iRow).Value
                        Else
                            dTotalNTx = dTotalNTx + grdInvoice.Item("cAmount", iRow).Value
                        End If
                    Else
                        dTotalNTx = dTotalNTx + grdInvoice.Item("cAmount", iRow).Value
                    End If
                Else
                    If isTaxable(0, iRow) Then
                        dTotalTx = dTotalTx + grdInvoice.Item("cAmount", iRow).Value
                    Else
                        dTotalNTx = dTotalNTx + grdInvoice.Item("cAmount", iRow).Value
                    End If
                End If

            Next
            dTax = (dTotalTx * (dPercentage / 100))
            dTotal = dTotalTx + dTotalNTx + dTax

            lblPercentage.Text = Format(dPercentage, "0.00")
            lblTaxAmt.Text = Format(dTax, "##,##0.00")
            lblTotal.Text = Format(CDec(dTotal), "##,##0.00")

            'Call computeretailersdiscount()
            Return dTotal
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error")
        End Try


    End Function
    Private Sub total()
        Try
            With Me.grdInvoice
                Dim xCnt As Integer = 0
                Dim xTotal As Decimal = 0
                For xCnt = 0 To .RowCount - 1
                    If .Item("cDescription", xCnt).Value IsNot Nothing Then
                        If .Item("cAmount", xCnt).Value <> 0 Then
                            xTotal += CDec(.Item("cAmount", xCnt).Value)
                        End If
                    End If
                Next
                lblTotal.Text = Format(CDec(xTotal), "##,##0.00")
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub computesalesdiscount()
        Try
            If lblTotal.Text <> "0.00" Then
                lblSaleDiscount.Text = Format(CDec(lblTotal.Text * (CDec(m_GetSalesDiscountPercentage(cboSD.SelectedItem, Me)) / 100)), "##,##0.00")
            Else
                lblSaleDiscount.Text = "0.00"
            End If
        Catch ex As Exception
            lblSaleDiscount.Text = "0.00"
        End Try

    End Sub
    Private Sub computeretailersdiscount()
        Try
            If lblTotal.Text <> "0.00" Then
                lblRetDiscount.Text = Format(CDec(CDec(lblTotal.Text) - CDec(lblSaleDiscount.Text)) * (CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)) / 100), "##,##0.00")
            Else
                lblRetDiscount.Text = "0.00"
            End If
        Catch ex As Exception
            lblRetDiscount.Text = "0.00"
        End Try
    End Sub
    Private Sub calculatedueamount()
        lblBalance.Text = Format(CDec(CDec(lblTotal.Text) - (CDec(lblSaleDiscount.Text) + CDec(lblRetDiscount.Text) + CDec(lblPayments.Text))), "##,##0.00")
    End Sub

    Private Sub m_deleteItem(ByVal skey As String)
        Try
            Dim sSQLCmd As String = "DELETE FROM tInvoice_item WHERE fxKeyInvoiceItem = '" & skey & "'"
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record Successfully Deleted!", "Delete Invoice Item")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Delete Invoice Item")
        End Try
    End Sub
    Private Sub m_deleteCMItem(ByVal skey As String)
        Dim sSQLCmd As String = "DELETE FROM tCreditMemo_item where fxKeyCreditItem='" & skey & "' "
        SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
    End Sub
    Private Sub cancelinvoice()
        Try
            Dim sSQLCmd As String = " UPDATE tInvoice SET fbCancelled = 1 where fxKeyInvoice='" & sKeyInvoice & "' "
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
        End Try
    End Sub

    Private Sub frm_cust_CreateInvoice_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        gInvoiceKey = ""

        gKeySalesAccount = ""
        gKeyPayAccount = ""
        gKeyTaxAccount = ""
        gKeyOutputTax = ""
        gKeyCOSAccount = ""
        gKeyMerchAcnt = ""
        gKeyDiscount = ""
        gTaxPercent = "0.00"
        gOutputTaxPercent = "0.00"

        frmAccountability.cboSalesAcnt.Text = ""
        frmAccountability.cboPayAcnt.Text = ""
        frmAccountability.cboTaxAcnt.Text = ""
        frmAccountability.cboOutputTax.Text = ""
        frmAccountability.cboCOS.Text = ""
        frmAccountability.cboMerch.Text = ""
        frmAccountability.cboDiscount.Text = ""
        frmAccountability.txtPercent.Text = "0.00"
        frmAccountability.txtPer1.Text = "0.00"
    End Sub

    Private Sub grdInvoice_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdInvoice.EditingControlShowing
        psEdit = 0
        Dim comboBoxColumn As DataGridViewComboBoxColumn = grdInvoice.Columns("cItem")
        If (grdInvoice.CurrentCellAddress.X = comboBoxColumn.DisplayIndex) Then
            Dim cb As ComboBox = e.Control
            If (cb IsNot Nothing) Then
                cb.DropDownStyle = ComboBoxStyle.DropDown
                cb.AutoCompleteMode = AutoCompleteMode.Suggest
                cb.AutoCompleteSource = AutoCompleteSource.ListItems
            End If
        End If

        'Dim textboxColumn As DataGridViewTextBoxColumn = grdInvoice.Columns("cItem")
        'If (grdInvoice.CurrentCellAddress.X = textboxColumn.DisplayIndex) Then
        '    Dim tb As TextBox = e.Control
        '    If (tb IsNot Nothing) Then
        '        tb.AutoCompleteSource = AutoCompleteSource.CustomSource
        '        tb.AutoCompleteMode = AutoCompleteMode.Suggest
        '        tb.AutoCompleteCustomSource.AddRange(myArray)

        '    End If
        'End If

        Dim comboBoxTax As DataGridViewComboBoxColumn = grdInvoice.Columns("cTax")
        If (grdInvoice.CurrentCellAddress.X = comboBoxTax.DisplayIndex) Then
            Dim cbo As ComboBox = e.Control
            If (cbo IsNot Nothing) Then
                cbo.DropDownStyle = ComboBoxStyle.DropDown
                cbo.AutoCompleteMode = AutoCompleteMode.Suggest
                cbo.AutoCompleteSource = AutoCompleteSource.ListItems
            End If
        End If
    End Sub
    Private Sub grdInvoice_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdInvoice.CellValueChanged
        If psEdit = 0 Then
            If e.RowIndex >= 0 Then
                Try

                    Dim sItemTemp As String = grdInvoice.CurrentRow.Cells(cItem).Value.ToString
                    Dim sKeyItemTemp As String = m_GetItemByName(sItemTemp)
                    Dim dtItemTemp As DataTable = m_GetItem(sKeyItemTemp).Tables(0)

                    Dim dQuantity As Long = 0
                    Dim dAmount As Decimal = 0
                    Dim dPrice As Decimal = 0

                    If mkDefaultValues(grdInvoice.CurrentRow.Index, cQuantity) <> Nothing Then
                        dQuantity = grdInvoice.CurrentRow.Cells(cQuantity).Value
                    End If

                    If mkDefaultValues(grdInvoice.CurrentRow.Index, cPrice) <> Nothing Then
                        dPrice = grdInvoice.CurrentRow.Cells(cPrice).Value
                    End If


                    If e.ColumnIndex = cPrice Then
                        If sItemTemp <> "" Then
                            Try
                                If dPrice <> dtItemTemp.Rows(0)("fdPrice") Then
                                    Dim sWarn As String
                                    Dim msgWarn As DialogResult
                                    sWarn = "You have changed the price for: "
                                    sWarn &= sItemTemp & vbCrLf & "Do you want to update the item with the new cost?"
                                    msgWarn = MessageBox.Show(sWarn, "Item's Price Changed", MessageBoxButtons.YesNo)
                                    If msgWarn = Windows.Forms.DialogResult.Yes Then
                                        updateItem(sKeyItemTemp, dPrice)
                                    End If
                                End If
                                dAmount = dPrice * dQuantity
                                grdInvoice.CurrentRow.Cells(cAmount).Value = Format(CDec(dAmount), "##,##0.00")
                            Catch ex As Exception
                            End Try
                        End If
                    End If

                    If e.ColumnIndex = cItem Then
                        If sItemTemp <> "" Then
                            Try
                                grdInvoice.CurrentRow.Cells("cDescription").Value = dtItemTemp.Rows(0)("fcItemDescription")
                                grdInvoice.CurrentRow.Cells("cUnit").Value = dtItemTemp.Rows(0)("fcUnitAbbreviation")
                                grdInvoice.CurrentRow.Cells("cPrice").Value = Format(CDec(dtItemTemp.Rows(0)("fdPrice")), "##,##0.00##")
                                grdInvoice.CurrentRow.Cells("cTax").Value = dtItemTemp.Rows(0)("fcSalesTaxCodeName")
                            Catch
                                MessageBox.Show(Err.Description, "Invoice Item")
                            End Try
                        End If
                    End If

                    If e.ColumnIndex = cQuantity Then
                        Try
                            dAmount = dQuantity * dPrice
                            'grdInvoice.CurrentRow.Cells(cAmount).Value = dAmount.ToString
                            grdInvoice.CurrentRow.Cells(cAmount).Value = Format(CDec(dAmount), "##,##0.00##")
                        Catch
                            MessageBox.Show(Err.Description, "Data Validation")

                        End Try

                    End If

                    'If mkDefaultValues(e.RowIndex, cAmount) <> Nothing Then
                    total() ' getTotal()
                    computetotalwithtax()
                    'End If

                Catch ex As Exception
                    '      MsgBox(Err.Description, MsgBoxStyle.Information)
                End Try
            End If
        End If
    End Sub
    Private Sub grdInvoice_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdInvoice.CellContentClick
        psEdit = 0
    End Sub
    Private Sub grdInvoice_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdInvoice.CellClick
        psEdit = 0
    End Sub
    Private Sub grdInvoice_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles grdInvoice.Validating
        getTotal()
    End Sub
    Private Sub grdInvoice_UserDeletedRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles grdInvoice.UserDeletedRow
        getTotal()
    End Sub
    Private Sub grdInvoice_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles grdInvoice.UserDeletingRow
        If grdInvoice.Rows.Count > 0 Then
            If MessageBox.Show("Are you sure you want to delete this record?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                If Me.Text <> "Create Invoice" Then
                    ReDim Preserve sKeyToDel(iDel)
                    sKeyToDel(iDel) = e.Row.Cells(cKeyInvoiceItem).Value.ToString
                    m_deleteItem(sKeyToDel(iDel))
                    iDel += 1
                End If
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub dteInvoiceDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstClassList.ValueChanged
        IsDateRestricted()
    End Sub
    Private Sub IsDateRestricted()
        If checkDate.checkIfRestricted(lstClassList.Value) Then
            btnSave.Enabled = False 'true if date is NOT within range of period restricted
            btnAccountability.Enabled = False
        Else
            btnSave.Enabled = True 'false if date is within range of period restricted
            btnAccountability.Enabled = True
        End If
    End Sub

    Private Sub txtSDAmt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSDAmt.Validating
        txtSDAmt.Text = Format(CDec(txtSDAmt.Text), "##,##0.00")
        lblSaleDiscount.Text = txtSDAmt.Text
        Call calculatedueamount()
    End Sub
    Private Sub txtRDAmt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtRDAmt.Validating
        txtRDAmt.Text = Format(CDec(txtRDAmt.Text), "##,##0.00")
        lblRetDiscount.Text = txtRDAmt.Text
        Call calculatedueamount()
    End Sub

    Private Sub lblSaleDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblSaleDiscount.TextChanged
        If psEdit = 0 Then
            Try
                Call calculatedueamount()
            Catch ex As Exception
            End Try
        End If
    End Sub
    Private Sub lblRetDiscount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblRetDiscount.TextChanged
        If psEdit = 0 Then
            Try
                Call calculatedueamount()
            Catch ex As Exception
            End Try
        End If
    End Sub
    Private Sub lblTotal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblTotal.TextChanged
        If psEdit = 0 Then
            Try
                If IsNumeric(lblBalance.Text) And IsNumeric(lblTotal.Text) And IsNumeric(lblPayments.Text) Then
                    lblBalance.Text = Format(CDec(((CDec(lblTotal.Text) - CDec(lblSaleDiscount.Text)) - CDec(lblRetDiscount.Text)) - CDec(lblPayments.Text)), "##,##0.00")
                    'lblBalance.Text = FormatNumber(lblBalance.Text, 2, , , TriState.False)
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub cboViaName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboViaName.SelectedIndexChanged
        cboViaID.SelectedIndex = cboViaName.SelectedIndex

        If cboViaName.SelectedIndex = 1 Then
            frm_MF_shippingmethod.Text = "New Shipping Method"
            frm_MF_shippingmethod.KeyShippingMethod = Guid.NewGuid.ToString
            frm_MF_shippingmethod.ShowDialog()
        End If
    End Sub

    Private Sub cboSD_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSD.SelectedIndexChanged
        If cboSD.SelectedIndex = 1 Then
            frmSalesDiscount.ShowDialog()
            Call m_GetSalesDiscount(cboSD, Me)
        ElseIf cboSD.SelectedIndex > 2 Then
            lblSDpercent.Text = CStr(Format(CDec(m_GetSalesDiscountPercentage(cboSD.SelectedItem, Me)), "##0.0#")) + "%"
            Call computesalesdiscount()
        ElseIf cboSD.SelectedIndex = 0 Then
            lblSaleDiscount.Text = "0.00"
            lblSDpercent.Text = "(0.0%)"
            Call calculatedueamount()
        End If
        computeretailersdiscount()
        lblSalesDiscountAmount.Text = ComputeSalesDiscountAmountForGL(CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")), Format((CDec(dPercentage)), "0.00"), lblSaleDiscount.Text)

    End Sub
    Private Sub cboRD_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRD.SelectedIndexChanged
        If cboRD.SelectedIndex = 1 Then
            frmRetailersDiscount.ShowDialog()
            Call m_GetRetailerDiscount(cboRD, Me)
        ElseIf cboRD.SelectedIndex > 2 Then
            lblRDPercent.Text = CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")) + "%"
            Call computeretailersdiscount()
        ElseIf cboRD.SelectedIndex = 0 Then
            lblRetDiscount.Text = "0.00"
            lblRDPercent.Text = "(0.0%)"
            Call calculatedueamount()
        End If
        lblSalesDiscountAmount.Text = ComputeSalesDiscountAmountForGL(CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")), Format((CDec(dPercentage)), "0.00"), lblSaleDiscount.Text)
    End Sub
    Private Sub cboTaxName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTaxName.SelectedIndexChanged
        cboTaxID.SelectedIndex = cboTaxName.SelectedIndex
        sKeyTax = cboTaxID.SelectedItem.ToString

        If sKeyTax <> "" Then
            Dim dtTax As DataSet = m_GetTax(sKeyTax)
            dPercentage = dtTax.Tables(0).Rows(0)("fdTaxRate")
        Else
            dPercentage = 0
        End If
        lblPercentage.Text = Format((CDec(dPercentage)), "0.00")

        Try
            With Me.grdInvoice
                Dim xCnt As Integer = 0
                Dim xTotal As Decimal = 0
                For xCnt = 0 To .RowCount - 1
                    If .Item("cDescription", xCnt).Value IsNot Nothing Then
                        xTotal += CDec(.Item("cAmount", xCnt).Value)
                    End If
                Next
                lblTotal.Text = Format(CDec(xTotal), "##,##0.00")
                lblTaxAmt.Text = Format(CDec(xTotal * (CDec(lblPercentage.Text) / 100)), "##,##0.00")
                lblTotal.Text = Format(CDec(xTotal + lblTaxAmt.Text), "##,##0.00")
                computesalesdiscount()
                computeretailersdiscount()
            End With

        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information)
        End Try

        If cboTaxName.SelectedIndex = 1 Then
            frm_MF_taxAddEdit.Text = "New Sales Tax"
            frm_MF_taxAddEdit.KeyTax = Guid.NewGuid.ToString
            frm_MF_taxAddEdit.Show()
        End If
        lblSalesDiscountAmount.Text = ComputeSalesDiscountAmountForGL(CStr(Format(CDec(m_GetRetailersDiscountPercentage(cboRD.SelectedItem, Me)), "##0.0#")), Format((CDec(dPercentage)), "0.00"), lblSaleDiscount.Text)
    End Sub
    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        sKeyCustomer = cboCustomerID.SelectedItem

        If cboCustomerName.SelectedIndex = 1 Then
            frm_cust_masterCustomerAddEdit.MdiParent = Me.MdiParent
            frm_cust_masterCustomerAddEdit.KeyCustomer = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.KeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.IsNew = True
            frm_cust_masterCustomerAddEdit.Text = "Add Customer"
            frm_cust_masterCustomerAddEdit.Show()
        End If

        If sKeyCustomer <> "" Then
            m_loadShippingAddress(cboShipToAddName, cboShipToAddID, sKeyCustomer, "Customer")

            Dim dtAddress As DataSet = m_getAddress(sKeyCustomer, "Customer")

            Try
                Using rdAddress As DataTableReader = dtAddress.CreateDataReader
                    If rdAddress.Read Then
                        txtShippingAdd.Text = rdAddress.Item("fcAddress")
                        cboShipToAddID.SelectedItem = rdAddress.Item("fxKeyAddress").ToString
                        cboShipToAddName.SelectedIndex = cboShipToAddID.SelectedIndex
                    Else
                        txtShippingAdd.Text = ""
                    End If
                End Using
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            Try
                Dim dtCustomer As DataTable = m_GetCustomer(sKeyCustomer).Tables(0)
                txtBillingAdd.Text = dtCustomer.Rows(0)("fcBillingAddress").ToString
                cboTaxCodeID.SelectedItem = dtCustomer.Rows(0)("fxKeySalesTaxCode").ToString
                cboTaxCodeName.SelectedIndex = cboTaxCodeID.SelectedIndex
            Catch ex As Exception
            End Try


            'cboTermsID.SelectedItem = dtCustomer.Rows(0)("fxKeyTerms").ToString
            'cboTermsName.SelectedIndex = cboTermsID.SelectedIndex

        End If

    End Sub
    Private Sub cboRepName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRepName.SelectedIndexChanged
        cboRepID.SelectedIndex = cboRepName.SelectedIndex
        sKeyRep = cboRepID.SelectedItem
        If cboRepName.SelectedIndex = 1 Then
            frm_MF_salesRepAddEdit.KeySalesRep = Guid.NewGuid.ToString
            frm_MF_salesRepAddEdit.Text = "New Sales Rep"
            frm_MF_salesRepAddEdit.Show()
        End If
    End Sub
    Private Sub cboShipToAddName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboShipToAddName.SelectedIndexChanged
        cboShipToAddID.SelectedIndex = cboShipToAddName.SelectedIndex
        sKeyShipAdd = cboShipToAddID.SelectedItem

        If cboShipToAddName.SelectedIndex = 1 Then
            frm_cust_masterCustomerAddress.KeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddress.KeyCustomer = sKeyCustomer
            frm_cust_masterCustomerAddress.IsShippingAdd = True
            frm_cust_masterCustomerAddress.ShowDialog()
        End If

        If cboShipToAddName.SelectedIndex <> 1 And cboShipToAddName.SelectedIndex <> 0 Then
            If sKeyShipAdd <> "" Then
                Dim dtAddress As DataTable = m_getAddressByID(sKeyShipAdd).Tables(0)
                txtShippingAdd.Text = dtAddress.Rows(0)("fcAddress").ToString
            End If
        Else
            txtShippingAdd.Text = sCoAddress
        End If

    End Sub
    Private Sub cboTaxCodeName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxCodeName.SelectedIndexChanged
        cboTaxCodeID.SelectedIndex = cboTaxCodeName.SelectedIndex
        sKeySalesTaxCode = cboTaxCodeID.SelectedItem

        'getTotal()

        If cboTaxCodeName.SelectedIndex = 1 Then
            frm_vend_SalesTaxAdd.Text = "New Sales Tax Code"
            frm_vend_SalesTaxAdd.KeySalesTaxCode = Guid.NewGuid.ToString
            frm_vend_SalesTaxAdd.Show()
        End If
    End Sub
    Private Sub cboTermsName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTermsName.SelectedIndexChanged
        cboTermsID.SelectedIndex = cboTermsName.SelectedIndex
        sKeyTerms = cboTermsID.SelectedItem

        If cboTermsName.SelectedIndex = 1 Then
            frm_MF_termsAddEdit.Keyterms = Guid.NewGuid.ToString
            frm_MF_termsAddEdit.MdiParent = Me.MdiParent
            frm_MF_termsAddEdit.Text = "Add Terms"
            frm_MF_termsAddEdit.Show()
        End If

        If sKeyTerms <> "" Then
            Dim nNetDueDays As Integer
            Dim dDiscountPercent As Long
            Dim nDiscountDays As Integer

            Try
                Using dtTerms As DataTable = m_getTerms(sKeyTerms).Tables(0)
                    Using rd As DataTableReader = dtTerms.CreateDataReader
                        If rd.Read Then
                            If rd.Item("fbStandard") Then
                                nNetDueDays = CDbl(rd.Item("fnDayNetDue"))
                                dDiscountPercent = rd.Item("fdDiscountPercentage")
                                nDiscountDays = rd.Item("fnDayDiscount")

                                dteDueDate.Value = dteDeliverydate.Value.AddDays(nNetDueDays)
                            End If
                        End If
                    End Using
                End Using
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        Else
            dteDueDate.Value = Now
        End If
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        createColumn()
        iDel = 0
        sKeyToDel(iDel) = ""
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If validateAccountability("Invoice") = "Kulang" Then
            frmAccountability.ShowDialog()
        Else
            lblSubTotal.Text = "0.00"
            gInvoiceKey = ""
            Me.Dispose()
            Me.Close()
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If validateAccountability("Invoice") = "Kulang" Then
            frmAccountability.ShowDialog()
            Exit Sub
        End If

        If frmMain.ClassToolStripMenuItem.Visible = True Then
            If txtClassName.Text <> "" Then
                ClassRefno = txtClassRefno.Text
            Else
                MsgBox("The selected class doesn't exist in the database!" + vbCr + "Please, create it first.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
                Exit Sub
            End If
        End If
        If cboCustomerID.SelectedItem = "" Then
            MessageBox.Show("Customer field cannot be empty!", "Data Validation")
        ElseIf grdInvoice.Rows.Count <= 1 Then
            MessageBox.Show("Item field cannot be empty!", "Data Validation")
        Else
            If bWithPayment() = True Then
                Dim x As DialogResult
                Dim sWarn As String = "There are payments applied to this invoice. " & vbCrLf & _
                                "Changing the amount will cause the payments " & vbCrLf & _
                                "to be applied differently. " & vbCrLf & vbCrLf & _
                                "Do you want to change it anyway?"
                x = MessageBox.Show(sWarn, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If x = Windows.Forms.DialogResult.Yes Then
                    updateInvoice()
                End If
            Else
                updateInvoice()
            End If
        End If
    End Sub
    Private Sub cboSD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSD.Click
        psEdit = 0
    End Sub
    Private Sub cboRD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRD.Click
        psEdit = 0
    End Sub
    Private Sub cboTaxName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTaxName.Click
        psEdit = 0
    End Sub
    Private Sub ts_Preview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Preview.Click
        Print_Invoice_Selection.ShowDialog()
    End Sub
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        If gInvoiceKey = "" Then
            MessageBox.Show("Please save first before previewing.", "Invoice", MessageBoxButtons.OK)
        Else
            Print_Invoice_Selection.ShowDialog()
        End If
    End Sub
    Private Sub btnDelRows_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelRows.Click
        Dim xCnt As Integer
        With grdInvoice
            Try
                For xCnt = 0 To .RowCount - 1
                    If .Rows(xCnt).Cells("cSelect").Value = True Then
                        If .Rows(xCnt).Cells("cDescription").Value IsNot Nothing Then
                            If gInvoiceKey <> "" Then
                                Call m_deleteCMItem(.Rows(xCnt).Cells(0).Value.ToString)
                            End If
                            .Rows.Remove(.Rows(xCnt))
                        End If
                    End If
                Next
            Catch ex As Exception
            End Try
        End With
    End Sub
    Private Function getAccount_Description(ByVal AccntID As String)
        Dim sSqlcmd As String = "SELECT acnt_desc FROM dbo.mAccounts WHERE acnt_id ='" & AccntID & "' AND co_id ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlcmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
        End Try
    End Function
    Private Sub btnAccountability_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccountability.Click

        If gKeySalesAccount <> "" Or gKeySalesAccount <> Nothing Then
            frmAccountability.cboSalesAcnt.SelectedItem = getAccount_Description(gKeySalesAccount)
        End If
        If gKeyPayAccount <> "" Or gKeyPayAccount <> Nothing Then
            frmAccountability.cboPayAcnt.SelectedItem = getAccount_Description(gKeyPayAccount)
        End If
        If gKeyTaxAccount <> "" Or gKeyTaxAccount <> Nothing Then
            frmAccountability.cboTaxAcnt.SelectedItem = getAccount_Description(gKeyTaxAccount)
        End If
        If gKeyOutputTax <> "" Or gKeyOutputTax <> Nothing Then
            frmAccountability.cboOutputTax.SelectedItem = getAccount_Description(gKeyOutputTax)
        End If
        If gKeyCOSAccount <> "" Or gKeyCOSAccount <> Nothing Then
            frmAccountability.cboCOS.SelectedItem = getAccount_Description(gKeyCOSAccount)
        End If
        If gKeyMerchAcnt <> "" Or gKeyMerchAcnt <> Nothing Then
            frmAccountability.cboMerch.SelectedItem = getAccount_Description(gKeyMerchAcnt)
        End If
        If gKeyDiscount <> "" Or gKeyDiscount <> Nothing Then
            frmAccountability.cboDiscount.SelectedItem = getAccount_Description(gKeyDiscount)
        End If


        frmAccountability.txtPercent.Text = gTaxPercent
        frmAccountability.txtPer1.Text = gOutputTaxPercent

        frmAccountability.ShowDialog()

    End Sub
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If chkCancel.Checked = True Then
            If sKeyInvoice <> "" Then
                If MsgBox("Are you sure to cancel this invoice?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    Call cancelinvoice()
                    frm_cust_masterCustomer.loadGrdCustomer2("Invoices")
                    Me.Close()
                Else
                    chkCancel.Checked = False
                End If
            Else
                chkCancel.Checked = False
            End If
        End If
    End Sub
    Private Sub btnSubTotal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubTotal.Click
        Dim sTotal As Decimal = 0.0
        Dim xCnt As Integer
        With grdInvoice
            Try
                For xCnt = 0 To .RowCount - 1
                    If .Rows(xCnt).Cells("cSelect").Value = True Then
                        If .Rows(xCnt).Cells("cDescription").Value IsNot Nothing Then
                            sTotal += .Rows(xCnt).Cells("cAmount").Value
                        End If
                    End If
                Next
                lblSubTotal.Text = Format(CDec(sTotal), "##,##0.00")
            Catch ex As Exception
            End Try
        End With
    End Sub

#End Region
#Region "Disabled Functions/Procedures"
    Private Sub grdInvoice_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdInvoice.CellPainting
        '    Dim sf As New StringFormat
        '    sf.Alignment = StringAlignment.Center
        '    If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdInvoice.Rows.Count Then
        '        e.PaintBackground(e.ClipBounds, True)
        '        e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
        '        e.Handled = True
        '    End If
    End Sub
    Private Sub grdInvoice_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles grdInvoice.MouseClick
        '    With grdInvoice
        '        If 16777216 = Windows.Forms.MouseButtons.Right Then
        '            Dim xlistview As New ListBox
        '            Me.Controls.Add(xlistview)
        '            xlistview.Items.Add("delete item")
        '            xlistview.Name = "xlistview"
        '        End If
        '    End With

    End Sub
    Private Sub grdInvoice_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles grdInvoice.CellValidating
        '    psEdit = 0
        '    Call total()
    End Sub
    Private Sub lblTaxAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblTaxAmt.TextChanged
        '    Try
        '        'If psEdit = 0 Then
        '        total() ' getTotal()
        '        computetotalwithtax()
        '        ' End If
        '    Catch ex As Exception
        '    End Try
    End Sub
    Private Sub computetotalwithtax()
        'cboTaxID.SelectedIndex = cboTaxName.SelectedIndex
        'sKeyTax = cboTaxID.SelectedItem.ToString

        'If sKeyTax <> "" Then
        '    Dim dtTax As DataSet = m_GetTax(sKeyTax)
        '    dPercentage = dtTax.Tables(0).Rows(0)("fdTaxRate")
        'Else
        '    dPercentage = 0
        'End If
        'lblPercentage.Text = Format(CDec(dPercentage), "0.00")

        'Try
        '    With Me.grdInvoice
        '        Dim xCnt As Integer = 0
        '        Dim xTotal As Decimal = 0
        '        For xCnt = 0 To .RowCount - 1
        '            If .Item("cDescription", xCnt).Value IsNot Nothing Then
        '                xTotal += CDec(.Item("cAmount", xCnt).Value)
        '            End If
        '        Next
        '        lblTaxAmt.Text = "0.00"
        '        lblTotal.Text = "0.00"
        '        lblTotal.Text = Format(CDec(xTotal), "##,##0.00")
        '        lblTaxAmt.Text = Format(CDec(xTotal * (CDec(lblPercentage.Text) / 100)), "##,##0.00")
        '        lblTotal.Text = Format(CDec(CDec(lblTotal.Text) + CDec(lblTaxAmt.Text)), "##,##0.00")
        '    End With
        'Catch ex As Exception
        '    MsgBox(Err.Description, MsgBoxStyle.Information)
        'End Try

        'If cboTaxName.SelectedIndex = 1 Then
        '    frm_MF_taxAddEdit.Text = "New Sales Tax"
        '    frm_MF_taxAddEdit.KeyTax = Guid.NewGuid.ToString
        '    frm_MF_taxAddEdit.Show()
        'End If
    End Sub
#End Region




    Private Sub BtnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowse.Click
        FrmBrowseClass.Mode = "BROWSE MODE"
        FrmBrowseClass.ShowDialog()
        txtClassRefno.Text = FrmBrowseClass.Data2
        txtClassName.Text = FrmBrowseClass.Data1
    End Sub

    Private Sub lblSaleDiscount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblSaleDiscount.Click

    End Sub
End Class