<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_creditOption
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cust_creditOption))
        Me.lblMessage = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.rBtnRetain = New System.Windows.Forms.RadioButton
        Me.rBtnRefund = New System.Windows.Forms.RadioButton
        Me.rBtnInvoice = New System.Windows.Forms.RadioButton
        Me.btnOk = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lblMessage
        '
        Me.lblMessage.Location = New System.Drawing.Point(22, 21)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(247, 57)
        Me.lblMessage.TabIndex = 0
        Me.lblMessage.Text = "This credit memo or refund  has a remaing balance  which you may use."
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(22, 78)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(247, 25)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "What would you like to do with this credit?"
        '
        'rBtnRetain
        '
        Me.rBtnRetain.AutoSize = True
        Me.rBtnRetain.Location = New System.Drawing.Point(81, 106)
        Me.rBtnRetain.Name = "rBtnRetain"
        Me.rBtnRetain.Size = New System.Drawing.Size(159, 17)
        Me.rBtnRetain.TabIndex = 2
        Me.rBtnRetain.TabStop = True
        Me.rBtnRetain.Text = "Retain as an available credit"
        Me.rBtnRetain.UseVisualStyleBackColor = True
        '
        'rBtnRefund
        '
        Me.rBtnRefund.AutoSize = True
        Me.rBtnRefund.Location = New System.Drawing.Point(81, 137)
        Me.rBtnRefund.Name = "rBtnRefund"
        Me.rBtnRefund.Size = New System.Drawing.Size(89, 17)
        Me.rBtnRefund.TabIndex = 3
        Me.rBtnRefund.TabStop = True
        Me.rBtnRefund.Text = "Give a refund"
        Me.rBtnRefund.UseVisualStyleBackColor = True
        '
        'rBtnInvoice
        '
        Me.rBtnInvoice.AutoSize = True
        Me.rBtnInvoice.Location = New System.Drawing.Point(81, 168)
        Me.rBtnInvoice.Name = "rBtnInvoice"
        Me.rBtnInvoice.Size = New System.Drawing.Size(115, 17)
        Me.rBtnInvoice.TabIndex = 4
        Me.rBtnInvoice.TabStop = True
        Me.rBtnInvoice.Text = "Apply to an invoice"
        Me.rBtnInvoice.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOk.Location = New System.Drawing.Point(96, 208)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(74, 34)
        Me.btnOk.TabIndex = 5
        Me.btnOk.Text = "Ok"
        Me.btnOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'frm_cust_creditOption
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.rBtnInvoice)
        Me.Controls.Add(Me.rBtnRefund)
        Me.Controls.Add(Me.rBtnRetain)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblMessage)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(300, 300)
        Me.MinimumSize = New System.Drawing.Size(300, 300)
        Me.Name = "frm_cust_creditOption"
        Me.Text = "Available Credit"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rBtnRetain As System.Windows.Forms.RadioButton
    Friend WithEvents rBtnRefund As System.Windows.Forms.RadioButton
    Friend WithEvents rBtnInvoice As System.Windows.Forms.RadioButton
    Friend WithEvents btnOk As System.Windows.Forms.Button
End Class
