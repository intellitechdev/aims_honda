﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMasterfile_AccountRegister
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMasterfile_AccountRegister))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtsearch = New System.Windows.Forms.TextBox()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.txtAccountNumber = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnBrowseEmployee = New System.Windows.Forms.Button()
        Me.txtSchedule = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.btnBrowseAccount = New System.Windows.Forms.Button()
        Me.txtaccountname = New System.Windows.Forms.TextBox()
        Me.txtaccountcode = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgvListAccountRegister = New System.Windows.Forms.DataGridView()
        Me.AccountNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Account = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Schedule = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fcClosed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnBrowseAccountNumber = New System.Windows.Forms.Button()
        Me.CBClosedList = New System.Windows.Forms.CheckBox()
        Me.CBclose = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtAccountFilter = New System.Windows.Forms.TextBox()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvListAccountRegister, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtsearch
        '
        Me.txtsearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtsearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtsearch.Location = New System.Drawing.Point(56, 439)
        Me.txtsearch.Name = "txtsearch"
        Me.txtsearch.Size = New System.Drawing.Size(224, 20)
        Me.txtsearch.TabIndex = 139
        '
        'btnclose
        '
        Me.btnclose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnclose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnclose.Image = CType(resources.GetObject("btnclose.Image"), System.Drawing.Image)
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(769, 435)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(66, 28)
        Me.btnclose.TabIndex = 132
        Me.btnclose.Text = "Close"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btndelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete.Location = New System.Drawing.Point(697, 435)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(66, 28)
        Me.btndelete.TabIndex = 131
        Me.btndelete.Text = "Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnupdate
        '
        Me.btnupdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnupdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnupdate.Image = CType(resources.GetObject("btnupdate.Image"), System.Drawing.Image)
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(625, 435)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(66, 28)
        Me.btnupdate.TabIndex = 130
        Me.btnupdate.Text = "Edit"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnsave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsave.Location = New System.Drawing.Point(553, 435)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(66, 28)
        Me.btnsave.TabIndex = 129
        Me.btnsave.Text = "New"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'txtAccountNumber
        '
        Me.txtAccountNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtAccountNumber.Location = New System.Drawing.Point(145, 10)
        Me.txtAccountNumber.Name = "txtAccountNumber"
        Me.txtAccountNumber.ReadOnly = True
        Me.txtAccountNumber.Size = New System.Drawing.Size(348, 20)
        Me.txtAccountNumber.TabIndex = 141
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label5.Location = New System.Drawing.Point(12, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 13)
        Me.Label5.TabIndex = 140
        Me.Label5.Text = "Account Number :"
        '
        'btnBrowseEmployee
        '
        Me.btnBrowseEmployee.Enabled = False
        Me.btnBrowseEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnBrowseEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBrowseEmployee.Location = New System.Drawing.Point(499, 85)
        Me.btnBrowseEmployee.Name = "btnBrowseEmployee"
        Me.btnBrowseEmployee.Size = New System.Drawing.Size(25, 25)
        Me.btnBrowseEmployee.TabIndex = 139
        Me.btnBrowseEmployee.Text = "..."
        Me.btnBrowseEmployee.UseVisualStyleBackColor = True
        '
        'txtSchedule
        '
        Me.txtSchedule.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtSchedule.Location = New System.Drawing.Point(145, 114)
        Me.txtSchedule.MaxLength = 20
        Me.txtSchedule.Name = "txtSchedule"
        Me.txtSchedule.Size = New System.Drawing.Size(348, 20)
        Me.txtSchedule.TabIndex = 138
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label4.Location = New System.Drawing.Point(12, 117)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 137
        Me.Label4.Text = "Schedule:"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtName.Location = New System.Drawing.Point(145, 88)
        Me.txtName.Name = "txtName"
        Me.txtName.ReadOnly = True
        Me.txtName.Size = New System.Drawing.Size(348, 20)
        Me.txtName.TabIndex = 136
        '
        'btnBrowseAccount
        '
        Me.btnBrowseAccount.Enabled = False
        Me.btnBrowseAccount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnBrowseAccount.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBrowseAccount.Location = New System.Drawing.Point(499, 59)
        Me.btnBrowseAccount.Name = "btnBrowseAccount"
        Me.btnBrowseAccount.Size = New System.Drawing.Size(25, 25)
        Me.btnBrowseAccount.TabIndex = 135
        Me.btnBrowseAccount.Text = "..."
        Me.btnBrowseAccount.UseVisualStyleBackColor = True
        '
        'txtaccountname
        '
        Me.txtaccountname.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtaccountname.Location = New System.Drawing.Point(145, 62)
        Me.txtaccountname.Name = "txtaccountname"
        Me.txtaccountname.ReadOnly = True
        Me.txtaccountname.Size = New System.Drawing.Size(348, 20)
        Me.txtaccountname.TabIndex = 134
        '
        'txtaccountcode
        '
        Me.txtaccountcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtaccountcode.Location = New System.Drawing.Point(145, 36)
        Me.txtaccountcode.Name = "txtaccountcode"
        Me.txtaccountcode.ReadOnly = True
        Me.txtaccountcode.Size = New System.Drawing.Size(348, 20)
        Me.txtaccountcode.TabIndex = 133
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label3.Location = New System.Drawing.Point(12, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 132
        Me.Label3.Text = "Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label2.Location = New System.Drawing.Point(12, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 131
        Me.Label2.Text = "Account Title :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label6.Location = New System.Drawing.Point(12, 39)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(81, 13)
        Me.Label6.TabIndex = 130
        Me.Label6.Text = "Account Code :"
        '
        'dgvListAccountRegister
        '
        Me.dgvListAccountRegister.AllowUserToAddRows = False
        Me.dgvListAccountRegister.AllowUserToDeleteRows = False
        Me.dgvListAccountRegister.AllowUserToResizeColumns = False
        Me.dgvListAccountRegister.AllowUserToResizeRows = False
        Me.dgvListAccountRegister.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvListAccountRegister.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListAccountRegister.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvListAccountRegister.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvListAccountRegister.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AccountNumber, Me.fcID, Me.fcName, Me.Account, Me.Schedule, Me.fcClosed})
        Me.dgvListAccountRegister.Location = New System.Drawing.Point(7, 170)
        Me.dgvListAccountRegister.Name = "dgvListAccountRegister"
        Me.dgvListAccountRegister.RowHeadersVisible = False
        Me.dgvListAccountRegister.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvListAccountRegister.Size = New System.Drawing.Size(830, 259)
        Me.dgvListAccountRegister.TabIndex = 129
        '
        'AccountNumber
        '
        Me.AccountNumber.FillWeight = 68.28489!
        Me.AccountNumber.HeaderText = "Account Number"
        Me.AccountNumber.Name = "AccountNumber"
        Me.AccountNumber.ReadOnly = True
        Me.AccountNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AccountNumber.Width = 123
        '
        'fcID
        '
        Me.fcID.FillWeight = 68.28489!
        Me.fcID.HeaderText = "ID"
        Me.fcID.Name = "fcID"
        Me.fcID.ReadOnly = True
        Me.fcID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.fcID.Width = 124
        '
        'fcName
        '
        Me.fcName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.fcName.FillWeight = 68.28489!
        Me.fcName.HeaderText = "Name"
        Me.fcName.Name = "fcName"
        Me.fcName.ReadOnly = True
        Me.fcName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Account
        '
        Me.Account.FillWeight = 68.28489!
        Me.Account.HeaderText = "Account"
        Me.Account.Name = "Account"
        Me.Account.ReadOnly = True
        Me.Account.Width = 124
        '
        'Schedule
        '
        Me.Schedule.FillWeight = 68.28489!
        Me.Schedule.HeaderText = "Schedule"
        Me.Schedule.Name = "Schedule"
        Me.Schedule.ReadOnly = True
        Me.Schedule.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Schedule.Width = 123
        '
        'fcClosed
        '
        Me.fcClosed.FillWeight = 116.1308!
        Me.fcClosed.HeaderText = "Closed"
        Me.fcClosed.Name = "fcClosed"
        Me.fcClosed.Width = 50
        '
        'btnBrowseAccountNumber
        '
        Me.btnBrowseAccountNumber.Enabled = False
        Me.btnBrowseAccountNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.btnBrowseAccountNumber.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBrowseAccountNumber.Location = New System.Drawing.Point(499, 7)
        Me.btnBrowseAccountNumber.Name = "btnBrowseAccountNumber"
        Me.btnBrowseAccountNumber.Size = New System.Drawing.Size(25, 25)
        Me.btnBrowseAccountNumber.TabIndex = 142
        Me.btnBrowseAccountNumber.Text = "..."
        Me.btnBrowseAccountNumber.UseVisualStyleBackColor = True
        '
        'CBClosedList
        '
        Me.CBClosedList.AutoSize = True
        Me.CBClosedList.BackColor = System.Drawing.Color.Transparent
        Me.CBClosedList.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.CBClosedList.Location = New System.Drawing.Point(775, 12)
        Me.CBClosedList.Name = "CBClosedList"
        Me.CBClosedList.Size = New System.Drawing.Size(58, 17)
        Me.CBClosedList.TabIndex = 143
        Me.CBClosedList.Text = "Closed"
        Me.CBClosedList.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBClosedList.UseVisualStyleBackColor = False
        '
        'CBclose
        '
        Me.CBclose.BackColor = System.Drawing.Color.Transparent
        Me.CBclose.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBclose.Enabled = False
        Me.CBclose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.CBclose.Location = New System.Drawing.Point(12, 140)
        Me.CBclose.Name = "CBclose"
        Me.CBclose.Size = New System.Drawing.Size(148, 24)
        Me.CBclose.TabIndex = 144
        Me.CBclose.Text = "Closed   "
        Me.CBclose.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label1.Location = New System.Drawing.Point(6, 443)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 145
        Me.Label1.Text = "Filter:"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Label7.Location = New System.Drawing.Point(298, 443)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 13)
        Me.Label7.TabIndex = 147
        Me.Label7.Text = "Account:"
        '
        'txtAccountFilter
        '
        Me.txtAccountFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtAccountFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtAccountFilter.Location = New System.Drawing.Point(354, 439)
        Me.txtAccountFilter.Name = "txtAccountFilter"
        Me.txtAccountFilter.Size = New System.Drawing.Size(193, 20)
        Me.txtAccountFilter.TabIndex = 146
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.FillWeight = 68.28489!
        Me.DataGridViewTextBoxColumn1.HeaderText = "Account Number"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 138
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.FillWeight = 68.28489!
        Me.DataGridViewTextBoxColumn2.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 138
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.FillWeight = 68.28489!
        Me.DataGridViewTextBoxColumn3.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.FillWeight = 68.28489!
        Me.DataGridViewTextBoxColumn4.HeaderText = "Account"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 138
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.FillWeight = 68.28489!
        Me.DataGridViewTextBoxColumn5.HeaderText = "Schedule"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 138
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.FillWeight = 116.1308!
        Me.DataGridViewTextBoxColumn6.HeaderText = "Closed"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 138
        '
        'frmMasterfile_AccountRegister
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(845, 471)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtAccountFilter)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtsearch)
        Me.Controls.Add(Me.btnclose)
        Me.Controls.Add(Me.btndelete)
        Me.Controls.Add(Me.CBclose)
        Me.Controls.Add(Me.btnupdate)
        Me.Controls.Add(Me.CBClosedList)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.txtAccountNumber)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnBrowseEmployee)
        Me.Controls.Add(Me.txtSchedule)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.btnBrowseAccount)
        Me.Controls.Add(Me.txtaccountname)
        Me.Controls.Add(Me.txtaccountcode)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.dgvListAccountRegister)
        Me.Controls.Add(Me.btnBrowseAccountNumber)
        Me.Name = "frmMasterfile_AccountRegister"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Account Register"
        CType(Me.dgvListAccountRegister, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents txtAccountNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnBrowseEmployee As System.Windows.Forms.Button
    Friend WithEvents txtSchedule As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowseAccount As System.Windows.Forms.Button
    Friend WithEvents txtaccountname As System.Windows.Forms.TextBox
    Friend WithEvents txtaccountcode As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dgvListAccountRegister As System.Windows.Forms.DataGridView
    Friend WithEvents btnBrowseAccountNumber As System.Windows.Forms.Button
    Friend WithEvents CBClosedList As System.Windows.Forms.CheckBox
    Friend WithEvents CBclose As System.Windows.Forms.CheckBox
    Friend WithEvents txtsearch As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents AccountNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Account As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Schedule As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fcClosed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtAccountFilter As System.Windows.Forms.TextBox
End Class
