<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_item_billofmaterials
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.lst_item_fvBM = New System.Windows.Forms.ListView
        Me.btn_item_fvBMedit = New System.Windows.Forms.Button
        Me.btnOk = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Bill of Materials"
        '
        'lst_item_fvBM
        '
        Me.lst_item_fvBM.Location = New System.Drawing.Point(6, 25)
        Me.lst_item_fvBM.Name = "lst_item_fvBM"
        Me.lst_item_fvBM.Size = New System.Drawing.Size(626, 358)
        Me.lst_item_fvBM.TabIndex = 1
        Me.lst_item_fvBM.UseCompatibleStateImageBehavior = False
        '
        'btn_item_fvBMedit
        '
        Me.btn_item_fvBMedit.Location = New System.Drawing.Point(6, 389)
        Me.btn_item_fvBMedit.Name = "btn_item_fvBMedit"
        Me.btn_item_fvBMedit.Size = New System.Drawing.Size(93, 23)
        Me.btn_item_fvBMedit.TabIndex = 28
        Me.btn_item_fvBMedit.Text = "Edit Item"
        Me.btn_item_fvBMedit.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(539, 389)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(93, 23)
        Me.btnOk.TabIndex = 29
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'frm_item_billofmaterials
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(638, 424)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.btn_item_fvBMedit)
        Me.Controls.Add(Me.lst_item_fvBM)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_item_billofmaterials"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Bill of Materials Full View"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lst_item_fvBM As System.Windows.Forms.ListView
    Friend WithEvents btn_item_fvBMedit As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
End Class
