<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_item_masterItem
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_item_masterItem))
        Me.ts_item_master = New System.Windows.Forms.ToolStrip
        Me.ts_item_item = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_item_new = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_edit = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_delete = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_item_makeinactive = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_showinactive = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_hierarchical = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_flatview = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_customizecol = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_item_use = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_findtrans = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_item_printlist = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_resort = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_activities = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_item_createinvoice = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_salereceipt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_itemprices = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_buildassenblies = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_availability = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_item_purchaseorder = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_recEnterbill = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_recItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_billrecItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_quantity = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_reports = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_item_quickreport = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_item_pricelistrpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_itemlistrpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_item_allrpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_salerpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_SRitemdetail = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_SRitemsum = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_SRopenSObyitem = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_SRopenSObycust = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_item_purchasesrpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_purchaseVD = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_purchaseVS = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_purchaseID = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_purchaseIS = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_OpenPO = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_OpenPOC = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_inventoryrpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_invVD = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_invVS = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_invSSI = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_invSSV = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_invPhyWrksht = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_invPB = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_projectrpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_prjProfit = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_prjTimeItm = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_prjTimeCus = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_item_graphsrpt = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_grpSale = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_excel = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_item_excImport = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_item_excExport = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel
        Me.txtFilter = New System.Windows.Forms.ToolStripTextBox
        Me.BtnFilter = New System.Windows.Forms.ToolStripButton
        Me.grdItems = New System.Windows.Forms.DataGridView
        Me.chkIncludeInactive = New System.Windows.Forms.CheckBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ts_item_master.SuspendLayout()
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ts_item_master
        '
        Me.ts_item_master.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ts_item_master.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_item, Me.ts_item_activities, Me.ts_item_reports, Me.ts_item_excel, Me.ToolStripSeparator9, Me.ToolStripSeparator10, Me.ToolStripLabel1, Me.txtFilter, Me.BtnFilter})
        Me.ts_item_master.Location = New System.Drawing.Point(0, 0)
        Me.ts_item_master.Name = "ts_item_master"
        Me.ts_item_master.Size = New System.Drawing.Size(658, 25)
        Me.ts_item_master.TabIndex = 2
        Me.ts_item_master.Text = "ToolStrip1"
        '
        'ts_item_item
        '
        Me.ts_item_item.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ts_item_item.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_new, Me.ts_item_edit, Me.ts_item_delete, Me.ToolStripSeparator1, Me.ts_item_makeinactive, Me.ts_item_showinactive, Me.ts_item_hierarchical, Me.ts_item_flatview, Me.ts_item_customizecol, Me.ToolStripSeparator2, Me.ts_item_use, Me.ts_item_findtrans, Me.ToolStripSeparator3, Me.ts_item_printlist, Me.ts_item_resort})
        Me.ts_item_item.Image = CType(resources.GetObject("ts_item_item.Image"), System.Drawing.Image)
        Me.ts_item_item.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_item_item.Name = "ts_item_item"
        Me.ts_item_item.Size = New System.Drawing.Size(47, 22)
        Me.ts_item_item.Text = "Item"
        '
        'ts_item_new
        '
        Me.ts_item_new.Name = "ts_item_new"
        Me.ts_item_new.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.ts_item_new.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_new.Text = "New"
        '
        'ts_item_edit
        '
        Me.ts_item_edit.Name = "ts_item_edit"
        Me.ts_item_edit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.ts_item_edit.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_edit.Text = "Edit"
        Me.ts_item_edit.Visible = False
        '
        'ts_item_delete
        '
        Me.ts_item_delete.Name = "ts_item_delete"
        Me.ts_item_delete.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.ts_item_delete.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_delete.Text = "Delete"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(185, 6)
        '
        'ts_item_makeinactive
        '
        Me.ts_item_makeinactive.Name = "ts_item_makeinactive"
        Me.ts_item_makeinactive.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_makeinactive.Text = "Make Item Inactive"
        '
        'ts_item_showinactive
        '
        Me.ts_item_showinactive.Name = "ts_item_showinactive"
        Me.ts_item_showinactive.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_showinactive.Text = "Show Inactive View"
        '
        'ts_item_hierarchical
        '
        Me.ts_item_hierarchical.Name = "ts_item_hierarchical"
        Me.ts_item_hierarchical.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_hierarchical.Text = "Hierarchical View"
        Me.ts_item_hierarchical.Visible = False
        '
        'ts_item_flatview
        '
        Me.ts_item_flatview.Name = "ts_item_flatview"
        Me.ts_item_flatview.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_flatview.Text = "Flat View"
        Me.ts_item_flatview.Visible = False
        '
        'ts_item_customizecol
        '
        Me.ts_item_customizecol.Name = "ts_item_customizecol"
        Me.ts_item_customizecol.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_customizecol.Text = "Customize Columns"
        Me.ts_item_customizecol.Visible = False
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(185, 6)
        '
        'ts_item_use
        '
        Me.ts_item_use.Name = "ts_item_use"
        Me.ts_item_use.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_use.Text = "Use"
        Me.ts_item_use.Visible = False
        '
        'ts_item_findtrans
        '
        Me.ts_item_findtrans.Name = "ts_item_findtrans"
        Me.ts_item_findtrans.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_findtrans.Text = "Find Transaction"
        Me.ts_item_findtrans.Visible = False
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(185, 6)
        '
        'ts_item_printlist
        '
        Me.ts_item_printlist.Name = "ts_item_printlist"
        Me.ts_item_printlist.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_printlist.Text = "Print List"
        '
        'ts_item_resort
        '
        Me.ts_item_resort.Name = "ts_item_resort"
        Me.ts_item_resort.Size = New System.Drawing.Size(188, 22)
        Me.ts_item_resort.Text = "Re-sort List"
        '
        'ts_item_activities
        '
        Me.ts_item_activities.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ts_item_activities.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_createinvoice, Me.ts_item_salereceipt, Me.ts_item_itemprices, Me.ts_item_buildassenblies, Me.ts_item_availability, Me.ToolStripSeparator5, Me.ts_item_purchaseorder, Me.ts_item_recEnterbill, Me.ts_item_recItem, Me.ts_item_billrecItem, Me.ts_item_quantity})
        Me.ts_item_activities.Image = CType(resources.GetObject("ts_item_activities.Image"), System.Drawing.Image)
        Me.ts_item_activities.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_item_activities.Name = "ts_item_activities"
        Me.ts_item_activities.Size = New System.Drawing.Size(71, 22)
        Me.ts_item_activities.Text = "Activities"
        '
        'ts_item_createinvoice
        '
        Me.ts_item_createinvoice.Name = "ts_item_createinvoice"
        Me.ts_item_createinvoice.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_createinvoice.Text = "Create Invoice"
        '
        'ts_item_salereceipt
        '
        Me.ts_item_salereceipt.Name = "ts_item_salereceipt"
        Me.ts_item_salereceipt.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_salereceipt.Text = "Enter Sales Receipts"
        '
        'ts_item_itemprices
        '
        Me.ts_item_itemprices.Name = "ts_item_itemprices"
        Me.ts_item_itemprices.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_itemprices.Text = "Change Item Prices"
        '
        'ts_item_buildassenblies
        '
        Me.ts_item_buildassenblies.Name = "ts_item_buildassenblies"
        Me.ts_item_buildassenblies.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_buildassenblies.Text = "Build Assemblies"
        Me.ts_item_buildassenblies.Visible = False
        '
        'ts_item_availability
        '
        Me.ts_item_availability.Name = "ts_item_availability"
        Me.ts_item_availability.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_availability.Text = "Current Availability"
        Me.ts_item_availability.Visible = False
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(247, 6)
        '
        'ts_item_purchaseorder
        '
        Me.ts_item_purchaseorder.Name = "ts_item_purchaseorder"
        Me.ts_item_purchaseorder.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_purchaseorder.Text = "Create Purchase Order"
        '
        'ts_item_recEnterbill
        '
        Me.ts_item_recEnterbill.Name = "ts_item_recEnterbill"
        Me.ts_item_recEnterbill.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_recEnterbill.Text = "Receive Items && Enter Bill"
        '
        'ts_item_recItem
        '
        Me.ts_item_recItem.Name = "ts_item_recItem"
        Me.ts_item_recItem.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_recItem.Text = "Receive Items"
        '
        'ts_item_billrecItem
        '
        Me.ts_item_billrecItem.Name = "ts_item_billrecItem"
        Me.ts_item_billrecItem.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_billrecItem.Text = "Enter Bill for Received Items"
        '
        'ts_item_quantity
        '
        Me.ts_item_quantity.Name = "ts_item_quantity"
        Me.ts_item_quantity.Size = New System.Drawing.Size(250, 22)
        Me.ts_item_quantity.Text = "Adjust Quantity/Value on Hand"
        Me.ts_item_quantity.Visible = False
        '
        'ts_item_reports
        '
        Me.ts_item_reports.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ts_item_reports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_quickreport, Me.ToolStripSeparator4, Me.ts_item_pricelistrpt, Me.ts_item_itemlistrpt, Me.ToolStripSeparator6, Me.ts_item_allrpt})
        Me.ts_item_reports.Image = CType(resources.GetObject("ts_item_reports.Image"), System.Drawing.Image)
        Me.ts_item_reports.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_item_reports.Name = "ts_item_reports"
        Me.ts_item_reports.Size = New System.Drawing.Size(64, 22)
        Me.ts_item_reports.Text = "Reports"
        Me.ts_item_reports.Visible = False
        '
        'ts_item_quickreport
        '
        Me.ts_item_quickreport.Name = "ts_item_quickreport"
        Me.ts_item_quickreport.Size = New System.Drawing.Size(185, 22)
        Me.ts_item_quickreport.Text = "Quick Report"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(182, 6)
        '
        'ts_item_pricelistrpt
        '
        Me.ts_item_pricelistrpt.Name = "ts_item_pricelistrpt"
        Me.ts_item_pricelistrpt.Size = New System.Drawing.Size(185, 22)
        Me.ts_item_pricelistrpt.Text = "Price List"
        '
        'ts_item_itemlistrpt
        '
        Me.ts_item_itemlistrpt.Name = "ts_item_itemlistrpt"
        Me.ts_item_itemlistrpt.Size = New System.Drawing.Size(185, 22)
        Me.ts_item_itemlistrpt.Text = "Item Listing"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(182, 6)
        '
        'ts_item_allrpt
        '
        Me.ts_item_allrpt.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_salerpt, Me.ToolStripSeparator7, Me.ts_item_purchasesrpt, Me.ts_item_inventoryrpt, Me.ts_item_projectrpt, Me.ToolStripSeparator8, Me.ts_item_graphsrpt})
        Me.ts_item_allrpt.Name = "ts_item_allrpt"
        Me.ts_item_allrpt.Size = New System.Drawing.Size(185, 22)
        Me.ts_item_allrpt.Text = "Report on All Items"
        '
        'ts_item_salerpt
        '
        Me.ts_item_salerpt.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_SRitemdetail, Me.ts_item_SRitemsum, Me.ts_item_SRopenSObyitem, Me.ts_item_SRopenSObycust})
        Me.ts_item_salerpt.Name = "ts_item_salerpt"
        Me.ts_item_salerpt.Size = New System.Drawing.Size(147, 22)
        Me.ts_item_salerpt.Text = "Sales Report"
        '
        'ts_item_SRitemdetail
        '
        Me.ts_item_SRitemdetail.Name = "ts_item_SRitemdetail"
        Me.ts_item_SRitemdetail.Size = New System.Drawing.Size(254, 22)
        Me.ts_item_SRitemdetail.Text = "By Item Detail"
        '
        'ts_item_SRitemsum
        '
        Me.ts_item_SRitemsum.Name = "ts_item_SRitemsum"
        Me.ts_item_SRitemsum.Size = New System.Drawing.Size(254, 22)
        Me.ts_item_SRitemsum.Text = "By Item Summary"
        '
        'ts_item_SRopenSObyitem
        '
        Me.ts_item_SRopenSObyitem.Name = "ts_item_SRopenSObyitem"
        Me.ts_item_SRopenSObyitem.Size = New System.Drawing.Size(254, 22)
        Me.ts_item_SRopenSObyitem.Text = "Open Sales Order by Item"
        '
        'ts_item_SRopenSObycust
        '
        Me.ts_item_SRopenSObycust.Name = "ts_item_SRopenSObycust"
        Me.ts_item_SRopenSObycust.Size = New System.Drawing.Size(254, 22)
        Me.ts_item_SRopenSObycust.Text = "Open Sales Order by Customer"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(144, 6)
        '
        'ts_item_purchasesrpt
        '
        Me.ts_item_purchasesrpt.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_purchaseVD, Me.ts_item_purchaseVS, Me.ts_item_purchaseID, Me.ts_item_purchaseIS, Me.ts_item_OpenPO, Me.ts_item_OpenPOC})
        Me.ts_item_purchasesrpt.Name = "ts_item_purchasesrpt"
        Me.ts_item_purchasesrpt.Size = New System.Drawing.Size(147, 22)
        Me.ts_item_purchasesrpt.Text = "Purchases"
        '
        'ts_item_purchaseVD
        '
        Me.ts_item_purchaseVD.Name = "ts_item_purchaseVD"
        Me.ts_item_purchaseVD.Size = New System.Drawing.Size(275, 22)
        Me.ts_item_purchaseVD.Text = "Purchase by Vendor Detail"
        '
        'ts_item_purchaseVS
        '
        Me.ts_item_purchaseVS.Name = "ts_item_purchaseVS"
        Me.ts_item_purchaseVS.Size = New System.Drawing.Size(275, 22)
        Me.ts_item_purchaseVS.Text = "Purchase by Vendor Summary"
        '
        'ts_item_purchaseID
        '
        Me.ts_item_purchaseID.Name = "ts_item_purchaseID"
        Me.ts_item_purchaseID.Size = New System.Drawing.Size(275, 22)
        Me.ts_item_purchaseID.Text = "Purchase by Item Detail"
        '
        'ts_item_purchaseIS
        '
        Me.ts_item_purchaseIS.Name = "ts_item_purchaseIS"
        Me.ts_item_purchaseIS.Size = New System.Drawing.Size(275, 22)
        Me.ts_item_purchaseIS.Text = "Purchase by Item Summary"
        '
        'ts_item_OpenPO
        '
        Me.ts_item_OpenPO.Name = "ts_item_OpenPO"
        Me.ts_item_OpenPO.Size = New System.Drawing.Size(275, 22)
        Me.ts_item_OpenPO.Text = "Open Purchase Order"
        '
        'ts_item_OpenPOC
        '
        Me.ts_item_OpenPOC.Name = "ts_item_OpenPOC"
        Me.ts_item_OpenPOC.Size = New System.Drawing.Size(275, 22)
        Me.ts_item_OpenPOC.Text = "Open Purchase Order by Customer"
        '
        'ts_item_inventoryrpt
        '
        Me.ts_item_inventoryrpt.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_invVD, Me.ts_item_invVS, Me.ts_item_invSSI, Me.ts_item_invSSV, Me.ts_item_invPhyWrksht, Me.ts_item_invPB})
        Me.ts_item_inventoryrpt.Name = "ts_item_inventoryrpt"
        Me.ts_item_inventoryrpt.Size = New System.Drawing.Size(147, 22)
        Me.ts_item_inventoryrpt.Text = "Inventory"
        '
        'ts_item_invVD
        '
        Me.ts_item_invVD.Name = "ts_item_invVD"
        Me.ts_item_invVD.Size = New System.Drawing.Size(269, 22)
        Me.ts_item_invVD.Text = "Inventory Valuation Detail"
        '
        'ts_item_invVS
        '
        Me.ts_item_invVS.Name = "ts_item_invVS"
        Me.ts_item_invVS.Size = New System.Drawing.Size(269, 22)
        Me.ts_item_invVS.Text = "Inventory Valuation Summary"
        '
        'ts_item_invSSI
        '
        Me.ts_item_invSSI.Name = "ts_item_invSSI"
        Me.ts_item_invSSI.Size = New System.Drawing.Size(269, 22)
        Me.ts_item_invSSI.Text = "Inventory Stock Status by Item"
        '
        'ts_item_invSSV
        '
        Me.ts_item_invSSV.Name = "ts_item_invSSV"
        Me.ts_item_invSSV.Size = New System.Drawing.Size(269, 22)
        Me.ts_item_invSSV.Text = "Inventory Stock Status by Vendor"
        '
        'ts_item_invPhyWrksht
        '
        Me.ts_item_invPhyWrksht.Name = "ts_item_invPhyWrksht"
        Me.ts_item_invPhyWrksht.Size = New System.Drawing.Size(269, 22)
        Me.ts_item_invPhyWrksht.Text = "Physical Inventory Worksheet"
        '
        'ts_item_invPB
        '
        Me.ts_item_invPB.Name = "ts_item_invPB"
        Me.ts_item_invPB.Size = New System.Drawing.Size(269, 22)
        Me.ts_item_invPB.Text = "Pending Builds"
        '
        'ts_item_projectrpt
        '
        Me.ts_item_projectrpt.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_prjProfit, Me.ts_item_prjTimeItm, Me.ts_item_prjTimeCus})
        Me.ts_item_projectrpt.Name = "ts_item_projectrpt"
        Me.ts_item_projectrpt.Size = New System.Drawing.Size(147, 22)
        Me.ts_item_projectrpt.Text = "Project"
        '
        'ts_item_prjProfit
        '
        Me.ts_item_prjProfit.Name = "ts_item_prjProfit"
        Me.ts_item_prjProfit.Size = New System.Drawing.Size(180, 22)
        Me.ts_item_prjProfit.Text = "Item Profitability"
        '
        'ts_item_prjTimeItm
        '
        Me.ts_item_prjTimeItm.Name = "ts_item_prjTimeItm"
        Me.ts_item_prjTimeItm.Size = New System.Drawing.Size(180, 22)
        Me.ts_item_prjTimeItm.Text = "Time by Item"
        '
        'ts_item_prjTimeCus
        '
        Me.ts_item_prjTimeCus.Name = "ts_item_prjTimeCus"
        Me.ts_item_prjTimeCus.Size = New System.Drawing.Size(180, 22)
        Me.ts_item_prjTimeCus.Text = "Time by Customer"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(144, 6)
        '
        'ts_item_graphsrpt
        '
        Me.ts_item_graphsrpt.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_grpSale})
        Me.ts_item_graphsrpt.Name = "ts_item_graphsrpt"
        Me.ts_item_graphsrpt.Size = New System.Drawing.Size(147, 22)
        Me.ts_item_graphsrpt.Text = "Graphs"
        '
        'ts_item_grpSale
        '
        Me.ts_item_grpSale.Name = "ts_item_grpSale"
        Me.ts_item_grpSale.Size = New System.Drawing.Size(105, 22)
        Me.ts_item_grpSale.Text = "Sales"
        '
        'ts_item_excel
        '
        Me.ts_item_excel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ts_item_excel.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_item_excImport, Me.ts_item_excExport})
        Me.ts_item_excel.Image = CType(resources.GetObject("ts_item_excel.Image"), System.Drawing.Image)
        Me.ts_item_excel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_item_excel.Name = "ts_item_excel"
        Me.ts_item_excel.Size = New System.Drawing.Size(50, 22)
        Me.ts_item_excel.Text = "Excel"
        Me.ts_item_excel.Visible = False
        '
        'ts_item_excImport
        '
        Me.ts_item_excImport.Name = "ts_item_excImport"
        Me.ts_item_excImport.Size = New System.Drawing.Size(166, 22)
        Me.ts_item_excImport.Text = "Import Items"
        '
        'ts_item_excExport
        '
        Me.ts_item_excExport.Name = "ts_item_excExport"
        Me.ts_item_excExport.Size = New System.Drawing.Size(166, 22)
        Me.ts_item_excExport.Text = "Export All Items"
        Me.ts_item_excExport.Visible = False
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(172, 22)
        Me.ToolStripLabel1.Text = "Filter with the Item Name of:"
        '
        'txtFilter
        '
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(100, 25)
        Me.txtFilter.ToolTipText = "Type the Item Description of the item you want to appear. If you want to see all " & _
            "the item in the database, just leave it blank."
        '
        'BtnFilter
        '
        Me.BtnFilter.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.BtnFilter.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.BtnFilter.Name = "BtnFilter"
        Me.BtnFilter.Size = New System.Drawing.Size(55, 22)
        Me.BtnFilter.Text = "Filter"
        Me.BtnFilter.ToolTipText = "Filter by specific Item Name"
        '
        'grdItems
        '
        Me.grdItems.AllowUserToAddRows = False
        Me.grdItems.AllowUserToDeleteRows = False
        Me.grdItems.AllowUserToOrderColumns = True
        Me.grdItems.AllowUserToResizeRows = False
        Me.grdItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdItems.Location = New System.Drawing.Point(0, 45)
        Me.grdItems.Name = "grdItems"
        Me.grdItems.ReadOnly = True
        Me.grdItems.Size = New System.Drawing.Size(658, 356)
        Me.grdItems.TabIndex = 3
        '
        'chkIncludeInactive
        '
        Me.chkIncludeInactive.AutoSize = True
        Me.chkIncludeInactive.Location = New System.Drawing.Point(14, 28)
        Me.chkIncludeInactive.Name = "chkIncludeInactive"
        Me.chkIncludeInactive.Size = New System.Drawing.Size(116, 17)
        Me.chkIncludeInactive.TabIndex = 4
        Me.chkIncludeInactive.Text = "Include in&active"
        Me.chkIncludeInactive.UseVisualStyleBackColor = True
        '
        'frm_item_masterItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(658, 401)
        Me.Controls.Add(Me.chkIncludeInactive)
        Me.Controls.Add(Me.grdItems)
        Me.Controls.Add(Me.ts_item_master)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(665, 435)
        Me.Name = "frm_item_masterItem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Item Master"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ts_item_master.ResumeLayout(False)
        Me.ts_item_master.PerformLayout()
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ts_item_master As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_item_item As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_item_reports As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_item_activities As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_item_excel As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_item_new As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_edit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_delete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_item_makeinactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_showinactive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_hierarchical As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_flatview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_customizecol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_item_use As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_findtrans As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_item_printlist As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_resort As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_createinvoice As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_salereceipt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_itemprices As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_buildassenblies As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_availability As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_purchaseorder As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_recEnterbill As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_recItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_billrecItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_quantity As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_item_quickreport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_item_pricelistrpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_itemlistrpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_item_allrpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_salerpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_item_purchasesrpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_inventoryrpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_projectrpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_item_graphsrpt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_SRitemdetail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_SRitemsum As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_SRopenSObyitem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_SRopenSObycust As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_purchaseVD As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_purchaseVS As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_purchaseID As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_purchaseIS As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_OpenPO As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_OpenPOC As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_invVD As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_invVS As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_invSSI As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_invSSV As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_invPhyWrksht As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_invPB As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_prjProfit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_prjTimeItm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_prjTimeCus As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_grpSale As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_excImport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_item_excExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents grdItems As System.Windows.Forms.DataGridView
    Friend WithEvents chkIncludeInactive As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents txtFilter As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BtnFilter As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
End Class
