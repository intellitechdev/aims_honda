Public Class frm_item_changeprice

    Private Sub frm_item_changeprice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadItemList()
    End Sub

    Private Sub loadItemList()
        With grdItemList
            .Columns.Add("cSelect", "check")
            .Columns.Add("cItem", "Item")
            .Columns.Add("cDesc", "Description")
            .Columns.Add("cCurPrice", "Current Price")
            .Columns.Add("cNewPrice", "New Price")

            .Columns(0).Width = 40
            .Columns(1).Width = 100
            .Columns(2).Width = 100
            .Columns(3).Width = 80
            .Columns(4).Width = 80
        End With
    End Sub
End Class