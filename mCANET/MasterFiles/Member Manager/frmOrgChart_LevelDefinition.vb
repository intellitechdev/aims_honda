Imports System.Data.SqlClient
Public Class frmOrgChart_LevelDefinition
    Dim dt As DataTable
    Dim errmsg As String
    Dim add, edit As Boolean
    Dim next_level As Integer


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Me.Close()
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub LevelDef_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.btnAddSave.Text = "Add"
        Me.btnEditCancel.Text = "Edit"
        Me.btnDelete.Text = "Delete"
        LoadLevel()
        add = False
        edit = False

    End Sub

    Public Sub LoadLevel()
        Dim load As New clsPersonnel
        dt = load.LoadAllLevelDefinition(errmsg)

        If errmsg <> "" Then
            MessageBox.Show(errmsg, "Load", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Me.dtgLevelDef.DataSource = dt

            If dt.Rows.Count <> 0 Then
                next_level = CInt(Me.dt.Rows.Count)
            Else
                next_level = 0
            End If
        End If


    End Sub
    Public Sub clear()
        Me.txtcode.Text = ""
        Me.txtdesc.Text = ""
        Me.txtlevel.Text = ""
    End Sub

    Private Sub btnAddSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddSave.Click
        If Me.btnAddSave.Text = "Add" Then
            add = True
            Me.GroupBox1.Enabled = True
            Me.btnAddSave.Text = "Save"
            Me.btnEditCancel.Text = "Cancel"
            clear()
            Me.btnDelete.Enabled = False
            Me.txtlevel.Text = next_level
            Me.txtlevel.Enabled = False
            Me.btnEditCancel.Visible = True
        ElseIf Me.btnAddSave.Text = "Save" Then
            'add
            If Me.txtcode.Text <> "" And Me.txtdesc.Text <> "" And Me.txtlevel.Text <> "" Then

                If add = True Then
                    If MessageBox.Show("Are you sure you want to save this level?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Dim addlevel As New clsPersonnel
                        addlevel.LevelMaintenance(1, Me.txtlevel.Text, Me.txtcode.Text, Me.txtdesc.Text, errmsg)

                        If errmsg <> "" Then
                            MessageBox.Show(errmsg, "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Else
                            MessageBox.Show("Level Successfully Saved!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.btnAddSave.Text = "Add"
                            Me.btnEditCancel.Text = "Edit"
                            Me.btnEditCancel.Visible = True
                            add = False
                            edit = False
                            levelrow()
                            Me.LoadLevel()
                            Me.GroupBox1.Enabled = False
                            Me.btnDelete.Enabled = True

                        End If
                    End If
                End If



                If edit = True Then

                    If MessageBox.Show("Are you sure you want to update this level?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Dim addlevel As New clsPersonnel
                        addlevel.LevelMaintenance(2, Me.txtlevel.Text, Me.txtcode.Text, Me.txtdesc.Text, errmsg)
                        If errmsg <> "" Then
                            MessageBox.Show(errmsg, "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Else
                            MessageBox.Show("Level successfully Saved!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.btnAddSave.Text = "Add"
                            Me.btnEditCancel.Text = "Edit"
                            add = False
                            edit = False
                            levelrow()
                            Me.LoadLevel()
                            Me.GroupBox1.Enabled = False
                            Me.btnDelete.Enabled = True
                            Me.txtlevel.ReadOnly = False
                        End If
                    End If
                End If
            End If

        Else
            MessageBox.Show("Incomplete Inputs!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub btnEditCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        If Me.btnEditCancel.Text = "Edit" Then
            Me.GroupBox1.Enabled = True
            Me.btnAddSave.Text = "Save"
            Me.btnAddSave.Enabled = True
            Me.btnEditCancel.Text = "Cancel"
            Me.btnDelete.Enabled = False
            edit = True
            Call check_level()
            Me.txtlevel.Enabled = False
            Me.txtlevel.ReadOnly = True

        ElseIf Me.btnEditCancel.Text = "Cancel" Then
            Me.btnAddSave.Text = "Add"
            Me.btnEditCancel.Text = "Edit"
            Me.GroupBox1.Enabled = False
            add = False
            edit = False
            Me.txtcode.Text = ""
            Me.txtlevel.Text = ""
            Me.txtdesc.Text = ""
            Me.btnDelete.Enabled = True
            levelrow()
            Me.LoadLevel()
            Me.txtlevel.ReadOnly = False
        End If
    End Sub
#Region "Check Level"
    Public Sub check_level()
        If Me.txtlevel.Text = 0 Then
            'Call insert_division()
        End If
    End Sub
#End Region

    Private Sub dtgLevelDef_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgLevelDef.CellContentClick
        levelrow()
    End Sub
    Public Sub check_levelCode()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim addlevel As New clsPersonnel
        Dim add As New clsPersonnel
        Dim cmd As New SqlCommand("USP_CHECK_LEVELDEFINE", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@OrgLevelDescription", SqlDbType.VarChar, 50).Value = Me.txtdesc.Text
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.Read Then
                MessageBox.Show("The Description has already been used, Please try another one", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call check_leveldescUpdate()
                Me.LoadLevel()
            End If
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Public Sub check_code_duplicate()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim addlevel As New clsPersonnel
        Dim add As New clsPersonnel
        Dim cmd As New SqlCommand("USP_CHECK_LEVELDEFINE_CODE", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@OrgLevelCode", SqlDbType.VarChar, 50).Value = Me.txtcode.Text
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.Read Then
                MessageBox.Show("The Code has already been used, Please try another one", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call check_leveldescUpdate()
                Me.LoadLevel()
            End If
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Public Sub check_leveldescUpdate()

        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration

        Dim cmd As New SqlCommand("USP_LEVEL_DEFINITION_UPDATE", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@OrgLevel", SqlDbType.Int).Value = Me.txtlevel.Text
        cmd.Parameters.Add("@OrgLevelCode", SqlDbType.VarChar, 50).Value = Me.txtcode.Text
        cmd.Parameters.Add("@OrgLevelDescription", SqlDbType.VarChar, 50).Value = Me.txtdesc.Text
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()

    End Sub
    Private Sub dtgLevelDef_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgLevelDef.CellEnter

        levelrow()
    End Sub
    Public Sub levelrow()
        If Me.dt.Rows.Count <> 0 Then

            Me.txtlevel.Text = Me.dtgLevelDef.CurrentRow.Cells(0).Value
            Me.txtcode.Text = Me.dtgLevelDef.CurrentRow.Cells(1).Value
            Me.txtdesc.Text = Me.dtgLevelDef.CurrentRow.Cells(2).Value

        End If


    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If MessageBox.Show("Are you sure you want to delete this Level?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim deletelevel As New clsPersonnel
                deletelevel.LevelMaintenance(3, Me.txtlevel.Text, Me.txtcode.Text, Me.txtdesc.Text, errmsg)
                If errmsg <> "" Then
                    MessageBox.Show(errmsg, "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else
                    MessageBox.Show("Level Successfully Deleted!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    levelrow()
                    Me.LoadLevel()
                End If
            End If
        Catch EX As Exception
            MessageBox.Show("Cannot be deleted it is used by another process", "Delete failed", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub DELETE_ORGLEVEL()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration

        Dim cmd As New SqlCommand("USP_DELETE_ORGLEVEL", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@OrgLevel", SqlDbType.Int).Value = dtgLevelDef.CurrentRow.Cells(0).Value
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#Region "Insert to Division in Payroll chart"
    Private Sub insert_division()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_division_to_payroll_insert", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@fxKeyDivision", SqlDbType.Char, 2).Value = Me.txtlevel.Text
                .Add("@fcDivisionCode", SqlDbType.VarChar, 12).Value = Me.txtcode.Text
                .Add("@fcDivisionName", SqlDbType.VarChar, 30).Value = Me.txtdesc.Text
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Level Definition", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Insert to Department in Payroll chart"
    Private Sub insert_department()

    End Sub
#End Region
#Region "Insert to Section in Payroll chart"
    Private Sub insert_section()

    End Sub
#End Region
End Class
