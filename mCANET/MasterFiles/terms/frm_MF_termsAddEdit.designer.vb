<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_MF_termsAddEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_MF_termsAddEdit))
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtTermsName = New System.Windows.Forms.TextBox
        Me.rbtnStandard = New System.Windows.Forms.RadioButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtStdDaysDue = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtStdPercentage = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtStdDaysDiscount = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtDtDaysDiscount = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtDtPercentage = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtDtDaysDue = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.rbtnDate = New System.Windows.Forms.RadioButton
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtDtDaysNxDue = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.btnOk = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.chkInactive = New System.Windows.Forms.CheckBox
        Me.txtInt1 = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.txtInt2 = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Terms"
        '
        'txtTermsName
        '
        Me.txtTermsName.Location = New System.Drawing.Point(58, 12)
        Me.txtTermsName.Name = "txtTermsName"
        Me.txtTermsName.Size = New System.Drawing.Size(254, 21)
        Me.txtTermsName.TabIndex = 1
        '
        'rbtnStandard
        '
        Me.rbtnStandard.AutoSize = True
        Me.rbtnStandard.Location = New System.Drawing.Point(13, 48)
        Me.rbtnStandard.Name = "rbtnStandard"
        Me.rbtnStandard.Size = New System.Drawing.Size(77, 17)
        Me.rbtnStandard.TabIndex = 2
        Me.rbtnStandard.TabStop = True
        Me.rbtnStandard.Text = "Standard"
        Me.rbtnStandard.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(33, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Net due in"
        '
        'txtStdDaysDue
        '
        Me.txtStdDaysDue.Location = New System.Drawing.Point(105, 67)
        Me.txtStdDaysDue.Name = "txtStdDaysDue"
        Me.txtStdDaysDue.Size = New System.Drawing.Size(60, 21)
        Me.txtStdDaysDue.TabIndex = 4
        Me.txtStdDaysDue.Text = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(173, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "days."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(233, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(11, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "."
        '
        'txtStdPercentage
        '
        Me.txtStdPercentage.Location = New System.Drawing.Point(170, 93)
        Me.txtStdPercentage.Name = "txtStdPercentage"
        Me.txtStdPercentage.Size = New System.Drawing.Size(60, 21)
        Me.txtStdPercentage.TabIndex = 7
        Me.txtStdPercentage.Text = "0.00"
        Me.txtStdPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(33, 96)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(137, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Discount percentage is"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(233, 120)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "days."
        '
        'txtStdDaysDiscount
        '
        Me.txtStdDaysDiscount.Location = New System.Drawing.Point(170, 117)
        Me.txtStdDaysDiscount.Name = "txtStdDaysDiscount"
        Me.txtStdDaysDiscount.Size = New System.Drawing.Size(60, 21)
        Me.txtStdDaysDiscount.TabIndex = 10
        Me.txtStdDaysDiscount.Text = "0"
        Me.txtStdDaysDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(33, 120)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(132, 13)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Discount if paid within"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(300, 264)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(124, 13)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "th day of the month."
        '
        'txtDtDaysDiscount
        '
        Me.txtDtDaysDiscount.Location = New System.Drawing.Point(237, 261)
        Me.txtDtDaysDiscount.Name = "txtDtDaysDiscount"
        Me.txtDtDaysDiscount.Size = New System.Drawing.Size(60, 21)
        Me.txtDtDaysDiscount.TabIndex = 20
        Me.txtDtDaysDiscount.Text = "1"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(33, 264)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(158, 13)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "Discount if paid before the"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(233, 240)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(11, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "."
        '
        'txtDtPercentage
        '
        Me.txtDtPercentage.Location = New System.Drawing.Point(170, 237)
        Me.txtDtPercentage.Name = "txtDtPercentage"
        Me.txtDtPercentage.Size = New System.Drawing.Size(60, 21)
        Me.txtDtPercentage.TabIndex = 17
        Me.txtDtPercentage.Text = "0.00"
        Me.txtDtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(33, 240)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(137, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Discount percentage is"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(238, 188)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(124, 13)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "th day of the month."
        '
        'txtDtDaysDue
        '
        Me.txtDtDaysDue.Location = New System.Drawing.Point(170, 185)
        Me.txtDtDaysDue.Name = "txtDtDaysDue"
        Me.txtDtDaysDue.Size = New System.Drawing.Size(60, 21)
        Me.txtDtDaysDue.TabIndex = 14
        Me.txtDtDaysDue.Text = "1"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(33, 188)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(114, 13)
        Me.Label13.TabIndex = 13
        Me.Label13.Text = "Net due before the"
        '
        'rbtnDate
        '
        Me.rbtnDate.AutoSize = True
        Me.rbtnDate.Location = New System.Drawing.Point(13, 166)
        Me.rbtnDate.Name = "rbtnDate"
        Me.rbtnDate.Size = New System.Drawing.Size(94, 17)
        Me.rbtnDate.TabIndex = 12
        Me.rbtnDate.TabStop = True
        Me.rbtnDate.Text = "Date Driven"
        Me.rbtnDate.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(304, 215)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(107, 13)
        Me.Label14.TabIndex = 24
        Me.Label14.Text = "days of due date."
        '
        'txtDtDaysNxDue
        '
        Me.txtDtDaysNxDue.Location = New System.Drawing.Point(237, 212)
        Me.txtDtDaysNxDue.Name = "txtDtDaysNxDue"
        Me.txtDtDaysNxDue.Size = New System.Drawing.Size(60, 21)
        Me.txtDtDaysNxDue.TabIndex = 23
        Me.txtDtDaysNxDue.Text = "0"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(33, 215)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(209, 13)
        Me.Label15.TabIndex = 22
        Me.Label15.Text = "Due the next month if issued within"
        '
        'btnOk
        '
        Me.btnOk.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Location = New System.Drawing.Point(242, 322)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(87, 23)
        Me.btnOk.TabIndex = 25
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(332, 322)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 23)
        Me.btnCancel.TabIndex = 26
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'chkInactive
        '
        Me.chkInactive.AutoSize = True
        Me.chkInactive.Location = New System.Drawing.Point(332, 14)
        Me.chkInactive.Name = "chkInactive"
        Me.chkInactive.Size = New System.Drawing.Size(72, 17)
        Me.chkInactive.TabIndex = 27
        Me.chkInactive.Text = "Inactive"
        Me.chkInactive.UseVisualStyleBackColor = True
        '
        'txtInt1
        '
        Me.txtInt1.Location = New System.Drawing.Point(170, 141)
        Me.txtInt1.Name = "txtInt1"
        Me.txtInt1.Size = New System.Drawing.Size(41, 21)
        Me.txtInt1.TabIndex = 28
        Me.txtInt1.Text = "0"
        Me.txtInt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(33, 144)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(131, 13)
        Me.Label16.TabIndex = 29
        Me.Label16.Text = "Interest Charge if not"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(214, 145)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(19, 13)
        Me.Label17.TabIndex = 30
        Me.Label17.Text = "%"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(234, 145)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(132, 13)
        Me.Label18.TabIndex = 31
        Me.Label18.Text = ", paid after Due Date."
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(214, 284)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(19, 13)
        Me.Label20.TabIndex = 34
        Me.Label20.Text = "%"
        '
        'txtInt2
        '
        Me.txtInt2.Location = New System.Drawing.Point(170, 280)
        Me.txtInt2.Name = "txtInt2"
        Me.txtInt2.Size = New System.Drawing.Size(41, 21)
        Me.txtInt2.TabIndex = 32
        Me.txtInt2.Text = "0"
        Me.txtInt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(33, 284)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(131, 13)
        Me.Label21.TabIndex = 36
        Me.Label21.Text = "Interest Charge if not"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(234, 285)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(132, 13)
        Me.Label19.TabIndex = 37
        Me.Label19.Text = ", paid after Due Date."
        '
        'frm_MF_termsAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(433, 357)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.txtInt2)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txtInt1)
        Me.Controls.Add(Me.chkInactive)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtDtDaysNxDue)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtDtDaysDiscount)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtDtPercentage)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtDtDaysDue)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.rbtnDate)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtStdDaysDiscount)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtStdPercentage)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtStdDaysDue)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.rbtnStandard)
        Me.Controls.Add(Me.txtTermsName)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(441, 391)
        Me.Name = "frm_MF_termsAddEdit"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Terms "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTermsName As System.Windows.Forms.TextBox
    Friend WithEvents rbtnStandard As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtStdDaysDue As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtStdPercentage As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtStdDaysDiscount As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtDtDaysDiscount As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtDtPercentage As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtDtDaysDue As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents rbtnDate As System.Windows.Forms.RadioButton
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtDtDaysNxDue As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents chkInactive As System.Windows.Forms.CheckBox
    Friend WithEvents txtInt1 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtInt2 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
End Class
