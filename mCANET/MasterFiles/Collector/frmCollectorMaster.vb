﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmCollectorMaster
    Private gCon As New Clsappconfiguration()
    Dim CollectorID As Integer

#Region "Logic"

#End Region

#Region "Data Access Layer"
    Private Sub Save()
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_Collector_InsertUpdate",
                                                New SqlParameter("@PkCollector", CollectorID),
                                                New SqlParameter("@FcFullName", txtCollector.Text))
    End Sub

    Private Sub Delete()
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_Collector_Delete",
                                                New SqlParameter("@PkCollector", CollectorID))
    End Sub

    Private Sub LoadCollector()
        'grdList.Rows.Clear()
        'grdList.Columns.Clear()
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "spu_Collector_View")

        grdList.DataSource = ds.Tables(0)

        With grdList
            .Columns("PkCollector").Visible = False
            .Columns("FcFullName").HeaderText = "Full Name"
            .Columns("FcFullName").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("FcFullName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("FcFullName").ReadOnly = True
        End With
    End Sub

    Private Sub GetGridDetails()
        Try
            CollectorID = grdList.Item(0, grdList.CurrentRow.Index).Value
            txtCollector.Text = grdList.Item(1, grdList.CurrentRow.Index).Value.ToString
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

#End Region

#Region "Event Handler"

    Private Sub AddToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AddToolStripMenuItem.Click
        Save()
        LoadCollector()
        txtCollector.Clear()
        txtCollector.Enabled = False
        CollectorID = 0
    End Sub

    Private Sub frmCollectorMaster_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadCollector()
    End Sub

    Private Sub NewToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NewToolStripMenuItem.Click
        txtCollector.Clear()
        txtCollector.Enabled = True
        CollectorID = 0
    End Sub

    Private Sub EditToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EditToolStripMenuItem.Click
        txtCollector.Enabled = True
    End Sub

    Private Sub grdList_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdList.CellClick
        GetGridDetails()
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Delete()
        LoadCollector()
        txtCollector.Clear()
        txtCollector.Enabled = False
        CollectorID = 0
    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub
#End Region

End Class