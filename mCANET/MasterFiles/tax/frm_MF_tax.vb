Imports Microsoft.ApplicationBlocks.Data

Public Class frm_MF_tax
    Private sKeyTax As String
    Private gCon As New Clsappconfiguration
    Private sSQLCmdQuery As String = "SELECT * FROM mSalesTax WHERE fbActive <> 1 "

    Public Property SQLQuery() As String
        Get
            Return sSQLCmdQuery
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Private Sub ts_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        frm_MF_taxAddEdit.KeyTax = Guid.NewGuid.ToString
        frm_MF_taxAddEdit.MdiParent = Me.MdiParent
        frm_MF_taxAddEdit.Text = "Add Sales Tax"
        frm_MF_taxAddEdit.Show()
    End Sub

    Private Sub ts_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        frm_MF_taxAddEdit.MdiParent = Me.MdiParent
        frm_MF_taxAddEdit.KeyTax = sKeyTax
        frm_MF_taxAddEdit.Text = "Edit Sales Tax"
        frm_MF_taxAddEdit.Show()
    End Sub

    Private Sub frm_acc_terms_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm()
    End Sub

    Public Sub refreshForm()
        m_taxList(chkIncludeInactive.CheckState, grdTax)
        grdSettings()
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub
    Private Sub grdSettings()
        With grdTax
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns(0).Visible = False
            .Columns(1).Visible = False
            .Columns.Item(1).HeaderText = "Active"
            .Columns.Item(2).HeaderText = "Tax Name"
            .Columns.Item(3).HeaderText = "Rate %"
            If chkIncludeInactive.CheckState = CheckState.Checked Then
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).Visible = True
            End If
        End With
    End Sub

    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        m_taxList(chkIncludeInactive.CheckState, grdTax)
        grdSettings()
    End Sub

    Private Sub grdTax_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdTax.CellClick
        If Me.grdTax.SelectedCells.Count > 0 Then
            sKeyTax = grdTax.CurrentRow.Cells(0).Value.ToString

            If m_isActive("mSalesTax", "fxKeyTax", sKeyTax) Then
                ts_MkInactive.Text = "Make Inactive"
            Else
                ts_MkInactive.Text = "Make Active"
            End If
        End If
    End Sub

    Private Sub grdTax_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdTax.DoubleClick
        If Me.grdTax.SelectedCells.Count > 0 Then
            frm_MF_taxAddEdit.MdiParent = Me.MdiParent
            frm_MF_taxAddEdit.KeyTax = sKeyTax
            frm_MF_taxAddEdit.Text = "Edit Sales Tax"
            frm_MF_taxAddEdit.Show()
        End If
    End Sub

    Private Sub deleteTax()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mSalesTax WHERE fxKeyTax = '" & sKeyTax & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub

    Private Sub ts_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Delete.Click
        deleteTax()
        refreshForm()
    End Sub

    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub ts_showInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_showInactive.Click
        chkIncludeInactive.Checked = True
    End Sub

    Private Sub ts_MkInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_MkInactive.Click
        If ts_MkInactive.Text = "Make Active" Then
            m_mkActive("mSalesTax", "fxKeyTax", sKeyTax, True)
        Else
            m_mkActive("mSalesTax", "fxKeyTax", sKeyTax, False)
        End If
        refreshForm()
    End Sub

End Class