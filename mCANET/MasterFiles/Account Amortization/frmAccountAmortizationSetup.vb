﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes
Imports System.Threading, System.IO
Imports System.Data.OleDb

Public Class frmAccountAmortizationSetup

    Private gCon As New Clsappconfiguration()
    Dim acntKey As String
    Dim empKey As String
    Dim PkID As Integer = 0

    Dim filenym As String
    Dim fpath As String
    Private UploaderType As String
    Private xFilePath As String = ""

    Dim Col As String
    Dim ColVal As String

#Region "Data Access Layer"

    Private Sub AddColumn(ByVal ColumnID As String, ByVal ColumnName As String)
        Try
            'Dim column As String() =
            ' {PkAccount, FkAccount, FcAcntCode, FcAcntTitle}

            'Dim nColumnIndex As Integer
            With grdAmortization

                .Columns.Add(ColumnID, ColumnName)
                '.ClearSelection()
                'nColumnIndex = .Columns.Count - 1
                '.FirstDisplayedScrollingRowIndex = nColumnIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SelectColumns()
        grdAmortization.Columns.Clear()
        With grdAmortization
            .Columns.Add("FnPayNo", "Pay No.")
            .Columns.Add("FdDate", "Date")
        End With
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationColumn_ListByAccountID",
                                                     New SqlParameter("@acntID", acntKey))
        While rd.Read = True
            AddColumn(rd.Item(0).ToString, rd.Item(1).ToString)
        End While
    End Sub

    Private Sub GenerateTemplate()
        Try
            Dim xlApp As Microsoft.Office.Interop.Excel.Application
            Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
            Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
            Dim misValue As Object = System.Reflection.Missing.Value
            Dim i As Integer
            Dim j As Integer

            xlApp = New Microsoft.Office.Interop.Excel.Application
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("sheet1")

            With xlWorkSheet
                For Each column As DataGridViewColumn In grdAmortization.Columns
                    .Cells(1, column.Index + 1) = column.HeaderText
                Next
                For i = 0 To grdAmortization.RowCount - 1
                    For j = 0 To grdAmortization.ColumnCount - 1
                        For k As Integer = 1 To grdAmortization.Columns.Count
                            xlWorkSheet.Cells(1, k) = grdAmortization.Columns(k - 1).HeaderText
                            xlWorkSheet.Cells(i + 2, j + 1) = grdAmortization(j, i).Value
                        Next
                    Next
                Next
            End With

            xlWorkSheet.SaveAs(Path.GetFullPath(SaveFileDialog1.FileName))
            xlWorkBook.Close()
            xlApp.Quit()

            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)

            Dim res As MsgBoxResult
            res = MsgBox("Process completed, Would you like to open file?", MsgBoxStyle.YesNo)
            If (res = MsgBoxResult.Yes) Then
                Process.Start(Path.GetFullPath(SaveFileDialog1.FileName))
            End If
        Catch ex As Exception
            Exit Sub
        End Try

    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Import()
        grdAmortization.Rows.Clear()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyConnection.Open()
        Dim dbCommand As New OleDbCommand("select * from [Sheet1$]", MyConnection)
        Dim rd As OleDbDataReader
        Dim xCol As Integer


        rd = dbCommand.ExecuteReader
        Try
            Dim xRow As Integer = 0
            While rd.Read = True
                grdAmortization.Rows.Add()
                xCol = rd.FieldCount
                'Dim row(xCol) As String
                For i As Integer = 0 To xCol - 1
                    grdAmortization.Item(i, xRow).Value = rd.Item(i).ToString
                Next
                xRow += 1
                'grdList.Rows.Add(row)
            End While
            rd.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Save(ByVal FnPayNo As Integer, ByVal FdDate As Date, ByVal ColID As String, ByVal ColValue As String)
        Try
            Dim myid As New Guid(acntKey)
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_AccountAmortizationDetails_InsertUpdate",
                                                         New SqlParameter("@PkID", PkID),
                                                         New SqlParameter("@FkAccount", myid),
                                                         New SqlParameter("@FcColumnID", ColID),
                                                         New SqlParameter("@FcRefNo", txtRefNo.Text),
                                                         New SqlParameter("@FnPayNo", FnPayNo),
                                                         New SqlParameter("@FdDate", FdDate),
                                                         New SqlParameter("@FcAmount", ColValue))
            MsgBox("Saved!", MsgBoxStyle.Information, "Account Amortization Setup")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    'Private Sub LoadDebitCredit()
    '    'KeyAccountID = Nothing
    '    Dim myid As New Guid(gCompanyID)
    '    Dim ds As New DataSet
    '    Dim ad As New SqlDataAdapter
    '    Dim cmd As New SqlCommand("spu_AccountAmortization_View", gCon.sqlconn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.Add("@coid", SqlDbType.UniqueIdentifier).Value = myid
    '    Try
    '        ad.SelectCommand = cmd
    '        ad.Fill(ds, "Accounts")
    '        With cboAccount
    '            .ValueMember = "FkAccount"
    '            .DisplayMember = "acntwithcode"
    '            .DataSource = ds.Tables(0)
    '            .SelectedIndex = -1
    '            .Text = "Select"
    '        End With
    '        gCon.sqlconn.Close()
    '    Catch ex As Exception

    '    End Try

    'End Sub


#End Region

#Region "Event Handler"

    Private Sub btnSearchAcntRef_Click(sender As System.Object, e As System.EventArgs) Handles btnSearchAcntRef.Click
        Dim frm As New frmSLfilter
        frm.ShowDialog()
        If frm.DialogResult = DialogResult.OK Then
            txtRefNo.Text = frm.grdSAList.SelectedRows(0).Cells("fcDocNumber").Value.ToString()
            txtID.Text = frm.grdSAList.SelectedRows(0).Cells("fcEmployeeNo").Value.ToString()
            txtName.Text = frm.grdSAList.SelectedRows(0).Cells("fcFullname").Value.ToString()
            txtCode.Text = frm.grdSAList.SelectedRows(0).Cells("fcAccountCode").Value.ToString()
            txtTitle.Text = frm.grdSAList.SelectedRows(0).Cells("fcAccountName").Value.ToString()

            acntKey = frm.grdSAList.SelectedRows(0).Cells("fxKey_Account").Value.ToString()
            empKey = frm.grdSAList.SelectedRows(0).Cells("pk_Employee").Value.ToString()
            SelectColumns()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnGenerateTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateTemplate.Click
        SaveFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
        SaveFileDialog1.ShowDialog()
        If DialogResult.OK Then
            GenerateTemplate()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnUpload_Click(sender As System.Object, e As System.EventArgs) Handles btnUpload.Click
        Try
            OpenFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPath.Text = fpath
            xFilePath = txtPath.Text
            UploaderType = IO.Path.GetExtension(xFilePath)
            If DialogResult.OK Then
                Import()
            Else
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        For xRow As Integer = 0 To grdAmortization.RowCount - 1
            For i As Integer = 0 To grdAmortization.ColumnCount - 1
                Dim CellValue As String = grdAmortization.Item(i, xRow).Value.ToString()
                Dim CellHeader As String = grdAmortization.Columns(i).Name.ToString
                If i > 1 Then
                    If Col = "" Then
                        Col = Col + CellHeader
                    Else
                        Col = Col + ", " + CellHeader
                    End If
                End If

                If i > 1 Then
                    If ColVal = "" Then
                        ColVal = ColVal + CellValue
                    Else
                        ColVal = ColVal + ", " + CellValue
                    End If
                End If

            Next
            Save(grdAmortization.Item(0, xRow).Value, grdAmortization.Item(1, xRow).Value, Col, ColVal)
            Col = ""
            ColVal = ""
        Next
    End Sub

    Private Sub frmAccountAmortizationSetup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'LoadDebitCredit()
    End Sub

#End Region
End Class