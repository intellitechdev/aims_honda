﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLedgerFilter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkSubLedger = New System.Windows.Forms.CheckBox()
        Me.chkPayLedger = New System.Windows.Forms.CheckBox()
        Me.btnProceed = New System.Windows.Forms.Button()
        Me.btnCLose = New System.Windows.Forms.Button()
        Me.chkSLAccounts = New System.Windows.Forms.CheckBox()
        Me.chkDebit = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'chkSubLedger
        '
        Me.chkSubLedger.AutoSize = True
        Me.chkSubLedger.Location = New System.Drawing.Point(21, 17)
        Me.chkSubLedger.Name = "chkSubLedger"
        Me.chkSubLedger.Size = New System.Drawing.Size(205, 17)
        Me.chkSubLedger.TabIndex = 0
        Me.chkSubLedger.Text = "Print Subsidiary Ledger - Loans"
        Me.chkSubLedger.UseVisualStyleBackColor = True
        '
        'chkPayLedger
        '
        Me.chkPayLedger.AutoSize = True
        Me.chkPayLedger.Location = New System.Drawing.Point(21, 119)
        Me.chkPayLedger.Name = "chkPayLedger"
        Me.chkPayLedger.Size = New System.Drawing.Size(149, 17)
        Me.chkPayLedger.TabIndex = 1
        Me.chkPayLedger.Text = "Print Payment Ledger"
        Me.chkPayLedger.UseVisualStyleBackColor = True
        '
        'btnProceed
        '
        Me.btnProceed.Location = New System.Drawing.Point(56, 160)
        Me.btnProceed.Name = "btnProceed"
        Me.btnProceed.Size = New System.Drawing.Size(75, 23)
        Me.btnProceed.TabIndex = 2
        Me.btnProceed.Text = "Proceed"
        Me.btnProceed.UseVisualStyleBackColor = True
        '
        'btnCLose
        '
        Me.btnCLose.Location = New System.Drawing.Point(140, 160)
        Me.btnCLose.Name = "btnCLose"
        Me.btnCLose.Size = New System.Drawing.Size(75, 23)
        Me.btnCLose.TabIndex = 3
        Me.btnCLose.Text = "Close"
        Me.btnCLose.UseVisualStyleBackColor = True
        '
        'chkSLAccounts
        '
        Me.chkSLAccounts.AutoSize = True
        Me.chkSLAccounts.Location = New System.Drawing.Point(21, 52)
        Me.chkSLAccounts.Name = "chkSLAccounts"
        Me.chkSLAccounts.Size = New System.Drawing.Size(207, 17)
        Me.chkSLAccounts.TabIndex = 4
        Me.chkSLAccounts.Text = "Print Subsidiary Ledger - Credit"
        Me.chkSLAccounts.UseVisualStyleBackColor = True
        '
        'chkDebit
        '
        Me.chkDebit.AutoSize = True
        Me.chkDebit.Location = New System.Drawing.Point(21, 85)
        Me.chkDebit.Name = "chkDebit"
        Me.chkDebit.Size = New System.Drawing.Size(202, 17)
        Me.chkDebit.TabIndex = 5
        Me.chkDebit.Text = "Print Subsidiary Ledger - Debit"
        Me.chkDebit.UseVisualStyleBackColor = True
        '
        'frmLedgerFilter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(274, 191)
        Me.Controls.Add(Me.chkDebit)
        Me.Controls.Add(Me.chkSLAccounts)
        Me.Controls.Add(Me.btnCLose)
        Me.Controls.Add(Me.btnProceed)
        Me.Controls.Add(Me.chkPayLedger)
        Me.Controls.Add(Me.chkSubLedger)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmLedgerFilter"
        Me.Text = "Print Ledger"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkSubLedger As System.Windows.Forms.CheckBox
    Friend WithEvents chkPayLedger As System.Windows.Forms.CheckBox
    Friend WithEvents btnProceed As System.Windows.Forms.Button
    Friend WithEvents btnCLose As System.Windows.Forms.Button
    Friend WithEvents chkSLAccounts As System.Windows.Forms.CheckBox
    Friend WithEvents chkDebit As System.Windows.Forms.CheckBox
End Class
