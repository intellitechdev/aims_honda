﻿Public Class frmLedgerFilter

    Public creditRef As String
    Public debitref As String

    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        If chkSubLedger.Checked = True Then
            frmPrint.LoadLoanLedger2(frmVerification.lblLoanRef.Text)
            frmPrint.StartPosition = FormStartPosition.CenterScreen
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
            Exit Sub
        End If
        If chkPayLedger.Checked = True Then
            frmPrint.LoadLoanLedger(frmVerification.lblLoanRef.Text)
            frmPrint.StartPosition = FormStartPosition.CenterScreen
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
            Exit Sub
        End If
        If chkSLAccounts.Checked = True Then
            frmPrint._PrintLedger_Credit(frmVerification.lblIDno.Text, creditRef)
            frmPrint.StartPosition = FormStartPosition.CenterScreen
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
            Exit Sub
        End If
        If chkDebit.Checked Then
            frmPrint._PrintLedger_Debit(frmVerification.lblIDno.Text, debitref)
            frmPrint.StartPosition = FormStartPosition.CenterScreen
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
            Exit Sub
        End If
    End Sub

    Private Sub btnCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCLose.Click
        Me.Close()
    End Sub

    Private Sub chkSubLedger_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSubLedger.CheckedChanged
        If chkSubLedger.Checked = True Then
            chkPayLedger.Checked = False
            chkSLAccounts.Checked = False
            chkDebit.Checked = False
        End If
    End Sub

    Private Sub chkPayLedger_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkPayLedger.CheckedChanged
        If chkPayLedger.Checked = True Then
            chkSubLedger.Checked = False
            chkSLAccounts.Checked = False
            chkDebit.Checked = False
        End If
    End Sub

    Private Sub chkSLAccounts_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSLAccounts.CheckedChanged
        If chkSLAccounts.Checked = True Then
            chkSubLedger.Checked = False
            chkPayLedger.Checked = False
            chkDebit.Checked = False
        End If
    End Sub

    Private Sub chkDebit_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkDebit.CheckedChanged
        If chkDebit.Checked = True Then
            chkSLAccounts.Checked = False
            chkSubLedger.Checked = False
            chkPayLedger.Checked = False
        End If
    End Sub

End Class