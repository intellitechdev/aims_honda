<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_customizecolumns
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_customizecolumns))
        Me.Label1 = New System.Windows.Forms.Label
        Me.lstAvailableCols = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnChoseAll = New System.Windows.Forms.Button
        Me.btnChoseOne = New System.Windows.Forms.Button
        Me.btnRestoreOne = New System.Windows.Forms.Button
        Me.btnRestoreAll = New System.Windows.Forms.Button
        Me.lstChosenCols = New System.Windows.Forms.ListBox
        Me.grpCustomizecols = New System.Windows.Forms.GroupBox
        Me.btndefaultcols = New System.Windows.Forms.Button
        Me.btnOkcols = New System.Windows.Forms.Button
        Me.btncancelcols = New System.Windows.Forms.Button
        Me.grpCustomizecols.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Available Columns"
        '
        'lstAvailableCols
        '
        Me.lstAvailableCols.FormattingEnabled = True
        Me.lstAvailableCols.Items.AddRange(New Object() {"Active", "Account", "Account Type", "Account Code", "Balance Total", "Balance", "Budget", "Budget Beginning Date", "Description"})
        Me.lstAvailableCols.Location = New System.Drawing.Point(15, 38)
        Me.lstAvailableCols.Name = "lstAvailableCols"
        Me.lstAvailableCols.Size = New System.Drawing.Size(152, 186)
        Me.lstAvailableCols.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(218, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Chosen Columns"
        '
        'btnChoseAll
        '
        Me.btnChoseAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChoseAll.Location = New System.Drawing.Point(175, 68)
        Me.btnChoseAll.Name = "btnChoseAll"
        Me.btnChoseAll.Size = New System.Drawing.Size(40, 20)
        Me.btnChoseAll.TabIndex = 4
        Me.btnChoseAll.Text = ">>"
        Me.btnChoseAll.UseVisualStyleBackColor = True
        '
        'btnChoseOne
        '
        Me.btnChoseOne.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChoseOne.Location = New System.Drawing.Point(175, 94)
        Me.btnChoseOne.Name = "btnChoseOne"
        Me.btnChoseOne.Size = New System.Drawing.Size(40, 20)
        Me.btnChoseOne.TabIndex = 5
        Me.btnChoseOne.Text = ">"
        Me.btnChoseOne.UseVisualStyleBackColor = True
        '
        'btnRestoreOne
        '
        Me.btnRestoreOne.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRestoreOne.Location = New System.Drawing.Point(175, 154)
        Me.btnRestoreOne.Name = "btnRestoreOne"
        Me.btnRestoreOne.Size = New System.Drawing.Size(40, 20)
        Me.btnRestoreOne.TabIndex = 6
        Me.btnRestoreOne.Text = "<"
        Me.btnRestoreOne.UseVisualStyleBackColor = True
        '
        'btnRestoreAll
        '
        Me.btnRestoreAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRestoreAll.Location = New System.Drawing.Point(175, 180)
        Me.btnRestoreAll.Name = "btnRestoreAll"
        Me.btnRestoreAll.Size = New System.Drawing.Size(40, 20)
        Me.btnRestoreAll.TabIndex = 7
        Me.btnRestoreAll.Text = "<<"
        Me.btnRestoreAll.UseVisualStyleBackColor = True
        '
        'lstChosenCols
        '
        Me.lstChosenCols.FormattingEnabled = True
        Me.lstChosenCols.Location = New System.Drawing.Point(222, 38)
        Me.lstChosenCols.Name = "lstChosenCols"
        Me.lstChosenCols.Size = New System.Drawing.Size(151, 186)
        Me.lstChosenCols.TabIndex = 8
        '
        'grpCustomizecols
        '
        Me.grpCustomizecols.Controls.Add(Me.btndefaultcols)
        Me.grpCustomizecols.Controls.Add(Me.Label2)
        Me.grpCustomizecols.Controls.Add(Me.lstChosenCols)
        Me.grpCustomizecols.Controls.Add(Me.Label1)
        Me.grpCustomizecols.Controls.Add(Me.btnRestoreAll)
        Me.grpCustomizecols.Controls.Add(Me.lstAvailableCols)
        Me.grpCustomizecols.Controls.Add(Me.btnRestoreOne)
        Me.grpCustomizecols.Controls.Add(Me.btnChoseAll)
        Me.grpCustomizecols.Controls.Add(Me.btnChoseOne)
        Me.grpCustomizecols.Location = New System.Drawing.Point(8, 8)
        Me.grpCustomizecols.Name = "grpCustomizecols"
        Me.grpCustomizecols.Size = New System.Drawing.Size(391, 259)
        Me.grpCustomizecols.TabIndex = 9
        Me.grpCustomizecols.TabStop = False
        Me.grpCustomizecols.Text = "Chose Columns to be displayed"
        '
        'btndefaultcols
        '
        Me.btndefaultcols.Location = New System.Drawing.Point(266, 230)
        Me.btndefaultcols.Name = "btndefaultcols"
        Me.btndefaultcols.Size = New System.Drawing.Size(107, 22)
        Me.btndefaultcols.TabIndex = 9
        Me.btndefaultcols.Text = "Back to Default"
        Me.btndefaultcols.UseVisualStyleBackColor = True
        '
        'btnOkcols
        '
        Me.btnOkcols.Location = New System.Drawing.Point(220, 273)
        Me.btnOkcols.Name = "btnOkcols"
        Me.btnOkcols.Size = New System.Drawing.Size(84, 22)
        Me.btnOkcols.TabIndex = 10
        Me.btnOkcols.Text = "Ok"
        Me.btnOkcols.UseVisualStyleBackColor = True
        '
        'btncancelcols
        '
        Me.btncancelcols.Location = New System.Drawing.Point(308, 273)
        Me.btncancelcols.Name = "btncancelcols"
        Me.btncancelcols.Size = New System.Drawing.Size(84, 22)
        Me.btncancelcols.TabIndex = 11
        Me.btncancelcols.Text = "Cancel"
        Me.btncancelcols.UseVisualStyleBackColor = True
        '
        'frm_acc_customizecolumns
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(406, 302)
        Me.Controls.Add(Me.btncancelcols)
        Me.Controls.Add(Me.btnOkcols)
        Me.Controls.Add(Me.grpCustomizecols)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(414, 335)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(414, 335)
        Me.Name = "frm_acc_customizecolumns"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Customize Columns - Chart of Accounts"
        Me.grpCustomizecols.ResumeLayout(False)
        Me.grpCustomizecols.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lstAvailableCols As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnChoseAll As System.Windows.Forms.Button
    Friend WithEvents btnChoseOne As System.Windows.Forms.Button
    Friend WithEvents btnRestoreOne As System.Windows.Forms.Button
    Friend WithEvents btnRestoreAll As System.Windows.Forms.Button
    Friend WithEvents lstChosenCols As System.Windows.Forms.ListBox
    Friend WithEvents grpCustomizecols As System.Windows.Forms.GroupBox
    Friend WithEvents btndefaultcols As System.Windows.Forms.Button
    Friend WithEvents btnOkcols As System.Windows.Forms.Button
    Friend WithEvents btncancelcols As System.Windows.Forms.Button
End Class
