Public Class frmAcntDetails

    Private Sub frmAcntDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call load_transactions()
        Call compute_totals()
    End Sub

    Private Sub load_transactions()
        With grdAcntDetails
            Dim dtsDetails As DataTable = m_GetAccountDetails().Tables(0)
            .DataSource = dtsDetails.DefaultView

            .Columns("Ref #").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter
            .Columns("Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter

            .Columns("Code").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter
            .Columns("Credit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("Debit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight

            .Columns("Credit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Debit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Date").DefaultCellStyle.Format = "MM/dd/yyyy"

            .Columns(0).Visible = False
            .Columns(1).Width = 100
            .Columns(2).Width = 100
            .Columns(3).Width = 250
            .Columns(4).Width = 350
            .Columns(5).Width = 150
            .Columns(6).Width = 150
        End With
    End Sub

    Private Sub compute_totals()
        With Me.grdAcntDetails
            Dim xRow As Integer
            Dim xDebit As Decimal = 0.0
            Dim xCredit As Decimal = 0.0
            For xRow = 0 To .Rows.Count - 1
                If Not .Rows(xRow).Cells(1).Value Is Nothing Then
                    xDebit += CDec(.Rows(xRow).Cells("Debit").Value)
                End If
                If Not .Rows(xRow).Cells(1).Value Is Nothing Then
                    xCredit += CDec(.Rows(xRow).Cells("Credit").Value)
                End If
            Next
            txtDebit.Text = Format(CDec(xDebit), "##,##0.00")
            txtCredit.Text = Format(CDec(xCredit), "##,##0.00")
        End With
    End Sub

    Private Sub grdAcntDetails_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAcntDetails.CellContentClick

    End Sub

    Private Sub btnCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCLose.Click
        Me.Close()
    End Sub
End Class