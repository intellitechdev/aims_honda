Public Class frm_acc_chartaccounts

    Private gMaster As New modMasterFile
    Private gTrans As New clsTransactionFunctions
    Private sKeyTerms As String

    Private Sub frm_acc_chartaccounts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        grefreshGrd()
    End Sub
    Private Sub frm_acc_chartaccounts_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        'Dim defaultlocation As New Point(211, 402)
        Dim defaultsize As New Size(668, 395)
        If tsChartofAccounts.Location.Y = 397 Then
            '  chkIncludeInactive.Location = defaultlocation
            grdAccounts.Size = defaultsize
        ElseIf tsChartofAccounts.Location.Y <> 397 Then
            grdAccounts.Size = New Size(668, 687)
            ' chkIncludeInactive.Location = New Point(211, ToolStrip1.Location.Y + 5)
        End If
    End Sub

    Private Sub msNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msNew.Click
        gAccountID = ""
        frm_acc_newaccount.Show()
    End Sub
    Private Sub msEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msEdit.Click
        Try
            With Me.grdAccounts
                gEditEvent = 1
                gAcntType = .CurrentRow.Cells("Account Type").Value.ToString
                gAccountID = .CurrentRow.Cells("AccountID").Value.ToString
                frm_acc_editaccount.Text = "Edit Account"
                frm_acc_editaccount.ShowDialog()
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub msCustomize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msCustomize.Click
        frm_acc_customizecolumns.ShowDialog()
    End Sub
    Private Sub msWriteChecks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msWriteChecks.Click
        frm_acc_writechecks.ShowDialog()
    End Sub
    Private Sub msMkeDeposits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msMkeDeposits.Click
        frm_acc_makedeposit.ShowDialog()
    End Sub
    Private Sub msTransferFunds_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msTransferFunds.Click
        frm_acc_transferfunds.ShowDialog()
    End Sub
    Private Sub msGenJournalEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msGenJournalEntry.Click
        frm_acc_makeGeneralJournalEntry.ShowDialog()
    End Sub
    Private Sub msReconcile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msReconcile.Click
        frm_acc_reconcile.ShowDialog()
    End Sub
    Private Sub msUseRegister_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msUseRegister.Click
        frm_acc_accountregistry.ShowDialog()
    End Sub
    Private Sub msWTrialBal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msWTrialBal.Click
        frm_acc_workingtrialbalance.ShowDialog()
    End Sub
    Private Sub msShowInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msShowInactive.Click
        gLoadAccounts(Me.grdAccounts, 2, "Inactive Only", Me)
        Arrange_grdAccounts(grdAccounts)
    End Sub
    Private Sub msMkeInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msMkeInactive.Click
        If grdAccounts.SelectedRows.Count > 0 Then
            Try
                If msMkeInactive.Text = "Make Active" Then
                    m_mkActive("mAccounts", "acnt_id", sKeyTerms, True)
                Else
                    m_mkActive("mAccounts", "acnt_id", sKeyTerms, False)
                End If
                grefreshGrd()
            Catch ex As Exception
            End Try
        Else
            MsgBox("Please Select an Account First.", MsgBoxStyle.Information, "Change Account Status")
        End If
        
    End Sub
    Private Sub msDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msDelete.Click
        Try
            Dim stVal As String = grdAccounts.CurrentRow.Cells(0).Value.ToString
            gAccountID = stVal
            gMaster.gDeleteAccounts(Me)
            Call grefreshGrd()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub grdAccounts_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAccounts.CellClick
        Try
            If grdAccounts.SelectedRows.Count > 0 AndAlso Not grdAccounts.SelectedRows(0).Index = _
                                   grdAccounts.Rows.Count - 1 Then
                sKeyTerms = grdAccounts.CurrentRow.Cells("AccountID").Value.ToString
                If m_isActive("mAccounts", "acnt_id", sKeyTerms) Then
                    msMkeInactive.Text = "Make Inactive"
                Else
                    msMkeInactive.Text = "Make Active"
                End If
            End If
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.MsgBoxHelp, Me.Text)
        End Try
    End Sub
    Private Sub grdAccounts_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAccounts.CellDoubleClick
        Try
            If grdAccounts.SelectedRows.Count > 0 AndAlso Not grdAccounts.SelectedRows(0).Index = _
                                   grdAccounts.Rows.Count - 1 Then
                gKeyAccount = grdAccounts.CurrentRow.Cells("AccountID").Value.ToString
                frmAcntDetails.ShowDialog()
            End If
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.MsgBoxHelp, Me.Text)
        End Try
    End Sub
    Private Sub grefreshGrd()
        Me.grdAccounts.Columns.Clear()
        If chkIncludeInactive.Checked = True Then
            Call gLoadAccounts(Me.grdAccounts, 0, "", Me)
            Arrange_grdAccounts(grdAccounts)
        Else
            Call gLoadAccounts(Me.grdAccounts, 1, "", Me)
            Arrange_grdAccounts(grdAccounts)
        End If
    End Sub

    Private Sub Arrange_grdAccounts(ByVal grdAccount As DataGridView)
        Dim Index As Integer

        With grdAccount
            'Header Alignment
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.TopCenter

            'to correct header alignment
            For Index = 0 To .Columns.Count - 1
                .Columns(Index).HeaderText = "     " & .Columns(Index).Name
            Next

            'Visibility
            .Columns("AccountID").Visible = False

            'Width
            .Columns("Active").Width = 70
            .Columns("Account Type").Width = 150
            .Columns("Balance Total").Width = 140
            .Columns("Account").Width = 170
            .Columns("Account Code").Width = 170
            .Columns("Budget Beginning Date").Width = 200
            .Columns("Description").Width = 400

            'Alignment
            .Columns("Account Type").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Balance Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Balance").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Budget").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Budget Beginning Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            'Format
            .Columns("Balance Total").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Balance").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Budget").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Budget Beginning Date").DefaultCellStyle.Format = "MM/dd/yyyy"

        End With
    End Sub

    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        Call grefreshGrd()
    End Sub
    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        msShowInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub grdAccounts_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdAccounts.CellContentClick

    End Sub
End Class
