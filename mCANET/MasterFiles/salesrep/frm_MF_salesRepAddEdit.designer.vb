<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_MF_salesRepAddEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboSalesRepID = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtInitials = New System.Windows.Forms.TextBox
        Me.txtType = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.chkInactive = New System.Windows.Forms.CheckBox
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOk = New System.Windows.Forms.Button
        Me.cboSalesRepName = New System.Windows.Forms.ComboBox
        Me.txtSalesRepName = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Sales Rep Name"
        '
        'cboSalesRepID
        '
        Me.cboSalesRepID.FormattingEnabled = True
        Me.cboSalesRepID.Location = New System.Drawing.Point(105, 19)
        Me.cboSalesRepID.Name = "cboSalesRepID"
        Me.cboSalesRepID.Size = New System.Drawing.Size(165, 21)
        Me.cboSalesRepID.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Sales Rep Initials"
        '
        'txtInitials
        '
        Me.txtInitials.Location = New System.Drawing.Point(105, 52)
        Me.txtInitials.Name = "txtInitials"
        Me.txtInitials.Size = New System.Drawing.Size(165, 21)
        Me.txtInitials.TabIndex = 3
        '
        'txtType
        '
        Me.txtType.BackColor = System.Drawing.SystemColors.Window
        Me.txtType.Location = New System.Drawing.Point(106, 84)
        Me.txtType.Name = "txtType"
        Me.txtType.ReadOnly = True
        Me.txtType.Size = New System.Drawing.Size(165, 21)
        Me.txtType.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Sales RepType"
        '
        'chkInactive
        '
        Me.chkInactive.AutoSize = True
        Me.chkInactive.Location = New System.Drawing.Point(106, 116)
        Me.chkInactive.Name = "chkInactive"
        Me.chkInactive.Size = New System.Drawing.Size(65, 17)
        Me.chkInactive.TabIndex = 6
        Me.chkInactive.Text = "Inactive"
        Me.chkInactive.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(49, 159)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.TabIndex = 8
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(205, 159)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(127, 159)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 10
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'cboSalesRepName
        '
        Me.cboSalesRepName.FormattingEnabled = True
        Me.cboSalesRepName.Location = New System.Drawing.Point(105, 19)
        Me.cboSalesRepName.Name = "cboSalesRepName"
        Me.cboSalesRepName.Size = New System.Drawing.Size(165, 21)
        Me.cboSalesRepName.TabIndex = 12
        '
        'txtSalesRepName
        '
        Me.txtSalesRepName.BackColor = System.Drawing.SystemColors.Window
        Me.txtSalesRepName.Location = New System.Drawing.Point(105, 19)
        Me.txtSalesRepName.Name = "txtSalesRepName"
        Me.txtSalesRepName.ReadOnly = True
        Me.txtSalesRepName.Size = New System.Drawing.Size(147, 21)
        Me.txtSalesRepName.TabIndex = 13
        '
        'frm_MF_salesRepAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 196)
        Me.Controls.Add(Me.txtSalesRepName)
        Me.Controls.Add(Me.cboSalesRepName)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.chkInactive)
        Me.Controls.Add(Me.txtType)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtInitials)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboSalesRepID)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_MF_salesRepAddEdit"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "New Sales Rep"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboSalesRepID As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtInitials As System.Windows.Forms.TextBox
    Friend WithEvents txtType As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkInactive As System.Windows.Forms.CheckBox
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    ' Friend WithEvents AccountingDataSet As mCANET.AccountingDataSet
    Friend WithEvents cboSalesRepName As System.Windows.Forms.ComboBox
    Friend WithEvents txtSalesRepName As System.Windows.Forms.TextBox
    'Friend WithEvents SPU_SalesRepOption_ListTableAdapter As mCANET.AccountingDataSetTableAdapters.SPU_SalesRepOption_ListTableAdapter
End Class
