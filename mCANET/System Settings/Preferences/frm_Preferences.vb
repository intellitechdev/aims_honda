Public Class frm_Preferences

    Private Sub frm_Preferences_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With grdPreferences
            .Rows.Add("Accounting")
            .Rows.Add("Bills")
            .Rows.Add("Checking")
            .Rows.Add("Desktop View")
            .Rows.Add("Finance Charge")
            .Rows.Add("General")
            .Rows.Add("Integrated Applications")
            .Rows.Add("Items & Inventory")
            .Rows.Add("Jobs & Estimates")
            .Rows.Add("Reminders")
            .Rows.Add("Reports & Graphs")
            .Rows.Add("Sales & Customers")
            .Rows.Add("Sales Tax")
            .Rows.Add("Send Forms")
            .Rows.Add("Service Connection")
            .Rows.Add("Spelling")
            .Rows.Add("Tax")
            .Rows.Add("Time Tracking")
        End With
        tabSettings(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    End Sub

    Private Sub grdPreferences_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdPreferences.Click
        selectedPreferenceRow()
    End Sub

    Private Sub selectedPreferenceRow()
        Dim strPreference As String
        strPreference = grdPreferences.CurrentCell.Value
        Select Case strPreference
            Case "Accounting"
                tabSettings(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "Bills"
                tabSettings(0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "Checking"
                tabSettings(0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "Desktop View"
                tabSettings(0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "Finance Charge"
                tabSettings(0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "General"
                tabSettings(0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "Integrated Applications"
                tabSettings(0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "Items & Inventory"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "Jobs & Estimates"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "Reminders"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0)
            Case "Reports & Graphs"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0)
            Case "Sales & Customers"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0)
            Case "Sales Tax"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0)
            Case "Send Forms"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0)
            Case "Service Connection"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0)
            Case "Spelling"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0)
            Case "Tax"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0)
            Case "Time Tracking"
                tabSettings(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
            Case Else
                tabSettings(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        End Select
    End Sub

    Private Sub panelSettings(ByVal strPanelName As Panel)
        strPanelName.Visible = True
        strPanelName.Location = New Point(1, 0)
        strPanelName.Size = New Size(436, 380)
    End Sub

    Private Sub tabSettings(ByVal bAcctg As Boolean, _
    ByVal bBill As Boolean, ByVal bChecking As Boolean, _
    ByVal bDesktop As Boolean, ByVal bFinance As Boolean, _
    ByVal bGeneral As Boolean, ByVal bApplication As Boolean, _
    ByVal bItems As Boolean, ByVal bJobs As Boolean, _
    ByVal bReminders As Boolean, ByVal bReports As Boolean, _
    ByVal bSales As Boolean, ByVal bSalesTax As Boolean, _
    ByVal bSend As Boolean, ByVal bService As Boolean, _
    ByVal bspell As Boolean, ByVal bTax As Boolean, _
    ByVal bTime As Boolean)
        If bAcctg = True Then
            lblCaptionAcctg.Text = "Accounting"
            chkAcctgAutoFill.Text = "&Auto fill memo in general journal entry "
            panelSettings(pnlAccountingMyPref)
            panelSettings(pnlAccountingCoPref)
        ElseIf bReminders = True Then
            lblCaptionAcctg.Text = "Reminders"
            chkAcctgAutoFill.Text = "&Show reminders list when opening a company file"
            panelSettings(pnlAccountingMyPref)
        ElseIf bSend = True Then
            lblCaptionAcctg.Text = "Send Forms"
            chkAcctgAutoFill.Text = "Auto check the " & Chr(34) & "To be emailed" & Chr(34) & " checkbox if customer's preferred Send Method is Email"
            panelSettings(pnlAccountingMyPref)
        Else
            pnlAccountingMyPref.Visible = False
            pnlAccountingCoPref.Visible = False
        End If

        If bBill = True Then
            lblCaption.Text = "Bills"
            lblBill.Text = "There are no personal preferences for Bills."

            panelSettings(pnlBillMyPref)
            panelSettings(pnlBillCoPref)
        ElseIf bApplication = True Then
            lblCaption.Text = "Integrated Applications"
            lblBill.Text = "There are no personal preferences for Integrated Applications."
            panelSettings(pnlBillMyPref)
        ElseIf bItems = True Then
            lblCaption.Text = "Items & Inventory"
            lblBill.Text = "There are no personal preferences for Items & Inventory."
            panelSettings(pnlBillMyPref)
        ElseIf bJobs = True Then
            lblCaption.Text = "Jobs && Estimates"
            lblBill.Text = "There are no personal preferences for Jobs & Estimates."
            panelSettings(pnlBillMyPref)
        ElseIf bSalesTax = True Then
            lblCaption.Text = "Sales Tax"
            lblBill.Text = "There are no personal preferences for Sales Tax."
            panelSettings(pnlBillMyPref)
        ElseIf bTax = True Then
            lblCaption.Text = "Tax"
            lblBill.Text = "There are no personal preferences for Tax."
            panelSettings(pnlBillMyPref)
        ElseIf bTime = True Then
            lblCaption.Text = "Time Tracking"
            lblBill.Text = "There are no personal preferences for Time Tracking."
            panelSettings(pnlBillMyPref)
        Else
            pnlBillMyPref.Visible = False
            pnlBillCoPref.Visible = False
        End If

        If bChecking = True Then
            panelSettings(pnlCheckingMyPref)
            panelSettings(pnlCheckingCoPref)
        Else
            pnlCheckingMyPref.Visible = False
            pnlCheckingCoPref.Visible = False
        End If

        If bDesktop = True Then
            panelSettings(pnlDesktopMyPref)
            panelSettings(pnlDesktopCoPref)
        Else
            pnlDesktopMyPref.Visible = False
            pnlDesktopCoPref.Visible = False
        End If

        If bFinance = True Then
            panelSettings(pnlFinanceChargeMyPref)
            panelSettings(pnlFinanceChargeCoPref)
        Else
            pnlFinanceChargeMyPref.Visible = False
            pnlFinanceChargeCoPref.Visible = False
        End If

        If bGeneral = True Then
            panelSettings(pnlGeneralMyPref)
            panelSettings(pnlGeneralCoPref)
        Else
            pnlGeneralMyPref.Visible = False
            pnlGeneralCoPref.Visible = False
        End If

        If bApplication = True Then
            panelSettings(pnlIntegratedApplicationsCoPref)
        Else
            pnlIntegratedApplicationsCoPref.Visible = False
        End If

        If bItems = True Then
            panelSettings(pnlItemsCoPref)
        Else
            pnlItemsCoPref.Visible = False
        End If

        If bJobs = True Then
            panelSettings(pnlJobsCoPref)
        Else
            pnlJobsCoPref.Visible = False
        End If

        If bReminders = True Then
            panelSettings(pnlRemindersCoPref)
        Else
            pnlRemindersCoPref.Visible = False
        End If

        If bReports = True Then
            panelSettings(pnlReportsMyPref)
            panelSettings(pnlReportsCoPref)
        Else
            pnlReportsMyPref.Visible = False
            pnlReportsCoPref.Visible = False
        End If

        If bSales = True Then
            panelSettings(pnlSalesMyPref)
            panelSettings(pnlSalesCoPref)
        Else
            pnlSalesMyPref.Visible = False
            pnlSalesCoPref.Visible = False
        End If

        If bSalesTax = True Then
            panelSettings(pnlSTaxCoPref)
        Else
            pnlSTaxCoPref.Visible = False
        End If

        If bSend = True Then
            panelSettings(pnlSendCoPref)
        Else
            pnlSendCoPref.Visible = False
        End If

        If bService = True Then
            panelSettings(pnlServiceMyPref)
            panelSettings(pnlServiceCoPref)
        Else
            pnlServiceMyPref.Visible = False
            pnlServiceCoPref.Visible = False
        End If

        If bspell = True Then
            panelSettings(pnlSpellingCoPref)
            panelSettings(pnlSpellingMyPref)
        Else
            pnlSpellingCoPref.Visible = False
            pnlSpellingMyPref.Visible = False
        End If

        If bTime = True Then
            panelSettings(pnlTimeCoPref)
        Else
            pnlTimeCoPref.Visible = False
        End If

        If bTax = True Then
            panelSettings(pnlTaxCoPref)
        Else
            pnlTaxCoPref.Visible = False
        End If
    End Sub

   
End Class