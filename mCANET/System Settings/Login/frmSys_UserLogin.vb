Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmSys_UserLogin
    Private ClosingCondition As String
    Private gCon As New Clsappconfiguration

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call login()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If blnCloseMain = True Then
            End
        Else
            Me.Close()
        End If
    End Sub

    Public Sub login()

        If Username.Text <> "" Or Password.Text <> "" Then

            Dim sqlCmdText As String = "SYS_GetUser"
            sqlCmdText &= "  @LogName=" & Username.Text
            sqlCmdText &= ", @Password='" & Clsappconfiguration.GetEncryptedData(Password.Text) & "'"

            strSysCurrentPwd = Password.Text

            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sqlCmdText)
                    If rd.Read Then
                        intSysCurrentId = rd.Item("fxKeyUser")
                        strSysCurrentUserName = rd.Item("fcLogName")
                        strSysCurrentFullName = rd.Item("fcFullName")
                        strSysCurrentDesc = rd.Item("fcDescription")
                        strSysCurrentGroup = rd.Item("fcGroupName")
                        intSysCurrentGroupID = rd.Item("fxKeyGroup")
                        gUserName = rd.Item("fcLogName")
                        Timer2.Enabled = True
                        AuditTrail_Save("LOG-IN", "")
                    Else
                        MessageBox.Show("Login Failed", "UCORESOFTWARE: Login")
                    End If
                End Using
            Catch ex As Exception
                MessageBox.Show(Err.Description, "UCORESOFTWARE: Login")
            End Try
        Else
            MessageBox.Show("Username/Password is required to continue login into the system.", "UCORESOFTWARE: Login")
        End If
    End Sub

    Private Sub frmSys_UserLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim myDb As New Clsappconfiguration
        cbousertype.Text = ""
        blnCloseMain = True
        getCurDir()
        txtDatabase.Text = myDb.Database
        txtServer.Text = myDb.Server
        Username.Text = ""
        Password.Text = ""
    End Sub

    Private Sub Password_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            Call login()
        ElseIf e.Handled = True Then
            Call login()
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Dim a As String
        'a = Clsappconfiguration.GetDecryptedData(Password.Text)
        'MsgBox(a)
        Call login()
    End Sub

    Private Sub btnChangeDBSettings_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeDBSettings.Click
        frmSys_DatabaseSettings.ShowDialog()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim ConfirmClosing As String = MsgBox("Are you sure you want to exit?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Click Software- Accounting Systen")
        If ConfirmClosing = vbYes Then
            If blnCloseMain = True Then
                ClosingCondition = "Abort Launching"
                Timer2.Enabled = True
            Else
                Me.Close()
                Username.Text = ""
                Password.Text = ""
            End If
        Else
            Username.Focus()
        End If
    End Sub

    Private Sub Password_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Password.GotFocus
        Password.SelectAll()
    End Sub

    Private Sub Password_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Password.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            Call login()
        End If
    End Sub

    Private Sub Username_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Username.GotFocus
        Username.SelectAll()
    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Me.Opacity <> 1 Then
            Me.Opacity += 0.02
        Else
            Timer1.Enabled = False
        End If
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If Me.Opacity <> 0 Then
            Me.Opacity = Me.Opacity - 0.03
        Else
            If ClosingCondition = "Abort Launching" Then
                ClosingCondition = "Launch"
                End
            Else

                Me.Close()
                frmMain.Show()
            End If
            Timer2.Enabled = False
        End If


    End Sub

    Private Sub Password_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Password.TextChanged

    End Sub
End Class
