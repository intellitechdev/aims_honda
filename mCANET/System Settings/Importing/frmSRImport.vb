Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports vbAccelerator.Components.Controls

'TODO improve uploading by sales receipt summary
Public Class frmSRImport

    Private gcon As New Clsappconfiguration
    Private gTrans As New clsTransactionFunctions
    Private txtbox As New TextBox

#Region "Events"
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
        Me.Close()
    End Sub
    Private Sub btnBrowseSR_Summary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSR_Summary.Click
        displaySRDatatoGrid()
    End Sub
    Private Sub btnImportSR_Summary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportSR_Summary.Click
        If save_header() = True Then
            MsgBox("import successful...", MsgBoxStyle.Information, Me.Text)
        End If
    End Sub
    Private Sub btnBrowseSR_Particulars_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseSR_Particulars.Click
        displaySRDatatoGrid()
    End Sub
    Private Sub btnImportSR_Particulars_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportSR_Particulars.Click
        ProgressBarSRImport.Visible = True
        btnBrowseSR_Particulars.Enabled = False
        btnImportSR_Particulars.Enabled = False
        btnClose.Enabled = False
        BackgroundWorkerSRImport.RunWorkerAsync()
    End Sub

    Private Sub frmSRImport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If BackgroundWorkerSRImport.IsBusy Then
            MsgBox("Please Do not Close while Uploading." & vbNewLine & "This will damage your database.")
            e.Cancel = True
        End If
    End Sub
#End Region
#Region "Procedures/Functions"

    Private Sub displaySRDatatoGrid()

        Dim FilePath As String

        If Me.OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            FilePath = Me.OpenFile.FileName
        Else
            Exit Sub
        End If

        Dim SRDataset As DataSet = retrieveDataFromExcel(FilePath)

        Try
            'Counting Columns to determine table
            Dim CountColumns As Integer = countExcelSourceColumns(FilePath)

            Select Case CountColumns
                Case 4 'Summary
                    TabControl1.SelectTab("TabPageSummary")
                    With grdSummary
                        .DataSource = SRDataset
                        .AutoGenerateColumns = True
                        .DataMember = "Sales Receipt Data"
                    End With
                Case 6 'Particulars
                    TabControl1.SelectTab("TabPageParticulars")
                    With grdParticulars
                        .DataSource = SRDataset
                        .AutoGenerateColumns = True
                        .DataMember = "Sales Receipt Data"
                    End With
            End Select

        Catch ex As Exception

        End Try
    End Sub
    Public Function retrieveDataFromExcel(ByVal FilePath As String) As DataSet

        Dim excelConnection As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
                           FilePath & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
        Dim SRDataset = New DataSet

        Try
            excelConnection.Open()

            Dim TableSchema As DataTable = excelConnection.GetSchema("Tables")
            Dim sqlCommand As String = String.Format("SELECT * FROM [{0}]", TableSchema.Rows(0)("TABLE_NAME"))
            Dim exceladapter As New OleDbDataAdapter(sqlCommand, excelConnection)

            exceladapter.Fill(SRDataset, "Sales Receipt Data")
        Catch ex As OleDbException
            If ex.Message = "External table is not in the expected format." Then
                MsgBox("Please check the format of your Excel File." _
                        & vbNewLine & "Resave it to MS Excel 2003 Format." _
                        , MsgBoxStyle.Exclamation, "Browse SR File")
            End If
        Finally
            excelConnection.Close()
        End Try

        Return SRDataset
    End Function
    Public Function retrieveBarcodeSRDatafromExcel(ByVal FilePath As String) As DataSet
        Dim excelConnection As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
                          FilePath & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")

        excelConnection.Open()

        Dim TableSchema As DataTable = excelConnection.GetSchema("Tables")
        Dim sqlCommand As String = String.Format("SELECT barcode FROM [{0}]", TableSchema.Rows(0)("TABLE_NAME"))
        Dim exceladapter As New OleDbDataAdapter(sqlCommand, excelConnection)
        Dim SRDataset = New DataSet
        exceladapter.Fill(SRDataset, "Sales Receipt Data")

        excelConnection.Close()

        Return SRDataset
    End Function
    Public Function countExcelSourceColumns(ByVal FilePath As String) As Integer

        Dim NoOfColumns As Integer
        Dim excelConnection As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
                               FilePath & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
        excelConnection.Open()
        NoOfColumns = excelConnection.GetSchema("COLUMNS").Rows.Count
        excelConnection.Close()

        Return NoOfColumns
    End Function
    Private Function verifyIfBarcodesExist() As String

        Dim fcDescription As String = ""
        Dim sBarCode As String = ""
        Dim sDescription As String = ""
        Dim sAmount As Decimal = 0.0
        Dim rowcount As Integer
        Dim messageToUser As String
        Dim warning As String = "Some barcodes do not exist!"
        Dim NonExistingBarcodes As New List(Of String)
        Const FirstNonExistingBarcode As Integer = 1
        Dim count As Integer = 1

        With Me.grdParticulars
            For rowcount = 0 To .Rows.Count - 1

                If gTrans.gmkDefaultValues(rowcount, 2, grdParticulars) <> Nothing Then
                    sBarCode = (.Rows(rowcount).Cells(2).Value.ToString)
                End If
                If gTrans.gmkDefaultValues(rowcount, 3, grdParticulars) <> Nothing Then
                    sDescription = (.Rows(rowcount).Cells(3).Value)
                End If
                If gTrans.gmkDefaultValues(rowcount, 5, grdParticulars) <> Nothing Then
                    sAmount = (.Rows(rowcount).Cells(5).Value)
                End If

                Dim VerifierCommand As String = "SELECT TOP 1 fcDescription FROM mItemMaster "
                VerifierCommand &= "WHERE fcItemCode = '" & sBarCode & "'"
                VerifierCommand &= "OR fcBarcode = '" & sBarCode & "'"

                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, VerifierCommand)
                        If rd.Read() Then
                            fcDescription = rd.Item("fcDescription").ToString
                        Else
                            fcDescription = ""
                        End If
                    End Using
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Information, "Verify Barcode")
                End Try

                If fcDescription = "" Then

                    If count = FirstNonExistingBarcode Then
                        messageToUser = "The Following Barcodes do not exist in our database:"
                        messageToUser &= vbNewLine & "Please add these items first before proceeding."
                        messageToUser &= vbNewLine & sBarCode & " - " & sDescription
                        NonExistingBarcodes.Add(sBarCode)
                    Else
                        If Not NonExistingBarcodes.Contains(sBarCode) Then
                            messageToUser &= vbNewLine & sBarCode & " - " & sDescription
                            NonExistingBarcodes.Add(sBarCode)
                        End If
                    End If
                    count += 1
                End If

                If rowcount = .Rows.Count - 1 Then
                    If count > 1 Then
                        MsgBox(messageToUser, MsgBoxStyle.Exclamation)
                        warning = "Some barcodes do not exist!"
                    Else
                        warning = "All Barcodes Exist!"
                    End If
                End If
            Next
        End With

        Return warning
    End Function
    Private Function verifyIfSalesNoExist() As String
        Dim rowcount As Integer
        Dim count As Integer = 1
        Dim warning As String = "Some Sales No. do not exist!"
        Dim slocation_code As String = ""
        Dim tseqno As String = ""
        Dim sSaleNo As String = ""
        Dim saleNoInDatabase As String = ""
        Dim messageToUser = ""
        Dim NonExistingSalesNo As New List(Of String)
        Const FirstNonExistingSalesNO As Integer = 1

        With Me.grdParticulars
            For rowcount = 0 To .Rows.Count - 1
                'retrieves the Sales No from the gridview
                If gTrans.gmkDefaultValues(rowcount, 2, grdParticulars) <> Nothing Then
                    slocation_code = (.Rows(rowcount).Cells(0).Value.ToString)
                End If

                If gTrans.gmkDefaultValues(rowcount, 2, grdParticulars) <> Nothing Then
                    tseqno = (.Rows(rowcount).Cells(1).Value.ToString)
                End If

                sSaleNo = slocation_code + tseqno

                'evaluates if SalesNo Exist in our database
                Dim verifierCommand = "SELECT TOP 1 SaleNo FROM dbo.referenceNo_SaleNo_TransDate "
                verifierCommand &= "WHERE SaleNo = '" & sSaleNo & "'"

                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, verifierCommand)
                        If rd.Read() Then
                            saleNoInDatabase = rd.Item("SaleNo").ToString
                        Else
                            saleNoInDatabase = ""
                        End If
                    End Using
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Verify Sales No.")
                End Try

                If saleNoInDatabase = "" Then
                    If count = FirstNonExistingSalesNO Then
                        messageToUser = "The Following Sales No. do not exist in our database:"
                        messageToUser &= vbNewLine & "Please verify these first before proceeding."
                        messageToUser &= vbNewLine & sSaleNo
                        NonExistingSalesNo.Add(sSaleNo)
                    Else
                        If Not NonExistingSalesNo.Contains(sSaleNo) Then
                            messageToUser &= vbNewLine & sSaleNo
                            NonExistingSalesNo.Add(sSaleNo)
                        End If
                    End If
                    count += 1
                End If

                If rowcount = .Rows.Count - 1 Then
                    If count > 1 Then
                        MsgBox(messageToUser, MsgBoxStyle.Exclamation)
                        warning = "Some Sales No. do not exist!"
                    Else
                        warning = "All Sales No. Exist!"
                    End If
                End If

            Next
        End With

        Return warning
    End Function

    Private Sub loadpathHeader()
        If Me.OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtbox.Text = Me.OpenFile.FileName
            If gTrans.makelink_server(txtbox.Text) = "Successful..." Then
                Call verifygridhdr()
            Else
                MsgBox("Please Import excel file again...", MsgBoxStyle.Information, Me.Text)
            End If
        End If
    End Sub
    Private Sub loadpathitems()
        If Me.OpenFile.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtbox.Text = Me.OpenFile.FileName
            If gTrans.makelink_server(txtbox.Text) = "Successful..." Then
                Call verifygriditem()
            Else
                MsgBox("Please Import excel file again...", MsgBoxStyle.Information, Me.Text)
            End If
        End If
    End Sub

    Private Sub verifygridhdr()
        MsgBox(gTrans.getDatafromExcel("SELECT * FROM OPENQUERY(TESTPC, 'SELECT * FROM [summary$]')", grdSummary), MsgBoxStyle.Information, Me.Text)
    End Sub
    Private Sub verifygriditem()
        MsgBox(gTrans.getDatafromExcel("SELECT * FROM OPENQUERY(TSAPATH, 'SELECT * FROM [detailed$]')", grdParticulars), MsgBoxStyle.Information, Me.Text)
    End Sub

    Private Function save_header() As Boolean
        Dim xRow As Integer
        With Me.grdSummary
            For xRow = 0 To .Rows.Count - 1
                Dim sKeySR As String = Guid.NewGuid.ToString
                Dim sLoc As String = ""
                Dim tseqno As String = ""
                Dim scustomer As String = ""
                Dim sdate As Date
                Dim reference As String = ""

                If gTrans.gmkDefaultValues(xRow, 0, grdSummary) <> Nothing Then
                    If gTrans.gmkDefaultValues(xRow, 0, grdSummary) <> Nothing Then
                        sLoc = CStr(.Rows(xRow).Cells(0).Value)
                    End If
                    If gTrans.gmkDefaultValues(xRow, 1, grdSummary) <> Nothing Then
                        tseqno = (.Rows(xRow).Cells(1).Value.ToString)
                    End If
                    If gTrans.gmkDefaultValues(xRow, 2, grdSummary) <> Nothing Then
                        scustomer = (.Rows(xRow).Cells(2).Value)
                    End If
                    If gTrans.gmkDefaultValues(xRow, 3, grdSummary) <> Nothing Then
                        sdate = CDate(.Rows(xRow).Cells(3).Value)
                    End If

                    reference = sLoc + tseqno

                    Dim sSQLCmd As String = "usp_m_SALESRECEIPT_SummaryUploader "
                    sSQLCmd &= "@fxKeyCompany = '" & gCompanyID() & "'"
                    sSQLCmd &= ",@reference = '" & reference & "'"
                    sSQLCmd &= ",@fdDateTransact = '" & Microsoft.VisualBasic.FormatDateTime(sdate, DateFormat.ShortDate) & "'"
                    sSQLCmd &= ",@fcCustomerID = '" & sLoc & "'"

                    Try
                        SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
                    Catch ex As Exception
                        MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
                        Return False
                    End Try
                End If
            Next
            Return True
        End With
    End Function
    Private Function save_items() As Boolean
        Dim xRow As Integer
        With Me.grdParticulars

            For xRow = 0 To .Rows.Count - 1
                Dim sLoc As String = ""
                Dim tseqno As String = ""
                Dim referenceNo As String = ""
                Dim sBarCode As String = ""
                Dim sDescription As String = ""
                Dim sQuantity As Decimal = 0
                Dim sAmount As Decimal = 0.0
                If gTrans.gmkDefaultValues(xRow, 0, grdParticulars) <> Nothing Then

                    If gTrans.gmkDefaultValues(xRow, 0, grdParticulars) <> Nothing Then
                        sLoc = CStr(.Rows(xRow).Cells(0).Value)
                    End If

                    If gTrans.gmkDefaultValues(xRow, 1, grdParticulars) <> Nothing Then
                        tseqno = CStr(.Rows(xRow).Cells(1).Value)
                    End If

                    If gTrans.gmkDefaultValues(xRow, 2, grdParticulars) <> Nothing Then
                        sBarCode = (.Rows(xRow).Cells(2).Value.ToString)
                    End If

                    If gTrans.gmkDefaultValues(xRow, 4, grdParticulars) <> Nothing Then
                        sQuantity = (.Rows(xRow).Cells(4).Value)
                    End If

                    If gTrans.gmkDefaultValues(xRow, 5, grdParticulars) <> Nothing Then
                        sAmount = (.Rows(xRow).Cells(5).Value)
                    End If

                    referenceNo = sLoc + tseqno

                    Dim sSQLCmd As String = "usp_m_SALESRECEIPT_DetailsUploader "
                    sSQLCmd &= " @fcBarCode='" & sBarCode & "' "
                    sSQLCmd &= ",@fcReferenceNo='" & referenceNo & "' "
                    sSQLCmd &= ",@fdQuantity='" & sQuantity & "' "
                    sSQLCmd &= ",@fdAmount='" & sAmount & "' "
                    Try
                        SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Information, Me.Text)
                        Return False

                    End Try
                End If
            Next
            Return True
        End With
    End Function
    Private Sub BackgroundWorkerSRImport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorkerSRImport.DoWork
        If verifyIfSalesNoExist() = "All Sales No. Exist!" Then
            If verifyIfBarcodesExist() = "All Barcodes Exist!" Then
                If save_items() = True Then
                    MsgBox("Import SR Items Successful!", MsgBoxStyle.Information, Me.Text)
                End If
            Else
                BackgroundWorkerSRImport.CancelAsync()
                MsgBox("Sales Receipt Details Upload Cancelled!")
            End If
        Else
            BackgroundWorkerSRImport.CancelAsync()
            MsgBox("Sales Receipt Details Upload Cancelled!")
        End If
    End Sub
    Private Sub BackgroundWorkerSRImport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorkerSRImport.RunWorkerCompleted
        btnBrowseSR_Particulars.Enabled = True
        btnImportSR_Particulars.Enabled = True
        btnClose.Enabled = True
        ProgressBarSRImport.Visible = False
    End Sub
#End Region
#Region "Old Codes/Disabled Codes"
    'Private Sub btnImp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If save_header() = True And save_items() = True Then
    '        MsgBox("import successful...", MsgBoxStyle.Information, Me.Text)
    '    End If
    'End Sub
#End Region

End Class