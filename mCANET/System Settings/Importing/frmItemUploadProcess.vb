Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmItemUploadProcess

    Private fxKeyCompany As String = gCompanyID()
    Private gCon As New Clsappconfiguration
    Public xParent As frmItemMasterUploader
    Private xCnt As Integer = 0
    Private xCurrentItemCode As String = ""

    Private Sub ValidateAPandTermsandUploadSupplier()
        With xParent.grdUploader
            Dim xAP, xTerms As String
            Dim DTMissingAcnt As New DataTable("Missing Account")
            Dim DTTerms As New DataTable("Missing Terms")
            Dim sSqlCmd As String
            Dim DR As DataRow

            DTMissingAcnt.Columns.Add("AccountName", System.Type.GetType("System.String"))
            DTTerms.Columns.Add("Terms", System.Type.GetType("System.String"))
            xCnt = 0

            For Each xItem As DataGridViewRow In .Rows
                'validate AP
                xAP = xItem.Cells(15).Value.ToString
                xCurrentItemCode = " validation of " & xAP
                If xAP <> "" Then
                    sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & xAP & "' and co_id = '" & fxKeyCompany & "'"
                    Try
                        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                            If rd.HasRows = False Then
                                DR = DTMissingAcnt.NewRow
                                DR("AccountName") = xAP
                                DTMissingAcnt.Rows.Add(DR)
                            End If
                        End Using
                    Catch ex As Exception
                        ShowErrorMessage("ValidateAccountsPayable", Me.Name, ex.Message)
                    End Try
                End If
                xCnt += 1
            Next
            If DTMissingAcnt.Rows.Count <> 0 Then
                Dim xForm2 As New frmValidationResult
                For Each ValidatedItemRow As DataRow In DTMissingAcnt.Rows
                    xForm2.LstAccount.Items.Add(ValidatedItemRow.Item("AccountName").ToString)
                Next
                xForm2.lblValidationResult.Text = "Account/s that need to create first:"
                xForm2.xParent = Me
                xForm2.ShowDialog()
                Exit Sub
            End If

            'validate Terms
            For Each xItem As DataGridViewRow In .Rows
                xTerms = xItem.Cells(16).Value.ToString
                xCurrentItemCode = " validation of " & xTerms
                sSqlCmd = "select fcTermsName from dbo.mTerms where fcTermsName = '" & xTerms & "' and fxKeyCompany = '" & fxKeyCompany & "'"
                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                        If rd.HasRows = False Then
                            DR = DTTerms.NewRow
                            DR("Terms") = xTerms
                            DTTerms.Rows.Add(DR)
                        End If
                    End Using
                Catch ex As Exception
                    ShowErrorMessage("ValidateAccountsPayable", Me.Name, ex.Message)
                End Try
                xCnt += 1
            Next

            'upload Supplier
            If DTTerms.Rows.Count <> 0 Then
                Dim xForm2 As New frmValidationResult
                For Each ValidatedItemRow As DataRow In DTTerms.Rows
                    xForm2.LstAccount.Items.Add(ValidatedItemRow.Item("Terms").ToString)
                Next
                xForm2.lblValidationResult.Text = "Terms that need to create first:"
                xForm2.xParent = Me
                xForm2.ShowDialog()
                Exit Sub
            Else
                Dim LName, FName, MI As String
                Dim Salutation, Address, Contact As String
                Dim Phone, FAX, AltPhone As String
                Dim AltContact, EMail, CC As String
                Dim AcntNo, TIN, AcntPayable As String
                Dim Terms, CompanyName, fxKeyAddress As String
                Dim fxKeySupplier As String

                For Each xItem As DataGridViewRow In .Rows
                    fxKeySupplier = Guid.NewGuid.ToString
                    Address = ""
                    AcntPayable = ""
                    Terms = ""
                    CompanyName = xItem.Cells(0).Value.ToString

                    xCurrentItemCode = " uploading of " & CompanyName

                    Salutation = xItem.Cells(4).Value.ToString
                    LName = xItem.Cells(1).Value.ToString
                    FName = xItem.Cells(2).Value.ToString
                    MI = xItem.Cells(3).Value.ToString

                    Address = xItem.Cells(5).Value.ToString
                    If Address <> "" Then
                        fxKeyAddress = Guid.NewGuid.ToString
                        sSqlCmd = "insert into dbo.mAddress " & _
                                  "(fxKeyAddress, fxKeyID, fcAddress, fbSupplier, fuCreatedby, fdDateCreated) values " & _
                                  "('" & fxKeyAddress & "', '" & fxKeySupplier & "', '" & Address & "', '0', '" & gUserName & "', getdate())"
                        Try
                            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
                        Catch ex As Exception
                            ShowErrorMessage("ValidateAccountsPayable", Me.Name, ex.Message)
                        End Try
                    End If

                    Contact = xItem.Cells(6).Value.ToString
                    Phone = xItem.Cells(7).Value.ToString
                    FAX = xItem.Cells(8).Value.ToString
                    AltPhone = xItem.Cells(9).Value.ToString
                    AltContact = xItem.Cells(10).Value.ToString
                    EMail = xItem.Cells(11).Value.ToString
                    CC = xItem.Cells(12).Value.ToString
                    AcntNo = xItem.Cells(13).Value.ToString
                    TIN = xItem.Cells(14).Value.ToString

                    AcntPayable = xItem.Cells(15).Value.ToString
                    If AcntPayable <> "" Then
                        sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & AcntPayable & "' and co_id = '" & fxKeyCompany & "'"
                        Try
                            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                While rd.Read
                                    AcntPayable = rd.Item(0).ToString
                                End While
                            End Using
                        Catch ex As Exception
                            ShowErrorMessage("ValidateAccountsPayable", Me.Name, ex.Message)
                        End Try
                    End If

                    Terms = xItem.Cells(16).Value.ToString
                    If Terms <> "" Then
                        sSqlCmd = "select fxKeyTerms from dbo.mTerms where fcTermsName = '" & Terms & "' and fxKeyCompany = '" & fxKeyCompany & "'"
                        Try
                            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                While rd.Read
                                    Terms = rd.Item(0).ToString
                                End While
                            End Using
                        Catch ex As Exception
                            ShowErrorMessage("ValidateAccountsPayable", Me.Name, ex.Message)
                        End Try
                    End If

                    sSqlCmd = "usp_m_UploadSupplierMaster @fcSalutation = '" & Salutation & "', " & _
                             "@fcLastName = '" & LName & "', " & _
                             "@fcFirstName = '" & FName & "', " & _
                             "@fcMidName = '" & MI & "', " & _
                             "@fcAccountNo = '" & AcntNo & "', " & _
                             "@fcTIN = '" & TIN & "', " & _
                             "@fcContact = '" & Contact & "', " & _
                             "@fcAltContact = '" & AltContact & "', " & _
                             "@fcTelNo = '" & Phone & "', " & _
                             "@fcAltTelNo = '" & AltPhone & "', " & _
                             "@fcFaxNo = '" & FAX & "', " & _
                             "@fcEmail = '" & EMail & "', " & _
                             "@fxKeySupplier = '" & fxKeySupplier & "', " & _
                             "@fcCompanyName = '" & CompanyName & "', "

                    If Terms <> "" Then
                        sSqlCmd &= "@fxKeyTerms = '" & Terms & "', "
                    End If
                    If Address <> "" Then
                        sSqlCmd &= "@fxKeyAddress = '" & fxKeyAddress & "', "
                    End If
                    If AcntPayable <> "" Then
                        sSqlCmd &= "@fxKeyAPAccnt = '" & AcntPayable & "', "
                    End If
                    sSqlCmd &= "@fxKeyCompany = '" & fxKeyCompany & "'"

                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
                    Catch ex As Exception
                        ShowErrorMessage("ValidateAccountsPayable", Me.Name, ex.Message)
                    End Try
                    xCnt += 1
                Next
            End If
            MsgBox("Upload Supplier master sucessful!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access granted!")
        End With
    End Sub
    Private Sub ValidateCustomerMasterUploader()
        With xParent.grdUploader
            Dim xAcntReceivable, xSalesDiscntAcnt, xTaxCodeItem As String
            Dim xBillingAddress As String
            Dim DTMissingAcnt As New DataTable("Missing Account")
            Dim DTTaxCode As New DataTable("Tax Code")
            Dim sSqlCmd As String
            Dim DR As DataRow

            xCnt = 0
            DTMissingAcnt.Columns.Add("AccountName", System.Type.GetType("System.String"))
            DTTaxCode.Columns.Add("TaxCode", System.Type.GetType("System.String"))

            For Each xItem As DataGridViewRow In .Rows
                'validating Account Receivables
                xAcntReceivable = xItem.Cells(18).Value.ToString
                xCurrentItemCode = " validation of AR Account..."
                If xAcntReceivable <> "" Then
                    sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & xAcntReceivable & "' and co_id = '" & fxKeyCompany & "'"
                    Try
                        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                            If rd.HasRows = False Then
                                DR = DTMissingAcnt.NewRow
                                DR("AccountName") = xAcntReceivable
                                DTMissingAcnt.Rows.Add(DR)
                            End If
                        End Using
                    Catch ex As Exception
                        ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                    End Try
                End If
                xCnt += 1

                'validating Sales Discount Accounts
                xSalesDiscntAcnt = xItem.Cells(19).Value.ToString
                xCurrentItemCode = " validation of Sales Discount Account..."
                If xSalesDiscntAcnt <> "" Then
                    sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & xSalesDiscntAcnt & "' and co_id = '" & fxKeyCompany & "'"
                    Try
                        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                            If rd.HasRows = False Then
                                DR = DTMissingAcnt.NewRow
                                DR("AccountName") = xSalesDiscntAcnt
                                DTMissingAcnt.Rows.Add(DR)
                            End If
                        End Using
                    Catch ex As Exception
                        ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                    End Try
                End If
                xCnt += 1
            Next

            If DTMissingAcnt.Rows.Count <> 0 Then
                Dim xForm2 As New frmValidationResult
                For Each ValidatedItemRow As DataRow In DTMissingAcnt.Rows
                    xForm2.LstAccount.Items.Add(ValidatedItemRow.Item("AccountName").ToString)
                Next
                xForm2.lblValidationResult.Text = "Account/s that need to create first:"
                xForm2.Parent = Me
                xForm2.ShowDialog()
                Exit Sub
            End If

            'validating Tax Code
            For Each xTaxCode As DataGridViewRow In .Rows
                xTaxCodeItem = xTaxCode.Cells(13).Value.ToString
                xCurrentItemCode = " validation of Tax code " & xTaxCodeItem
                If xTaxCodeItem <> "" Then
                    sSqlCmd = "select fxKeySalesTaxCode from dbo.mSalesTaxCode where fcSalesTaxCodeName = '" & xTaxCodeItem & "' and fxKeyCompany = '" & fxKeyCompany & "'"
                    Try
                        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                            If rd.HasRows = False Then
                                DR = DTTaxCode.NewRow
                                DR("TaxCode") = xTaxCodeItem
                                DTTaxCode.Rows.Add(DR)
                            End If
                        End Using
                    Catch ex As Exception
                        ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                    End Try
                End If
                xCnt += 1
            Next

            If DTTaxCode.Rows.Count <> 0 Then
                Dim xForm2 As New frmValidationResult
                For Each ValidatedItemRow As DataRow In DTTaxCode.Rows
                    xForm2.LstAccount.Items.Add(ValidatedItemRow.Item("TaxCode").ToString)
                Next
                xForm2.lblValidationResult.Text = "Taxcode/s that need to create first:"
                xForm2.Parent = Me
                xForm2.ShowDialog()
                Exit Sub
            Else
                Dim SKeyName, LName, FName, MI As String
                Dim Salutation, xContact, AltContact As String
                Dim xPhone, xAltPhone, xFAX As String
                Dim xEmail, xCC, xAccountNo As String
                Dim xTaxCode, xClassification, xTerm As String
                Dim xRep, xARAcnt, xSDAcnt As String
                Dim xCompanyName As String

                For Each xCustomerRow As DataGridViewRow In .Rows
                    SKeyName = Guid.NewGuid.ToString
                    xCompanyName = xCustomerRow.Cells(0).Value.ToString
                    xBillingAddress = xCustomerRow.Cells(12).Value.ToString
                    LName = xCustomerRow.Cells(1).Value.ToString
                    FName = xCustomerRow.Cells(2).Value.ToString
                    MI = xCustomerRow.Cells(3).Value.ToString
                    xCurrentItemCode = " uploading of " & LName & ", " & FName & " " & MI
                    Salutation = xCustomerRow.Cells(4).Value.ToString
                    xContact = xCustomerRow.Cells(5).Value.ToString
                    AltContact = xCustomerRow.Cells(9).Value.ToString
                    xPhone = xCustomerRow.Cells(6).Value.ToString
                    xAltPhone = xCustomerRow.Cells(8).Value.ToString
                    xFAX = xCustomerRow.Cells(7).Value.ToString
                    xEmail = xCustomerRow.Cells(10).Value.ToString
                    xCC = xCustomerRow.Cells(11).Value.ToString
                    xAccountNo = xCustomerRow.Cells(17).Value.ToString

                    xTaxCode = xCustomerRow.Cells(13).Value.ToString
                    If xTaxCode <> "" Then
                        sSqlCmd = "select fxKeySalesTaxCode from dbo.mSalesTaxCode where fcSalesTaxCodeName = '" & xTaxCode & "' and fxKeyCompany = '" & fxKeyCompany & "'"
                        Try
                            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                If rd.Read Then
                                    xTaxCode = rd.Item(0).ToString
                                End If
                            End Using
                        Catch ex As Exception
                            ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                        End Try
                    End If

                    xClassification = xCustomerRow.Cells(14).Value.ToString
                    If xClassification <> "" Then
                        sSqlCmd = "select fxKeyCustomerType from dbo.mCustomer01Type where fcTypeName = '" & _
                                    xClassification & "' and fxKeyCompany = '" & fxKeyCompany & "'"
                        Try
                            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                If rd.Read() Then
                                    xClassification = rd.Item(0).ToString
                                End If
                            End Using
                        Catch ex As Exception
                            ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                        End Try
                    End If

                    xTerm = xCustomerRow.Cells(15).Value.ToString
                    If xTerm <> "" Then
                        sSqlCmd = "select fxKeyTerms from dbo.mTerms where fcTermsName = '" & xTerm & "' and fxKeyCompany = '" & fxKeyCompany & "'"
                        Try
                            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                If rd.Read() Then
                                    xTerm = rd.Item(0).ToString
                                Else
                                    xTerm = ""
                                End If
                            End Using
                        Catch ex As Exception
                            ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                        End Try
                    End If

                    xRep = xCustomerRow.Cells(16).Value.ToString
                    If xRep <> "" Then
                        sSqlCmd = "select t1.fxKeySalesRep from dbo.mSalesRep t1 " & _
                                    "inner join dbo.mSupplier00Master t2 on t1.fxKeyID = t2.fxKeySupplier " & _
                                    "where t2.fcSupplierName = '" & xRep & "' and t1.fxKeyCompany = '" & fxKeyCompany & "'"
                        Try
                            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                If rd.Read() Then
                                    xRep = rd.Item(0).ToString
                                Else
                                    xRep = ""
                                End If
                            End Using
                        Catch ex As Exception
                            ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                        End Try
                    End If

                    xARAcnt = xCustomerRow.Cells(18).Value.ToString
                    If xARAcnt <> "" Then
                        sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & xARAcnt & "' and co_id = '" & fxKeyCompany & "'"
                        Try
                            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                If rd.Read Then
                                    xARAcnt = rd.Item(0).ToString
                                End If
                            End Using
                        Catch ex As Exception
                            ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                        End Try
                    End If

                    xSDAcnt = xCustomerRow.Cells(19).Value.ToString
                    If xSDAcnt <> "" Then
                        sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & xSDAcnt & "' and co_id = '" & fxKeyCompany & "'"
                        Try
                            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                If rd.Read Then
                                    xSDAcnt = rd.Item(0).ToString
                                End If
                            End Using
                        Catch ex As Exception
                            ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                        End Try
                    End If
                    If xBillingAddress <> "" Then
                        sSqlCmd = "insert into dbo.mAddress " & _
                                "(fxKeyAddress, fcAddressType, fcAddressName, fxKeyID, fcAddress, fcNote) " & _
                                "values('" & Guid.NewGuid.ToString & "', 'Billing', 'Billing Address 1', '" & SKeyName & "', '" & xBillingAddress & "', '')"
                        Try
                            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
                        Catch ex As Exception
                            ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                        End Try

                    End If

                    sSqlCmd = "usp_m_customer_update "
                    sSqlCmd &= " @fxKeyCustomer = '" & SKeyName & "' "
                    sSqlCmd &= ", @fcCustomerName = '" & LName & ", " & FName & " " & MI & "' "
                    sSqlCmd &= ", @fdBalance = '0.00' "
                    sSqlCmd &= ", @fdDateAsOf = NULL "
                    sSqlCmd &= ", @fcCompanyName = '" & xCompanyName & "' "
                    sSqlCmd &= ", @fcSalutation = '" & Salutation & "' "
                    sSqlCmd &= ", @fcLastName = '" & LName & "' "
                    sSqlCmd &= ", @fcFirstName  = '" & FName & "' "
                    sSqlCmd &= ", @fcMidName = '" & MI & "' "
                    sSqlCmd &= ", @fcContactPerson1 = '" & xContact & "' "
                    sSqlCmd &= ", @fcContactPerson2 = '" & AltContact & "' "
                    sSqlCmd &= ", @fcTelNo1 = '" & xPhone & "' "
                    sSqlCmd &= ", @fcTelNo2 = '" & xAltPhone & "' "
                    sSqlCmd &= ", @fcFaxNo = '" & xFAX & "' "
                    sSqlCmd &= ", @fcEmail = '" & xEmail & "' "
                    sSqlCmd &= ", @fcCC = '" & xCC & "' "
                    sSqlCmd &= ", @fcAccountNo = '" & xAccountNo & "' "
                    sSqlCmd &= ", @fdCreditLimit ='0.00' "
                    sSqlCmd &= ", @fnResaleNo ='' "
                    sSqlCmd &= ", @fcCreditCardNo = '' "
                    sSqlCmd &= ", @fcNameOnCard = '' "
                    sSqlCmd &= ", @fcExpirationMonth = '' "
                    sSqlCmd &= ", @fcExpirationYear = '' "
                    sSqlCmd &= ", @fcAddressOnCard = '' "
                    sSqlCmd &= ", @fnPostalOnCard = '' "
                    sSqlCmd &= ", @fdDateStart = NULL "
                    sSqlCmd &= ", @fdDateEndTemp = NULL "
                    sSqlCmd &= ", @fdDateEnd = NULL "
                    sSqlCmd &= ", @fcJobDescription = '' "
                    sSqlCmd &= ", @fbActive =1 "
                    sSqlCmd &= ", @fxKeyCompany = '" & fxKeyCompany & "' "
                    sSqlCmd &= ", @fcAddress = '" & xBillingAddress & "' "
                    sSqlCmd &= ", @fxKeyAddress = NULL "
                    sSqlCmd &= ", @fxKeySendMethod = NULL "
                    sSqlCmd &= ", @fxKeyJobStatus = NULL "
                    sSqlCmd &= ", @fbDeleted = 0"
                    sSqlCmd &= ", @fcCreatedBy='" & gUserName & "' "
                    'sSqlCmd &= ", @fdDateCreated = getdate() "
                    sSqlCmd &= ", @fcUpdatedBy='" & gUserName & "' "
                    'sSqlCmd &= ", @fdDateUpdated = getdate() "

                    If xTaxCode <> "" Then
                        sSqlCmd &= ", @fxKeySalesTaxCode = '" & xTaxCode & "'"
                    End If

                    sSqlCmd &= ", @fcShippingAddress = NULL"

                    If xClassification <> "" Then
                        sSqlCmd &= ", @fxKeyCustomerType = '" & xClassification & "'"
                    End If

                    If xTerm <> "" Then
                        sSqlCmd &= ", @fxKeyTerms = '" & xTerm & "'"
                    End If

                    If xRep <> "" Then
                        sSqlCmd &= ", @fxKeyRep = '" & xRep & "'"
                    End If

                    sSqlCmd &= ", @fxKeyParent = NULL "
                    sSqlCmd &= ", @fcLevel = 1"
                    sSqlCmd &= ", @fcDefaultModule = 'Invoice'"
                    sSqlCmd &= ", @fxKeyAcntRecvble = " & IIf(xARAcnt = "", "NULL", "'" & xARAcnt & "'")
                    sSqlCmd &= ", @fxKeySalesDcnt = " & IIf(xSDAcnt = "", "NULL", "'" & xSDAcnt & "'")
                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
                    Catch ex As Exception
                        'ShowErrorMessage("ValidateCustomerMasterUploader", Me.Name, ex.Message)
                    End Try
                    xCnt += 1
                Next

                MsgBox("Upload Customer master sucessful!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access granted!")
            End If
        End With
    End Sub
    Private Sub ValidateData()
        Dim xRow As Integer = 0
        Dim sSqlCmd As String
        Dim xCOGs As String = ""
        Dim xAssets As String = ""
        Dim xIncome As String = ""
        Dim xTax As String = ""
        Dim DTMissingAccount As New DataTable("MissingAccount")
        Dim AccountAlreadyTag As Boolean = False
        Dim DR As DataRow

        DTMissingAccount.Columns.Add("AccountName", System.Type.GetType("System.String"))
        For Each xItem As DataGridViewRow In xParent.grdUploader.Rows
            xRow = xItem.Index

            'validating COGs Account
            xCOGs = IIf(xItem.Cells(6).Value Is Nothing, "", xItem.Cells(6).Value.ToString)
            xCurrentItemCode = "validation of " & xCOGs & " account"
            If xCOGs <> "" Then
                sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & xCOGs & "' and co_id = '" & fxKeyCompany & "'"
                Try
                    Using rd1 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                        If rd1.HasRows = False Then
                            For Each SubItemDT1 As DataRow In DTMissingAccount.Rows
                                If SubItemDT1.Item("AccountName") = xCOGs Then
                                    AccountAlreadyTag = True
                                    Exit For
                                End If
                            Next
                            If AccountAlreadyTag = False Then
                                DR = DTMissingAccount.NewRow
                                DR("AccountName") = xCOGs
                                DTMissingAccount.Rows.Add(DR)
                            End If
                        End If
                    End Using
                Catch ex As Exception
                    MsgBox("Error at ValidateData in frmItemUploadProcess.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                End Try
            End If
            xCnt += 1

            'validating Asset Account
            xAssets = IIf(xItem.Cells(7).Value Is Nothing, "", xItem.Cells(7).Value.ToString)
            xCurrentItemCode = "validation of " & xAssets & " account"
            If xAssets <> "" Then
                sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & xAssets & "' and co_id = '" & fxKeyCompany & "'"
                Try
                    Using rd2 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                        If rd2.HasRows = False Then
                            For Each SubItemDT1 As DataRow In DTMissingAccount.Rows
                                If SubItemDT1.Item("AccountName") = xAssets Then
                                    AccountAlreadyTag = True
                                    Exit For
                                End If
                            Next
                            If AccountAlreadyTag = False Then
                                DR = DTMissingAccount.NewRow
                                DR("AccountName") = xAssets
                                DTMissingAccount.Rows.Add(DR)
                            End If
                        End If
                    End Using
                Catch ex As Exception
                    MsgBox("Error at ValidateData in frmItemUploadProcess.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                End Try
            End If
            xCnt += 1

            'validating Income Account
            xIncome = IIf(xItem.Cells(8).Value Is Nothing, "", xItem.Cells(8).Value.ToString)
            xCurrentItemCode = "validation of " & xIncome & " account"
            If xAssets <> "" Then
                sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & xIncome & "' and co_id = '" & fxKeyCompany & "'"
                Try
                    Using rd3 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                        If rd3.HasRows = False Then
                            For Each SubItemDT1 As DataRow In DTMissingAccount.Rows
                                If SubItemDT1.Item("AccountName") = xIncome Then
                                    AccountAlreadyTag = True
                                    Exit For
                                End If
                            Next
                            If AccountAlreadyTag = False Then
                                DR = DTMissingAccount.NewRow
                                DR("AccountName") = xIncome
                                DTMissingAccount.Rows.Add(DR)
                            End If
                        End If
                    End Using
                Catch ex As Exception
                    MsgBox("Error at ValidateData in frmItemUploadProcess.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                End Try
            End If
            xCnt += 1

            'validating Tax Account
            xTax = IIf(xItem.Cells(9).Value Is Nothing, "", xItem.Cells(9).Value.ToString)
            xCurrentItemCode = "validation of " & xTax & " account"
            If xAssets <> "" Then
                sSqlCmd = "select acnt_id from dbo.mAccounts where acnt_name = '" & xIncome & "' and co_id = '" & fxKeyCompany & "'"
                Try
                    Using rd4 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                        If rd4.HasRows = False Then
                            For Each SubItemDT1 As DataRow In DTMissingAccount.Rows
                                If SubItemDT1.Item("AccountName") = xTax Then
                                    AccountAlreadyTag = True
                                    Exit For
                                End If
                            Next
                            If AccountAlreadyTag = False Then
                                DR = DTMissingAccount.NewRow
                                DR("AccountName") = xTax
                                DTMissingAccount.Rows.Add(DR)
                            End If
                        End If
                    End Using
                Catch ex As Exception
                    MsgBox("Error at ValidateData in frmItemUploadProcess.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                End Try
            End If
            xCnt += 1
        Next

        If DTMissingAccount.Rows.Count <> 0 Then
            Dim xForm2 As New frmValidationResult
            For Each ValidatedItemRow As DataRow In DTMissingAccount.Rows
                xForm2.LstAccount.Items.Add(ValidatedItemRow.Item("AccountName").ToString)
            Next
            xForm2.lblValidationResult.Text = "Account/s that need to create first:"
            xForm2.xParent = Me
            xForm2.ShowDialog()
        Else
            For Each xItem2 As DataGridViewRow In xParent.grdUploader.Rows
                xRow = xItem2.Index
                sSqlCmd = "usp_m_UploadCategory '" & xItem2.Cells(2).Value.ToString & "','" & fxKeyCompany & "','" & gUserName & "'"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
                Catch ex As Exception
                    MsgBox("Error at ValidateData in frmItemUploadProcess.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                End Try
            Next
            BeginUploadItem()
            MsgBox("Upload Item/s sucessful!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access granted!")
        End If
    End Sub
    Private Sub BeginUploadItem()
        Dim xRow As Integer = 0
        'xCnt = 0
        'ProgressBar.Maximum = xParent.grdUploader.Rows.Count

        With (xParent.grdUploader)

            For Each xItem As DataGridViewRow In .Rows
                xRow = xItem.Index
                UploadItem(xItem.Cells(0).Value.ToString, xItem.Cells(1).Value.ToString, xItem.Cells(4).Value.ToString, _
                           xItem.Cells(5).Value.ToString, xItem.Cells(3).Value.ToString, xItem.Cells(2).Value.ToString, _
                           xItem.Cells(6).Value.ToString, xItem.Cells(7).Value.ToString, xItem.Cells(8).Value.ToString, _
                           xItem.Cells(9).Value.ToString)
                xCnt += 1
            Next
        End With
    End Sub
    Private Sub UploadItem(ByVal ItemCode As String, ByVal Description As String, _
                            ByVal Cost As Decimal, ByVal Price As Decimal, ByVal ActiveStatus As Boolean, _
                            ByVal Category As String, ByVal COGsAccount As String, _
                            ByVal AssetAccount As String, ByVal IncomeAccount As String, _
                            ByVal TaxAccount As String)

        xCurrentItemCode = "uploading of " & ItemCode
        Dim sSqlCmd As String = "ItemMasterUploader " & _
                            "@fcItemCode = '" & ItemCode & "', " & _
                            "@fcDescription = '" & Description & "', " & _
                            "@fdUnitCost = '" & Cost & "', " & _
                            "@fdUnitPrice = '" & Price & "', @fbActive = " & IIf(ActiveStatus = True, 1, 0) & ", "

        If Category <> "" Then
            sSqlCmd &= "@fcCategory = '" & Category & "', "
        End If
        If COGsAccount <> "" Then
            sSqlCmd &= "@fcCOGsAcnt = '" & COGsAccount & "', "
        End If
        If AssetAccount <> "" Then
            sSqlCmd &= "@fcAssetsAcnt = '" & AssetAccount & "', "
        End If
        If IncomeAccount <> "" Then
            sSqlCmd &= "@fcIncomeAcnt = '" & IncomeAccount & "', "
        End If
        If TaxAccount <> "" Then
            sSqlCmd &= "@fcTaxAcnt = '" & TaxAccount & "', "
        End If

        sSqlCmd &= "@fxKeyCompany = '" & fxKeyCompany & "' , @fcCreatedBy = '" & gUserName & "'"

        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            MsgBox("Error at UploadItem in frmItemMasterUploader." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
    End Sub
    Private Sub UploadCOA(ByVal fxKeyAcntID As String, ByVal fxKeyCompany As String, ByVal fcLevel As Integer, _
                            ByVal acnt_code As String, ByVal acnt_desc As String, _
                            ByVal acnt_subof As String, ByVal fbActive As Integer, ByVal acnt_type As String)
        'Dim sSqlCmd As String = "Usp_t_UploadChartOfAccounts " & _
        '                        "@fxKeyAccount = '" & fxKeyAcntID & "', " & _
        '                        "@fxKeyCompany = '" & fxKeyCompany & "', " & _
        '                        "@fcLevel = " & fcLevel & ", " & _
        '                        "@acnt_code = '" & acnt_code & "', " & _
        '                        "@acnt_desc = '" & acnt_desc & "', " & _
        '                        "@fbActive = " & fbActive & ", " & _
        '                        "@created_by = '" & gUserName & "', "
        Dim storedProcedure As String = "Usp_t_UploadChartOfAccounts"


        'If acnt_subof <> "" Then
        '    sSqlCmd &= "@acnt_sub = 1, " & _
        '                "@acnt_subof= '" & acnt_subof & "'"
        'Else
        '    sSqlCmd &= "@acnt_sub = 0"
        'End If

        'If acnt_type <> "" Then
        '    sSqlCmd &= ", @acnt_type = '" & acnt_type & "'"
        'End If

        Dim IsAccountSubOf As Char
        If acnt_subof <> "" Then
            IsAccountSubOf = "1"
        Else
            IsAccountSubOf = "0"
            acnt_subof = Nothing
        End If

        Try
            'NOTE: Modified: To avoid getting error on "'" delimeters
            '- by kuyajd
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, storedProcedure, _
                        New SqlParameter("@fxKeyAccount", fxKeyAcntID), _
                        New SqlParameter("@fxKeyCompany", fxKeyCompany), _
                        New SqlParameter("@fcLevel", fcLevel), _
                        New SqlParameter("@acnt_code", acnt_code), _
                        New SqlParameter("@acnt_desc", acnt_desc), _
                        New SqlParameter("@fbActive", fbActive), _
                        New SqlParameter("@created_by", gUserName), _
                        New SqlParameter("@acnt_sub", IsAccountSubOf), _
                        New SqlParameter("@acnt_subof", acnt_subof), _
                        New SqlParameter("@acnt_type", acnt_type))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ValidateAccountTypes()
        Try
            Dim xAcntType As String = ""
            Dim sSqlCmd As String
            Dim fxKeyCompany As String = gCompanyID()
            Dim DTMissingAccountType As New DataTable("Account Type")
            Dim Dr As DataRow
            Dim IsTypeTagged As Boolean = False

            DTMissingAccountType.Columns.Add("AccountType", System.Type.GetType("System.String"))
            DTMissingAccountType.PrimaryKey = New DataColumn() {DTMissingAccountType.Columns("AccountType")}

            xCnt = 0

            With xParent.grdUploader
                For Each xItem As DataGridViewRow In .Rows
                    xAcntType = xItem.Cells(5).Value.ToString
                    If xAcntType <> "" Then
                        xCurrentItemCode = "validation of " & xAcntType & " account type."
                        sSqlCmd = "select * from dbo.mAccountType where acnt_type = '" & xAcntType & "' and co_id = '" & fxKeyCompany & "'"
                        Try
                            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                If rd.HasRows = False Then
                                    For Each xType As DataRow In DTMissingAccountType.Rows
                                        If xType.Item(0).ToString = xAcntType Then
                                            IsTypeTagged = True
                                            Exit For
                                        Else
                                            IsTypeTagged = False
                                            Exit For
                                        End If
                                    Next
                                    If IsTypeTagged = False Then
                                        Dr = DTMissingAccountType.NewRow
                                        Dr("AccountType") = xAcntType

                                        'Note: Modified: Eliminates Duplicate Display of Account Types
                                        'This will help user not to be confuse on what account types to be added 
                                        '- by kuyajd
                                        If DTMissingAccountType.Rows.Contains(xAcntType) = False Then
                                            DTMissingAccountType.Rows.Add(Dr)
                                        End If
                                        ' ----------------------------------------------------------------------

                                        IsTypeTagged = True
                                    End If
                                End If
                            End Using
                        Catch ex As Exception
                            ShowErrorMessage("ValidateAccountTypes", Me.Name, ex.Message)
                        End Try
                    End If
                    xCnt += 1
                Next
                If DTMissingAccountType.Rows.Count <> 0 Then
                    Dim xForm2 As New frmValidationResult
                    For Each ValidatedItemRow As DataRow In DTMissingAccountType.Rows
                        xForm2.LstAccount.Items.Add(ValidatedItemRow.Item("AccountType").ToString)
                    Next
                    xForm2.lblValidationResult.Text = "Account type/s that need to create first:"
                    xForm2.Parent = Me
                    xForm2.ShowDialog()
                Else
                    With xParent.grdUploader
                        Dim fxKeyAcnt, fclevel, AcntCode As String
                        Dim AcntDesc, AcntSubOf, fbActive As String
                        Dim AcntType As String

                        For Each xItem As DataGridViewRow In .Rows
                            fxKeyAcnt = xItem.Cells("xfxKeyAccount").Value.ToString
                            fclevel = xItem.Cells(0).Value.ToString
                            AcntCode = xItem.Cells(1).Value.ToString
                            AcntDesc = xItem.Cells(2).Value.ToString
                            xCurrentItemCode = "uploading of " & AcntDesc & " account."

                            If xItem.Cells(7).Value IsNot Nothing Then
                                AcntSubOf = xItem.Cells(7).Value.ToString
                            Else
                                AcntSubOf = ""
                            End If

                            fbActive = IIf(xItem.Cells(4).Value.ToString = "True", 1, 0)
                            AcntType = xItem.Cells(5).Value.ToString


                            UploadCOA(fxKeyAcnt, fxKeyCompany, fclevel, _
                                      AcntCode, AcntDesc, AcntSubOf, _
                                      fbActive, AcntType)
                            xCnt += 1
                        Next
                        MsgBox("Upload account/s sucessful!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access granted!")
                    End With
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub BgWValidate_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BgWValidate.DoWork
        Select Case xParent.xMode
            Case "Item Master"
                ValidateData()

            Case "Chart of Accounts"
                ValidateAccountTypes()

            Case "Customer Master"
                ValidateCustomerMasterUploader()

            Case "Supplier Master"
                ValidateAPandTermsandUploadSupplier()

        End Select

    End Sub
    Private Sub frmItemUploadProcess_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Timer1.Enabled = True
        If xParent.grdUploader.AllowUserToAddRows = True Then
            xParent.grdUploader.AllowUserToAddRows = False
        End If
        Select Case xParent.xMode
            Case "Item Master"
                ProgressBar.Maximum = xParent.grdUploader.Rows.Count * 5

            Case "Chart of Accounts"
                ProgressBar.Maximum = xParent.grdUploader.Rows.Count * 2

            Case "Customer Master"
                ProgressBar.Maximum = xParent.grdUploader.Rows.Count * 4

            Case "Supplier Master"
                ProgressBar.Maximum = xParent.grdUploader.Rows.Count * 3
        End Select

        BgWValidate.RunWorkerAsync()
    End Sub
    Private Sub BgWValidate_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BgWValidate.RunWorkerCompleted
        If xParent.grdUploader.AllowUserToAddRows = False Then
            xParent.grdUploader.AllowUserToAddRows = True
        End If
        Timer1.Enabled = False
        Me.Close()
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        ProgressBar.Value = xCnt
        lblProcess.Text = "Processing " & xCurrentItemCode
    End Sub
End Class