'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: October 13, 2010                   ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

'                   ########################################################
'                   ### Updated by: Josh "Palladium" Dadulla             ###
'                   ### Date Created: 9/16/2012                          ###
'                   ### Website: http://divergentsociety.net             ###
'                   ########################################################
'

Imports System.Data.OleDb, System.IO

Public Class frmMasterUploader
    Private xFilePath As String = ""
    Private gDT As New DataTable
    Private gOleConn As New OleDbConnection
    Private gOleCmd As New OleDbCommand
    Private UploaderType As String
    Public xMode As String = ""


    Private sampleDate As Date
    Public Property GetSampleDate() As Date
        Get
            Return sampleDate
        End Get
        Set(ByVal value As Date)
            sampleDate = value
        End Set
    End Property


    Private isSourceInvalid As Boolean
    Public Property GetIsSourceInvalid() As Boolean
        Get
            Return isSourceInvalid
        End Get
        Set(ByVal value As Boolean)
            isSourceInvalid = value
        End Set
    End Property


    Private user As String
    Public Property GetUser() As String
        Get
            Return user
        End Get
        Set(ByVal value As String)
            user = value
        End Set
    End Property


    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Public Sub GetConnection(ByVal FilePath As String)
        'check if the file exists
        Try
            If Not File.Exists(FilePath) Then
                MessageBox.Show("File does not exist or access is denied!", "Upload", MessageBoxButtons.OK)
            Else
                'connection string
                Dim gCnstring As String
                If UploaderType = ".xlsx" Then
                    gCnstring = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1""")
                Else
                    gCnstring = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1""", FilePath)
                End If
                gOleConn = New OleDbConnection(gCnstring)
                gOleCmd = gOleConn.CreateCommand
                gOleConn.Open()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub UploadExcelFile()
        If xFilePath <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(xFilePath)
            Dim FileName As String = xFilePath

            Dim oleDA As New OleDbDataAdapter("SELECT * FROM [Sheet1$]", gOleConn)
            Try
                'get data from excel
                oleDA.Fill(gDT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = gDT.Rows.Count

                If gDT.Rows.Count <> 0 Then
                    grdUploader.DataSource = gDT
                End If

                'Format Gridview
                Select Case xMode
                    Case "Journal Entries"
                        GrdUploader.Columns("Debit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Credit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                        GrdUploader.Columns("Debit").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Credit").DefaultCellStyle.Format = "n2"


                    Case "Loan Master"
                        GrdUploader.Columns("EmpNo").DefaultCellStyle.Format = "####"
                        GrdUploader.Columns("Terms").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Principal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Principal").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Interest Rate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Interest Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Interest Amount").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Service Fee Rate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Service Fee Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Service Fee Amount").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Capital Share Amount").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Capital Share Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Penalty Rate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Penalty").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Penalty").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Monthly Amortization").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Monthly Amortization").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Paydate FR").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Paydate TO").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Last Update").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Interest Refund").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Interest Refund").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Loan No").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Loan Status").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Loan Classification").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Total Payable").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Total Payable").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Balance").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Balance").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                    Case "Loan Payment"
                        GrdUploader.Columns(7).DefaultCellStyle.Format = "##,##0.00"
                        GrdUploader.Columns(8).DefaultCellStyle.Format = "##,##0.00"
                    Case "Member Contribution"
                        'GrdUploader.Columns(1).DefaultCellStyle.Format = "####"
                        GrdUploader.Columns("Withdrawal").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Deposit").DefaultCellStyle.Format = "n2"
                        'Case "Account Register"

                End Select


            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show(ex.Message, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Finally
                gOleConn.Close()
            End Try
        End If
    End Sub

    Private Sub GenerateTemplate()
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value

        xlApp = New Excel.ApplicationClass
        xlWorkBook = xlApp.Workbooks.Add(misValue)
        xlWorkSheet = xlWorkBook.Sheets("Sheet1")

        Select Case xMode
            Case Is = "Chart of Accounts"
                xlWorkSheet.Cells(1, 1) = "Level" '0
                xlWorkSheet.Cells(1, 2) = "New Account Codes" '1
                xlWorkSheet.Cells(1, 3) = "Account Description" '2
                xlWorkSheet.Cells(1, 4) = "Sub Account of" '3
                xlWorkSheet.Cells(1, 5) = "Active" '4
                xlWorkSheet.Cells(1, 6) = "Account Type" '5

                xlWorkSheet.Columns(1).ColumnWidth = 5
                xlWorkSheet.Columns(2).ColumnWidth = 20
                xlWorkSheet.Columns(3).ColumnWidth = 50
                xlWorkSheet.Columns(4).ColumnWidth = 15
                xlWorkSheet.Columns(5).ColumnWidth = 8
                xlWorkSheet.Columns(6).ColumnWidth = 15

                Dim style As Excel.Style = xlWorkSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
                style.Font.Bold = True
                style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow)
                xlWorkSheet.Cells(1, 1).style = "NewStyle"
                xlWorkSheet.Cells(1, 2).style = "NewStyle"
                xlWorkSheet.Cells(1, 3).style = "NewStyle"
                xlWorkSheet.Cells(1, 4).style = "NewStyle"
                xlWorkSheet.Cells(1, 6).style = "NewStyle"

            Case Is = "Supplier List"
                xlWorkSheet.Cells(1, 1) = "Supplier Code" '1
                xlWorkSheet.Cells(1, 2) = "Supplier Name" '2
                xlWorkSheet.Cells(1, 3) = "Company Name" '3
                xlWorkSheet.Cells(1, 4) = "Salutation" '4
                xlWorkSheet.Cells(1, 5) = "Last Name" '5
                xlWorkSheet.Cells(1, 6) = "First Name" '6
                xlWorkSheet.Cells(1, 7) = "Middle Name" '7
                xlWorkSheet.Cells(1, 8) = "Main Address" '8
                xlWorkSheet.Cells(1, 9) = "Account No" '8
                xlWorkSheet.Cells(1, 10) = "TIN" '9
                xlWorkSheet.Cells(1, 11) = "Credit Limit" '10
                xlWorkSheet.Cells(1, 12) = "Contact Person 1" '11
                xlWorkSheet.Cells(1, 13) = "Contact Person 2" '12
                xlWorkSheet.Cells(1, 14) = "Tel No 1" '13
                xlWorkSheet.Cells(1, 15) = "Tel No 2" '14
                xlWorkSheet.Cells(1, 16) = "Fax No" '15
                xlWorkSheet.Cells(1, 17) = "Email Address" '16
                xlWorkSheet.Cells(1, 18) = "Name on Printed Check" '17
                xlWorkSheet.Cells(1, 19) = "Note" '18
                xlWorkSheet.Cells(1, 20) = "Accounts Payable (acnt_code)" '19
                xlWorkSheet.Cells(1, 21) = "Accounts Payable (acnt_name)" '20
                xlWorkSheet.Cells(1, 22) = "Supplier Type" '21
                xlWorkSheet.Cells(1, 23) = "Terms" '22
                xlWorkSheet.Cells(1, 24) = "Cost Center" '23

                xlWorkSheet.Columns(1).ColumnWidth = 15
                xlWorkSheet.Columns(2).ColumnWidth = 35
                xlWorkSheet.Columns(3).ColumnWidth = 35
                xlWorkSheet.Columns(4).ColumnWidth = 10
                xlWorkSheet.Columns(5).ColumnWidth = 20
                xlWorkSheet.Columns(6).ColumnWidth = 20
                xlWorkSheet.Columns(7).ColumnWidth = 20 'Middle Name
                xlWorkSheet.Columns(8).ColumnWidth = 80 'Primary Address
                xlWorkSheet.Columns(9).ColumnWidth = 15
                xlWorkSheet.Columns(10).ColumnWidth = 15
                xlWorkSheet.Columns(11).ColumnWidth = 15
                xlWorkSheet.Columns(12).ColumnWidth = 15
                xlWorkSheet.Columns(13).ColumnWidth = 15
                xlWorkSheet.Columns(14).ColumnWidth = 15
                xlWorkSheet.Columns(15).ColumnWidth = 15
                xlWorkSheet.Columns(16).ColumnWidth = 15
                xlWorkSheet.Columns(17).ColumnWidth = 15 'Email 
                xlWorkSheet.Columns(18).ColumnWidth = 15
                xlWorkSheet.Columns(19).ColumnWidth = 15
                xlWorkSheet.Columns(20).ColumnWidth = 32
                xlWorkSheet.Columns(21).ColumnWidth = 32
                xlWorkSheet.Columns(22).ColumnWidth = 15
                xlWorkSheet.Columns(23).ColumnWidth = 20



                Dim style As Excel.Style = xlWorkSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
                style.Font.Bold = True
                style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow)
                xlWorkSheet.Cells(1, 1).Style = "NewStyle"
                xlWorkSheet.Cells(1, 2).Style = "NewStyle"
                xlWorkSheet.Cells(1, 3).Style = "NewStyle"
                xlWorkSheet.Cells(1, 8).Style = "NewStyle"
                xlWorkSheet.Cells(1, 20).Style = "NewStyle"
                xlWorkSheet.Cells(1, 21).Style = "NewStyle"
                xlWorkSheet.Cells(1, 23).Style = "NewStyle"


            Case Is = "Customer List"
                xlWorkSheet.Cells(1, 1) = "Customer Code" '1
                xlWorkSheet.Cells(1, 2) = "Customer Name" '2
                xlWorkSheet.Cells(1, 3) = "Company Name" '3
                xlWorkSheet.Cells(1, 4) = "Salutation" '4
                xlWorkSheet.Cells(1, 5) = "Last Name" '5
                xlWorkSheet.Cells(1, 6) = "First Name" '6
                xlWorkSheet.Cells(1, 7) = "Middle Name" '7
                xlWorkSheet.Cells(1, 8) = "Accounts Receivable (Acnt_Code)" '8
                xlWorkSheet.Cells(1, 9) = "Accounts Receivable (Acnt_Name)" '9
                xlWorkSheet.Cells(1, 10) = "Sales Discount Account (Acnt_Code)" '10
                xlWorkSheet.Cells(1, 11) = "Sales Discount Account (Acnt_Desc)" '11
                xlWorkSheet.Cells(1, 12) = "Primary Address" '12
                xlWorkSheet.Cells(1, 13) = "Contact Person1" '13
                xlWorkSheet.Cells(1, 14) = "Contact Person 2" '14
                xlWorkSheet.Cells(1, 15) = "Tel No 1" '15
                xlWorkSheet.Cells(1, 16) = "Tel No 2" '16
                xlWorkSheet.Cells(1, 17) = "Fax No" '17
                xlWorkSheet.Cells(1, 18) = "Email" '18
                xlWorkSheet.Cells(1, 19) = "Bank Account No" '19
                xlWorkSheet.Cells(1, 20) = "Credit Limit" '20
                xlWorkSheet.Cells(1, 21) = "Note" '21
                xlWorkSheet.Cells(1, 22) = "CreditCard No" '22
                xlWorkSheet.Cells(1, 23) = "Name On Card" '23
                xlWorkSheet.Cells(1, 24) = "Expiration Year" '24
                xlWorkSheet.Cells(1, 25) = "Expiration Month" '25
                xlWorkSheet.Cells(1, 26) = "Address On Card" '26
                xlWorkSheet.Cells(1, 27) = "Customer Type" '27
                xlWorkSheet.Cells(1, 28) = "Terms Name" '28
                xlWorkSheet.Cells(1, 29) = "Sales Rep" '29
                xlWorkSheet.Cells(1, 30) = "Sales Tax Code Name" '30

                xlWorkSheet.Columns(1).ColumnWidth = 15
                xlWorkSheet.Columns(2).ColumnWidth = 35
                xlWorkSheet.Columns(3).ColumnWidth = 35
                xlWorkSheet.Columns(4).ColumnWidth = 10
                xlWorkSheet.Columns(5).ColumnWidth = 20
                xlWorkSheet.Columns(6).ColumnWidth = 20
                xlWorkSheet.Columns(7).ColumnWidth = 20 'Middle Name
                xlWorkSheet.Columns(8).ColumnWidth = 32
                xlWorkSheet.Columns(9).ColumnWidth = 32
                xlWorkSheet.Columns(10).ColumnWidth = 32
                xlWorkSheet.Columns(11).ColumnWidth = 32
                xlWorkSheet.Columns(12).ColumnWidth = 80 'Primary Address
                xlWorkSheet.Columns(13).ColumnWidth = 15
                xlWorkSheet.Columns(14).ColumnWidth = 15
                xlWorkSheet.Columns(15).ColumnWidth = 15
                xlWorkSheet.Columns(16).ColumnWidth = 15
                xlWorkSheet.Columns(17).ColumnWidth = 15
                xlWorkSheet.Columns(18).ColumnWidth = 15 'Email 
                xlWorkSheet.Columns(19).ColumnWidth = 15
                xlWorkSheet.Columns(20).ColumnWidth = 15
                xlWorkSheet.Columns(21).ColumnWidth = 15
                xlWorkSheet.Columns(22).ColumnWidth = 15
                xlWorkSheet.Columns(23).ColumnWidth = 15
                xlWorkSheet.Columns(24).ColumnWidth = 15
                xlWorkSheet.Columns(25).ColumnWidth = 15
                xlWorkSheet.Columns(26).ColumnWidth = 15
                xlWorkSheet.Columns(27).ColumnWidth = 15
                xlWorkSheet.Columns(28).ColumnWidth = 15
                xlWorkSheet.Columns(29).ColumnWidth = 15
                xlWorkSheet.Columns(30).ColumnWidth = 15
                xlWorkSheet.Columns(31).ColumnWidth = 15

                Dim style As Excel.Style = xlWorkSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
                style.Font.Bold = True
                style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow)
                xlWorkSheet.Cells(1, 1).Style = "NewStyle"
                xlWorkSheet.Cells(1, 2).Style = "NewStyle"
                xlWorkSheet.Cells(1, 3).Style = "NewStyle"
                xlWorkSheet.Cells(1, 8).Style = "NewStyle"
                xlWorkSheet.Cells(1, 9).Style = "NewStyle"
                xlWorkSheet.Cells(1, 12).Style = "NewStyle"

            Case Is = "Account Register"
                xlWorkSheet.Cells(1, 1) = "DocNumber"
                xlWorkSheet.Cells(1, 2) = "Employee No." '1
                xlWorkSheet.Cells(1, 3) = "FullName" '2
                xlWorkSheet.Cells(1, 4) = "Schedule" '3
                xlWorkSheet.Cells(1, 5) = "Closed" '4

                xlWorkSheet.Columns(1).ColumnWidth = 18
                xlWorkSheet.Columns(2).ColumnWidth = 15 '1
                xlWorkSheet.Columns(3).ColumnWidth = 50 '2
                xlWorkSheet.Columns(4).ColumnWidth = 10 '3
                xlWorkSheet.Columns(5).ColumnWidth = 50 '4

                Dim style As Excel.Style = xlWorkSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
                style.Font.Bold = True
                style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow)
                xlWorkSheet.Cells(1, 1).style = "NewStyle"
                xlWorkSheet.Cells(1, 2).Style = "NewStyle"
                xlWorkSheet.Cells(1, 3).Style = "NewStyle"
                xlWorkSheet.Cells(1, 4).Style = "NewStyle"
                xlWorkSheet.Cells(1, 5).Style = "NewStyle"

            Case "Journal Entries"
                xlWorkSheet.Cells(1, 1) = "Doctype"
                xlWorkSheet.Cells(1, 2) = "Account Code" '1
                xlWorkSheet.Cells(1, 3) = "Account Name" '2
                xlWorkSheet.Cells(1, 4) = "Date" '3
                xlWorkSheet.Cells(1, 5) = "Particulars" '4
                xlWorkSheet.Cells(1, 6) = "Subsidiary No" '5
                xlWorkSheet.Cells(1, 7) = "Reference No" '6
                xlWorkSheet.Cells(1, 8) = "Debit" '7
                xlWorkSheet.Cells(1, 9) = "Credit" '8
                xlWorkSheet.Cells(1, 10) = "In House"


                xlWorkSheet.Columns(1).ColumnWidth = 18
                xlWorkSheet.Columns(2).ColumnWidth = 15 '1
                xlWorkSheet.Columns(3).ColumnWidth = 50 '2
                xlWorkSheet.Columns(4).ColumnWidth = 10 '3
                xlWorkSheet.Columns(5).ColumnWidth = 50 '4
                xlWorkSheet.Columns(6).ColumnWidth = 20 '5
                xlWorkSheet.Columns(7).ColumnWidth = 20 '6
                xlWorkSheet.Columns(8).ColumnWidth = 15 '7
                xlWorkSheet.Columns(9).ColumnWidth = 15 '8
                xlWorkSheet.Columns(10).ColumnWidth = 15


                Dim style As Excel.Style = xlWorkSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
                style.Font.Bold = True
                style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow)
                xlWorkSheet.Cells(1, 1).style = "NewStyle"
                xlWorkSheet.Cells(1, 2).Style = "NewStyle"
                xlWorkSheet.Cells(1, 3).Style = "NewStyle"
                xlWorkSheet.Cells(1, 4).Style = "NewStyle"
                xlWorkSheet.Cells(1, 5).Style = "NewStyle"
                xlWorkSheet.Cells(1, 8).Style = "NewStyle"
                xlWorkSheet.Cells(1, 9).Style = "NewStyle"

        End Select
        xlWorkSheet.Columns.Columns.HorizontalAlignment = 3
        xlWorkSheet.SaveAs(xFilePath)

        xlWorkBook.Close()
        xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)

        'MsgBox("Excel file created , you can find the file c:\")

        Dim ps As New ProcessStartInfo
        ps.UseShellExecute = True
        ps.FileName = xFilePath
        Process.Start(ps)

    End Sub

    Private Sub BtnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        DlgSaveUploader.ShowDialog()
    End Sub

    Private Sub DlgSaveUploader_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DlgSaveUploader.FileOk
        xFilePath = DlgSaveUploader.FileName
        Call GenerateTemplate()
    End Sub

    Private Sub DlgOpenUploader_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DlgOpenUploader.FileOk
        txtUploader.Text = DlgOpenUploader.FileName
        xFilePath = txtUploader.Text
        UploaderType = IO.Path.GetExtension(xFilePath)
        If GrdUploader.AllowUserToAddRows = True Then
            GrdUploader.AllowUserToAddRows = False
        End If
        gDT.Rows.Clear()
        gDT.Columns.Clear()

        Call UploadExcelFile()

        If GrdUploader.AllowUserToAddRows = False Then
            GrdUploader.AllowUserToAddRows = True
        End If
    End Sub

    Private Sub BtnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowse.Click
        DlgOpenUploader.ShowDialog()
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim xForm As New FrmMasterUploaderProcess
        xForm.GetUser() = Me.GetUser()
        xForm.xParent = Me
        xForm.ShowDialog()
    End Sub

    Private Sub frmMasterUploader_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub frmLoanMasterUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = xMode + " Uploader"
        txtTitleLabel.Text = xMode + " Uploader"
        txtDescription.Text = "Upload " + xMode + " here. Make sure you follow the template required for this uploader."
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        DlgSaveUploader.ShowDialog()
    End Sub

    Private Sub menuNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuNew.Click
        DlgSaveUploader.ShowDialog()
    End Sub

    Private Sub menuOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuOpen.Click
        DlgOpenUploader.ShowDialog()
    End Sub

    Private Sub menu_Close(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuClose.Click
        Close()
    End Sub

   
End Class