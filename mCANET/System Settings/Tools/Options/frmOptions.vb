Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmOptions

    Private gCon As New Clsappconfiguration()

#Region "Account Configuration"
    Private Sub FormatGridAccountConfiguration()
        Dim account As New DataGridViewComboBoxColumn
        Dim accountName As New DataGridViewTextBoxColumn
        Dim IsClosingCapital As New DataGridViewCheckBoxColumn


        Dim dtAccounts As DataTable = m_displayAccountsDS().Tables(0)

        With account
            .Items.Clear()
            .Name = "cAccounts"
            .HeaderText = "Accounts"
            .DataPropertyName = "acnt_id"
            .ValueMember = "acnt_id"
            .DisplayMember = "acnt_name"
            .DataSource = dtAccounts
        End With

        With accountName
            .DataPropertyName = "acnt_name"
            .Name = "cAccountName"
        End With

        With IsClosingCapital
            .Name = "cIsClosingCapital"
            .HeaderText = "Is Closing Capital"
            .DataPropertyName = "isCapitalForClosingEntry"
        End With

        With grdAccountConfig
            .Columns.Clear()
            .Columns.Add(account)
            .Columns.Add(IsClosingCapital)
            .Columns.Add(accountName)

            .Columns("cAccountName").Visible = False
        End With



    End Sub

    Private Sub LoadAccountsConfigurationDataToGrid()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_ConfigureAccounts", _
                New SqlParameter("@compID", gCompanyID()))

        grdAccountConfig.DataSource() = ds.Tables(0)
    End Sub

    Private Sub btnAcctgClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAcctgClose.Click
        Close()
    End Sub
#End Region

    Private Sub frmOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '1. Accounts Configuration
        Call LoadAccountsConfigurationDataToGrid()
        Call FormatGridAccountConfiguration()

    End Sub

    'TODO
    ' Saving and Validations
End Class