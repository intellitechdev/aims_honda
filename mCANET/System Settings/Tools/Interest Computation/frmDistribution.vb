﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmDistribution
    Private mycon As New Clsappconfiguration
    Private DividendKey As String
    Private PatronageKey As String
    Dim entryID As String

    Private Sub btnDividend_Click(sender As System.Object, e As System.EventArgs) Handles btnDividend.Click
        Try
            frmJVEntry.xMode = "JV"
            frmJVEntry.xType = "Dividend"
            frmJVEntry.ShowDialog()
            If frmJVEntry.DialogResult = Windows.Forms.DialogResult.OK Then
                txtDividend.Text = frmJVEntry.grdList.SelectedRows(0).Cells(1).Value.ToString()
                DividendKey = frmJVEntry.grdList.SelectedRows(0).Cells(0).Value.ToString()
            End If
        Catch ex As Exception
            txtDividend.Text = ""
            DividendKey = ""
        End Try

    End Sub

    Private Sub btnPatronage_Click(sender As System.Object, e As System.EventArgs) Handles btnPatronage.Click
        Try
            frmJVEntry.xMode = "JV"
            frmJVEntry.xType = "Patronage"
            frmJVEntry.ShowDialog()
            If frmJVEntry.DialogResult = Windows.Forms.DialogResult.OK Then
                txtPatronage.Text = frmJVEntry.grdList.SelectedRows(0).Cells(1).Value.ToString()
                PatronageKey = frmJVEntry.grdList.SelectedRows(0).Cells(0).Value.ToString()
            End If
        Catch ex As Exception
            txtPatronage.Text = ""
            PatronageKey = ""
        End Try
    End Sub

    Private Sub frmDistribution_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnGetDetails_Click(sender As System.Object, e As System.EventArgs) Handles btnGetDetails.Click
        grdList.Rows.Clear()
        Call LoadDividend()
        Call LoadPatronage()
        Call ComputeTotals()
    End Sub

    Private Sub LoadDividend()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, CommandType.StoredProcedure, "usp_t_tGenJournalLedger_getEntry_PerJVKey1",
                                                                        New SqlParameter("@JVKeyID", DividendKey))
        While rd.Read
            AddDetailItem(rd.Item("cMemID").ToString, rd.Item("cID").ToString, rd.Item("cName").ToString, rd.Item("cAccountRef").ToString,
                    rd.Item("cAccounts").ToString, rd.Item("cAccountCode").ToString, rd.Item("cAccountTitle").ToString,
                    rd.Item("Credit"), rd.Item("Debit"))
        End While
    End Sub

    Private Sub LoadPatronage()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, CommandType.StoredProcedure, "usp_t_tGenJournalLedger_getEntry_PerJVKey1",
                                                                        New SqlParameter("@JVKeyID", PatronageKey))
        While rd.Read
            AddDetailItem(rd.Item("cMemID").ToString, rd.Item("cID").ToString, rd.Item("cName").ToString, rd.Item("cAccountRef").ToString,
                    rd.Item("cAccounts").ToString, rd.Item("cAccountCode").ToString, rd.Item("cAccountTitle").ToString,
                    rd.Item("Credit"), rd.Item("Debit"))
        End While
    End Sub

    Private Sub AddDetailItem(ByVal memKey As String,
                              ByVal memID As String,
                              ByVal memName As String,
                              ByVal acntRef As String,
                              ByVal acntKey As String,
                              ByVal acntCode As String,
                              ByVal acntTitle As String,
                              ByVal Debit As Decimal,
                              ByVal Credit As Decimal)

        Try
            Dim row As String() =
             {memKey, memID, memName, acntRef, acntKey, acntCode, acntTitle, Format(CDec(Debit), "##,##0.00"), Format(CDec(Credit), "##,##0.00")}

            Dim nRowIndex As Integer
            With grdList

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
                '.Rows.Add()
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End Try
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If BackgroundWorker1.IsBusy = False Then
            pbStatus.Visible = True
            BackgroundWorker1.WorkerReportsProgress = True
            BackgroundWorker1.RunWorkerAsync()
        Else
            BackgroundWorker1.CancelAsync()
            pbStatus.Visible = True
            BackgroundWorker1.WorkerReportsProgress = True
            BackgroundWorker1.RunWorkerAsync()
        End If
    End Sub

    Private Sub saveEntryHeader(ByVal guid As String)
        Dim user As String = frmMain.currentUser.Text
        Dim maxSerialNo As Integer = Generate_JournalNo_Serial(dtpPostingDate.Value.Date, "JV")
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CAS_t_GeneralJournal_Header_InsertUpdate", _
                    New SqlParameter("@fxKeyJVNo", guid),
                    New SqlParameter("@companyID", gCompanyID()),
                    New SqlParameter("@fdTransDate", dtpPostingDate.Value),
                    New SqlParameter("@fdTotAmt", CDec(txtTotalCredit.Text)),
                    New SqlParameter("@user", user),
                    New SqlParameter("@doctypeInitial", "CHV"),
                    New SqlParameter("@maxSerialNo", maxSerialNo),
                    New SqlParameter("@fiEntryNo", txtEntryNo.Text),
                    New SqlParameter("@fbIsCancelled", 0),
                    New SqlParameter("@fbIsPosted", 1),
                    New SqlParameter("@fdDatePrepared", dtpPostingDate.Value))
            Call UpdateDocDate()
            Call saveCreditEntryPerSL(guid)

        Catch ex As Exception
            MsgBox("Error on saving Entry Header. (Private Sub saveEntryHeader)", MsgBoxStyle.Exclamation, "Distribution")
            Exit Sub
        End Try
    End Sub

    Private Sub ComputeTotals()
        Dim totalDebit As Decimal
        Dim totalCredit As Decimal

        Try
            For Each xRow As DataGridViewRow In grdList.Rows
                Dim Debit As Object = GetValuesInDataGridView(grdList, "Debit", xRow.Index)
                Dim Credit As Object = GetValuesInDataGridView(grdList, "Credit", xRow.Index)

                If Debit IsNot Nothing Then
                    totalDebit += Debit
                End If

                If Credit IsNot Nothing Then
                    totalCredit += Credit
                End If
            Next

        Catch ex As InvalidCastException
            MessageBox.Show("You have entered an invalid data. Please recheck your entries.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        txtTotalDebit.Text = Format(CDec(totalDebit), "##,##0.00")
        txtTotalCredit.Text = Format(CDec(totalCredit), "##,##0.00")
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialNo As Integer
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialNo = rd.Item("maxSerialNo")
            Else
                serialNo = 0
            End If
        End Using

        Return serialNo
    End Function

    Private Sub BackgroundWorker1_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        entryID = System.Guid.NewGuid.ToString()
        saveEntryHeader(entryID)
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        Me.pbStatus.Value = e.ProgressPercentage
        Me.lblStatus.Text = "Processing... " & e.ProgressPercentage & "%"
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        pbStatus.Visible = False
        lblStatus.Text = ""
    End Sub

    Private Sub saveCreditEntryPerSL(ByVal guid As String)
        Dim journalNo As String = txtEntryNo.Text
        Dim jvDetailRecordID As String = System.Guid.NewGuid.ToString()
        Dim sMemo As String = ""
        Dim pkJournalID As String = guid

        Try
            For xRow As Integer = 0 To grdList.RowCount - 1
                pRowCount = grdList.RowCount
                Dim memKey As String = GetValuesInDataGridView(grdList, "memKey", xRow)
                If memKey <> "" Then
                    SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save",
                                                      New SqlParameter("@fiEntryNo", journalNo), _
                                                      New SqlParameter("@fxKeyAccount", GetValuesInDataGridView(grdList, "acntKey", xRow)), _
                                                      New SqlParameter("@fdDebit", CDec(GetValuesInDataGridView(grdList, "Debit", xRow))), _
                                                      New SqlParameter("@fdCredit", CDec(GetValuesInDataGridView(grdList, "Credit", xRow))), _
                                                      New SqlParameter("@fcMemo", sMemo), _
                                                      New SqlParameter("@fxKeyNameID", GetValuesInDataGridView(grdList, "memKey", xRow)), _
                                                      New SqlParameter("@fxKey", System.Guid.NewGuid.ToString()), _
                                                      New SqlParameter("@fk_JVHeader", guid), _
                                                      New SqlParameter("@transDate", dtpPostingDate.Value),
                                                      New SqlParameter("@fcLoanRef", Nothing),
                                                      New SqlParameter("@fcAccRef", GetValuesInDataGridView(grdList, "acntRef", xRow)))
                    Dim percent As Integer = (xRow / pRowCount) * 100
                    BackgroundWorker1.ReportProgress(percent)
                    mycon.sqlconn.Close()
                Else
                    SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save1",
                                                      New SqlParameter("@fiEntryNo", journalNo), _
                                                      New SqlParameter("@fxKeyAccount", GetValuesInDataGridView(grdList, "acntKey", xRow)), _
                                                      New SqlParameter("@fdDebit", CDec(GetValuesInDataGridView(grdList, "Debit", xRow))), _
                                                      New SqlParameter("@fdCredit", CDec(GetValuesInDataGridView(grdList, "Credit", xRow))), _
                                                      New SqlParameter("@fcMemo", sMemo), _
                                                      New SqlParameter("@fxKey", System.Guid.NewGuid.ToString()), _
                                                      New SqlParameter("@fk_JVHeader", guid), _
                                                      New SqlParameter("@transDate", dtpPostingDate.Value),
                                                      New SqlParameter("@fcLoanRef", Nothing),
                                                      New SqlParameter("@fcAccRef", GetValuesInDataGridView(grdList, "acntRef", xRow)))
                    Dim percent As Integer = (xRow / pRowCount) * 100
                    BackgroundWorker1.ReportProgress(percent)
                    mycon.sqlconn.Close()
                End If
            Next
            Dim sSQLCmd As String = "DELETE FROM tJVEntry_items WHERE fdDebit = 0 AND fdCredit = 0"
            SqlHelper.ExecuteScalar(mycon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox("CheckMembersSL")
            Exit Sub
        End Try
    End Sub

    Private Sub UpdateDocDate()
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update", _
                    New SqlParameter("@fcDocNumber", txtEntryNo.Text),
                    New SqlParameter("@fcDoctype", "CHECK VOUCHER"),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception
            MsgBox("Error on updating document number. (Private Sub UpdateDocDate)", MsgBoxStyle.Exclamation, "Distribution")
            Exit Sub
        End Try
    End Sub

    Private Sub btnCheckVoucher_Click(sender As System.Object, e As System.EventArgs) Handles btnCheckVoucher.Click
        Try
            frmJVEntry.xMode = "CHV"
            frmJVEntry.ShowDialog()
            If frmJVEntry.DialogResult = Windows.Forms.DialogResult.OK Then
                txtEntryNo.Text = frmJVEntry.grdList.SelectedRows(0).Cells(0).Value.ToString()
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub
End Class