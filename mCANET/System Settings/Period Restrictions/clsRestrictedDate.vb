Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class clsRestrictedDate

    Dim databaseConnection As New Clsappconfiguration

    Public Function checkIfRestricted(ByVal validDate As Date) As Boolean
        Dim SQLCommand As String = "SELECT fcDateTo,fcDateFrom "
        Dim fromDate As Date
        Dim toDate As Date

        SQLCommand &= "FROM dbo.tPeriodRestrictions "
        SQLCommand &= "WHERE fbOpen = 1 and fxKeyPeriod =" & validDate.Month

        Using DateRange As SqlDataReader = SqlHelper.ExecuteReader(databaseConnection.cnstring, CommandType.Text, SQLCommand)

            If DateRange.Read Then
                fromDate = DateRange.Item(0)
                toDate = DateRange.Item(1)

                'This is will compare if the date entered is within date range restricted
                If (Date.Compare(validDate, fromDate) = 1) And _
                   (Date.Compare(validDate, toDate.AddDays(1)) = -1) Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Using
    End Function

End Class
