Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Module modValidateData

    Private gCon As New Clsappconfiguration

    Public Function m_includeInactive(ByVal sSQLCmd As String) As Boolean
        sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    If rd.HasRows = True Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting : Validate Data")
        End Try
    End Function

    Public Function m_isTaxable(ByVal sKey) As Boolean
        Dim bTaxable As Boolean
        Dim dtTax As DataSet = m_GetSalesTaxCode(sKey)
        Try
            Using rd As DataTableReader = dtTax.CreateDataReader
                If rd.Read Then
                    bTaxable = rd.Item("fbTaxable")
                Else
                    bTaxable = False
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
        Return bTaxable
    End Function

    Public Function m_isActive(ByVal sTable As String, ByVal sField As String, ByVal sKey As String) As Boolean

        Dim sSQLCmd As String = "SELECT * FROM " & sTable
        sSQLCmd &= " WHERE fbActive = 1 and " & sField & " = '" & sKey & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    If rd.HasRows = True Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting : Is Active")
        End Try
    End Function

    Public Sub m_mkActive(ByVal sTable As String, ByVal sField As String, ByVal sKey As String, ByVal bActive As Boolean)
        Dim sSQLCmd As String = "UPDATE " & sTable & " SET fbActive ="
        sSQLCmd &= IIf(bActive = True, 1, 0) & " WHERE " & sField & " ='" & sKey & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting : Make Active/Inactive")
        End Try
    End Sub

    Public Function m_accountcodevalidation(ByVal accountcode As String) As String
        Dim xCnt As Integer = 0
        Dim sValue As String = ""
        Dim sStr() As String
        Dim sMsg As String = ""
        Dim sSQLCmd As String = "select acnt_code from dbo.mAccounts "
        sSQLCmd &= "where acnt_type ='" & gAcntTypeID() & "' and co_id='" & gCompanyID() & "' and acnt_deleted='" & 0 & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sValue += rd.Item(0).ToString + ","
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        sStr = Split(sValue, ",")
        For xCnt = 0 To sStr.Length - 2
            If sStr(xCnt) = accountcode Then
                sMsg = "exist"
            End If
        Next
        Return sMsg
    End Function

    Public Function m_segmentvalidation(ByVal accountcode As String) As String

        Dim xCnt As Integer = 0
        Dim sValue As String = ""
        Dim sStr() As String
        Dim sMsg As String = ""

        Dim sSQLCmd As String = "select seg_value from dbo.mAccountType "
        sSQLCmd &= "where co_id='" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sValue += rd.Item(0).ToString + ","
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
        sStr = Split(sValue, ",")
        For xCnt = 0 To sStr.Length - 2
            If sStr(xCnt) = accountcode Then
                sMsg = "exist"
            End If
        Next
        Return sMsg
    End Function


End Module
