Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_MF_paymentMethodAddEdit
    Private gCon As New Clsappconfiguration

    Private sKeyPayMethod As String
    Private sKeyPayType As String
    Private sKeyPayTypeInitial As String

    Public Property KeyPayMethod() As String
        Get
            Return sKeyPayMethod
        End Get
        Set(ByVal value As String)
            sKeyPayMethod = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub addEditTerms()
        Dim sSQLCmd As String = "usp_m_paymentMethod_update "
        sSQLCmd &= "@fxKeyPaymentMethod = '" & sKeyPayMethod & "'"
        sSQLCmd &= ",@fcPaymentMethodName = '" & txtPayName.Text & "'"
        sSQLCmd &= ",@fbActive =" & IIf(chkInactive.Checked = True, 0, 1)
        sSQLCmd &= ",@fxKeyPaymentType = '" & sKeyPayType & "'"
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MsgBox("Record successfully updated", MsgBoxStyle.Information, "Add/Edit Payment Method")
            m_paymentMethodList(frm_MF_paymentMethod.chkIncludeInactive.CheckState, frm_MF_paymentMethod.grdPayment)
            m_chkSettings(frm_MF_paymentMethod.chkIncludeInactive, frm_MF_paymentMethod.SQLQuery)
            m_loadPaymentMethod(frm_cust_masterCustomerAddEdit.cboPaymentMethodName, frm_cust_masterCustomerAddEdit.cboPaymentMethodID)
            m_loadPaymentMethod(frm_cust_ReceivePayment.cboPaymentMethodName, frm_cust_ReceivePayment.cboPaymentMethodID)
        Catch ex As Exception
            'Throw ex
            MessageBox.Show(Err.Description, "Add/Edit Payment Method")
        End Try

    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        addEditTerms()
        Me.Close()
    End Sub

    Private Sub loadPayment()
        Dim sSQLCmd As String = "SELECT * FROM mCustomer03PaymentMethod "
        sSQLCmd &= "WHERE fxKeyPaymentMethod = '" & sKeyPayMethod & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtPayName.Text = rd.Item("fcPaymentMethodName")
                    chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
                    sKeyPayTypeInitial = rd.Item("fxKeyPaymentType").ToString
                Else
                    txtPayName.Text = ""
                    chkInactive.Checked = False
                    sKeyPayTypeInitial = ""
                End If
            End Using
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Load Payment Method")
        End Try
    End Sub

    Private Sub frm_MF_termsAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sKeyPayMethod = "" Or sKeyPayMethod = Nothing Then
            sKeyPayMethod = Guid.NewGuid.ToString
            Me.Text = "Add Payment Method"
        End If
        loadPayment()
        loadType()

        'If sKeyPayTypeInitial <> "" Then
        '    cboPayType.SelectedValue = sKeyPayTypeInitial
        'End If
        'sKeyPayType = sKeyPayTypeInitial
    End Sub


    Private Sub loadType()
        Dim sSQLCmd As String = "SELECT fxKeyPaymentType, fcPaymentTypeName "
        sSQLCmd &= "FROM mCustomer04PaymentType "
        sSQLCmd &= "WHERE fxKeyCompany = '" & gCompanyID() & "' "
        sSQLCmd &= "ORDER BY fcPaymentTypeName"
        Try
            Dim dtPayment As DataSet = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            cboPayType.DataSource = dtPayment.Tables(0).DefaultView
            cboPayType.ValueMember = "fxKeyPaymentType"
            cboPayType.DisplayMember = "fcPaymentTypeName"
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Payment Type")
        End Try
    End Sub

    Private Sub cboPayType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayType.SelectedIndexChanged
        Try

            If cboPayType.SelectedValue.ToString <> "System.Data.DataRowView" Then
                sKeyPayType = cboPayType.SelectedValue.ToString
            Else
                sKeyPayType = ""
            End If

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Validation")
        End Try

    End Sub
End Class