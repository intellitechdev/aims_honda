﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmGeneralJournal
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        picLoading.Visible = True
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
        'LoadReport()
    End Sub
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\Adjustments.rpt")
        'rptsummary.Load(Application.StartupPath & "\Accounting Reports\SubReportJournalListingDaily.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@coid", gCompanyID())
        rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text)
        'rptsummary.SetParameterValue("@coid", gCompanyID(), "SubReportJournalListingDaily.rpt")
        'rptsummary.SetParameterValue("@fdTransDate", dtpListing.Text, "SubReportJournalListingDaily.rpt")

    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        If RBDaily.Checked = True Then
            LoadReport()
        Else
            Exit Sub
        End If

    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub frmGeneralJournal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        rptsummary.Close()
    End Sub
    Private Sub RBDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RBDaily.CheckedChanged
        If RBDaily.Checked = True Then
            dtpListing.Visible = True
            RBMonthly.Checked = False
            dtpMonth.Visible = False
        End If
        If RBMonthly.Checked = True Then
            RBDaily.Checked = False
            dtpListing.Visible = False
            dtpMonth.Visible = True
        End If
    End Sub

    Private Sub frmJournalListingReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RBDaily.Checked = False
        RBMonthly.Checked = False
    End Sub
    Friend Class IntervalItem
        Public Property Text As String
        Public Property Value As Integer
    End Class
End Class