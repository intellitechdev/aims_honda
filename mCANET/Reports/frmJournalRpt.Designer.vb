<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJournalRpt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJournalRpt))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboEmployeeNo = New MTGCComboBox
        Me.btnPreview = New System.Windows.Forms.Button
        Me.txtReviewed = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.crvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.bgwProcessReport = New System.ComponentModel.BackgroundWorker
        Me.btnHidePanel = New System.Windows.Forms.Button
        Me.Panel1.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.cboEmployeeNo)
        Me.Panel1.Controls.Add(Me.btnPreview)
        Me.Panel1.Controls.Add(Me.txtReviewed)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(856, 71)
        Me.Panel1.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Member:"
        '
        'cboEmployeeNo
        '
        Me.cboEmployeeNo.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboEmployeeNo.ArrowColor = System.Drawing.Color.Black
        Me.cboEmployeeNo.BackColor = System.Drawing.Color.White
        Me.cboEmployeeNo.BindedControl = CType(resources.GetObject("cboEmployeeNo.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboEmployeeNo.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboEmployeeNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboEmployeeNo.ColumnNum = 2
        Me.cboEmployeeNo.ColumnWidth = "300; 50"
        Me.cboEmployeeNo.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboEmployeeNo.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboEmployeeNo.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboEmployeeNo.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboEmployeeNo.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboEmployeeNo.DisplayMember = "Text"
        Me.cboEmployeeNo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboEmployeeNo.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cboEmployeeNo.DropDownForeColor = System.Drawing.Color.Black
        Me.cboEmployeeNo.DropDownHeight = 350
        Me.cboEmployeeNo.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.cboEmployeeNo.DropDownWidth = 450
        Me.cboEmployeeNo.GridLineColor = System.Drawing.Color.LightGray
        Me.cboEmployeeNo.GridLineHorizontal = False
        Me.cboEmployeeNo.GridLineVertical = False
        Me.cboEmployeeNo.IntegralHeight = False
        Me.cboEmployeeNo.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboEmployeeNo.Location = New System.Drawing.Point(109, 12)
        Me.cboEmployeeNo.ManagingFastMouseMoving = True
        Me.cboEmployeeNo.ManagingFastMouseMovingInterval = 30
        Me.cboEmployeeNo.Name = "cboEmployeeNo"
        Me.cboEmployeeNo.SelectedItem = Nothing
        Me.cboEmployeeNo.SelectedValue = Nothing
        Me.cboEmployeeNo.Size = New System.Drawing.Size(231, 22)
        Me.cboEmployeeNo.Sorted = True
        Me.cboEmployeeNo.TabIndex = 7
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(357, 25)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(96, 40)
        Me.btnPreview.TabIndex = 4
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'txtReviewed
        '
        Me.txtReviewed.Location = New System.Drawing.Point(109, 42)
        Me.txtReviewed.Name = "txtReviewed"
        Me.txtReviewed.Size = New System.Drawing.Size(231, 21)
        Me.txtReviewed.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Reviewed By:"
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(357, 240)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(124, 95)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 15
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'crvRpt
        '
        Me.crvRpt.ActiveViewIndex = -1
        Me.crvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvRpt.DisplayStatusBar = False
        Me.crvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvRpt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.crvRpt.Location = New System.Drawing.Point(0, 71)
        Me.crvRpt.Name = "crvRpt"
        Me.crvRpt.SelectionFormula = ""
        Me.crvRpt.ShowGotoPageButton = False
        Me.crvRpt.ShowGroupTreeButton = False
        Me.crvRpt.ShowParameterPanelButton = False
        Me.crvRpt.ShowRefreshButton = False
        Me.crvRpt.Size = New System.Drawing.Size(856, 458)
        Me.crvRpt.TabIndex = 13
        Me.crvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.crvRpt.ViewTimeSelectionFormula = ""
        '
        'bgwProcessReport
        '
        Me.bgwProcessReport.WorkerSupportsCancellation = True
        '
        'btnHidePanel
        '
        Me.btnHidePanel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnHidePanel.Location = New System.Drawing.Point(771, 73)
        Me.btnHidePanel.Name = "btnHidePanel"
        Me.btnHidePanel.Size = New System.Drawing.Size(78, 23)
        Me.btnHidePanel.TabIndex = 14
        Me.btnHidePanel.Text = "Show"
        Me.btnHidePanel.UseVisualStyleBackColor = False
        '
        'frmJournalRpt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(856, 529)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.btnHidePanel)
        Me.Controls.Add(Me.crvRpt)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(409, 376)
        Me.Name = "frmJournalRpt"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Journal Voucher"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents crvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents txtReviewed As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents bgwProcessReport As System.ComponentModel.BackgroundWorker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents cboEmployeeNo As MTGCComboBox
    Friend WithEvents btnHidePanel As System.Windows.Forms.Button
End Class
