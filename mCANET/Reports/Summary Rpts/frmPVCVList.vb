Public Class frmPVCVList

    Private Sub btnGenList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenList.Click
        Call load_list()
    End Sub

    Private Sub load_list()
        With Me.grdList
            .Columns.Clear()
            Dim dtsBills As DataTable = m_displayAPCVlisting(dtefrom.Value, dteEnd.Value, choice()).Tables(0)
            .DataSource = dtsBills.DefaultView
            .Columns(0).Width = 130
            .Columns(1).Width = 100
            .Columns(2).Width = 80
            .Columns(3).Width = 250
            .Columns(4).Width = 300
            .Columns(4).Width = 100
        End With
    End Sub

    Private Function choice() As Integer
        Dim result As Integer
        If chkAP.Checked = True Then
            result = 1
        ElseIf chkCV.Checked = True Then
            result = 0
        End If
        Return result
    End Function

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If choice() = 1 Then
            gStartDate = dtefrom.Value
            gEndDate = dteEnd.Value
            frmAcntPayableVoucher_list.Show()
        ElseIf choice() = 0 Then
            gStartDate = dtefrom.Value
            gEndDate = dteEnd.Value
            frmCheckVoucher_list.Show()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCV.CheckedChanged

    End Sub
End Class