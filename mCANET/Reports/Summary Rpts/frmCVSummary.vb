Public Class frmCVSummary

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ComboBox1.SelectedItem <> "" Then
            gPayeeID = m_GetNameID(ComboBox1.SelectedItem, " Supplier")
            gRecurringStartDate = DateTimePicker1.Value
            gRecurringEndDate = DateTimePicker2.Value
            frmChkVoucher_summary.Show()
        End If
    End Sub

    Private Sub frmCVSummary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call m_GetSuppliers(ComboBox1, Me)
    End Sub
End Class