Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared


Public Class frmsupplierMasterReport


    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub frmSupplierMasterReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmSupplierMasterReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        rptsummary.Load(Application.StartupPath & "\Accounting Reports\SupplierMasterReport.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, "sa")
        rptsummary.Refresh()

        rptsummary.SetParameterValue("@companyName", gCompanyName)
        rptsummary.SetParameterValue("@SupplierName", getSupplierName(gKeySupplier))
        rptsummary.SetParameterValue("@transactionCoverage", frm_vend_masterVendor.dteDateRange.SelectedItem)
        rptsummary.SetParameterValue("@companyAddress", getCompanyAddress())

     
        Me.crvSupplierMaster.Visible = True
        Me.crvSupplierMaster.ReportSource = rptsummary
        Me.Cursor = Cursors.Default
        frm_vend_masterVendor.Cursor = Cursors.Default

    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table

        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    'tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

  

    
    Private Sub grdReportData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub crvSupplierMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles crvSupplierMaster.Load

    End Sub
End Class