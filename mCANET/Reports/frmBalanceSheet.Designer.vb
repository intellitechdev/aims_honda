<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBalanceSheet
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBalanceSheet))
        Me.crvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dateFrom = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboLoadLevels = New System.Windows.Forms.ComboBox()
        Me.dateTo = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnGO = New System.Windows.Forms.Button()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.bgwLoadReport = New System.ComponentModel.BackgroundWorker()
        Me.Panel1.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'crvRpt
        '
        Me.crvRpt.ActiveViewIndex = -1
        Me.crvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvRpt.EnableDrillDown = False
        Me.crvRpt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.crvRpt.Location = New System.Drawing.Point(0, 28)
        Me.crvRpt.Name = "crvRpt"
        Me.crvRpt.SelectionFormula = ""
        Me.crvRpt.ShowGotoPageButton = False
        Me.crvRpt.ShowGroupTreeButton = False
        Me.crvRpt.ShowParameterPanelButton = False
        Me.crvRpt.ShowRefreshButton = False
        Me.crvRpt.Size = New System.Drawing.Size(662, 394)
        Me.crvRpt.TabIndex = 11
        Me.crvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.crvRpt.ViewTimeSelectionFormula = ""
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dateFrom)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.cboLoadLevels)
        Me.Panel1.Controls.Add(Me.dateTo)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btnGO)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(662, 28)
        Me.Panel1.TabIndex = 10
        '
        'dateFrom
        '
        Me.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateFrom.Location = New System.Drawing.Point(198, 3)
        Me.dateFrom.Name = "dateFrom"
        Me.dateFrom.Size = New System.Drawing.Size(98, 21)
        Me.dateFrom.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Type:"
        '
        'cboLoadLevels
        '
        Me.cboLoadLevels.FormattingEnabled = True
        Me.cboLoadLevels.Items.AddRange(New Object() {"Detailed"})
        Me.cboLoadLevels.Location = New System.Drawing.Point(61, 4)
        Me.cboLoadLevels.Name = "cboLoadLevels"
        Me.cboLoadLevels.Size = New System.Drawing.Size(131, 21)
        Me.cboLoadLevels.TabIndex = 7
        '
        'dateTo
        '
        Me.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dateTo.Location = New System.Drawing.Point(325, 3)
        Me.dateTo.Name = "dateTo"
        Me.dateTo.Size = New System.Drawing.Size(98, 21)
        Me.dateTo.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(302, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(18, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "to"
        '
        'btnGO
        '
        Me.btnGO.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGO.Location = New System.Drawing.Point(429, 2)
        Me.btnGO.Name = "btnGO"
        Me.btnGO.Size = New System.Drawing.Size(141, 23)
        Me.btnGO.TabIndex = 4
        Me.btnGO.Text = "Preview Report"
        Me.btnGO.UseVisualStyleBackColor = True
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(258, 159)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(123, 98)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 12
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'bgwLoadReport
        '
        '
        'frmBalanceSheet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(662, 422)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.crvRpt)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmBalanceSheet"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Balance Sheet"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnGO As System.Windows.Forms.Button
    Friend WithEvents dateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboLoadLevels As System.Windows.Forms.ComboBox
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents bgwLoadReport As System.ComponentModel.BackgroundWorker
    Friend WithEvents dateFrom As System.Windows.Forms.DateTimePicker
End Class
