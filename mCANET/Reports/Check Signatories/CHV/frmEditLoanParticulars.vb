﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmEditLoanParticulars
    Private gCon As New Clsappconfiguration()
    Private xCount As String
    Public docnum As String
    Public xLoan As String

    Private Sub frmEditLoanParticulars_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadItems()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Public Sub LoadItems()
        grdLoanParticulars.Rows.Clear()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CAS_rpt_PrintVoucher_LoanParticulars1", _
                                   New SqlParameter("@DocNum", docnum),
                                   New SqlParameter("@LoanNo", xLoan))
        Try

            While rd.Read
                Dim row As String() = New String() {rd.Item(1).ToString, Format(CDec(rd.Item(3).ToString), "##,##0.00"), rd.Item(11).ToString}
                grdLoanParticulars.Rows.Add(row)
            End While

            rd.Close()
            'AuditTrail_Save("DATABASE", "Data Migration Tools > Account Forward Balance > View " & txtDocNumber.Text)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub grdLoanParticulars_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdLoanParticulars.CellClick
        xCount = grdLoanParticulars.Item("fdCount", e.RowIndex).Value.ToString
        txtAccountTitle.Text = grdLoanParticulars.Item("AccountTitle", e.RowIndex).Value.ToString
        txtAmount.Text = grdLoanParticulars.Item("Amount", e.RowIndex).Value.ToString
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim sSQLCmd As String = "DELETE FROM tbl_RebateEditable WHERE fdCount='" & xCount & "' "
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        grdLoanParticulars.Rows.Remove(grdLoanParticulars.CurrentRow)
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim sSQLCmd As String = "UPDATE tbl_RebateEditable SET AccountTitle = '" & txtAccountTitle.Text & "', fdAmount= " & CDec(txtAmount.Text) & " WHERE fdCount='" & xCount & "' "
        SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        MsgBox("Saved", MsgBoxStyle.Information)
        LoadItems()
    End Sub
End Class