<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBudgetMonitoring
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvBudgetMonitoring = New System.Windows.Forms.DataGridView
        Me.lblAccount = New System.Windows.Forms.Label
        Me.dteAsof = New System.Windows.Forms.DateTimePicker
        Me.cboAccounts = New System.Windows.Forms.ComboBox
        Me.lblYear = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.lblAsOfDate = New System.Windows.Forms.Label
        Me.btnPreview = New System.Windows.Forms.Button
        Me.PanelBudget = New System.Windows.Forms.Panel
        Me.txtBudgetAmount = New System.Windows.Forms.TextBox
        Me.lblBudget = New System.Windows.Forms.Label
        Me.lblSelectBills = New System.Windows.Forms.Label
        Me.btnSelectParkedBills = New System.Windows.Forms.Button
        Me.crvReport = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        CType(Me.dgvBudgetMonitoring, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelBudget.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvBudgetMonitoring
        '
        Me.dgvBudgetMonitoring.AllowUserToAddRows = False
        Me.dgvBudgetMonitoring.AllowUserToDeleteRows = False
        Me.dgvBudgetMonitoring.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBudgetMonitoring.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvBudgetMonitoring.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBudgetMonitoring.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBudgetMonitoring.Location = New System.Drawing.Point(0, 94)
        Me.dgvBudgetMonitoring.Name = "dgvBudgetMonitoring"
        Me.dgvBudgetMonitoring.Size = New System.Drawing.Size(774, 504)
        Me.dgvBudgetMonitoring.TabIndex = 1
        '
        'lblAccount
        '
        Me.lblAccount.AutoSize = True
        Me.lblAccount.Location = New System.Drawing.Point(9, 19)
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.Size = New System.Drawing.Size(103, 13)
        Me.lblAccount.TabIndex = 0
        Me.lblAccount.Text = "Choose an account:"
        '
        'dteAsof
        '
        Me.dteAsof.CustomFormat = "MMMMMdd, yyyy"
        Me.dteAsof.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dteAsof.Location = New System.Drawing.Point(362, 36)
        Me.dteAsof.Name = "dteAsof"
        Me.dteAsof.Size = New System.Drawing.Size(175, 20)
        Me.dteAsof.TabIndex = 2
        '
        'cboAccounts
        '
        Me.cboAccounts.FormattingEnabled = True
        Me.cboAccounts.Location = New System.Drawing.Point(12, 35)
        Me.cboAccounts.Name = "cboAccounts"
        Me.cboAccounts.Size = New System.Drawing.Size(247, 21)
        Me.cboAccounts.TabIndex = 3
        '
        'lblYear
        '
        Me.lblYear.AutoSize = True
        Me.lblYear.Location = New System.Drawing.Point(262, 17)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(88, 13)
        Me.lblYear.TabIndex = 4
        Me.lblYear.Text = "Year to evaluate:"
        '
        'cboYear
        '
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"})
        Me.cboYear.Location = New System.Drawing.Point(265, 35)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(91, 21)
        Me.cboYear.TabIndex = 5
        '
        'lblAsOfDate
        '
        Me.lblAsOfDate.AutoSize = True
        Me.lblAsOfDate.Location = New System.Drawing.Point(364, 17)
        Me.lblAsOfDate.Name = "lblAsOfDate"
        Me.lblAsOfDate.Size = New System.Drawing.Size(34, 13)
        Me.lblAsOfDate.TabIndex = 6
        Me.lblAsOfDate.Text = "As of:"
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(657, 35)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(105, 23)
        Me.btnPreview.TabIndex = 2
        Me.btnPreview.Text = "Preview Report"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'PanelBudget
        '
        Me.PanelBudget.Controls.Add(Me.txtBudgetAmount)
        Me.PanelBudget.Controls.Add(Me.lblBudget)
        Me.PanelBudget.Controls.Add(Me.lblSelectBills)
        Me.PanelBudget.Controls.Add(Me.btnSelectParkedBills)
        Me.PanelBudget.Controls.Add(Me.btnPreview)
        Me.PanelBudget.Controls.Add(Me.lblAsOfDate)
        Me.PanelBudget.Controls.Add(Me.cboYear)
        Me.PanelBudget.Controls.Add(Me.lblYear)
        Me.PanelBudget.Controls.Add(Me.cboAccounts)
        Me.PanelBudget.Controls.Add(Me.dteAsof)
        Me.PanelBudget.Controls.Add(Me.lblAccount)
        Me.PanelBudget.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelBudget.Location = New System.Drawing.Point(0, 0)
        Me.PanelBudget.Name = "PanelBudget"
        Me.PanelBudget.Size = New System.Drawing.Size(774, 94)
        Me.PanelBudget.TabIndex = 0
        '
        'txtBudgetAmount
        '
        Me.txtBudgetAmount.Location = New System.Drawing.Point(617, 66)
        Me.txtBudgetAmount.Name = "txtBudgetAmount"
        Me.txtBudgetAmount.Size = New System.Drawing.Size(145, 20)
        Me.txtBudgetAmount.TabIndex = 8
        Me.txtBudgetAmount.Text = "0.00"
        Me.txtBudgetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBudget
        '
        Me.lblBudget.AutoSize = True
        Me.lblBudget.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudget.Location = New System.Drawing.Point(453, 70)
        Me.lblBudget.Name = "lblBudget"
        Me.lblBudget.Size = New System.Drawing.Size(118, 13)
        Me.lblBudget.TabIndex = 2
        Me.lblBudget.Text = "Budget for the Year"
        '
        'lblSelectBills
        '
        Me.lblSelectBills.AutoSize = True
        Me.lblSelectBills.Location = New System.Drawing.Point(9, 69)
        Me.lblSelectBills.Name = "lblSelectBills"
        Me.lblSelectBills.Size = New System.Drawing.Size(330, 13)
        Me.lblSelectBills.TabIndex = 2
        Me.lblSelectBills.Text = "Please select the parked bills you want to be generated in the report:"
        '
        'btnSelectParkedBills
        '
        Me.btnSelectParkedBills.Location = New System.Drawing.Point(546, 35)
        Me.btnSelectParkedBills.Name = "btnSelectParkedBills"
        Me.btnSelectParkedBills.Size = New System.Drawing.Size(105, 23)
        Me.btnSelectParkedBills.TabIndex = 7
        Me.btnSelectParkedBills.Text = "Display Bills"
        Me.btnSelectParkedBills.UseVisualStyleBackColor = True
        '
        'crvReport
        '
        Me.crvReport.ActiveViewIndex = -1
        Me.crvReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvReport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvReport.Location = New System.Drawing.Point(0, 94)
        Me.crvReport.Name = "crvReport"
        Me.crvReport.SelectionFormula = ""
        Me.crvReport.ShowRefreshButton = False
        Me.crvReport.Size = New System.Drawing.Size(774, 504)
        Me.crvReport.TabIndex = 2
        Me.crvReport.ViewTimeSelectionFormula = ""
        Me.crvReport.Visible = False
        '
        'frmBudgetMonitoring
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(774, 598)
        Me.Controls.Add(Me.crvReport)
        Me.Controls.Add(Me.dgvBudgetMonitoring)
        Me.Controls.Add(Me.PanelBudget)
        Me.Name = "frmBudgetMonitoring"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Budget Monitoring"
        CType(Me.dgvBudgetMonitoring, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelBudget.ResumeLayout(False)
        Me.PanelBudget.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvBudgetMonitoring As System.Windows.Forms.DataGridView
    Friend WithEvents lblAccount As System.Windows.Forms.Label
    Friend WithEvents dteAsof As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboAccounts As System.Windows.Forms.ComboBox
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblAsOfDate As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents PanelBudget As System.Windows.Forms.Panel
    Friend WithEvents btnSelectParkedBills As System.Windows.Forms.Button
    Friend WithEvents lblSelectBills As System.Windows.Forms.Label
    Friend WithEvents lblBudget As System.Windows.Forms.Label
    Friend WithEvents txtBudgetAmount As System.Windows.Forms.TextBox
    Friend WithEvents crvReport As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
