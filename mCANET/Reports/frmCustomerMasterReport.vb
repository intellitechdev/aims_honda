Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmCustomerMasterReport

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub frmCustomerMasterReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmCustomerMasterReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomerMasterReport.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, "sa")
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@companyName", gCompanyName)
        rptsummary.SetParameterValue("@fxKeyCustomer", gKeyCustomer)
        rptsummary.SetParameterValue("@CustomerName", getCustomerName(gKeyCustomer))
        rptsummary.SetParameterValue("@transactionCoverage", gtransactionsCoveredDate)
        rptsummary.SetParameterValue("@companyAddress", getCompanyAddress())

        If gTransactionType = "All Transactions" Then
            rptsummary.SetParameterValue("@fcType", "")
        Else
            rptsummary.SetParameterValue("@fcType", gTransactionType)
        End If

        rptsummary.SetParameterValue("@fdDateFrom", gDateFrom)
        rptsummary.SetParameterValue("@fdDateTo", gDateTo)


        Me.crvCustomerMaster.Visible = True
        Me.crvCustomerMaster.ReportSource = rptsummary
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

End Class