﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmPrintBatchCheck
    Private gCon As New Clsappconfiguration()
    Dim TransKey As String

    Private Sub btnCheckVoucher_Click(sender As System.Object, e As System.EventArgs) Handles btnCheckVoucher.Click
        Try
            frmJVEntry.xMode = "CHVUsed"
            frmJVEntry.ShowDialog()
            If frmJVEntry.DialogResult = Windows.Forms.DialogResult.OK Then
                txtCheckNumber.Text = frmJVEntry.grdList.SelectedRows(0).Cells(1).Value.ToString()
                TransKey = frmJVEntry.grdList.SelectedRows(0).Cells(0).Value.ToString()
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnGetDetails_Click(sender As System.Object, e As System.EventArgs) Handles btnGetDetails.Click
        grdCheckDetails.Rows.Clear()
        LoadDetails()
    End Sub

    Private Sub LoadDetails()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_CIMS_Distribution_GetDetails",
                                                                        New SqlParameter("@fk_tJVEntry", TransKey))
        While rd.Read
            AddDetailItem(rd.Item(0).ToString, rd.Item(1))
        End While
    End Sub

    Private Sub AddDetailItem(ByVal memName As String,
                              ByVal amount As Decimal)

        Try
            Dim row As String() =
             {False, memName, Format(CDec(amount), "##,##0.00"), ""}

            Dim nRowIndex As Integer
            With grdCheckDetails

                .Rows.Add(row)
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End Try
    End Sub

    Private Sub btnAddCheck_Click(sender As System.Object, e As System.EventArgs) Handles btnAddCheck.Click

    End Sub
End Class