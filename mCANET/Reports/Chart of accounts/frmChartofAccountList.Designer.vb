﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChartofAccountList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvChartofAccount = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.SuspendLayout()
        '
        'crvChartofAccount
        '
        Me.crvChartofAccount.ActiveViewIndex = -1
        Me.crvChartofAccount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvChartofAccount.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvChartofAccount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvChartofAccount.Location = New System.Drawing.Point(0, 0)
        Me.crvChartofAccount.Name = "crvChartofAccount"
        Me.crvChartofAccount.ShowGroupTreeButton = False
        Me.crvChartofAccount.Size = New System.Drawing.Size(735, 442)
        Me.crvChartofAccount.TabIndex = 0
        Me.crvChartofAccount.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'frmChartofAccountList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(735, 442)
        Me.Controls.Add(Me.crvChartofAccount)
        Me.Name = "frmChartofAccountList"
        Me.Text = "Chart of Account List"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvChartofAccount As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
