﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmCheckReport
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Dim MonthValue As String
    Public xTransID As String
    Private Sub frmCheckReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        picLoading.Visible = True
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        On Error Resume Next
        Dim CheckNum As String
        'Dim i As Integer = frmGeneralJournalEntries.grdListofEntries.CurrentRow.Index
        'CheckNum = frmGeneralJournalEntries.grdListofEntries.Item("CheckNo", 2).Value.ToString()
        'Dim CheckNum As String = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdListofEntries, "CheckNo", 0)

        rptsummary.Load(Application.StartupPath & "\Accounting Reports\Check.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@FX_CheckNumber", txtcheckNo.Text)
        rptsummary.SetParameterValue("@transID", xTransID)
    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub SignatoriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SignatoriesToolStripMenuItem.Click
        frmSignatories.ShowDialog()
    End Sub
End Class