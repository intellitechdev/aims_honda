<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGeneralLedger
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGeneralLedger))
        Me.panelTop = New System.Windows.Forms.Panel
        Me.btnApplyFilters = New System.Windows.Forms.Button
        Me.btnGenerate = New System.Windows.Forms.Button
        Me.crvGeneralLedger = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.bgwLoadReports = New System.ComponentModel.BackgroundWorker
        Me.loadingPic = New System.Windows.Forms.PictureBox
        Me.panelTop.SuspendLayout()
        CType(Me.loadingPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'panelTop
        '
        Me.panelTop.Controls.Add(Me.btnApplyFilters)
        Me.panelTop.Controls.Add(Me.btnGenerate)
        Me.panelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelTop.Location = New System.Drawing.Point(0, 0)
        Me.panelTop.Name = "panelTop"
        Me.panelTop.Size = New System.Drawing.Size(572, 60)
        Me.panelTop.TabIndex = 0
        '
        'btnApplyFilters
        '
        Me.btnApplyFilters.Image = Global.CSAcctg.My.Resources.Resources.ksirtet
        Me.btnApplyFilters.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnApplyFilters.Location = New System.Drawing.Point(12, 14)
        Me.btnApplyFilters.Name = "btnApplyFilters"
        Me.btnApplyFilters.Size = New System.Drawing.Size(129, 37)
        Me.btnApplyFilters.TabIndex = 16
        Me.btnApplyFilters.Text = "Modify Report"
        Me.btnApplyFilters.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnApplyFilters.UseVisualStyleBackColor = True
        '
        'btnGenerate
        '
        Me.btnGenerate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerate.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerate.Location = New System.Drawing.Point(147, 14)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(100, 37)
        Me.btnGenerate.TabIndex = 7
        Me.btnGenerate.Text = "Generate"
        Me.btnGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'crvGeneralLedger
        '
        Me.crvGeneralLedger.ActiveViewIndex = -1
        Me.crvGeneralLedger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvGeneralLedger.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.crvGeneralLedger.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvGeneralLedger.EnableDrillDown = False
        Me.crvGeneralLedger.EnableRefresh = False
        Me.crvGeneralLedger.Location = New System.Drawing.Point(0, 60)
        Me.crvGeneralLedger.Name = "crvGeneralLedger"
        Me.crvGeneralLedger.SelectionFormula = ""
        Me.crvGeneralLedger.ShowCloseButton = False
        Me.crvGeneralLedger.ShowGroupTreeButton = False
        Me.crvGeneralLedger.ShowParameterPanelButton = False
        Me.crvGeneralLedger.ShowRefreshButton = False
        Me.crvGeneralLedger.Size = New System.Drawing.Size(572, 526)
        Me.crvGeneralLedger.TabIndex = 1
        Me.crvGeneralLedger.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.crvGeneralLedger.ViewTimeSelectionFormula = ""
        '
        'bgwLoadReports
        '
        '
        'loadingPic
        '
        Me.loadingPic.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.loadingPic.Image = Global.CSAcctg.My.Resources.Resources.LoadingData
        Me.loadingPic.Location = New System.Drawing.Point(232, 280)
        Me.loadingPic.Name = "loadingPic"
        Me.loadingPic.Size = New System.Drawing.Size(117, 80)
        Me.loadingPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.loadingPic.TabIndex = 2
        Me.loadingPic.TabStop = False
        Me.loadingPic.Visible = False
        '
        'frmGeneralLedger
        '
        Me.AcceptButton = Me.btnGenerate
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(572, 586)
        Me.Controls.Add(Me.loadingPic)
        Me.Controls.Add(Me.crvGeneralLedger)
        Me.Controls.Add(Me.panelTop)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmGeneralLedger"
        Me.ShowInTaskbar = False
        Me.Text = "General Ledger"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panelTop.ResumeLayout(False)
        CType(Me.loadingPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelTop As System.Windows.Forms.Panel
    Friend WithEvents crvGeneralLedger As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents loadingPic As System.Windows.Forms.PictureBox
    Friend WithEvents bgwLoadReports As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnGenerate As System.Windows.Forms.Button
    Friend WithEvents btnApplyFilters As System.Windows.Forms.Button
End Class
