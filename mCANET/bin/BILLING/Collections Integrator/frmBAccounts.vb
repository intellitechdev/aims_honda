﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBAccounts

    Private con As New Clsappconfiguration
    Public doctype As String = ""
    Dim ds As DataSet
    Public cellPos As Integer
    Public xmode As String = ""

    Private Sub frmBAccounts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isLoaded("") Then
            txtSearch.Text = ""
            ActiveControl = txtSearch
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        doctype = ""
        xmode = ""
        dgvList.Columns.Clear()
        dgvList.DataSource = Nothing
        Me.Close()
    End Sub

    Private Function isLoaded(ByVal key As String) As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Collections_SelectAccts",
                                           New SqlParameter("@key", key))
            dgvList.DataSource = ds.Tables(0)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function isSelected() As Boolean
        Try
            'frmCollections_Integrator.dgvList.CurrentRow.Cells(8).Value = dgvList.SelectedRows(0).Cells(0).Value.ToString
            'frmCollections_Integrator.dgvList.CurrentRow.Cells(7).Value = dgvList.SelectedRows(0).Cells(1).Value.ToString
            If xmode = "bill" Then
                frmCollectionsBilling.txtDefaulAcct.Tag = dgvList.SelectedRows(0).Cells(0).Value.ToString
                frmCollectionsBilling.txtDefaulAcct.Text = dgvList.SelectedRows(0).Cells(1).Value.ToString
            Else
                frmCollectionsIntegrator.dgvList.CurrentRow.Cells(4).Value = dgvList.SelectedRows(0).Cells(0).Value.ToString
                frmCollectionsIntegrator.dgvList.CurrentRow.Cells(5).Value = dgvList.SelectedRows(0).Cells(1).Value.ToString
            End If

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                Me.Close()
                xmode = ""
            End If
        End If
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If isLoaded(txtSearch.Text) Then

        End If
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        If isSelected() Then
            Me.Close()
            xmode = ""
        End If
    End Sub

    Private Sub dgvList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvList.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                Me.Close()
                xmode = ""
            End If
        End If
    End Sub

End Class