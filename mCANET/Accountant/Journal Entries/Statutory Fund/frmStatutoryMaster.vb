﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes

Public Class frmStatutoryMaster
    Private mycon As New Clsappconfiguration
    Private key As Long

    Private Sub btnclose_Click(sender As System.Object, e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub StatutoryInsertUpdate()
        Dim MyGuid As New Guid(modGlobalDeclarations.gCompanyID)
        Dim accoungGuid As New Guid(Me.txtAccount.Tag.ToString())
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "spu_Statutory_InsertUpdate",
                                      New SqlParameter("@fxKeyStatutoryID", key),
                                      New SqlParameter("@fxKeyCompany", MyGuid),
                                      New SqlParameter("@fxKeyAccountID", accoungGuid),
                                      New SqlParameter("@fnPercent", Me.txtPercent.Text),
                                      New SqlParameter("@isYear", Me.rbYear.Checked),
                                      New SqlParameter("@isMonth", Me.rbMonth.Checked))
            MessageBox.Show("Record Succesfully Updated!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Statutory....", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub

    Private Sub StatutoryDelete()
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "spu_Statutory_Delete",
                                      New SqlParameter("@fxKeyStatutoryID", key))
            MessageBox.Show("Record Succesfully Deleted!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Delete Statutory....", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        Me.txtAccount.Enabled = True
        Me.txtPercent.Enabled = True
        Me.rbMonth.Enabled = True
        Me.rbYear.Enabled = True
        Me.txtAccount.Clear()
        Me.txtPercent.Text = "0.00"

        Me.btnSave.Visible = True
        Me.btnCancel.Visible = True

        Me.btnNew.Visible = False
        Me.btnEdit.Visible = False
        Me.btndelete.Visible = False
        key = 0
    End Sub

    Private Sub btnEdit_Click(sender As System.Object, e As System.EventArgs) Handles btnEdit.Click
        If Me.dgvListAccountRegister.SelectedRows.Count <> 0 Then
            Me.txtAccount.Enabled = True
            Me.txtPercent.Enabled = True
            Me.rbMonth.Enabled = True
            Me.rbYear.Enabled = True

            Me.btnSave.Visible = True
            Me.btnCancel.Visible = True

            Me.btnNew.Visible = False
            Me.btnEdit.Visible = False
            Me.btndelete.Visible = False
            key = Me.dgvListAccountRegister.CurrentRow.Cells(0).Value
        End If
    End Sub

    Private Sub btndelete_Click(sender As System.Object, e As System.EventArgs) Handles btndelete.Click
        If Me.dgvListAccountRegister.SelectedRows.Count <> 0 Then
            If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
                StatutoryDelete()
                AuditTrail_Save("Statutory Master", "Delete Data")
                Me.txtAccount.Clear()
                Me.txtPercent.Text = "0.00"
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        StatutoryInsertUpdate()
        If key = 0 Then
            AuditTrail_Save("Statutory Master", "Insert Data")
        Else
            AuditTrail_Save("Statutory Master", "Update Data")
        End If

    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.txtAccount.Enabled = False
        Me.txtPercent.Enabled = False
        Me.rbMonth.Enabled = False
        Me.rbYear.Enabled = False
        Me.txtAccount.Clear()
        Me.txtPercent.Text = "0.00"

        Me.btnSave.Visible = False
        Me.btnCancel.Visible = False

        Me.btnNew.Visible = True
        Me.btnEdit.Visible = True
        Me.btndelete.Visible = True
    End Sub

End Class