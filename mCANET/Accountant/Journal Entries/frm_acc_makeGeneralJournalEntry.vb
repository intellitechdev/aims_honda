Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.IO

'TODO ?grdListofEntries.CurrentRow.Cells(colEntNo).Value.ToString --->>>
'TODO use this to select the JV Entry to delete the entries that does not balance
'TODO strengten validations for credit and debit equality on:
'TODO 1)on exit 2)on selection of another unposted entry 3)on saving and
'TODO 4)Entering New Entry
Public Class frm_acc_makeGeneralJournalEntry

#Region "Declarations"

    Private gTrans As New clsTransactionFunctions
    Private gMaster As New modMasterFile
    Private gCon As New Clsappconfiguration

    Const colEntKey = 0
    Const colEntDate = 1
    Const colEntNo = 2
    Const colEntAdj = 3
    Const colEntName = 4
    Const colEntMemo = 5
    Const colEntTotalAmt = 6
    Const colcreatedby = 7
    Const colupdateby = 8
    Const colEntPost = 9

    Dim Account As String
    Private Mode As String = "Edit"

#End Region
#Region "Properties"

    Private journalEntryID As String
    Public Property GetJournalEntryId() As String
        Get
            Return journalEntryID
        End Get
        Set(ByVal value As String)
            journalEntryID = value
        End Set
    End Property

#End Region
#Region "Events"
    Dim checkDate As New clsRestrictedDate

    Private Sub DisableGrdGenJournal()
        grdGenJournalDetails.Enabled = False
        grdGenJournalDetails.DefaultCellStyle.BackColor = grdGenJournalDetails.BackgroundColor
        btnDeleteRow.Enabled = False
        BtnBrowseClass.Enabled = False
    End Sub
    Private Sub EnableGrdGenJournal()
        grdGenJournalDetails.Enabled = True
        grdGenJournalDetails.DefaultCellStyle.BackColor = Color.White
        btnDeleteRow.Enabled = True
        BtnBrowseClass.Enabled = True
    End Sub

    Private Sub btn_genjour_saveNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_genjour_saveNew.Click

        Account = grdGenJournalDetails.Item(0, 0).Value

        If IsdebitCreditbalance() = False Then
            MsgBox("Debit and Credit amount are not equal! " & vbNewLine & _
                "Please verify this entry before saving.", MsgBoxStyle.Exclamation, _
                "Debit and Credit Verifier!")
        ElseIf WasMemoEntered() = False Then
            MsgBox("You have not entered Memo required for this Entry." & vbNewLine & _
                "Please enter it on the first line of the Memo Column.", MsgBoxStyle.Exclamation _
                , "Memo Verifier")
        Else
            saveLedger()
            gfiJVKey = Guid.NewGuid.ToString
        End If
        Mode = "New"
    End Sub

    Private Function VerifyJVNumber() As Boolean
        If Mode = "New" Then
            Dim XsistedJV As String
            Dim ssqlVerifier As String = "SELECT fiEntryNo FROM dbo.tJVEntry WHERE fiEntryNo='" & txtJVNumber.Text & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, ssqlVerifier)
                    While rd.Read
                        XsistedJV = rd.Item(0).ToString
                    End While
                End Using
            Catch ex As Exception
                Return Nothing
            End Try

            If XsistedJV Is Nothing Then
                Return True
            Else
                MsgBox("The system detected that the JV number already exist!." + vbCr + "To avoid data losses, the system will automatically generate another JV number.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Existing JV number Detected!")
                Return False
            End If
        End If
    End Function
    Private Sub btnDisplay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisplay.Click
        Call DisplayListofEntries()
    End Sub
    Private Sub btnDeleteRow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteRow.Click
        Dim sSQLCmd As String
        If MsgBox("Do you really want to delete this record?", MsgBoxStyle.YesNo, "Delete JV") = MsgBoxResult.Yes Then
            With Me.grdGenJournalDetails
                If .Rows.Count - 1 > 0 Then
                    Dim xRow As Integer = .CurrentRow.Index
                    Dim sfxKey As String = ""

                    If mkDefaultValues(xRow, 7) <> Nothing Then
                        sfxKey = .Rows(xRow).Cells(7).Value.ToString
                    End If

                    ListDeleted.Items.Add(sfxKey)
                    .Rows.Remove(.Rows(xRow))

                    'sSQLCmd = "INSERT INTO dbo.sysDeletedJVEntries"
                    'sSQLCmd &= " (sysDeletedJVEntries) VALUES ('" & sfxKey & "')"
                    'SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)


                    Call computetotaldebitcredit()
                    If isJVEmpty() Then
                        sSQLCmd = "DELETE FROM tJVEntry WHERE fiEntryNo = '" & txtJVNumber.Text & "'"
                        SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                        Call CreateGridrow_GenJournalDetail()
                        Call DisplayListofEntries()
                    End If
                End If
            End With
        End If
    End Sub
    Private Sub btn_genjour_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_genjour_New.Click
        Mode = "New"

        Call EnableGrdGenJournal()
        Call ClearJVGrid()


    End Sub
    Private Sub btn_genjour_saveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_genjour_saveClose.Click
        Account = grdGenJournalDetails.Item(0, 0).Value

        Call verifier2()
        Mode = "Edit"
    End Sub

    Private Sub ts_preview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_preview.Click
        frmJournalRpt.ShowDialog()
    End Sub
    Private Sub ts_genjour_history_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_genjour_history.Click
        If getUsersGroup(gUserName) = 3 Then
            Me.Close()
            frmJVPosting.Show()
        Else
            MsgBox("You don't have enought rights to access this module, Please contact your system administrator to access this module.", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub frm_acc_makeGeneralJournalEntry_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IsdebitCreditbalance() = False Then
            If MsgBox("Debit and Credit amount are not equal! " & vbNewLine & _
                    "Are you sure you want to exit? You will lose " & vbNewLine & _
                    "all information on this entry unless this is corrected and saved.", MsgBoxStyle.YesNo, "Journal Entry") = MsgBoxResult.Yes Then
                'deleteJVEntry(grdListofEntries.CurrentRow.Cells(colEntNo).Value.ToString)
            Else
                e.Cancel = True
            End If
        End If
    End Sub
    Private Sub frm_acc_makeGeneralJournalEntry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Mode = "Loading"
        gfiJVKey = Guid.NewGuid.ToString

        Call GenerateJVNumber()
        Call CreateGridrow_GenJournalDetail()
        Call DisplayListofEntries()

        Call IsDateRestricted()

        If getUsersGroup(gUserName) = 3 Then
            btnDeleteJV.Visible = True
        Else
            btnDeleteJV.Visible = False
        End If

        If frmMain.LblClassActivator.Visible = True Then
            LblClassName.Visible = True
            Label8.Visible = True
            BtnBrowseClass.Visible = True
            BtnBrowseClass.Enabled = True
        Else
            LblClassName.Visible = False
            Label8.Visible = False
            BtnBrowseClass.Visible = False
            BtnBrowseClass.Enabled = False
        End If
    End Sub

    Private Sub DisplayJournalEntriesDetails()
        gfiJVKey = ""

        Try
            With Me.grdListofEntries
                If .RowCount > 1 Then
                    If Not .CurrentRow.Cells(colEntKey).Value.ToString Is Nothing Then
                        gfiJVKey = (.CurrentRow.Cells(colEntKey).Value.ToString)
                        gfiEntryNo = (.CurrentRow.Cells(colEntNo).Value.ToString)

                        'this evaluates if the JV entry is posted or not
                        'if posted, the entry details are displayed else not
                        If .CurrentRow.Cells(colEntPost).Value IsNot Nothing Then
                            If .CurrentRow.Cells(colEntPost).Value <> True Then
                                dte_genjour_date.Value = CDate(.CurrentRow.Cells(colEntDate).Value)
                                'CreateGridrow_GenJournalDetail()
                                gLoadEntries(.CurrentRow.Cells(colEntNo).Value.ToString)
                                EnableGrdGenJournal()
                            Else
                                'TODO the entry number must be the entry number of the JV Entry Selected
                                'CreateGridrow_GenJournalDetail()
                                dte_genjour_date.Value = CDate(.CurrentRow.Cells(colEntDate).Value)
                                txtJVNumber.Text = .CurrentRow.Cells(colEntNo).Value.ToString()
                                DisableGrdGenJournal()
                            End If
                        End If

                    End If
                End If
            End With
        Catch ex As Exception
        End Try

        computetotaldebitcredit()
    End Sub

    Private Sub grdListofEntries_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdListofEntries.CellClick
        Mode = "Loading"
        Mode = "Edit"


        Call DisplayJournalEntriesDetails()
        Call CreateGridrow_GenJournalDetail()

        If frmMain.LblClassActivator.Visible = True Then
            Dim xClassRefNo As String
            Dim xfxKeyJVNo As String = Convert.ToString(grdListofEntries.Item(0, grdListofEntries.CurrentRow.Index).Value)
            Dim sSqlGetJVClassRefNo As String = "SELECT fxKeyClassRefNo FROM dbo.tJVEntry WHERE fxKeyJVNo = '" & xfxKeyJVNo & "' AND fxKeyCompany = '" & gCompanyID() & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlGetJVClassRefNo)
                    While rd.Read
                        xClassRefNo = rd.Item(0).ToString
                    End While
                End Using
            Catch ex As Exception
            End Try
            If xClassRefNo Is Nothing Or xClassRefNo = "" Or xClassRefNo = " " Then
                LblClassName.Text = "Class not define"
                txtClassRefno.Text = ""
            Else
                Dim sSqlFindClassCmd As String = "SELECT ClassRefnumber, classname FROM dbo.mClasses WHERE ClassRefnumber = '" & xClassRefNo & "'"
                Dim dbClassName As String
                Dim ClassNameSpliter(0) As String
                Try
                    Using rd2 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlFindClassCmd)
                        While rd2.Read
                            txtClassRefno.Text = rd2.Item(0).ToString
                            dbClassName = rd2.Item(1).ToString
                            ClassNameSpliter = Split(dbClassName, "���")
                            LblClassName.Text = ClassNameSpliter(0).ToString
                        End While
                    End Using
                Catch ex As Exception
                End Try
            End If
        End If

        If e.RowIndex = grdListofEntries.NewRowIndex Then
            btnDeleteJV.Enabled = False
        Else
            btnDeleteJV.Enabled = True
        End If
    End Sub
    Private Sub grdListofEntries_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdListofEntries.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdListofEntries.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub

    Private Sub grdListofEntries_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdListofEntries.KeyDown
        DisplayJournalEntriesDetails()
        CreateGridrow_GenJournalDetail()
    End Sub
    Private Sub grdListofEntries_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdListofEntries.KeyUp
        DisplayJournalEntriesDetails()
        CreateGridrow_GenJournalDetail()
    End Sub

    Private Sub grdGenJournal_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellLeave
        computetotaldebitcredit()
        Account = grdGenJournalDetails.Item(0, 0).Value
    End Sub

    Private Sub grdGenJournal_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdGenJournalDetails.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdGenJournalDetails.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If



    End Sub

    Private Sub grdGenJournal_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournalDetails.CellValueChanged
        Account = grdGenJournalDetails.Item(0, 0).Value.ToString()
        If grdGenJournalDetails.CurrentRow.Cells(0).Value IsNot Nothing Then
            If Mode = "Edit" Then

                If e.ColumnIndex = 0 Then
                    If IsAccountActive() = False Then
                        MsgBox("Account is Already Inactive. Please choose another Account.", MsgBoxStyle.Exclamation, "Account Inactive")
                        grdGenJournalDetails.CurrentRow.Cells(0).Value = ""
                    End If
                End If
            End If
        End If
    End Sub


    Private Function IsAccountActive() As Boolean
        Dim Status As Integer = 0
        If grdGenJournalDetails.CurrentRow.Cells(0).Value Is Nothing Then
            MsgBox("You need to select an account.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No account detected")
            grdGenJournalDetails.Select()

        Else

            Account = grdGenJournalDetails.CurrentRow.Cells(0).Value.ToString()
            If Account = "" Then
                Account = grdGenJournalDetails.CurrentRow.Cells(0).Value.ToString()
            End If

            Dim _Account_temp As String() = Split(Account, "|")

            Dim Account_code As String = _Account_temp(0)
            Dim Account_desc As String = _Account_temp(1)

            Dim sSQLCmd As String = "usp_t_getAccountStatus "
            sSQLCmd &= "@acnt_code = '" & Trim(Account_code) & "'"
            sSQLCmd &= ",@acnt_desc = '" & Trim(Account_desc) & "'"
            sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"


            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                    If rd.Read Then
                        Status = CInt(rd.Item("fbActive"))
                    End If

                End Using
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            If Status = 0 Then
                IsAccountActive = False
            Else
                IsAccountActive = True
            End If

            Return IsAccountActive
        End If
    End Function

    Private Sub grdGenJournal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdGenJournalDetails.Click
        Mode = "Edit"
    End Sub

    Private Sub grdGenJournal_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdGenJournalDetails.CurrentCellDirtyStateChanged
        If grdGenJournalDetails.IsCurrentCellDirty Then
            grdGenJournalDetails.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub


    Private Sub grdGenJournal_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdGenJournalDetails.EditingControlShowing
        Dim comboBoxColumn As DataGridViewComboBoxColumn = Me.grdGenJournalDetails.Columns("cAccounts")
        If (grdGenJournalDetails.CurrentCellAddress.X = comboBoxColumn.DisplayIndex) Then
            Dim cb As ComboBox = e.Control
            If (cb IsNot Nothing) Then
                cb.DropDownStyle = ComboBoxStyle.DropDown
                cb.AutoCompleteMode = AutoCompleteMode.Suggest
                cb.AutoCompleteSource = AutoCompleteSource.ListItems
            End If
        End If
        Dim comboBoxTax As DataGridViewComboBoxColumn = Me.grdGenJournalDetails.Columns("cName")
        If (grdGenJournalDetails.CurrentCellAddress.X = comboBoxTax.DisplayIndex) Then
            Dim cbo As ComboBox = e.Control
            If (cbo IsNot Nothing) Then
                cbo.DropDownStyle = ComboBoxStyle.DropDown
                cbo.AutoCompleteMode = AutoCompleteMode.Suggest
                cbo.AutoCompleteSource = AutoCompleteSource.ListItems
            End If
        End If
    End Sub
    Private Sub grdGenJournal_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdGenJournalDetails.Validated
        computetotaldebitcredit()
    End Sub


    Private Sub dte_genjour_date_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dte_genjour_date.ValueChanged
        'Dim stmp() As String
        'Dim stmpid As String
        'stmp = Split(txt_genjour_entryno.Text, "-")
        'stmpid = "JV" + CStr(Format(CDec(dte_genjour_date.Value.Year), "0000")) + "-" + CStr(Format(CDec(dte_genjour_date.Value.Month), "00")) + "-" + CStr(Format(CDec(dte_genjour_date.Value.Day), "00")) + "-" + stmp(3)

        GenerateJVNumber()
        IsDateRestricted()
    End Sub

#End Region
#Region "Functions/Procedures"

    Public Sub recordtoLogFile(ByVal data_string As String, ByVal FilePath As String)
        Dim file_stream As New FileStream(Application.StartupPath & "\" & FilePath, FileMode.Append)
        Dim stream_writer As New StreamWriter(file_stream)
        stream_writer.WriteLine(data_string)
        stream_writer.Flush()
        file_stream.Close()
    End Sub

    Public Sub ClearJVGrid()
        'Call gTrans.gGridClear(Me.grdGenJournalDetails)

        GenerateJVNumber()
        GetJournalEntryId() = Guid.NewGuid.ToString

        grdGenJournalDetails.DataSource = Nothing
        Call CreateGridrow_GenJournalDetail()

        Me.txt_genjour_totaldebit.Text = "0.00"
        Me.txt_genjour_totalcredit.Text = "0.00"
    End Sub
    Private Sub gUpdateEntry(ByVal psKeyEntries As String)
        Dim sSQLCmd As String = "UPDATE    tJVEntry Set fdPosted ='" & 1 & "' where fiEntryNo='" & psKeyEntries & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
        End Try
    End Sub
    Public Sub gLoadEntries(ByVal psKeyEntries As String)
        With Me.grdGenJournalDetails
            Try
                Dim dtsTable As DataSet = m_GetGLJournalEntries(psKeyEntries)
                Dim xRow As Integer = 0
                txtJVNumber.Text = dtsTable.Tables(0).Rows(0).Item("EntryNo")

                .DataSource = dtsTable.Tables(0)



                'For xRow = 0 To dtsTable.Tables(0).Rows.Count - 1
                '    .Rows.Add()
                '    .Item("cAccounts", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcDescription")
                '    .Item("cDebit", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdDebit")), "##,##0.00")
                '    .Item("cCredit", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdCredit")), "##,##0.00")
                '    .Item("cMemo", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcMemo")
                '    .Item("cNote", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcNote")
                '    .Item("cName", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("Name")
                '    .Item("cBillable", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fbBillable")
                '    .Item("fxKey", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fxKey")
                'Next

            Catch ex As Exception
                MessageBox.Show(Err.Description, "Load Ledger Entries")
            End Try
        End With
    End Sub

    Private Sub GenerateJVNumber()
        Dim stemp As String = ""
        Dim xCnt As Integer = 0
        Dim sSQLCmd As String = "usp_xxxrandomid "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    stemp = rd.Item(0).ToString
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        txtJVNumber.Text = "JV" + CStr(Format(CDec(dte_genjour_date.Value.Year), "0000")) + "-" + CStr(Format(CDec(dte_genjour_date.Value.Month), "00")) + "-" + CStr(Format(CDec(dte_genjour_date.Value.Day), "00")) + "-" + CStr(stemp)
    End Sub

    Private Sub CreateGridrow_GenJournalDetail()

        Dim fxKey As New DataGridViewTextBoxColumn
        Dim colAccounts As New DataGridViewComboBoxColumn
        Dim txtDebit As New DataGridViewTextBoxColumn
        Dim txtCredit As New DataGridViewTextBoxColumn
        Dim txtMemo As New DataGridViewTextBoxColumn
        Dim txtNote As New DataGridViewTextBoxColumn
        Dim chkBillable As New DataGridViewCheckBoxColumn
        Dim cboNames As New DataGridViewComboBoxColumn

        Dim dtAccounts As DataTable = m_displayAccountsDS().Tables(0)
        Dim dtNames As DataTable = m_DisplayNamesJV().Tables(0)

        With colAccounts
            .Items.Clear()
            .HeaderText = "Accounts"
            .DataPropertyName = "fcDescription"
            .Name = "cAccounts"
            .DataSource = dtAccounts.DefaultView
            .DisplayMember = "code_description"
            .ValueMember = "code_description"
            .DropDownWidth = 500
            .AutoComplete = True
            .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            .MaxDropDownItems = 10
            .Resizable = DataGridViewTriState.True
        End With

        With cboNames
            .Items.Clear()
            .HeaderText = "Name"
            .DataPropertyName = "Name"
            .Name = "cName"
            .DataSource = dtNames.DefaultView
            .DisplayMember = "fcName"
            .ValueMember = "fcName"
            .DropDownWidth = 200
            .AutoComplete = True
            .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            .MaxDropDownItems = 10
            .Resizable = DataGridViewTriState.True
        End With

        With fxKey
            .Name = "fxKey"
            .DataPropertyName = "fxKey"
        End With

        With txtDebit
            .HeaderText = "Debit"
            .Name = "cDebit"
            .DataPropertyName = "fdDebit"

            .Width = 90
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtCredit
            .HeaderText = "Credit"
            .Name = "cCredit"
            .DataPropertyName = "fdCredit"
            .Width = 90
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtMemo
            .HeaderText = "Memo"
            .Name = "cMemo"
            .DataPropertyName = "fcMemo"
            .Width = 90
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With txtNote
            .HeaderText = "Note"
            .Name = "cNote"
            .DataPropertyName = "fcNote"
            .Width = 90
            .SortMode = DataGridViewColumnSortMode.NotSortable
        End With

        With chkBillable
            .FlatStyle = FlatStyle.Standard
            .HeaderText = "Billable"
            .Name = "cBillable"
            .DataPropertyName = "fbBillable"
            .Width = 50
        End With

        With Me.grdGenJournalDetails
            .Columns.Clear()
            .Columns.Add(colAccounts)
            .Columns.Add(txtMemo)
            .Columns.Add(txtDebit)
            .Columns.Add(txtCredit)

            .Columns.Add(txtNote)
            .Columns.Add(cboNames)
            .Columns.Add(chkBillable)
            .Columns.Add(fxKey)

            .Columns("cAccounts").Width = 220
            .Columns("cDebit").Width = 80
            .Columns("cCredit").Width = 80
            .Columns("cMemo").Width = 350
            .Columns("cNote").Width = 80
            .Columns("cName").Width = 200
            .Columns("cBillable").Width = 50
            .Columns("fxKey").Visible = False

            .Columns("cDebit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("cCredit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight

            .Columns("cDebit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("cCredit").DefaultCellStyle.Format = "##,##0.00"

            .Columns("cNote").Visible = False
        End With
    End Sub
    Private Sub load_genjour_lstofentries(ByVal sKeyEntryNo As String)
        With Me.grdGenJournalDetails
            .DataSource = m_GetGLJournalEntries(sKeyEntryNo).Tables(0)
            .Columns(0).Width = 120
            .Columns(1).Width = 100
            .Columns(2).Width = 80
            .Columns(3).Width = 150
            .Columns(4).Width = 150
            .Columns(5).Width = 100
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns(5).DefaultCellStyle.Format = "##,##0.00"
        End With
    End Sub
    Private Sub deleteJVEntry(ByVal JVEntryNo As String)
        
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "usp_t_jvEntryDelete", _
                New SqlParameter("@fiEntryNo", JVEntryNo))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Delete JV Entry", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Function isJVEmpty()
        Dim sSQLCmd As String
        sSQLCmd = "SELECT * FROM tJVEntry_items WHERE fiEntryNo = '" & txtJVNumber.Text & "'"

        Dim bEmpty As Boolean

        Try

            Using dr As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                bEmpty = True
                If dr.Read Then
                    bEmpty = False
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return bEmpty
    End Function
    Private Function isJVNoUsed() As Boolean
        Dim sSQLCmd As String
        sSQLCmd = "SELECT * FROM tJVEntry WHERE fiEntryNo = '" & txtJVNumber.Text & "'"
        sSQLCmd &= " AND fxKeyJVNo <> '" & gfiJVKey & "'"
        Dim bReturn As Boolean

        Try

            Using dr As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                bReturn = False
                If dr.Read Then
                    bReturn = True
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

        Return bReturn
    End Function
    Private Function isKeyNew() As Boolean
        Dim sSQLCmd As String
        Dim bReturn As Boolean
        sSQLCmd = "SELECT fxKeyJVNo FROM tJVEntry WHERE fxKeyJVNo = '" & gfiJVKey & "'"

        Try
            Using dr As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                bReturn = True
                If dr.Read Then
                    bReturn = False
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return bReturn
    End Function

    'TODO Possible error: when other entry is selected but the debit and credit did not refresh.
    'TODO center the text in the message
    Private Function IsdebitCreditbalance() As Boolean
        If txt_genjour_totaldebit.Text <> txt_genjour_totalcredit.Text Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function WasMemoEntered() As Boolean
        If mkDefaultValues(0, 3) <> Nothing Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub verifier2()
        If txt_genjour_totaldebit.Text = txt_genjour_totalcredit.Text Then
            If mkDefaultValues(0, 3) <> Nothing Then
                If saveLedger() Then
                    Call gTrans.gGridClear(Me.grdGenJournalDetails)
                    Me.txt_genjour_totaldebit.Text = "0.00"
                    Me.txt_genjour_totalcredit.Text = "0.00"
                    gfiJVKey = Nothing
                    Me.Close()
                End If
            Else
                MsgBox("You forgot to put a memo on the first row...", MsgBoxStyle.Information, Me.Text)
            End If
        Else
            MsgBox("Debit and Credit amount are not equal, please verify the amount distribution...", MsgBoxStyle.Information, Me.Text)
        End If
    End Sub

    Private Function saveLedger() As Boolean

        If Mode = "New" Then
            Do While VerifyJVNumber() <> True
                GenerateJVNumber()
                VerifyJVNumber()
            Loop
        End If

        If isKeyNew() Then
            While isJVNoUsed()
                Call GenerateJVNumber()
            End While
        End If

        If saveJV_header() = True Then
            If saveJV_details() = True Then
                Dim sSQL As String

                'to update database/ removing all jventryitems that has been deleted from the datagridview
                Dim DeletedItem As String
                Do While ListDeleted.Items.Count <> 0
                    ListDeleted.SelectedIndex = 0
                    ListDeleted.Select()
                    DeletedItem = ListDeleted.SelectedItem
                    sSQL = "DELETE FROM tJVEntry_items "
                    sSQL &= "WHERE fxKeyJVDetails ='" & DeletedItem & "'"
                    Try
                        SqlHelper.ExecuteDataset(gCon.sqlconn, CommandType.Text, sSQL)
                    Catch ex As Exception
                    End Try
                    ListDeleted.Items.Remove(ListDeleted.SelectedItem)
                Loop




                Try

                    MsgBox("Save successful...", MsgBoxStyle.Information, Me.Text)
                    Mode = "Edit"
                Catch ex As Exception
                    MsgBox("Please reenter your data.", MsgBoxStyle.Information, "Error")
                End Try

                Call gTrans.gGridClear(Me.grdGenJournalDetails)
                Me.txt_genjour_totaldebit.Text = "0.00"
                Me.txt_genjour_totalcredit.Text = "0.00"

                Call GenerateJVNumber()
                Call CreateGridrow_GenJournalDetail()
                Call DisplayListofEntries()
                Return True
            Else
                MsgBox("Save NOT successful...", MsgBoxStyle.Critical, Me.Text)
                Return False
            End If
        Else
            MsgBox("Save NOT successful...", MsgBoxStyle.Critical, Me.Text)
            Return False
        End If
    End Function

    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdGenJournalDetails.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdGenJournalDetails.Rows(irow).Cells(iCol).Value)
    End Function
    Private Function saveJV_header() As Boolean
        Dim sNameID As String = ""
        Dim sMemo As String = ""
        Dim sNote As String = ""
        With Me.grdGenJournalDetails
            If .Rows.Count > 0 Then

                If mkDefaultValues(0, 5) <> Nothing Then
                    Dim stmp() As String
                    stmp = Split(.Rows(0).Cells(5).Value.ToString, "/")
                    sNameID = m_getname(stmp(0), stmp(1))
                End If
                If mkDefaultValues(0, 3) <> Nothing Then
                    sMemo = .Rows(0).Cells(3).Value.ToString
                End If
                If mkDefaultValues(0, 4) <> Nothing Then
                    sNote = .Rows(0).Cells(4).Value.ToString
                End If
                Dim sSQLCmd As String = "usp_t_tJVEntry_save "
                sSQLCmd &= " @fxKeyJVNo = '" & GetJournalEntryId() & "' "
                sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
                sSQLCmd &= ",@fiEntryNo='" & txtJVNumber.Text & "' "
                sSQLCmd &= ",@fdTransDate='" & Microsoft.VisualBasic.FormatDateTime(dte_genjour_date.Value, DateFormat.ShortDate) & "' "
                sSQLCmd &= ",@fbAdjust='" & checkifadjusted() & "' "
                sSQLCmd &= ",@fxKeyNameID=" & IIf(sNameID = "", "NULL", "'" & sNameID & "'") & " "
                sSQLCmd &= ",@fcMemo='" & sMemo & "' "
                sSQLCmd &= ",@fcNote='" & sNote & "' "
                sSQLCmd &= ",@fdTotAmt='" & CDec(txt_genjour_totaldebit.Text) & "' "
                sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "
                sSQLCmd &= ",@fxKeyClassRefNo='" & txtClassRefno.Text & "' "
                Try
                    SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                    Return True
                Catch ex As Exception
                    Return False
                End Try
            Else
                Return False
            End If
        End With
    End Function
    Private Function saveJV_details() As Boolean
        Dim xCnt As Integer
        With Me.grdGenJournalDetails
            If .Rows.Count > 0 Then
                For xCnt = 0 To .Rows.Count - 1
                    Dim sAccount As String = ""
                    Dim dDebit As Decimal = 0.0
                    Dim dCredit As Decimal = 0.0
                    Dim sMemo As String = ""
                    Dim sNote As String = ""
                    Dim sNameID As String = ""
                    Dim sBillable As Integer = 0
                    Dim sfxKey As String = ""
                    Dim account_Desc As String
                    Dim account_Code As String


                    If mkDefaultValues(xCnt, 0) <> Nothing Then
                        If mkDefaultValues(xCnt, 0) <> Nothing Then
                            account_Code = .Rows(xCnt).Cells(0).Value.ToString
                            account_Code = account_Code.Substring(0, account_Code.IndexOf(" | "))
                            sAccount = m_GetAccountID_byAccountCode(account_Code)
                        End If
                        If mkDefaultValues(xCnt, 1) <> Nothing Then
                            dDebit = CDec(.Rows(xCnt).Cells("cDebit").Value)
                        End If
                        If mkDefaultValues(xCnt, 2) <> Nothing Then
                            dCredit = CDec(.Rows(xCnt).Cells("cCredit").Value)
                        End If
                        If mkDefaultValues(xCnt, 3) <> Nothing Then
                            sMemo = .Rows(xCnt).Cells("cMemo").Value.ToString
                        End If
                        If mkDefaultValues(xCnt, 4) <> Nothing Then
                            sNote = .Rows(xCnt).Cells("cNote").Value.ToString
                        End If
                        If mkDefaultValues(xCnt, 5) <> Nothing Then
                            Dim stmp() As String
                            stmp = Split(.Rows(xCnt).Cells("cAccounts").Value.ToString, "/")
                            sNameID = m_getname(stmp(0), stmp(1))
                        End If
                        If mkDefaultValues(xCnt, 6) <> Nothing Then
                            sBillable = 1
                        Else
                            sBillable = 0
                        End If
                        If mkDefaultValues(xCnt, 7) <> Nothing Then
                            sfxKey = .Rows(xCnt).Cells("fxKey").Value.ToString
                        End If

                        Dim sSQLCmd As String = "usp_t_tJVEntry_details_save "
                        sSQLCmd &= " @fiEntryNo='" & txtJVNumber.Text & "' "
                        sSQLCmd &= ",@fxKeyAccount='" & sAccount & "' "
                        sSQLCmd &= ",@fdDebit='" & dDebit & "' "
                        sSQLCmd &= ",@fdCredit='" & dCredit & "' "
                        sSQLCmd &= ",@fcMemo='" & sMemo & "' "
                        sSQLCmd &= ",@fcNote='" & sNote & "' "
                        sSQLCmd &= ",@fxKeyNameID=" & IIf(sNameID = "", "NULL", "'" & sNameID & "'") & " "
                        sSQLCmd &= ",@fbBillable='" & sBillable & "' "
                        sSQLCmd &= ",@fxKey=" & IIf(sfxKey = "", "NULL", "'" & sfxKey & "'") & " "
                        sSQLCmd &= ",@fk_JVHeader='" & GetJournalEntryId() & "'"
                        sSQLCmd &= ",@transDate='" & dte_genjour_date.Value.Date & "'"
                        Try
                            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
                        Catch ex As Exception
                            Return False
                        End Try
                    End If
                Next
                Return True
            End If
        End With
    End Function
    Private Function checkifadjusted() As Integer
        Dim xVal As Integer = 0
        If chk_genjour_adjEntry.Checked Then
            xVal = 1
        End If
        Return xVal
    End Function
    Private Sub DisplayListofEntries()
        With Me.grdListofEntries
            .Columns.Clear()
            .DataSource = m_GetGLJournalEntriesPerDate(Microsoft.VisualBasic.FormatDateTime(dtFrom.Value, DateFormat.ShortDate), _
             Microsoft.VisualBasic.FormatDateTime(dtTo.Value, DateFormat.ShortDate)).Tables(0)
            .Columns(colEntDate).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns(colEntNo).Width = 140
            .Columns(colEntAdj).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns(colEntName).Width = 250
            .Columns(colEntMemo).Width = 140
            .Columns(colEntTotalAmt).Width = 100
            .Columns(colcreatedby).HeaderText = "Created By"
            .Columns(colupdateby).HeaderText = "Updated by"
            .Columns(colcreatedby).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns(colupdateby).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .Columns(colEntPost).Width = 50
            .Columns(colEntAdj).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(colEntTotalAmt).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns(colEntTotalAmt).DefaultCellStyle.Format = "##,##0.00"

            .Columns(colEntKey).Visible = False

            .Columns(0).ReadOnly = True
            .Columns(1).ReadOnly = True
            .Columns(2).ReadOnly = True
            .Columns(3).ReadOnly = True
            .Columns(4).ReadOnly = True
            .Columns(5).ReadOnly = True
            .Columns(6).ReadOnly = True
            .Columns(7).ReadOnly = True
            .Columns(8).ReadOnly = True
            .Columns(9).ReadOnly = True

        End With
    End Sub
    Private Sub computetotaldebitcredit()
        Dim xCnt As Integer
        Dim xTotalDebit As Decimal = 0
        Dim xTotalCredit As Decimal = 0

        With grdGenJournalDetails
            For xCnt = 0 To .Rows.Count - 1
                If .Rows(xCnt).Cells(0).Value IsNot Nothing Then
                    xTotalDebit += CDec(.Rows(xCnt).Cells("cDebit").Value)
                End If
                If .Rows(xCnt).Cells(0).Value IsNot Nothing Then
                    xTotalCredit += CDec(.Rows(xCnt).Cells("cCredit").Value)
                End If
            Next
            txt_genjour_totaldebit.Text = Format(CDec(xTotalDebit), "##,##0.00")
            txt_genjour_totalcredit.Text = Format(CDec(xTotalCredit), "##,##0.00")
        End With
    End Sub
    Private Function calculateamount(ByVal colnum As Integer) As Decimal
        Dim xRow As Integer
        Dim xVal As Decimal = 0
        With grdGenJournalDetails
            For xRow = 0 To .Rows.Count - 1
                If .Item(colnum, xRow).Value IsNot Nothing Then
                    xVal += CDec(.Item(colnum, xRow).Value)
                End If
            Next
        End With
        Return xVal
    End Function
    Private Sub IsDateRestricted()
        'Validate Date
        If checkDate.checkIfRestricted(dte_genjour_date.Value) Then
            btn_genjour_saveNew.Enabled = False 'true if date is NOT within range of period restricted
            btn_genjour_saveClose.Enabled = False
            ts_genjour_history.Enabled = False
        Else
            btn_genjour_saveNew.Enabled = True 'false if date is within range of period restricted
            btn_genjour_saveClose.Enabled = True
            ts_genjour_history.Enabled = True
        End If
    End Sub

#End Region
#Region "Old/Disables codes"

    'Private Sub grdListofEntries_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdListofEntries.CellValueChanged
    '    'With Me.grdListofEntries
    '    '    If .CurrentCellAddress.X = 6 Then
    '    '        If MsgBox("Are you sure, you want to post this entry?...", MsgBoxStyle.YesNo, Me.Text) = MsgBoxResult.Yes Then
    '    '            Call gUpdateEntry(.CurrentRow.Cells(0).Value.ToString)
    '    '        Else
    '    '            .CurrentRow.Cells(6).Value = False
    '    '        End If
    '    '    End If
    '    'End With
    'End Sub

    'Private Sub verifyID()
    '    If gfiEntryNo <> "" Then
    '        Try
    '            Dim sSQLCmd As String = "DELETE FROM tJVEntry where fiEntryNO='" & gfiEntryNo & "' "
    '            SqlHelper.ExecuteScalar(gCon.sqlconn, CommandType.Text, sSQLCmd)
    '        Catch ex As Exception
    '        End Try
    '    End If
    'End Sub

    'Private Sub grdGenJournal_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdGenJournal.CellValidated
    '    Try
    'With grdGenJournal
    '    If .CurrentCellAddress.X = 1 Then
    '        .Item(.CurrentCellAddress.X, .Rows.Count - 1).Value = Nothing
    '        If calculateamount(.CurrentCellAddress.X) > calculateamount(.CurrentCellAddress.X + 1) Then
    '            If .Item(.CurrentCellAddress.X + 1, .CurrentRow.Index).Value Is Nothing Then
    '                If .Item(.CurrentCellAddress.X, .CurrentRow.Index).Value IsNot Nothing Then
    '                    .Item(.CurrentCellAddress.X + 1, .CurrentRow.Index + 1).Value = calculateamount(.CurrentCellAddress.X) - calculateamount(.CurrentCellAddress.X + 1)
    '                End If
    '            Else
    '                .Item(.CurrentCellAddress.X + 1, .CurrentRow.Index).Value = Nothing
    '                Try
    '                    .Item(.CurrentCellAddress.X + 1, .CurrentRow.Index + 1).Value = calculateamount(.CurrentCellAddress.X) - calculateamount(.CurrentCellAddress.X + 1)
    '                Catch ex As Exception
    '                End Try
    '            End If
    '        End If
    '    ElseIf .CurrentCellAddress.X = 2 Then
    '        .Item(.CurrentCellAddress.X, .Rows.Count - 1).Value = Nothing
    '        If calculateamount(.CurrentCellAddress.X) > calculateamount(.CurrentCellAddress.X - 1) Then
    '            If .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index).Value Is Nothing Then
    '                If .Item(.CurrentCellAddress.X, .CurrentRow.Index).Value IsNot Nothing Then
    '                    .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index + 1).Value = calculateamount(.CurrentCellAddress.X) - calculateamount(.CurrentCellAddress.X - 1)
    '                End If
    '            Else
    '                .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index).Value = Nothing
    '                .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index + 1).Value = calculateamount(.CurrentCellAddress.X) - calculateamount(.CurrentCellAddress.X - 1)
    '            End If
    '        ElseIf calculateamount(.CurrentCellAddress.X) < calculateamount(.CurrentCellAddress.X - 1) Then
    '            If .CurrentRow.Index <> .Rows.Count - 1 Then
    '                .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index + 1).Value = Nothing
    '            End If
    '            If .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index).Value Is Nothing Then
    '                If .Item(.CurrentCellAddress.X, .CurrentRow.Index).Value IsNot Nothing Then
    '                    .Item(.CurrentCellAddress.X, .CurrentRow.Index + 1).Value = calculateamount(.CurrentCellAddress.X - 1) - calculateamount(.CurrentCellAddress.X)
    '                End If
    '            ElseIf .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index).Value IsNot Nothing Then
    '                If .Item(.CurrentCellAddress.X, .CurrentRow.Index).Value IsNot Nothing Then
    '                    .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index).Value = Nothing
    '                    .Item(.CurrentCellAddress.X, .CurrentRow.Index + 1).Value = calculateamount(.CurrentCellAddress.X - 1) - calculateamount(.CurrentCellAddress.X)
    '                End If
    '            End If
    '        ElseIf calculateamount(.CurrentCellAddress.X) = calculateamount(.CurrentCellAddress.X - 1) Then
    '            If .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index).Value IsNot Nothing Then
    '                .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index).Value = Nothing
    '                .Item(.CurrentCellAddress.X - 1, .CurrentRow.Index + 1).Value = calculateamount(.CurrentCellAddress.X) - calculateamount(.CurrentCellAddress.X - 1)
    '            End If
    '        End If
    '    End If
    'End With
    '        Call computetotaldebitcredit()
    '    Catch ex As Exception
    '    End Try
    'End Sub
#End Region

    Private Sub grdListofEntries_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdListofEntries.CellContentClick
        Mode = "Edit"
    End Sub
    Private Sub Save_JV_ENtry_Items_to_dumpFile(ByVal fiEntryNo As String)
        Dim xfxKeyJVDetails As String = ""
        Dim xfiEntryNo As String = ""
        Dim xfxKeyAccount As String = ""
        Dim xfdDebit As String = ""
        Dim xfdCredit As String = ""
        Dim xfcMemo As String = ""
        Dim xfcNote As String = ""
        Dim xfxKeyNameID As String = ""
        Dim xfbBillable As String = ""
        Dim xfdCount As String = ""


        Dim sSqlCmd As String = "SELECT * FROM dbo.tJVEntry_items WHERE fiEntryNo='" & fiEntryNo & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    If rd.Item(0).ToString = "" Or rd.Item(0).ToString Is Nothing Then
                        xfxKeyJVDetails = "NULL, "
                    Else
                        xfxKeyJVDetails = "CAST('" & rd.Item(0).ToString & "' AS uniqueidentifier), "
                    End If

                    xfiEntryNo = rd.Item(1).ToString

                    If rd.Item(2).ToString = "" Or rd.Item(2).ToString Is Nothing Then
                        xfxKeyAccount = "NULL, "
                    Else
                        xfxKeyAccount = "CAST('" & rd.Item(2).ToString & "' AS uniqueidentifier), "
                    End If

                    xfdDebit = rd.Item(3).ToString
                    xfdCredit = rd.Item(4).ToString
                    xfcMemo = rd.Item(5).ToString
                    xfcNote = rd.Item(6).ToString

                    If rd.Item(7).ToString = "" Or rd.Item(7).ToString Is Nothing Then
                        xfxKeyNameID = "NULL, "
                    Else
                        xfxKeyNameID = "CAST('" & rd.Item(7).ToString & "' AS uniqueidentifier), "
                    End If

                    If rd.Item(8).ToString = "True" Then
                        xfbBillable = "1"
                    Else
                        xfbBillable = "0"
                    End If

                    xfdCount = rd.Item(9).ToString

                    Dim sSqlSaveCmd As String = "INSERT INTO dbo.tDeletedJVEntry_Items"
                    sSqlSaveCmd &= "(fxKeyJVDetails, fiEntryNo, fxKeyAccount, fdDebit, fdCredit, fcMemo, fcNote, fxKeyNameID, " & _
                                    "fbBillable, fdCount, fdDeletedBy) "
                    sSqlSaveCmd &= "VALUES (" & xfxKeyJVDetails & "'" & xfiEntryNo & "', " & xfxKeyAccount
                    sSqlSaveCmd &= "CAST('" & xfdDebit & "' AS DECIMAL(9)), CAST('" & xfdCredit & "' AS DECIMAL(9)), '" & _
                                xfcMemo & "', '" & xfcNote & "', " & xfxKeyNameID & _
                                "CAST('" & xfbBillable & "' AS BIT), CAST('" & xfdCount & "' AS INT), '" & gUserName & "')"
                    Try
                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlSaveCmd)
                    Catch ex As Exception
                    End Try

                End While
            End Using
        Catch ex As Exception
        End Try


    End Sub
    Private Sub Save_JV_ENtry_to_dumpFile(ByVal fxKeyJVNo As String)
        Dim xfxKeyJVNo As String = ""
        Dim xfxKeyCompany As String = ""
        Dim xfiEntryNo As String = ""
        Dim xfdTransDate As String = ""
        Dim xfbAdjust As String = ""
        Dim xfxKeyNameID As String = ""
        Dim xfcMemo As String = ""
        Dim xfcNote As String = ""
        Dim xfdTotAmt As String = ""
        Dim xfuCreatedBy As String = ""
        Dim xfdDateCreated As String = ""
        Dim xfuUpdatedBy As String = ""
        Dim xfdDateUpdated As String = ""
        Dim xfdPosted As String = ""

        Dim sSqlCmd As String = "SELECT * FROM dbo.tJVEntry WHERE fiEntryNo='" & _
                                fxKeyJVNo & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    If rd.Item(0).ToString = "" Or rd.Item(0).ToString Is Nothing Then
                        xfxKeyJVNo = "NULL, "
                    Else
                        xfxKeyJVNo = "CAST('" & rd.Item(0).ToString & "' AS uniqueidentifier), "
                    End If

                    If rd.Item(1).ToString = "" Or rd.Item(1).ToString Is Nothing Then
                        xfxKeyCompany = "NULL, "
                    Else
                        xfxKeyCompany = "CAST('" & rd.Item(1).ToString & "' AS uniqueidentifier), '"
                    End If

                    xfiEntryNo = rd.Item(2).ToString
                    xfdTransDate = rd.Item(3).ToString

                    If rd.Item(4).ToString = "True" Then
                        xfbAdjust = "1"
                    Else
                        xfbAdjust = "0"
                    End If

                    If rd.Item(5).ToString = "" Or rd.Item(5).ToString Is Nothing Then
                        xfxKeyNameID = "NULL, "
                    Else
                        xfxKeyNameID = "CAST('" & rd.Item(5).ToString & "' AS uniqueidentifier), '"
                    End If

                    xfcMemo = rd.Item(6).ToString
                    xfcNote = rd.Item(7).ToString
                    xfdTotAmt = rd.Item(8).ToString
                    xfuCreatedBy = rd.Item(9).ToString
                    xfdDateCreated = rd.Item(10).ToString
                    xfuUpdatedBy = rd.Item(11).ToString
                    xfdDateUpdated = rd.Item(12).ToString

                    If rd.Item(13).ToString = "True" Then
                        xfdPosted = "1"
                    Else
                        xfdPosted = "0"
                    End If
                End While
            End Using
        Catch ex As Exception
        End Try

        Dim sSqlSaveCmd As String = "INSERT INTO dbo.tDeletedJVEntry"
        sSqlSaveCmd &= "(fxKeyJVNo, fxKeyCompany, fiEntryNo, fdTransDate, fbAdjust, fxKeyNameID, " & _
                        "fcMemo, fcNote, fdTotAmt, fuCreatedBy, fdDateCreated, fuUpdatedBy, fdDateUpdated, fdPosted, fdDeleteBy) "
        sSqlSaveCmd &= "VALUES (" & xfxKeyJVNo & xfxKeyCompany & xfiEntryNo & "', '" & xfdTransDate & "', CAST('" & _
                        xfbAdjust & "' AS bit), " & xfxKeyNameID & xfcMemo & "', '" & xfcNote & "', CAST('" & _
                        xfdTotAmt & "' AS Decimal(18,2)), '" & xfuCreatedBy & "', '" & xfdDateCreated & "', '" & _
                        xfuUpdatedBy & "', '" & xfdDateUpdated & "', CAST('" & xfdPosted & "' AS Bit), '" & _
                        gUserName & "')"
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlSaveCmd)
        Catch ex As Exception
            MsgBox(ex)
        End Try
    End Sub
    Private Sub btnDeleteJV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteJV.Click

        If grdListofEntries.SelectedRows.Count < 1 Then
            MsgBox("Please Select the JV Entry Rows to be deleted first.", MsgBoxStyle.Exclamation, "Delete JV Entry")
        Else
            If MsgBox("Deleting Entries cannot be undone. Are you sure you want to continue?", MsgBoxStyle.OkCancel) = MsgBoxResult.Ok Then
                Const JVEntryNoColumn As Integer = 2
                Const PostedColumn As Integer = 9

                For Each selectedRows As DataGridViewRow In grdListofEntries.SelectedRows
                    If isJVPosted(PostedColumn, selectedRows.Index) = False Then
                        Dim JVENtryNo As String = grdListofEntries.Item(JVEntryNoColumn, selectedRows.Index).Value.ToString.Trim
                        'Save_JV_ENtry_Items_to_dumpFile(JVENtryNo)
                        'Save_JV_ENtry_to_dumpFile(JVENtryNo)
                        deleteJVEntry(JVENtryNo)
                    Else
                        MsgBox("Cannot delete the " & grdListofEntries.Item(JVEntryNoColumn, selectedRows.Index).Value.ToString & vbCr & "because it is already posted.", _
                            MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access for " & _
                            grdListofEntries.Item(JVEntryNoColumn, selectedRows.Index).Value.ToString & " denied")
                    End If
                Next
                CreateGridrow_GenJournalDetail()
                DisplayListofEntries()
            End If
        End If

    End Sub
    Private Function isJVPosted(ByVal PostedColumn As Integer, ByVal SelectedRow As Integer) As Boolean
        If grdListofEntries.Item(PostedColumn, SelectedRow).Value = True Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub SplitContainer1_SplitterMoved(ByVal sender As System.Object, ByVal e As System.Windows.Forms.SplitterEventArgs) Handles SplitContainer1.SplitterMoved

    End Sub
    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub
    Private Sub ts_genjour_find_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_genjour_find.Click

    End Sub
    Private Sub BtnBrowseClass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowseClass.Click
        FrmBrowseClass.Mode = "BROWSE MODE"
        FrmBrowseClass.ShowDialog()
        LblClassName.Text = FrmBrowseClass.Data1
        txtClassRefno.Text = FrmBrowseClass.Data2
    End Sub
    Private Sub Label8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.Click

    End Sub
    Private Sub LblClassName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LblClassName.Click

    End Sub
End Class