<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccntEntries_Deposit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.ToolStripTop = New System.Windows.Forms.ToolStrip
        Me.dtTo = New System.Windows.Forms.DateTimePicker
        Me.txt_genjour_totaldebit = New System.Windows.Forms.TextBox
        Me.txt_genjour_totalcredit = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblDateTo = New System.Windows.Forms.Label
        Me.grdListofEntries = New System.Windows.Forms.DataGridView
        Me.lblDateFrom = New System.Windows.Forms.Label
        Me.dtFrom = New System.Windows.Forms.DateTimePicker
        Me.lblDateRange = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblPostingNotification = New System.Windows.Forms.Label
        Me.dteGeneralJournal = New System.Windows.Forms.DateTimePicker
        Me.lblTotal = New System.Windows.Forms.Label
        Me.SplitGeneralJournal = New System.Windows.Forms.SplitContainer
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtGeneralJournalNo = New System.Windows.Forms.TextBox
        Me.lblEntryNo = New System.Windows.Forms.Label
        Me.grdGenJournalDetails = New System.Windows.Forms.DataGridView
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.txtTotalCredit = New System.Windows.Forms.TextBox
        Me.lbllCredit = New System.Windows.Forms.Label
        Me.lblDebit = New System.Windows.Forms.Label
        Me.txtTotalDebit = New System.Windows.Forms.TextBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.bgwJournalEntries = New System.ComponentModel.BackgroundWorker
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.btnPreviewJV = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSaveClose = New System.Windows.Forms.Button
        Me.btnSaveNew = New System.Windows.Forms.Button
        Me.btnNew = New System.Windows.Forms.Button
        Me.btnDisplay = New System.Windows.Forms.Button
        Me.toolStripPostJournal = New System.Windows.Forms.ToolStripButton
        Me.ToolStripTop.SuspendLayout()
        CType(Me.grdListofEntries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitGeneralJournal.Panel1.SuspendLayout()
        Me.SplitGeneralJournal.Panel2.SuspendLayout()
        Me.SplitGeneralJournal.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.grdGenJournalDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStripTop
        '
        Me.ToolStripTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripPostJournal})
        Me.ToolStripTop.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripTop.Name = "ToolStripTop"
        Me.ToolStripTop.Size = New System.Drawing.Size(767, 25)
        Me.ToolStripTop.TabIndex = 53
        Me.ToolStripTop.Text = "ToolStrip1"
        '
        'dtTo
        '
        Me.dtTo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtTo.Location = New System.Drawing.Point(309, 7)
        Me.dtTo.Name = "dtTo"
        Me.dtTo.Size = New System.Drawing.Size(155, 23)
        Me.dtTo.TabIndex = 54
        '
        'txt_genjour_totaldebit
        '
        Me.txt_genjour_totaldebit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txt_genjour_totaldebit.BackColor = System.Drawing.Color.White
        Me.txt_genjour_totaldebit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt_genjour_totaldebit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_genjour_totaldebit.Location = New System.Drawing.Point(83, 19)
        Me.txt_genjour_totaldebit.Name = "txt_genjour_totaldebit"
        Me.txt_genjour_totaldebit.Size = New System.Drawing.Size(94, 14)
        Me.txt_genjour_totaldebit.TabIndex = 55
        Me.txt_genjour_totaldebit.Text = "0.00"
        Me.txt_genjour_totaldebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_genjour_totalcredit
        '
        Me.txt_genjour_totalcredit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txt_genjour_totalcredit.BackColor = System.Drawing.Color.White
        Me.txt_genjour_totalcredit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt_genjour_totalcredit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_genjour_totalcredit.Location = New System.Drawing.Point(239, 19)
        Me.txt_genjour_totalcredit.Name = "txt_genjour_totalcredit"
        Me.txt_genjour_totalcredit.Size = New System.Drawing.Size(91, 14)
        Me.txt_genjour_totalcredit.TabIndex = 56
        Me.txt_genjour_totalcredit.Text = "0.00"
        Me.txt_genjour_totalcredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(183, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 13)
        Me.Label7.TabIndex = 57
        Me.Label7.Text = "Credit"
        '
        'lblDateTo
        '
        Me.lblDateTo.AutoSize = True
        Me.lblDateTo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTo.Location = New System.Drawing.Point(283, 11)
        Me.lblDateTo.Name = "lblDateTo"
        Me.lblDateTo.Size = New System.Drawing.Size(19, 15)
        Me.lblDateTo.TabIndex = 53
        Me.lblDateTo.Text = "To"
        '
        'grdListofEntries
        '
        Me.grdListofEntries.AllowUserToOrderColumns = True
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        Me.grdListofEntries.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.grdListofEntries.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdListofEntries.BackgroundColor = System.Drawing.Color.LightSteelBlue
        Me.grdListofEntries.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdListofEntries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdListofEntries.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdListofEntries.Location = New System.Drawing.Point(0, 33)
        Me.grdListofEntries.MultiSelect = False
        Me.grdListofEntries.Name = "grdListofEntries"
        Me.grdListofEntries.ReadOnly = True
        Me.grdListofEntries.Size = New System.Drawing.Size(767, 180)
        Me.grdListofEntries.TabIndex = 45
        '
        'lblDateFrom
        '
        Me.lblDateFrom.AutoSize = True
        Me.lblDateFrom.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateFrom.Location = New System.Drawing.Point(86, 11)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(36, 15)
        Me.lblDateFrom.TabIndex = 52
        Me.lblDateFrom.Text = "From"
        '
        'dtFrom
        '
        Me.dtFrom.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFrom.Location = New System.Drawing.Point(122, 7)
        Me.dtFrom.Name = "dtFrom"
        Me.dtFrom.Size = New System.Drawing.Size(155, 23)
        Me.dtFrom.TabIndex = 51
        Me.dtFrom.Value = New Date(2011, 1, 1, 0, 0, 0, 0)
        '
        'lblDateRange
        '
        Me.lblDateRange.AutoSize = True
        Me.lblDateRange.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateRange.Location = New System.Drawing.Point(12, 11)
        Me.lblDateRange.Name = "lblDateRange"
        Me.lblDateRange.Size = New System.Drawing.Size(73, 15)
        Me.lblDateRange.TabIndex = 50
        Me.lblDateRange.Text = "Date Range:"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(12, 10)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(33, 15)
        Me.lblDate.TabIndex = 43
        Me.lblDate.Text = "Date"
        '
        'lblPostingNotification
        '
        Me.lblPostingNotification.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblPostingNotification.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostingNotification.Location = New System.Drawing.Point(168, 98)
        Me.lblPostingNotification.Name = "lblPostingNotification"
        Me.lblPostingNotification.Size = New System.Drawing.Size(465, 23)
        Me.lblPostingNotification.TabIndex = 46
        Me.lblPostingNotification.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblPostingNotification.Visible = False
        '
        'dteGeneralJournal
        '
        Me.dteGeneralJournal.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteGeneralJournal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteGeneralJournal.Location = New System.Drawing.Point(50, 6)
        Me.dteGeneralJournal.Name = "dteGeneralJournal"
        Me.dteGeneralJournal.Size = New System.Drawing.Size(116, 23)
        Me.dteGeneralJournal.TabIndex = 44
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(12, 12)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(37, 15)
        Me.lblTotal.TabIndex = 53
        Me.lblTotal.Text = "Total:"
        '
        'SplitGeneralJournal
        '
        Me.SplitGeneralJournal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitGeneralJournal.Location = New System.Drawing.Point(0, 25)
        Me.SplitGeneralJournal.Name = "SplitGeneralJournal"
        Me.SplitGeneralJournal.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitGeneralJournal.Panel1
        '
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.picLoading)
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.lblPostingNotification)
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.grdGenJournalDetails)
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.Panel1)
        Me.SplitGeneralJournal.Panel1.Controls.Add(Me.Panel3)
        '
        'SplitGeneralJournal.Panel2
        '
        Me.SplitGeneralJournal.Panel2.Controls.Add(Me.grdListofEntries)
        Me.SplitGeneralJournal.Panel2.Controls.Add(Me.Panel4)
        Me.SplitGeneralJournal.Panel2.Controls.Add(Me.Panel2)
        Me.SplitGeneralJournal.Size = New System.Drawing.Size(767, 467)
        Me.SplitGeneralJournal.SplitterDistance = 211
        Me.SplitGeneralJournal.TabIndex = 54
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtGeneralJournalNo)
        Me.Panel1.Controls.Add(Me.lblEntryNo)
        Me.Panel1.Controls.Add(Me.dteGeneralJournal)
        Me.Panel1.Controls.Add(Me.lblDate)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(767, 35)
        Me.Panel1.TabIndex = 0
        '
        'txtGeneralJournalNo
        '
        Me.txtGeneralJournalNo.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtGeneralJournalNo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGeneralJournalNo.Location = New System.Drawing.Point(249, 6)
        Me.txtGeneralJournalNo.Name = "txtGeneralJournalNo"
        Me.txtGeneralJournalNo.Size = New System.Drawing.Size(151, 23)
        Me.txtGeneralJournalNo.TabIndex = 46
        '
        'lblEntryNo
        '
        Me.lblEntryNo.AutoSize = True
        Me.lblEntryNo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEntryNo.Location = New System.Drawing.Point(185, 10)
        Me.lblEntryNo.Name = "lblEntryNo"
        Me.lblEntryNo.Size = New System.Drawing.Size(58, 15)
        Me.lblEntryNo.TabIndex = 45
        Me.lblEntryNo.Text = "Entry No."
        '
        'grdGenJournalDetails
        '
        Me.grdGenJournalDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdGenJournalDetails.BackgroundColor = System.Drawing.Color.White
        Me.grdGenJournalDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdGenJournalDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdGenJournalDetails.DefaultCellStyle = DataGridViewCellStyle6
        Me.grdGenJournalDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdGenJournalDetails.GridColor = System.Drawing.Color.DarkGray
        Me.grdGenJournalDetails.Location = New System.Drawing.Point(0, 35)
        Me.grdGenJournalDetails.Name = "grdGenJournalDetails"
        Me.grdGenJournalDetails.Size = New System.Drawing.Size(767, 140)
        Me.grdGenJournalDetails.TabIndex = 46
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblTotal)
        Me.Panel3.Controls.Add(Me.txtTotalCredit)
        Me.Panel3.Controls.Add(Me.lbllCredit)
        Me.Panel3.Controls.Add(Me.lblDebit)
        Me.Panel3.Controls.Add(Me.txtTotalDebit)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 175)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(767, 36)
        Me.Panel3.TabIndex = 1
        '
        'txtTotalCredit
        '
        Me.txtTotalCredit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtTotalCredit.BackColor = System.Drawing.Color.White
        Me.txtTotalCredit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotalCredit.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCredit.Location = New System.Drawing.Point(386, 14)
        Me.txtTotalCredit.Name = "txtTotalCredit"
        Me.txtTotalCredit.ReadOnly = True
        Me.txtTotalCredit.Size = New System.Drawing.Size(91, 16)
        Me.txtTotalCredit.TabIndex = 51
        Me.txtTotalCredit.Text = "0.00"
        Me.txtTotalCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbllCredit
        '
        Me.lbllCredit.AutoSize = True
        Me.lbllCredit.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbllCredit.Location = New System.Drawing.Point(329, 14)
        Me.lbllCredit.Name = "lbllCredit"
        Me.lbllCredit.Size = New System.Drawing.Size(40, 15)
        Me.lbllCredit.TabIndex = 52
        Me.lbllCredit.Text = "Credit"
        '
        'lblDebit
        '
        Me.lblDebit.AutoSize = True
        Me.lblDebit.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDebit.Location = New System.Drawing.Point(173, 14)
        Me.lblDebit.Name = "lblDebit"
        Me.lblDebit.Size = New System.Drawing.Size(36, 15)
        Me.lblDebit.TabIndex = 52
        Me.lblDebit.Text = "Debit"
        '
        'txtTotalDebit
        '
        Me.txtTotalDebit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtTotalDebit.BackColor = System.Drawing.Color.White
        Me.txtTotalDebit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotalDebit.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalDebit.Location = New System.Drawing.Point(216, 13)
        Me.txtTotalDebit.Name = "txtTotalDebit"
        Me.txtTotalDebit.ReadOnly = True
        Me.txtTotalDebit.Size = New System.Drawing.Size(94, 16)
        Me.txtTotalDebit.TabIndex = 50
        Me.txtTotalDebit.Text = "0.00"
        Me.txtTotalDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.btnPreviewJV)
        Me.Panel4.Controls.Add(Me.btnClose)
        Me.Panel4.Controls.Add(Me.btnSaveClose)
        Me.Panel4.Controls.Add(Me.btnSaveNew)
        Me.Panel4.Controls.Add(Me.btnNew)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 213)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(767, 39)
        Me.Panel4.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnDisplay)
        Me.Panel2.Controls.Add(Me.dtTo)
        Me.Panel2.Controls.Add(Me.lblDateTo)
        Me.Panel2.Controls.Add(Me.lblDateFrom)
        Me.Panel2.Controls.Add(Me.dtFrom)
        Me.Panel2.Controls.Add(Me.lblDateRange)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(767, 33)
        Me.Panel2.TabIndex = 1
        '
        'bgwJournalEntries
        '
        Me.bgwJournalEntries.WorkerSupportsCancellation = True
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.ajax_loader
        Me.picLoading.Location = New System.Drawing.Point(274, 91)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(240, 30)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLoading.TabIndex = 47
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'btnPreviewJV
        '
        Me.btnPreviewJV.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreviewJV.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.btnPreviewJV.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPreviewJV.Location = New System.Drawing.Point(3, 6)
        Me.btnPreviewJV.Name = "btnPreviewJV"
        Me.btnPreviewJV.Size = New System.Drawing.Size(82, 30)
        Me.btnPreviewJV.TabIndex = 2
        Me.btnPreviewJV.Text = "Preview"
        Me.btnPreviewJV.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreviewJV.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(639, 5)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(123, 31)
        Me.btnClose.TabIndex = 47
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSaveClose
        '
        Me.btnSaveClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveClose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveClose.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSaveClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSaveClose.Location = New System.Drawing.Point(254, 5)
        Me.btnSaveClose.Name = "btnSaveClose"
        Me.btnSaveClose.Size = New System.Drawing.Size(122, 31)
        Me.btnSaveClose.TabIndex = 46
        Me.btnSaveClose.Text = "Save && &Close"
        Me.btnSaveClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSaveClose.UseVisualStyleBackColor = True
        Me.btnSaveClose.Visible = False
        '
        'btnSaveNew
        '
        Me.btnSaveNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveNew.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveNew.Image = Global.CSAcctg.My.Resources.Resources.filenew
        Me.btnSaveNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSaveNew.Location = New System.Drawing.Point(382, 5)
        Me.btnSaveNew.Name = "btnSaveNew"
        Me.btnSaveNew.Size = New System.Drawing.Size(122, 31)
        Me.btnSaveNew.TabIndex = 45
        Me.btnSaveNew.Text = "&Save"
        Me.btnSaveNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSaveNew.UseVisualStyleBackColor = True
        Me.btnSaveNew.Visible = False
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.Image = Global.CSAcctg.My.Resources.Resources._new
        Me.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNew.Location = New System.Drawing.Point(510, 5)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(123, 31)
        Me.btnNew.TabIndex = 44
        Me.btnNew.Text = "&New"
        Me.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNew.UseVisualStyleBackColor = True
        Me.btnNew.Visible = False
        '
        'btnDisplay
        '
        Me.btnDisplay.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisplay.Image = Global.CSAcctg.My.Resources.Resources.report
        Me.btnDisplay.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDisplay.Location = New System.Drawing.Point(470, 6)
        Me.btnDisplay.Name = "btnDisplay"
        Me.btnDisplay.Size = New System.Drawing.Size(112, 24)
        Me.btnDisplay.TabIndex = 55
        Me.btnDisplay.Text = "Display List"
        Me.btnDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDisplay.UseVisualStyleBackColor = True
        '
        'toolStripPostJournal
        '
        Me.toolStripPostJournal.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.toolStripPostJournal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStripPostJournal.Name = "toolStripPostJournal"
        Me.toolStripPostJournal.Size = New System.Drawing.Size(88, 22)
        Me.toolStripPostJournal.Text = "Post Entries"
        Me.toolStripPostJournal.Visible = False
        '
        'frmAccntEntries_Deposit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 492)
        Me.Controls.Add(Me.SplitGeneralJournal)
        Me.Controls.Add(Me.ToolStripTop)
        Me.Controls.Add(Me.txt_genjour_totalcredit)
        Me.Controls.Add(Me.txt_genjour_totaldebit)
        Me.Controls.Add(Me.Label7)
        Me.Name = "frmAccntEntries_Deposit"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Deposit Accounting Entries"
        Me.ToolStripTop.ResumeLayout(False)
        Me.ToolStripTop.PerformLayout()
        CType(Me.grdListofEntries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitGeneralJournal.Panel1.ResumeLayout(False)
        Me.SplitGeneralJournal.Panel2.ResumeLayout(False)
        Me.SplitGeneralJournal.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdGenJournalDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStripTop As System.Windows.Forms.ToolStrip
    Friend WithEvents toolStripPostJournal As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnPreviewJV As System.Windows.Forms.Button
    Friend WithEvents btnDisplay As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSaveClose As System.Windows.Forms.Button
    Friend WithEvents btnSaveNew As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents dtTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_genjour_totaldebit As System.Windows.Forms.TextBox
    Friend WithEvents txt_genjour_totalcredit As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblDateTo As System.Windows.Forms.Label
    Friend WithEvents grdListofEntries As System.Windows.Forms.DataGridView
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateRange As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblPostingNotification As System.Windows.Forms.Label
    Friend WithEvents dteGeneralJournal As System.Windows.Forms.DateTimePicker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents SplitGeneralJournal As System.Windows.Forms.SplitContainer
    Friend WithEvents grdGenJournalDetails As System.Windows.Forms.DataGridView
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtTotalCredit As System.Windows.Forms.TextBox
    Friend WithEvents lbllCredit As System.Windows.Forms.Label
    Friend WithEvents lblDebit As System.Windows.Forms.Label
    Friend WithEvents txtTotalDebit As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtGeneralJournalNo As System.Windows.Forms.TextBox
    Friend WithEvents lblEntryNo As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents bgwJournalEntries As System.ComponentModel.BackgroundWorker
End Class
