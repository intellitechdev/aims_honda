Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmJVPosting
    Private ctr As Integer = 0
    Private gCon As New Clsappconfiguration

    'Private Sub frmJVPosting_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    '    pCallCompany(frm_acc_makeGeneralJournalEntry)
    'End Sub

    Private Sub frmJVPosting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboView.SelectedItem = "ALL"
        dteFrom.Value = SetAsFirstDayOfTheMonth()
        Call DisplayListOfEntries()
        Call LoadDoctype()
    End Sub

    Private Function SetAsFirstDayOfTheMonth() As Date
        'Monthly
        Dim thisDate As Date

        Dim dateNow As Date = Date.Now.Date
        thisDate = dateNow.AddDays(-dateNow.Day + 1)

        Return thisDate
    End Function

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteTo.ValueChanged
        Call DisplayListOfEntries()
    End Sub

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPost.Click
        If getUsersGroup(gUserName) = 3 Then
            If ctr = 1 Then
                If MsgBox("You are about to post " & DgvForPosting.Item("EntryNo", DgvForPosting.CurrentRow.Index).Value.ToString _
                    & ". Do you want to continue?", MsgBoxStyle.OkCancel, "JV Posting") = MsgBoxResult.Ok Then
                    Call PostSelectedEntries()
                    Call DisplayListOfEntries()
                End If
            Else
                DgvForPosting.SelectAll()
                Dim xrow As Integer = 0


                If ctr = 0 Then
                    MsgBox("Please select a journal entries.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No Entry selected")
                Else
                    If ctr > 1 Then
                        If MsgBox("You are about to post multiple Journal Entries. Do you want to continue?", MsgBoxStyle.OkCancel, "JV Posting") = MsgBoxResult.Ok Then
                            Call PostSelectedEntries()
                            Call DisplayListOfEntries()
                        End If
                    End If
                End If
            End If
        Else
            MsgBox("you don't have enought rights to post this entry, pls contact your system administrator to access this module...", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Me.DgvForPosting.Columns.Clear()
        'pCallCompany(frm_acc_makeGeneralJournalEntry)
        Me.Close()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
      PreviewJournalVoucher()
    End Sub

    Private Sub PreviewJournalVoucher()
        Try
            Dim pkJournalID As String = DgvForPosting.CurrentRow.Cells("fxKeyJVNo").Value.ToString()
            If pkJournalID <> "" Then
                With frmJournalRpt
                    .GetJournalID() = pkJournalID
                    .MdiParent = frmMain
                    .Show()
                End With
            Else
                MsgBox("There is no selected entry to display the voucher. Please select first.", _
                            MsgBoxStyle.Information, Me.Text)
            End If
        Catch ex As NullReferenceException
            MessageBox.Show("There is no selected entry to display the voucher. Please select first.", _
                            "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PostSelectedEntries()
        Dim xRow As Integer = 0
        If ctr = 0 Then
            MsgBox("Please select a journal entries.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No Entry selected")
        Else
            DgvForPosting.SelectAll()
            For Each selectedRows As DataGridViewRow In DgvForPosting.SelectedRows
                xRow = selectedRows.Index
                If DgvForPosting.Item("columnSelect", xRow).Value = True Then
                    If DgvForPosting.Item("Posted", xRow).Value = False Or DgvForPosting.Item("Amount", xRow).Value = 0 Then
                        postentries(DgvForPosting.Item("fxKeyJVNo", xRow).Value.ToString())
                    Else
                        MsgBox("The JV Evtry " & DgvForPosting.Item("newJournalNo", xRow).Value & _
                        " are already posted. Please select an unposted JV Entry.", MsgBoxStyle.Critical + _
                        MsgBoxStyle.OkOnly, "Access Denied")
                    End If
                End If
            Next
        End If

        ctr = 0
    End Sub

    Private Sub UnpostSelectedEntries()
        Dim xRow As Integer = 0
        If ctr = 0 Then
            MsgBox("Please select a transaction entries.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No Entry selected")
        Else
            DgvForPosting.SelectAll()
            For Each selectedRows As DataGridViewRow In DgvForPosting.SelectedRows
                xRow = selectedRows.Index
                If DgvForPosting.Item("columnSelect", xRow).Value = True Then
                    If DgvForPosting.Item("Posted", xRow).Value = True Or DgvForPosting.Item("Amount", xRow).Value > 0 Then
                        unpostentries(DgvForPosting.Item("fxKeyJVNo", xRow).Value.ToString())
                    Else
                        MsgBox("The Transaction Entry " & DgvForPosting.Item("newJournalNo", xRow).Value & _
                        " are already posted. Please select an unposted JV Entry.", MsgBoxStyle.Critical + _
                        MsgBoxStyle.OkOnly, "Access Denied")
                    End If
                End If
            Next
        End If

        ctr = 0
    End Sub

    Private Sub CancelSelectedEntries()
        Dim xRow As Integer = 0
        If ctr = 0 Then
            MsgBox("Please select a transaction entries.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No Entry selected")
        Else
            DgvForPosting.SelectAll()
            For Each selectedRows As DataGridViewRow In DgvForPosting.SelectedRows
                xRow = selectedRows.Index
                If DgvForPosting.Item("columnSelect", xRow).Value = True Then
                    If DgvForPosting.Item("Posted", xRow).Value = True Or DgvForPosting.Item("Amount", xRow).Value > 0 Then
                        CancelEntries(DgvForPosting.Item("fxKeyJVNo", xRow).Value.ToString())
                    Else
                        MsgBox("The Transaction Entry " & DgvForPosting.Item("newJournalNo", xRow).Value & _
                        " are already cancelled. Please select cancelled JV Entry.", MsgBoxStyle.Critical + _
                        MsgBoxStyle.OkOnly, "Access Denied")
                    End If
                End If
            Next
        End If

        ctr = 0
    End Sub

    Private Sub CancelEntries(ByVal sEntryNo As String)
        Dim sSQLCmd As String = "update tJVEntry Set fbCancelled='1' where fxKeyJVNo='" & sEntryNo & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            'MsgBox("Posting Successful!", MsgBoxStyle.Exclamation)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub unpostentries(ByVal sEntryNo As String)
        Dim sSQLCmd As String = "update tJVEntry Set fdPosted='0' where fxKeyJVNo='" & sEntryNo & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            'MsgBox("Posting Successful!", MsgBoxStyle.Exclamation)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub postentries(ByVal sEntryNo As String)
        Dim sSQLCmd As String = "update tJVEntry Set fdPosted='1' where fxKeyJVNo='" & sEntryNo & "' "
        Try
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            'MsgBox("Posting Successful!", MsgBoxStyle.Exclamation)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(DgvForPosting.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, DgvForPosting.Rows(irow).Cells(iCol).Value)
    End Function

    Private Sub DisplayListOfEntries()
        Dim bView As Boolean
        Dim bPosted As Boolean
        Dim bCancelled As Boolean
        Dim docType As String = cboDocType.Text
        Dim ColumnSelection As New DataGridViewCheckBoxColumn

        With ColumnSelection
            .Name = "columnSelect"
        End With

        Select Case cboView.SelectedItem.ToString
            Case "ALL"
                bView = True
            Case "Cancelled"
                bCancelled = True
                bView = False
                bPosted = False
            Case "Posted"
                bView = False
                bCancelled = False
                bPosted = True
            Case Else
                bView = False
                bPosted = False
                bCancelled = False
        End Select

        With Me.DgvForPosting
            .Columns.Clear()
            .Columns.Add(ColumnSelection)

            .DataSource = m_GetGLJournalEntriesforPosting(Microsoft.VisualBasic.FormatDateTime(Me.dteFrom.Value, DateFormat.ShortDate), _
             Microsoft.VisualBasic.FormatDateTime(Me.dteTo.Value, DateFormat.ShortDate), bView, bPosted, bCancelled, docType).Tables(0)
           
            .Columns("Adjust").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("Amount").DefaultCellStyle.Format = "##,##0.00"

            .Columns("Adjust").Visible = False
            .Columns("fxKeyJVNo").Visible = False

            .Columns("Date").ReadOnly = True
            .Columns("EntryNo").ReadOnly = True
            .Columns("Adjust").ReadOnly = True
            .Columns("Name").ReadOnly = True
            .Columns("Memo").ReadOnly = True
            .Columns("Amount").ReadOnly = True
            .Columns("Posted").ReadOnly = True
            .Columns("Cancelled").ReadOnly = True

            .Sort(DgvForPosting.Columns("EntryNo"), System.ComponentModel.ListSortDirection.Descending)

            .Columns("columnSelect").Width = 60
            .Columns("Date").Width = 70
            .Columns("EntryNo").Width = 70
            .Columns("Adjust").Width = 40
            .Columns("Name").Width = 120
            .Columns("Memo").Width = 140
            .Columns("Amount").Width = 80
            .Columns("Posted").Width = 60
            .Columns("Cancelled").Width = 70


            .Columns("columnSelect").HeaderText = "Selected"
        End With
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgvForPosting.CellClick
        Try
            gfiEntryNo = DgvForPosting.CurrentRow.Cells("EntryNo").Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    'Private Sub DgvForPosting_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles DgvForPosting.CellPainting
    '    Dim sf As New StringFormat
    '    sf.Alignment = StringAlignment.Center
    '    If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < DgvForPosting.Rows.Count Then
    '        e.PaintBackground(e.ClipBounds, True)
    '        e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
    '        e.Handled = True
    '    End If
    'End Sub

    Private Sub cboView_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub HiglightSelectedRow()
        If DgvForPosting.Rows.Count <> 0 Then
            If DgvForPosting.Item("columnSelect", DgvForPosting.CurrentRow.Index).Value = True Then
                DgvForPosting.CurrentRow.DefaultCellStyle.BackColor = Color.LightBlue
            Else
                DgvForPosting.CurrentRow.DefaultCellStyle.BackColor = Color.White
            End If
        End If
    End Sub

    Private Sub DgvForPosting_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DgvForPosting.CurrentCellDirtyStateChanged
        If DgvForPosting.IsCurrentCellDirty Then
            DgvForPosting.CommitEdit(DataGridViewDataErrorContexts.Commit)

            If DgvForPosting.Item("columnSelect", DgvForPosting.CurrentRow.Index).Value = True Then
                ctr += 1
            Else
                ctr -= 1
            End If
        End If


    End Sub

    Private Sub DgvForPosting_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgvForPosting.CellValueChanged
        Call HiglightSelectedRow()
    End Sub

    Private Sub dteFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteFrom.ValueChanged
        Try
            Call DisplayListOfEntries()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnUnpost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnpost.Click
        If getUsersGroup(gUserName) = 3 Then
            If ctr = 1 Then
                If MsgBox("You are about to unpost " & DgvForPosting.Item("EntryNo", DgvForPosting.CurrentRow.Index).Value.ToString _
                    & ". Do you want to continue?", MsgBoxStyle.OkCancel, "Transaction Un-Posting") = MsgBoxResult.Ok Then
                    Call UnpostSelectedEntries()
                    Call DisplayListOfEntries()
                End If
            Else
                DgvForPosting.SelectAll()
                Dim xrow As Integer = 0


                If ctr = 0 Then
                    MsgBox("Please select a journal entries.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No Entry selected")
                Else
                    If ctr > 1 Then
                        If MsgBox("You are about to post multiple Entries. Do you want to continue?", MsgBoxStyle.OkCancel, "JV Posting") = MsgBoxResult.Ok Then
                            Call UnpostSelectedEntries()
                            Call DisplayListOfEntries()
                        End If
                    End If
                End If
            End If
        Else
            MsgBox("you don't have enought rights to post this entry, pls contact your system administrator to access this module...", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnCancelled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelled.Click
        If getUsersGroup(gUserName) = 3 Then
            If ctr = 1 Then
                If MsgBox("You are about to cancel " & DgvForPosting.Item("EntryNo", DgvForPosting.CurrentRow.Index).Value.ToString _
                    & ". Do you want to continue?", MsgBoxStyle.OkCancel, "Cancelling Transaction Entry") = MsgBoxResult.Ok Then
                    Call CancelSelectedEntries()
                    Call DisplayListOfEntries()
                End If
            Else
                DgvForPosting.SelectAll()
                Dim xrow As Integer = 0


                If ctr = 0 Then
                    MsgBox("Please select a journal entries.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No Entry selected")
                Else
                    If ctr > 1 Then
                        If MsgBox("You are about to cancel multiple Entries. Do you want to continue?", MsgBoxStyle.OkCancel, "Cancelling Transaction Entry") = MsgBoxResult.Ok Then
                            Call CancelSelectedEntries()
                            Call DisplayListOfEntries()
                        End If
                    End If
                End If
            End If
        Else
            MsgBox("you don't have enought rights to post this entry, pls contact your system administrator to access this module...", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub cboView_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboView.SelectedIndexChanged
        Call DisplayListOfEntries()
    End Sub
    Private Sub LoadDoctype()
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("Masterfile_LoadDoctype", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Document")
            With cboDoctype
                .ValueMember = "pk_Initial"
                .DisplayMember = "fcDocName"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Loans")
        End Try

    End Sub

    Private Sub cboDocType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDocType.SelectedIndexChanged
        Call DisplayListOfEntries()
    End Sub

End Class