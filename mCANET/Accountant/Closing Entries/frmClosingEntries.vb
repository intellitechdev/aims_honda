Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmClosingEntries

    Private gCon As New Clsappconfiguration()
    Private gUser As String = frmMain.currentUser.Text


    Private closeFrom As Date
    Public Property GetPeriodFrom() As Date
        Get
            Return closeFrom
        End Get
        Set(ByVal value As Date)
            closeFrom = value
        End Set
    End Property

    Private clostTo As Date
    Public Property GetPeriodTo() As Date
        Get
            Return clostTo
        End Get
        Set(ByVal value As Date)
            clostTo = value
        End Set
    End Property

    Private Function LoadClosingEntryList(ByVal companyID As String, ByVal dtFrom As Date, ByVal dtTo As Date, ByVal entryNo As String) As DataSet
        Try
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_LoadClosingListEntries", _
                New SqlParameter("@co_id", companyID), _
                New SqlParameter("@dtPeriodFrom", dtFrom), _
                New SqlParameter("@dtPeriodTo", dtTo), _
                New SqlParameter("@closingEntryNo", entryNo))
            Return ds
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub FormatClosingListEntries()
        Dim pkEntryID As New DataGridViewTextBoxColumn
        Dim transDate As New DataGridViewTextBoxColumn
        Dim entryNo As New DataGridViewTextBoxColumn
        Dim particulars As New DataGridViewTextBoxColumn
        Dim amount As New DataGridViewTextBoxColumn
        Dim isPosted As New DataGridViewTextBoxColumn
        Dim createdBy As New DataGridViewTextBoxColumn
        Dim dateCreated As New DataGridViewTextBoxColumn
        Dim updatedBy As New DataGridViewTextBoxColumn
        Dim dateUpdated As New DataGridViewTextBoxColumn

        Dim periodFrom As New DataGridViewTextBoxColumn
        Dim periodTo As New DataGridViewTextBoxColumn

        With periodFrom
            .Name = "cPeriodFrom"
            .DataPropertyName = "Period From"
            .HeaderText = "Closed From"
        End With

        With periodTo
            .Name = "cPeriodTo"
            .DataPropertyName = "Period To"
            .HeaderText = "Close To"
        End With

        With pkEntryID
            .Name = "cPkEntryID"
            .DataPropertyName = "pkClosingEntryID"
        End With

        With transDate
            .Name = "cDate"
            .DataPropertyName = "dtClosingDate"
            .HeaderText = "Date"
        End With

        With entryNo
            .Name = "cEntryNo"
            .DataPropertyName = "closingEntryNo"
            .HeaderText = "Entry No"
        End With

        With particulars
            .Name = "cParticulars"
            .DataPropertyName = "fcParticulars"
            .HeaderText = "Particulars"
        End With

        With amount
            .Name = "cAmount"
            .DataPropertyName = "fdAmount"
            .HeaderText = "Amount"
        End With

        With isPosted
            .Name = "cIsPosted"
            .DataPropertyName = "fbPosted"
            .HeaderText = "Posted"
        End With

        With createdBy
            .Name = "cCreatedBy"
            .DataPropertyName = "fcCreatedBy"
            .HeaderText = "Created By"
        End With

        With dateCreated
            .Name = "cDateCreated"
            .DataPropertyName = "dtDateCreated"
            .HeaderText = "Date Created"
        End With

        With updatedBy
            .Name = "cUpdatedBy"
            .DataPropertyName = "fcUpdatedBy"
            .HeaderText = "Updated By"
        End With

        With dateUpdated
            .Name = "cDateUpdated"
            .DataPropertyName = "dtDateUpdated"
            .HeaderText = "Date Updated"
        End With

        With grdClosingList
            .Columns.Clear()
            .Columns.Add(pkEntryID)
            .Columns.Add(transDate)
            .Columns.Add(periodFrom)
            .Columns.Add(periodTo)
            .Columns.Add(entryNo)
            .Columns.Add(particulars)
            .Columns.Add(amount)
            .Columns.Add(isPosted)
            .Columns.Add(createdBy)
            .Columns.Add(dateCreated)
            .Columns.Add(updatedBy)
            .Columns.Add(dateUpdated)

            .Columns("cDate").Width = 50
            .Columns("cPeriodFrom").Width = 50
            .Columns("cPeriodTo").Width = 50
            .Columns("cEntryNo").Width = 70
            .Columns("cParticulars").Width = 220
            .Columns("cIsPosted").Width = 40

            .Columns("cAmount").DefaultCellStyle.Format = "n2"
            .Columns("cAmount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("cIsPosted").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns("cPkEntryID").Visible = False
            .Columns("cUpdatedBy").Visible = False
            .Columns("cDateUpdated").Visible = False
        End With
    End Sub

    Private Function LoadDetailsEntries(ByVal pkDetailID As String) As DataSet
        Dim ds As New DataSet

        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_LoadClosingEntriesDetails", _
                    New SqlParameter("@pkClosingEntries_Details", pkDetailID))
        Catch ex As Exception
            Throw ex
        End Try

        Return ds
    End Function

    Private Sub FormatClosingEntriesDetailed()
        Dim pkDetailID As New DataGridViewTextBoxColumn
        Dim acntID As New DataGridViewTextBoxColumn
        Dim accountCode As New DataGridViewTextBoxColumn
        Dim accountName As New DataGridViewTextBoxColumn
        Dim particulars As New DataGridViewTextBoxColumn
        Dim debit As New DataGridViewTextBoxColumn
        Dim credit As New DataGridViewTextBoxColumn

        With pkDetailID
            .Name = "cPkDetailID"
            .DataPropertyName = "pkClosingEntries_Details"
            .HeaderText = "pkDetailID"
        End With

        With acntID
            .Name = "cAcntID"
            .DataPropertyName = "acnt_id"
            .HeaderText = "Account ID"
        End With

        With accountCode
            .Name = "cAccountCode"
            .DataPropertyName = "acnt_code"
            .HeaderText = "Account Code"
        End With

        With accountName
            .Name = "cAccountName"
            .DataPropertyName = "acnt_name"
            .HeaderText = "Account Name"
        End With

        With particulars
            .Name = "cParticulars"
            .DataPropertyName = "fcParticulars"
            .HeaderText = "Particulars"
        End With

        With debit
            .Name = "cDebit"
            .DataPropertyName = "fdDebit"
            .HeaderText = "Debit"
        End With

        With credit
            .Name = "cCredit"
            .DataPropertyName = "fdCredit"
            .HeaderText = "Credit"
        End With

        With grdClosingDetails
            .Columns.Clear()
            .Columns.Add(pkDetailID)
            .Columns.Add(acntID)
            .Columns.Add(accountCode)
            .Columns.Add(accountName)
            .Columns.Add(particulars)
            .Columns.Add(debit)
            .Columns.Add(credit)

            .Columns("cPkDetailID").Visible = False
            .Columns("cAcntID").Visible = False

            .Columns("cAccountCode").Width = 50
            .Columns("cParticulars").Width = 250
            .Columns("cDebit").Width = 100
            .Columns("cCredit").Width = 100

            .Columns("cDebit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("cCredit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("cDebit").DefaultCellStyle.Format = "n2"
            .Columns("cCredit").DefaultCellStyle.Format = "n2"

        End With

    End Sub

    Private Sub CreateClosingEntries(ByVal dtClose As Date, ByVal dtPeriodFrom As Date, ByVal dtPeriodTo As Date)
        Try
            Dim companyID As String = gCompanyID()

            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_CreateClosingEntries", _
                New SqlParameter("@dtDateClose", dtClose), _
                New SqlParameter("@co_id", companyID), _
                New SqlParameter("@dtPeriodFrom", dtPeriodFrom), _
                New SqlParameter("@dtPeriodTo", dtPeriodTo), _
                New SqlParameter("@user", gUser))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PostSelectedEntry()
        If MessageBox.Show("Are you sure you want to post selected entry?", "Post Entry", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Try
                Dim pkEntryID As String = grdClosingList.Item("cPkEntryID", grdClosingList.CurrentRow.Index).Value.ToString()
                Dim isPosted As String = grdClosingList.Item("cIsPosted", grdClosingList.CurrentRow.Index).Value.ToString()

                If isPosted = "False" Then
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_PostClosingEntry", _
                        New SqlParameter("@pkEntryID", pkEntryID))
                Else
                    MessageBox.Show("User Error! You cannot post this because it is already posted!", "Post Entry", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Catch ex As NullReferenceException
                MessageBox.Show("User Error! There is nothing to post. Please select first.", "Post Entry", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub PostClosingEntry()

    End Sub

    Private Function SetAsFirstDayOfTheMonth() As Date
        'Monthly
        Dim thisDate As Date

        Dim dateNow As Date = Date.Now.Date
        thisDate = dateNow.AddDays(-dateNow.Day + 1)

        Return thisDate
    End Function

    Private Sub DisplayClosingListToGrid()
        Dim compID As String = gCompanyID()
        Dim dtFrom As Date = dtPeriodFrom.Value.Date
        Dim dtTo As Date = dtPeriodTo.Value.Date
        Dim entryNo As String = IIf(txtEntryNoFilter.Text = "", Nothing, txtEntryNoFilter.Text)

        grdClosingList.DataSource = LoadClosingEntryList(compID, dtFrom, dtTo, entryNo).Tables(0)
        Call FormatClosingListEntries()
    End Sub

    Private Sub frmClosingEntries_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtPeriodFrom.Value = SetAsFirstDayOfTheMonth()
        Call DisplayClosingListToGrid()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub btnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Call DisplayClosingListToGrid()
    End Sub

    Private Sub grdClosingList_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdClosingList.CellClick
        Try
            Dim pkEntryID As String = grdClosingList.Item("cPkEntryID", e.RowIndex).Value.ToString()
            grdClosingDetails.DataSource = LoadDetailsEntries(pkEntryID).Tables(0)
            Call FormatClosingEntriesDetailed()
        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Try
            Dim pkEntryID As String = grdClosingList.Item("cPkEntryID", grdClosingList.CurrentRow.Index).Value.ToString()

            With frmClosingEntryVoucher
                .GetpkEntryID = pkEntryID
                .MdiParent = frmMain
                .Show()
            End With

        Catch ex As NullReferenceException
            MessageBox.Show("There is no selected entry to display the voucher. Please select first.", _
                            "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Call PostSelectedEntry()
        Call DisplayClosingListToGrid()
    End Sub

    Private Sub grdClosingList_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles grdClosingList.UserDeletingRow
        If MessageBox.Show("Are you sure you want to delete selected entry?", "Post Entry", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Try
                Dim pkEntryID As String = grdClosingList.Item("cPkEntryID", grdClosingList.CurrentRow.Index).Value.ToString()
                Dim isPosted As String = grdClosingList.Item("cIsPosted", grdClosingList.CurrentRow.Index).Value.ToString()

                If isPosted = "False" Then
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_DeleteClosingEntry", _
                        New SqlParameter("@pkEntryID", pkEntryID))
                Else
                    MessageBox.Show("User Error! You cannot delete this because it is already posted!", "Post Entry", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    e.Cancel = True
                End If
            Catch ex As NullReferenceException
                MessageBox.Show("User Error! There is nothing to post. Please select first.", "Post Entry", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Cancel = True
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        frmDialogCreateEntry.ShowDialog()
    End Sub

    Public Sub ExecuteClosingProcess()
        picLoading.Visible = True
        bgwCreateEntries.RunWorkerAsync()
    End Sub

    Private Sub bgwCreateEntries_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwCreateEntries.DoWork
        Try
            Dim dateNow As Date = Date.Now.Date

            Call CreateClosingEntries(dateNow, GetPeriodFrom, GetPeriodTo)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub bgwCreateEntries_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwCreateEntries.RunWorkerCompleted
        picLoading.Visible = False

        Call DisplayClosingListToGrid()

        MessageBox.Show("You must post the closing entries in order to finally close the period.", "Closing Entries", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Public Function IsPeriodClosed(ByVal transDate As Date, ByVal control As Windows.Forms.Control) As Boolean
        Dim result As String
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CAS_t_IsPeriodClosed", _
                    New SqlParameter("@transactionDate", transDate))
                If rd.Read() Then
                    result = rd.Item(0)
                End If

                If result = "True" Then
                    control.Enabled = False
                    MessageBox.Show("You cannot create or modify any transaction belonging to a period which has been closed. Please check your closing entries.", _
                            "Transaction Denied", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return True
                Else
                    control.Enabled = True
                    Return False
                End If

            End Using
        Catch ex As Exception
            Return False
            Throw ex
        End Try
    End Function
End Class
