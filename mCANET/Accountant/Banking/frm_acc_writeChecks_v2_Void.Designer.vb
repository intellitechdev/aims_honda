<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_writeChecks_v2_Void
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_writeChecks_v2_Void))
        Me.btnCancel = New System.Windows.Forms.Button
        Me.grpVoidType = New System.Windows.Forms.GroupBox
        Me.optReprint = New System.Windows.Forms.RadioButton
        Me.optCancelPayment = New System.Windows.Forms.RadioButton
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.btnOK = New System.Windows.Forms.Button
        Me.txtReason = New System.Windows.Forms.TextBox
        Me.lblReason = New System.Windows.Forms.Label
        Me.lblCheckNo = New System.Windows.Forms.Label
        Me.txtCheckNo = New System.Windows.Forms.TextBox
        Me.grpVoidType.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(223, 226)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(81, 33)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'grpVoidType
        '
        Me.grpVoidType.Controls.Add(Me.optReprint)
        Me.grpVoidType.Controls.Add(Me.optCancelPayment)
        Me.grpVoidType.Location = New System.Drawing.Point(110, 12)
        Me.grpVoidType.Name = "grpVoidType"
        Me.grpVoidType.Size = New System.Drawing.Size(194, 82)
        Me.grpVoidType.TabIndex = 0
        Me.grpVoidType.TabStop = False
        Me.grpVoidType.Text = "What do you want to do?"
        '
        'optReprint
        '
        Me.optReprint.AutoSize = True
        Me.optReprint.Checked = True
        Me.optReprint.Location = New System.Drawing.Point(18, 22)
        Me.optReprint.Name = "optReprint"
        Me.optReprint.Size = New System.Drawing.Size(65, 19)
        Me.optReprint.TabIndex = 0
        Me.optReprint.TabStop = True
        Me.optReprint.Text = "Reprint"
        Me.optReprint.UseVisualStyleBackColor = True
        '
        'optCancelPayment
        '
        Me.optCancelPayment.AutoSize = True
        Me.optCancelPayment.Location = New System.Drawing.Point(18, 47)
        Me.optCancelPayment.Name = "optCancelPayment"
        Me.optCancelPayment.Size = New System.Drawing.Size(112, 19)
        Me.optCancelPayment.TabIndex = 1
        Me.optCancelPayment.Text = "Cancel Payment"
        Me.optCancelPayment.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(92, 82)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'btnOK
        '
        Me.btnOK.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOK.Location = New System.Drawing.Point(156, 226)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(61, 33)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        Me.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'txtReason
        '
        Me.txtReason.Location = New System.Drawing.Point(12, 160)
        Me.txtReason.Multiline = True
        Me.txtReason.Name = "txtReason"
        Me.txtReason.Size = New System.Drawing.Size(292, 60)
        Me.txtReason.TabIndex = 2
        '
        'lblReason
        '
        Me.lblReason.AutoSize = True
        Me.lblReason.Location = New System.Drawing.Point(9, 142)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(50, 15)
        Me.lblReason.TabIndex = 3
        Me.lblReason.Text = "Reason:"
        '
        'lblCheckNo
        '
        Me.lblCheckNo.AutoSize = True
        Me.lblCheckNo.Location = New System.Drawing.Point(9, 98)
        Me.lblCheckNo.Name = "lblCheckNo"
        Me.lblCheckNo.Size = New System.Drawing.Size(63, 15)
        Me.lblCheckNo.TabIndex = 1
        Me.lblCheckNo.Text = "Check No.:"
        '
        'txtCheckNo
        '
        Me.txtCheckNo.Location = New System.Drawing.Point(12, 116)
        Me.txtCheckNo.Name = "txtCheckNo"
        Me.txtCheckNo.Size = New System.Drawing.Size(292, 23)
        Me.txtCheckNo.TabIndex = 1
        '
        'frm_acc_writeChecks_v2_Void
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(316, 267)
        Me.Controls.Add(Me.txtCheckNo)
        Me.Controls.Add(Me.lblCheckNo)
        Me.Controls.Add(Me.lblReason)
        Me.Controls.Add(Me.txtReason)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.grpVoidType)
        Me.Controls.Add(Me.btnCancel)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_acc_writeChecks_v2_Void"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Void Checks"
        Me.grpVoidType.ResumeLayout(False)
        Me.grpVoidType.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents grpVoidType As System.Windows.Forms.GroupBox
    Friend WithEvents optReprint As System.Windows.Forms.RadioButton
    Friend WithEvents optCancelPayment As System.Windows.Forms.RadioButton
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents txtReason As System.Windows.Forms.TextBox
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents lblCheckNo As System.Windows.Forms.Label
    Friend WithEvents txtCheckNo As System.Windows.Forms.TextBox
End Class
