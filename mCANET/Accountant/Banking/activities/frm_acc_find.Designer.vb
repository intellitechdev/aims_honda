<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_find
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_find))
        Me.txtRefno = New System.Windows.Forms.TextBox
        Me.btnFind = New System.Windows.Forms.Button
        Me.cboPayee = New System.Windows.Forms.ComboBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.Rdopayee = New System.Windows.Forms.RadioButton
        Me.RdoReference = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'txtRefno
        '
        Me.txtRefno.Enabled = False
        Me.txtRefno.Location = New System.Drawing.Point(121, 39)
        Me.txtRefno.Name = "txtRefno"
        Me.txtRefno.Size = New System.Drawing.Size(228, 21)
        Me.txtRefno.TabIndex = 1
        Me.txtRefno.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnFind
        '
        Me.btnFind.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFind.Image = CType(resources.GetObject("btnFind.Image"), System.Drawing.Image)
        Me.btnFind.Location = New System.Drawing.Point(169, 66)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(87, 35)
        Me.btnFind.TabIndex = 2
        Me.btnFind.Text = "Find"
        Me.btnFind.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'cboPayee
        '
        Me.cboPayee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPayee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPayee.FormattingEnabled = True
        Me.cboPayee.Location = New System.Drawing.Point(73, 12)
        Me.cboPayee.MaxDropDownItems = 20
        Me.cboPayee.Name = "cboPayee"
        Me.cboPayee.Size = New System.Drawing.Size(276, 21)
        Me.cboPayee.Sorted = True
        Me.cboPayee.TabIndex = 3
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(262, 66)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 35)
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Rdopayee
        '
        Me.Rdopayee.AutoSize = True
        Me.Rdopayee.Checked = True
        Me.Rdopayee.Location = New System.Drawing.Point(7, 16)
        Me.Rdopayee.Name = "Rdopayee"
        Me.Rdopayee.Size = New System.Drawing.Size(60, 17)
        Me.Rdopayee.TabIndex = 8
        Me.Rdopayee.TabStop = True
        Me.Rdopayee.Text = "Payee"
        Me.Rdopayee.UseVisualStyleBackColor = True
        '
        'RdoReference
        '
        Me.RdoReference.AutoSize = True
        Me.RdoReference.Location = New System.Drawing.Point(7, 43)
        Me.RdoReference.Name = "RdoReference"
        Me.RdoReference.Size = New System.Drawing.Size(106, 17)
        Me.RdoReference.TabIndex = 9
        Me.RdoReference.Text = "Reference No."
        Me.RdoReference.UseVisualStyleBackColor = True
        '
        'frm_acc_find
        '
        Me.AcceptButton = Me.btnFind
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(355, 106)
        Me.Controls.Add(Me.RdoReference)
        Me.Controls.Add(Me.Rdopayee)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.cboPayee)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.txtRefno)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_acc_find"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Find"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtRefno As System.Windows.Forms.TextBox
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents cboPayee As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Rdopayee As System.Windows.Forms.RadioButton
    Friend WithEvents RdoReference As System.Windows.Forms.RadioButton
End Class
