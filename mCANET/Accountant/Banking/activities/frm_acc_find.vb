Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frm_acc_find
    Private gCon As New Clsappconfiguration
    Private Sub frm_acc_find_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call m_cDisplayNames(cboPayee, True)
    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFind.Click

        frm_acc_writechecks.Lbltemprefno.Text = txtRefno.Text

        If cboPayee.SelectedItem = "" And txtRefno.Text = "" Then 'No payee name and reference number supplied
            MsgBox("Please select a valid payee name or type a reference number.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
        Else

            'No reference number supplied
            If txtRefno.Text = "" And cboPayee.SelectedItem <> "" Then
                xVar = Convert.ToString(DBNull.Value)
                gPayeeID = getnameid()
                gPayeeRefno = xVar
                gPayeeName = cboPayee.SelectedItem

                frm_acc_writechecks.psEditMode = True
                frm_acc_writechecks.grdLoadExpense.Visible = True
                frm_acc_writechecks.gExpenses.Visible = False
                frm_acc_writechecks.cbochk_payorder.SelectedItem = gPayeeName
                frm_acc_writechecks.load_acc_expensedetails()

                cboPayee.Text = "(Pls. select a Payee name.)"
                txtRefno.Text = ""
                cboPayee.Items.Clear()
                Close()
            Else
                ' No payee name supplied
                If txtRefno.Text <> "" And cboPayee.SelectedItem = "" Then
                    xVar = txtRefno.Text
                    gName = Get_Name_by_Reference(xVar)
                    gType = Get_Type_by_Reference(xVar)
                    gPayeeID = GetPayeeID(gName, gType)
                    gPayeeRefno = xVar
                    gPayeeName = gName + " - " + gType

                    frm_acc_writechecks.psEditMode = True
                    frm_acc_writechecks.grdLoadExpense.Visible = True
                    frm_acc_writechecks.gExpenses.Visible = False
                    frm_acc_writechecks.cbochk_payorder.SelectedItem = gPayeeName
                    frm_acc_writechecks.cbochk_payorder.Text = gPayeeName
                    frm_acc_writechecks.lblchk_payorder.Text = frm_acc_writechecks.cbochk_payorder.SelectedItem
                    frm_acc_writechecks.load_acc_expensedetails()

                    cboPayee.Text = "(Pls. select a Payee name.)"
                    txtRefno.Text = ""
                    cboPayee.Items.Clear()
                    Close()
                Else
                    'payee name and reference number supplied
                    xVar = txtRefno.Text
                    gPayeeID = getnameid()
                    gPayeeRefno = xVar
                    gPayeeName = cboPayee.SelectedItem

                    frm_acc_writechecks.psEditMode = True
                    frm_acc_writechecks.grdLoadExpense.Visible = True
                    frm_acc_writechecks.gExpenses.Visible = False
                    frm_acc_writechecks.cbochk_payorder.SelectedItem = gPayeeName
                    frm_acc_writechecks.load_acc_expensedetails()

                    cboPayee.Text = "(Pls. select a Payee name.)"
                    txtRefno.Text = ""
                    cboPayee.Items.Clear()
                    Close()
                End If
            End If
        End If
    End Sub

    Private Function GetPayeeID(ByVal name As String, ByVal type As String)
        Dim sSqlGetId As String
        If type = "Supplier" Or type = "supplier" Then
            sSqlGetId = "SELECT fxKeySupplier FROM dbo.mSupplier00Master WHERE fcSupplierName='" & name & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlGetId)
                    While rd.Read
                        Return rd.Item(0).ToString
                    End While
                End Using
            Catch ex As Exception
                Return Nothing
            End Try
        End If
    End Function
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        cboPayee.Text = "(Pls. select a Payee name.)"
        txtRefno.Text = ""
        Me.Close()
    End Sub

    Private Function getnameid() As String
        If cboPayee.SelectedItem = "" Or cboPayee.SelectedItem = "(Pls. select a Payee name.)" Then
            'Return cboPayee.SelectedItem
            'xVarPayee = Convert.ToString(DBNull.Value)
            Return m_GetNameID(gName, gType)
            'Return xVarPayee

        Else

            Dim sName() As String
            xVarPayee = cboPayee.SelectedItem
            'sName = Split(cboPayee.SelectedItem, "-")
            sName = Split(xVarPayee, "-")
            Return m_GetNameID(sName(0), sName(1))
        End If
    End Function

    Private Sub RdoReference_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RdoReference.CheckedChanged
        If RdoReference.Checked = True Then
            txtRefno.Enabled = True
            cboPayee.Enabled = False
            cboPayee.Text = "(Pls. select a Payee name.)"
        End If
    End Sub

    Private Sub Rdopayee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Rdopayee.CheckedChanged
        If Rdopayee.Checked = True Then
            txtRefno.Enabled = False
            cboPayee.Enabled = True
            txtRefno.Text = ""
        End If
    End Sub

    Private Sub txtRefno_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRefno.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 _
        AndAlso Not IsNumeric(e.KeyChar) Then
            Beep()
            e.Handled = True
        End If
    End Sub

    Private Sub txtRefno_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRefno.TextChanged

    End Sub
End Class