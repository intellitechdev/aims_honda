﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Imports WPM_EDCRYPT
Imports System.Text.RegularExpressions
Public Class frmAddBank
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim i As Integer
    Dim compID As String = gCompanyID()
    Dim BankID As String

    Private Sub frmAddBank_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call BAnkList()
        Call SelectedColumn()
    End Sub

    Private Sub SelectedColumn()
        Try
            gAccountID = dgvlistAddBank.SelectedRows(0).Cells(0).Value.ToString()
            gAccountName = dgvlistAddBank.SelectedRows(0).Cells(1).Value.ToString()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BAnkList()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "bankfile_ListBankChart",
                                      New SqlParameter("@co_id", compID))

        dgvlistAddBank.DataSource = ds.Tables(0)

        With dgvlistAddBank
            .Columns("AccountID").Visible = False
            .Columns("BankName").HeaderText = "Bank Name"
        End With
    End Sub
   
    Private Sub dgvlistAddBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvlistAddBank.Click
        Call SelectedColumn()
    End Sub
   
    Private Sub InsertBank(ByVal BancID As String, ByVal AccID As String, ByVal companyID As String, ByVal AccountNym As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "bankfile_CheckDuplicate", _
                                   New SqlParameter("@FK_AcntId", gAccountID),
                                   New SqlParameter("@FK_CoId", companyID))
        If rd.Read = True Then
            MsgBox("Bank Already Added")
            Exit Sub
        End If

        SqlHelper.ExecuteNonQuery(cs, CommandType.StoredProcedure, "bankfile_Insert",
                                  New SqlParameter("PK_BankID", BancID),
                                  New SqlParameter("FK_AcntId", gAccountID),
                                  New SqlParameter("FK_CoId", companyID),
                                  New SqlParameter("fcBankName", gAccountName))
        txtSearch.Text = ""
    End Sub
    Public Property GetBankID() As Integer
        Get
            Return BankID
        End Get
        Set(ByVal value As Integer)
            BankID = value
        End Set

    End Property

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Dim BankID As String

        If GetBankID <> 0 Then
            BankID = GetBankID
        Else
            BankID = Guid.NewGuid.ToString
        End If
        InsertBank(BankID, gAccountID, compID, gAccountName)
        MsgBox("Bank Saved", vbInformation, "Save Succeed")
        frmCheckIssuance.BankList()
        Me.Close()
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Then
            Call BAnkList()
            Call SelectedColumn()
        Else
            Call BankListFilter()
            Call SelectedColumn()
        End If
    End Sub

    Private Sub BankListFilter()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "bankfile_filter",
                                      New SqlParameter("@co_id", compID),
                                      New SqlParameter("@acnt_name", txtSearch.Text))

        dgvlistAddBank.DataSource = ds.Tables(0)

        With dgvlistAddBank
            .Columns("AccountID").Visible = False
            .Columns("BankName").HeaderText = "Bank Name"
        End With
    End Sub
End Class