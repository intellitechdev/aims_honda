Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmBankFileConvert
    Private gcon As New Clsappconfiguration
    
#Region "property"
    Private datestart As Date
    Public Property getdatestart() As Date
        Get
            Return datestart
        End Get
        Set(ByVal value As Date)
            datestart = value
        End Set
    End Property
    Private batch As Integer
    Public Property getbatch() As Integer
        Get
            Return batch
        End Get
        Set(ByVal value As Integer)
            batch = value
        End Set
    End Property
    Private ceilingamt As Decimal
    Public Property getceilingamt() As Decimal
        Get
            Return ceilingamt
        End Get
        Set(ByVal value As Decimal)
            ceilingamt = value
        End Set
    End Property
    Private rowcount As Integer
    Public Property getrowcount() As Integer
        Get
            Return rowcount
        End Get
        Set(ByVal value As Integer)
            rowcount = value
        End Set
    End Property
    Private AmountNetPay As Decimal
    Public Property getAmount() As Decimal
        Get
            Return AmountNetPay
        End Get
        Set(ByVal value As Decimal)
            AmountNetPay = value
        End Set
    End Property
    Private AccountNo As String
    Public Property getAcctNo() As String
        Get
            Return AccountNo
        End Get
        Set(ByVal value As String)
            AccountNo = value
        End Set
    End Property
  
#End Region
    Private Sub frmBankFileConvert_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'frmBankFileConvert.WindowState.Norma()
        bankfile(getdatestart, getbatch, getceilingamt)
        amounttopay(getdatestart())

    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub
    Private Sub Textfilesave()
        Try
            Dim myfile As String = "D:\BankFile1.txt"
            Dim detail As String
            Dim header As String = Me.grdbankfile.Rows(0).Cells(0).Value.ToString
            Dim trailer As String = Me.grdbankfile.Rows(0).Cells(2).Value.ToString

            Dim objWriter As New System.IO.StreamWriter(myfile)

            objWriter.WriteLine(header)
            For Each xRow As DataGridViewRow In grdbankfile.Rows
                detail = xRow.Cells(1).Value
                objWriter.WriteLine(detail)
            Next
            objWriter.WriteLine(trailer)
            objWriter.Close()

            MessageBox.Show("Writing succesful!", "Bank File", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception

            MessageBox.Show("Failure writing to file! Did you create the file 'C:\BankFile.txt'?", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
        End Try
    End Sub
    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Try
            Call Textfilesave()
            Call Historicalrelease(getAmount(), getbatch(), getdatestart())
            Call Updatepaybills(getAmount(), Me.txtaudit.Text, gCompanyID())
            Call Loopamounttopay()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub amounttopay(ByVal paydate As Date)
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand("AmounttoPAY", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@dtfrom", SqlDbType.Date).Value = paydate
        End With
        Try
            da.SelectCommand = cmd
            da.Fill(ds, "amount to pay")
            Me.grdamoutotpay.DataSource = ds
            grdamoutotpay.DataMember = "amount to pay"
            grdamoutotpay.Columns(0).Width = 80
            grdamoutotpay.Columns(1).Width = 50
        Catch ex As Exception
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub bankfile(ByVal date1 As Date, ByVal batch As Integer, ByVal ceilingamt As Decimal)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand("bankfile", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@daterel", SqlDbType.Date).Value = date1
            .Add("@batchNo", SqlDbType.Int).Value = batch
            '.Add("@ceilingAmnt", SqlDbType.Decimal).Value = ceilingamt
        End With

        Try
            da.SelectCommand = cmd
            da.Fill(ds, "Bankfile")
            grdbankfile.ReadOnly = True
            Me.grdbankfile.DataSource = ds
            Me.grdbankfile.Visible = False
            grdbankfile.DataMember = "Bankfile"
            grdbankfile.Columns(0).Width = 80
            grdbankfile.Columns(1).Width = 50
            grdbankfile.Columns(2).Width = 150

        Catch ex As Exception

        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub btnCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        propdebitadvice()
        BankAdvice1.getdatestart() = getdatestart()
        Call BankAdvice1.Show()
        GenerateBankReports()
        'If BackgroundWorker1.IsBusy = True Or BackgroundWorker2.IsBusy = True Or BackgroundWorker3.IsBusy = True Then
        '    MessageBox.Show("Report is still Loading", "Report", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'Else
        '    PictureBox1.Visible = True
        '    BackgroundWorker1.RunWorkerAsync()
        'BackgroundWorker2.RunWorkerAsync()
        'BackgroundWorker3.RunWorkerAsync()
        'End If
    End Sub
    Private Sub frmBankFileConvert_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    End Sub
    Private Sub GenerateBankReports()
        Bank_Advice2.getbatch() = getbatch()
        Bank_Advice2.getdatestart = getdatestart()
        Call Bank_Advice2.Show()
    End Sub
    Private Sub propdebitadvice()
        Dim dateon As Date = Me.date1.Value.Date
        Dim audit As String = Me.txtaudit.Text
        Dim appr As String = Me.txtappr.Text
        DebitAdvice.getwitdate() = dateon
        DebitAdvice.getaudit() = audit
        DebitAdvice.getapprove() = appr
        DebitAdvice.getAcctNo() = getAcctNo()
        DebitAdvice.getAmount() = getAmount()
        Call DebitAdvice.Show()
    End Sub

    Private Sub Historicalrelease(ByVal amount As Decimal, ByVal batch As Integer, ByVal paydate As Date)
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "bankfileHistorical", _
            New SqlParameter("@amount", amount), _
            New SqlParameter("@batch", batch), _
            New SqlParameter("@date", paydate))
            trans.Commit()
            MessageBox.Show("Success!", "Historical Release")
        Catch ex As Exception
            trans.Rollback()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub Updatepaybills(ByVal netamount As Decimal, ByVal updateby As String, ByVal gCompanyID As String)
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "BankfieATMpaybills", _
            New SqlParameter("@amount", netamount), _
            New SqlParameter("@updateby", updateby), _
            New SqlParameter("@compID", gCompanyID))

            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub loopamount(ByVal bill As String, ByVal amount As Decimal, ByVal update As String)
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "updatebills", _
            New SqlParameter("@billID", bill), _
            New SqlParameter("@TotalAmountPaid", amount), _
            New SqlParameter("@updateby", update))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub insert()

    End Sub
    Private Sub Loopamounttopay()
        Dim bill As String
        Dim amount As Decimal
        Dim update As String = Me.txtappr.Text
        For Each xrow As DataGridViewRow In Me.grdamoutotpay.Rows
            bill = xrow.Cells(0).Value
            amount = xrow.Cells(1).Value
            Call loopamount(bill, amount, update)
        Next
        MessageBox.Show("UPDATED!", "BILLS")
    End Sub
End Class