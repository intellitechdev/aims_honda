<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_reconcile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_reconcile))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboReconAcnt = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.dte_recon_statementdate = New System.Windows.Forms.DateTimePicker
        Me.Label4 = New System.Windows.Forms.Label
        Me.txt_recon_beginBalance = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txt_recon_endBalance = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txt_recon_serviceChg = New System.Windows.Forms.TextBox
        Me.dte_recon_srvcChgDate = New System.Windows.Forms.DateTimePicker
        Me.cboSrvcAcnt = New System.Windows.Forms.ComboBox
        Me.cboIntAcnt = New System.Windows.Forms.ComboBox
        Me.dte_recon_intDate = New System.Windows.Forms.DateTimePicker
        Me.txt_recon_intEarned = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btn_recon_locdiscrep = New System.Windows.Forms.Button
        Me.btn_recon_lstrecon = New System.Windows.Forms.Button
        Me.btn_recon_continue = New System.Windows.Forms.Button
        Me.btn_recon_cancel = New System.Windows.Forms.Button
        Me.Label13 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(393, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Select and account to reconcile, and then enter the ending balance "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(34, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Account"
        '
        'cboReconAcnt
        '
        Me.cboReconAcnt.FormattingEnabled = True
        Me.cboReconAcnt.Location = New System.Drawing.Point(162, 62)
        Me.cboReconAcnt.Name = "cboReconAcnt"
        Me.cboReconAcnt.Size = New System.Drawing.Size(205, 21)
        Me.cboReconAcnt.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(34, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Statement Date"
        '
        'dte_recon_statementdate
        '
        Me.dte_recon_statementdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dte_recon_statementdate.Location = New System.Drawing.Point(162, 88)
        Me.dte_recon_statementdate.Name = "dte_recon_statementdate"
        Me.dte_recon_statementdate.Size = New System.Drawing.Size(144, 21)
        Me.dte_recon_statementdate.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(34, 119)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(111, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Beginning balance"
        '
        'txt_recon_beginBalance
        '
        Me.txt_recon_beginBalance.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txt_recon_beginBalance.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt_recon_beginBalance.Location = New System.Drawing.Point(162, 119)
        Me.txt_recon_beginBalance.Name = "txt_recon_beginBalance"
        Me.txt_recon_beginBalance.ReadOnly = True
        Me.txt_recon_beginBalance.Size = New System.Drawing.Size(145, 14)
        Me.txt_recon_beginBalance.TabIndex = 6
        Me.txt_recon_beginBalance.Text = "0.00"
        Me.txt_recon_beginBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(34, 147)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Ending balance"
        '
        'txt_recon_endBalance
        '
        Me.txt_recon_endBalance.Location = New System.Drawing.Point(162, 144)
        Me.txt_recon_endBalance.Name = "txt_recon_endBalance"
        Me.txt_recon_endBalance.Size = New System.Drawing.Size(144, 21)
        Me.txt_recon_endBalance.TabIndex = 8
        Me.txt_recon_endBalance.Text = "0.00"
        Me.txt_recon_endBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 199)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(261, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Enter any service charge or interest earned."
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Location = New System.Drawing.Point(12, 184)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(408, 1)
        Me.Panel2.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(37, 231)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(96, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Service Charge"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(194, 231)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(34, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Date"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(338, 231)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 13)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Account"
        '
        'txt_recon_serviceChg
        '
        Me.txt_recon_serviceChg.Location = New System.Drawing.Point(36, 247)
        Me.txt_recon_serviceChg.Name = "txt_recon_serviceChg"
        Me.txt_recon_serviceChg.Size = New System.Drawing.Size(132, 21)
        Me.txt_recon_serviceChg.TabIndex = 14
        Me.txt_recon_serviceChg.Text = "0.00"
        Me.txt_recon_serviceChg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dte_recon_srvcChgDate
        '
        Me.dte_recon_srvcChgDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dte_recon_srvcChgDate.Location = New System.Drawing.Point(191, 247)
        Me.dte_recon_srvcChgDate.Name = "dte_recon_srvcChgDate"
        Me.dte_recon_srvcChgDate.Size = New System.Drawing.Size(115, 21)
        Me.dte_recon_srvcChgDate.TabIndex = 15
        '
        'cboSrvcAcnt
        '
        Me.cboSrvcAcnt.FormattingEnabled = True
        Me.cboSrvcAcnt.Location = New System.Drawing.Point(341, 248)
        Me.cboSrvcAcnt.Name = "cboSrvcAcnt"
        Me.cboSrvcAcnt.Size = New System.Drawing.Size(182, 21)
        Me.cboSrvcAcnt.TabIndex = 16
        '
        'cboIntAcnt
        '
        Me.cboIntAcnt.FormattingEnabled = True
        Me.cboIntAcnt.Location = New System.Drawing.Point(341, 296)
        Me.cboIntAcnt.Name = "cboIntAcnt"
        Me.cboIntAcnt.Size = New System.Drawing.Size(182, 21)
        Me.cboIntAcnt.TabIndex = 22
        '
        'dte_recon_intDate
        '
        Me.dte_recon_intDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dte_recon_intDate.Location = New System.Drawing.Point(191, 295)
        Me.dte_recon_intDate.Name = "dte_recon_intDate"
        Me.dte_recon_intDate.Size = New System.Drawing.Size(115, 21)
        Me.dte_recon_intDate.TabIndex = 21
        '
        'txt_recon_intEarned
        '
        Me.txt_recon_intEarned.Location = New System.Drawing.Point(36, 295)
        Me.txt_recon_intEarned.Name = "txt_recon_intEarned"
        Me.txt_recon_intEarned.Size = New System.Drawing.Size(132, 21)
        Me.txt_recon_intEarned.TabIndex = 20
        Me.txt_recon_intEarned.Text = "0.00"
        Me.txt_recon_intEarned.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(338, 279)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Account"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(194, 279)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(34, 13)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "Date"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(36, 279)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 13)
        Me.Label12.TabIndex = 17
        Me.Label12.Text = "Interest Earned"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(12, 336)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(408, 1)
        Me.Panel1.TabIndex = 23
        '
        'btn_recon_locdiscrep
        '
        Me.btn_recon_locdiscrep.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_recon_locdiscrep.Location = New System.Drawing.Point(12, 351)
        Me.btn_recon_locdiscrep.Name = "btn_recon_locdiscrep"
        Me.btn_recon_locdiscrep.Size = New System.Drawing.Size(150, 23)
        Me.btn_recon_locdiscrep.TabIndex = 24
        Me.btn_recon_locdiscrep.Text = "Locate Discrepancies"
        Me.btn_recon_locdiscrep.UseVisualStyleBackColor = True
        '
        'btn_recon_lstrecon
        '
        Me.btn_recon_lstrecon.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_recon_lstrecon.Location = New System.Drawing.Point(166, 351)
        Me.btn_recon_lstrecon.Name = "btn_recon_lstrecon"
        Me.btn_recon_lstrecon.Size = New System.Drawing.Size(150, 23)
        Me.btn_recon_lstrecon.TabIndex = 25
        Me.btn_recon_lstrecon.Text = "Undo Last Reconcilation"
        Me.btn_recon_lstrecon.UseVisualStyleBackColor = True
        '
        'btn_recon_continue
        '
        Me.btn_recon_continue.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_recon_continue.Location = New System.Drawing.Point(320, 351)
        Me.btn_recon_continue.Name = "btn_recon_continue"
        Me.btn_recon_continue.Size = New System.Drawing.Size(108, 23)
        Me.btn_recon_continue.TabIndex = 26
        Me.btn_recon_continue.Text = "Continue"
        Me.btn_recon_continue.UseVisualStyleBackColor = True
        '
        'btn_recon_cancel
        '
        Me.btn_recon_cancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_recon_cancel.Location = New System.Drawing.Point(432, 351)
        Me.btn_recon_cancel.Name = "btn_recon_cancel"
        Me.btn_recon_cancel.Size = New System.Drawing.Size(108, 23)
        Me.btn_recon_cancel.TabIndex = 27
        Me.btn_recon_cancel.Text = "Cancel"
        Me.btn_recon_cancel.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(10, 26)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(176, 13)
        Me.Label13.TabIndex = 28
        Me.Label13.Text = "from your Account Statement"
        '
        'frm_acc_reconcile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(555, 394)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.btn_recon_cancel)
        Me.Controls.Add(Me.btn_recon_continue)
        Me.Controls.Add(Me.btn_recon_lstrecon)
        Me.Controls.Add(Me.btn_recon_locdiscrep)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cboIntAcnt)
        Me.Controls.Add(Me.dte_recon_intDate)
        Me.Controls.Add(Me.txt_recon_intEarned)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.cboSrvcAcnt)
        Me.Controls.Add(Me.dte_recon_srvcChgDate)
        Me.Controls.Add(Me.txt_recon_serviceChg)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_recon_endBalance)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txt_recon_beginBalance)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dte_recon_statementdate)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboReconAcnt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(561, 421)
        Me.Name = "frm_acc_reconcile"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Begin Reconciliation"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboReconAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dte_recon_statementdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_recon_beginBalance As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_recon_endBalance As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_recon_serviceChg As System.Windows.Forms.TextBox
    Friend WithEvents dte_recon_srvcChgDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboSrvcAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents cboIntAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents dte_recon_intDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_recon_intEarned As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btn_recon_locdiscrep As System.Windows.Forms.Button
    Friend WithEvents btn_recon_lstrecon As System.Windows.Forms.Button
    Friend WithEvents btn_recon_continue As System.Windows.Forms.Button
    Friend WithEvents btn_recon_cancel As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class
