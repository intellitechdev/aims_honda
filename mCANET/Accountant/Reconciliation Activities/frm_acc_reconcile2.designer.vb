<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_reconcile2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_reconcile2))
        Me.Label1 = New System.Windows.Forms.Label
        Me.grdChkPayments = New System.Windows.Forms.DataGridView
        Me.lblDatePeriod = New System.Windows.Forms.Label
        Me.chkHideTransaction = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnColDisplay = New System.Windows.Forms.Button
        Me.btnGoto = New System.Windows.Forms.Button
        Me.btnUnmarkAll = New System.Windows.Forms.Button
        Me.btnMarkAll = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.grdDepCredits = New System.Windows.Forms.DataGridView
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtBegBal = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblCntDep = New System.Windows.Forms.Label
        Me.lblCntChck = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtDepAmt = New System.Windows.Forms.TextBox
        Me.txtChkAmnt = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtdiffamt = New System.Windows.Forms.TextBox
        Me.txtclrbal = New System.Windows.Forms.TextBox
        Me.txtendbal = New System.Windows.Forms.TextBox
        Me.txtintearnd = New System.Windows.Forms.TextBox
        Me.txtsrvcchg = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.btnModify = New System.Windows.Forms.Button
        Me.btnReconcile = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        CType(Me.grdChkPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdDepCredits, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "for the period: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grdChkPayments
        '
        Me.grdChkPayments.AllowUserToOrderColumns = True
        Me.grdChkPayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdChkPayments.Location = New System.Drawing.Point(7, 28)
        Me.grdChkPayments.Name = "grdChkPayments"
        Me.grdChkPayments.Size = New System.Drawing.Size(408, 113)
        Me.grdChkPayments.TabIndex = 2
        '
        'lblDatePeriod
        '
        Me.lblDatePeriod.AutoSize = True
        Me.lblDatePeriod.Location = New System.Drawing.Point(92, 3)
        Me.lblDatePeriod.Name = "lblDatePeriod"
        Me.lblDatePeriod.Size = New System.Drawing.Size(81, 13)
        Me.lblDatePeriod.TabIndex = 3
        Me.lblDatePeriod.Text = "mm/dd/yyyy"
        Me.lblDatePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkHideTransaction
        '
        Me.chkHideTransaction.AutoSize = True
        Me.chkHideTransaction.Location = New System.Drawing.Point(499, 3)
        Me.chkHideTransaction.Name = "chkHideTransaction"
        Me.chkHideTransaction.Size = New System.Drawing.Size(301, 17)
        Me.chkHideTransaction.TabIndex = 4
        Me.chkHideTransaction.Text = "Hide transactions after the statement's end date"
        Me.chkHideTransaction.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnColDisplay)
        Me.GroupBox1.Controls.Add(Me.btnGoto)
        Me.GroupBox1.Controls.Add(Me.btnUnmarkAll)
        Me.GroupBox1.Controls.Add(Me.btnMarkAll)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.grdDepCredits)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.grdChkPayments)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 19)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(836, 177)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'btnColDisplay
        '
        Me.btnColDisplay.Location = New System.Drawing.Point(665, 147)
        Me.btnColDisplay.Name = "btnColDisplay"
        Me.btnColDisplay.Size = New System.Drawing.Size(163, 23)
        Me.btnColDisplay.TabIndex = 12
        Me.btnColDisplay.Text = "Columns to Display"
        Me.btnColDisplay.UseVisualStyleBackColor = True
        Me.btnColDisplay.Visible = False
        '
        'btnGoto
        '
        Me.btnGoto.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGoto.Location = New System.Drawing.Point(427, 147)
        Me.btnGoto.Name = "btnGoto"
        Me.btnGoto.Size = New System.Drawing.Size(87, 23)
        Me.btnGoto.TabIndex = 11
        Me.btnGoto.Text = "Go To"
        Me.btnGoto.UseVisualStyleBackColor = True
        '
        'btnUnmarkAll
        '
        Me.btnUnmarkAll.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnmarkAll.Location = New System.Drawing.Point(336, 147)
        Me.btnUnmarkAll.Name = "btnUnmarkAll"
        Me.btnUnmarkAll.Size = New System.Drawing.Size(87, 23)
        Me.btnUnmarkAll.TabIndex = 10
        Me.btnUnmarkAll.Text = "Unmark All"
        Me.btnUnmarkAll.UseVisualStyleBackColor = True
        '
        'btnMarkAll
        '
        Me.btnMarkAll.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMarkAll.Location = New System.Drawing.Point(245, 147)
        Me.btnMarkAll.Name = "btnMarkAll"
        Me.btnMarkAll.Size = New System.Drawing.Size(87, 23)
        Me.btnMarkAll.TabIndex = 9
        Me.btnMarkAll.Text = "Mark All"
        Me.btnMarkAll.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(422, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(162, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Deposits and Other Credits"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grdDepCredits
        '
        Me.grdDepCredits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDepCredits.Location = New System.Drawing.Point(421, 28)
        Me.grdDepCredits.Name = "grdDepCredits"
        Me.grdDepCredits.Size = New System.Drawing.Size(408, 113)
        Me.grdDepCredits.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(134, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Checks and Payments"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 214)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(112, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Beginning Balance"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBegBal
        '
        Me.txtBegBal.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtBegBal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBegBal.Location = New System.Drawing.Point(257, 215)
        Me.txtBegBal.Name = "txtBegBal"
        Me.txtBegBal.Size = New System.Drawing.Size(117, 14)
        Me.txtBegBal.TabIndex = 7
        Me.txtBegBal.Text = "0.00"
        Me.txtBegBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 231)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(191, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Items you have marked cleared"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCntDep
        '
        Me.lblCntDep.AutoSize = True
        Me.lblCntDep.Location = New System.Drawing.Point(44, 250)
        Me.lblCntDep.Name = "lblCntDep"
        Me.lblCntDep.Size = New System.Drawing.Size(14, 13)
        Me.lblCntDep.TabIndex = 9
        Me.lblCntDep.Text = "0"
        Me.lblCntDep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCntChck
        '
        Me.lblCntChck.AutoSize = True
        Me.lblCntChck.Location = New System.Drawing.Point(44, 266)
        Me.lblCntChck.Name = "lblCntChck"
        Me.lblCntChck.Size = New System.Drawing.Size(14, 13)
        Me.lblCntChck.TabIndex = 10
        Me.lblCntChck.Text = "0"
        Me.lblCntChck.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(72, 250)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(162, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Deposits and Other Credits"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(72, 266)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(134, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Checks and Payments"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepAmt
        '
        Me.txtDepAmt.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtDepAmt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDepAmt.Location = New System.Drawing.Point(257, 249)
        Me.txtDepAmt.Name = "txtDepAmt"
        Me.txtDepAmt.Size = New System.Drawing.Size(117, 14)
        Me.txtDepAmt.TabIndex = 13
        Me.txtDepAmt.Text = "0.00"
        Me.txtDepAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtChkAmnt
        '
        Me.txtChkAmnt.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtChkAmnt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtChkAmnt.Location = New System.Drawing.Point(257, 266)
        Me.txtChkAmnt.Name = "txtChkAmnt"
        Me.txtChkAmnt.Size = New System.Drawing.Size(117, 14)
        Me.txtChkAmnt.TabIndex = 14
        Me.txtChkAmnt.Text = "0.00"
        Me.txtChkAmnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtdiffamt)
        Me.GroupBox2.Controls.Add(Me.txtclrbal)
        Me.GroupBox2.Controls.Add(Me.txtendbal)
        Me.GroupBox2.Controls.Add(Me.txtintearnd)
        Me.GroupBox2.Controls.Add(Me.txtsrvcchg)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.btnModify)
        Me.GroupBox2.Location = New System.Drawing.Point(422, 196)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(415, 111)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        '
        'txtdiffamt
        '
        Me.txtdiffamt.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtdiffamt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtdiffamt.Location = New System.Drawing.Point(264, 86)
        Me.txtdiffamt.Name = "txtdiffamt"
        Me.txtdiffamt.Size = New System.Drawing.Size(117, 14)
        Me.txtdiffamt.TabIndex = 24
        Me.txtdiffamt.Text = "0.00"
        Me.txtdiffamt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtclrbal
        '
        Me.txtclrbal.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtclrbal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtclrbal.Location = New System.Drawing.Point(264, 70)
        Me.txtclrbal.Name = "txtclrbal"
        Me.txtclrbal.Size = New System.Drawing.Size(117, 14)
        Me.txtclrbal.TabIndex = 23
        Me.txtclrbal.Text = "0.00"
        Me.txtclrbal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtendbal
        '
        Me.txtendbal.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtendbal.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtendbal.Location = New System.Drawing.Point(264, 54)
        Me.txtendbal.Name = "txtendbal"
        Me.txtendbal.Size = New System.Drawing.Size(117, 14)
        Me.txtendbal.TabIndex = 22
        Me.txtendbal.Text = "0.00"
        Me.txtendbal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtintearnd
        '
        Me.txtintearnd.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtintearnd.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtintearnd.Location = New System.Drawing.Point(264, 37)
        Me.txtintearnd.Name = "txtintearnd"
        Me.txtintearnd.Size = New System.Drawing.Size(117, 14)
        Me.txtintearnd.TabIndex = 21
        Me.txtintearnd.Text = "0.00"
        Me.txtintearnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtsrvcchg
        '
        Me.txtsrvcchg.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtsrvcchg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtsrvcchg.Location = New System.Drawing.Point(264, 20)
        Me.txtsrvcchg.Name = "txtsrvcchg"
        Me.txtsrvcchg.Size = New System.Drawing.Size(117, 14)
        Me.txtsrvcchg.TabIndex = 16
        Me.txtsrvcchg.Text = "0.00"
        Me.txtsrvcchg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(115, 86)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(66, 13)
        Me.Label12.TabIndex = 20
        Me.Label12.Text = "Difference"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(115, 70)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(101, 13)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Cleared Balance"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(115, 53)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(94, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Ending Balance"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(115, 35)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(96, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Interest Earned"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(115, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Service Charge"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnModify
        '
        Me.btnModify.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModify.Location = New System.Drawing.Point(7, 13)
        Me.btnModify.Name = "btnModify"
        Me.btnModify.Size = New System.Drawing.Size(87, 23)
        Me.btnModify.TabIndex = 13
        Me.btnModify.Text = "Modify"
        Me.btnModify.UseVisualStyleBackColor = True
        '
        'btnReconcile
        '
        Me.btnReconcile.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReconcile.Location = New System.Drawing.Point(584, 313)
        Me.btnReconcile.Name = "btnReconcile"
        Me.btnReconcile.Size = New System.Drawing.Size(120, 23)
        Me.btnReconcile.TabIndex = 13
        Me.btnReconcile.Text = "Reconcile Now"
        Me.btnReconcile.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(708, 313)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(120, 23)
        Me.btnCancel.TabIndex = 16
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frm_acc_reconcile2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(842, 341)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnReconcile)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.txtChkAmnt)
        Me.Controls.Add(Me.txtDepAmt)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblCntChck)
        Me.Controls.Add(Me.lblCntDep)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtBegBal)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkHideTransaction)
        Me.Controls.Add(Me.lblDatePeriod)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_acc_reconcile2"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reconcile"
        CType(Me.grdChkPayments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdDepCredits, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grdChkPayments As System.Windows.Forms.DataGridView
    Friend WithEvents lblDatePeriod As System.Windows.Forms.Label
    Friend WithEvents chkHideTransaction As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grdDepCredits As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnGoto As System.Windows.Forms.Button
    Friend WithEvents btnUnmarkAll As System.Windows.Forms.Button
    Friend WithEvents btnMarkAll As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnColDisplay As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtBegBal As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblCntDep As System.Windows.Forms.Label
    Friend WithEvents lblCntChck As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtDepAmt As System.Windows.Forms.TextBox
    Friend WithEvents txtChkAmnt As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtdiffamt As System.Windows.Forms.TextBox
    Friend WithEvents txtclrbal As System.Windows.Forms.TextBox
    Friend WithEvents txtendbal As System.Windows.Forms.TextBox
    Friend WithEvents txtintearnd As System.Windows.Forms.TextBox
    Friend WithEvents txtsrvcchg As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnModify As System.Windows.Forms.Button
    Friend WithEvents btnReconcile As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
