Option Explicit On
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class clsTransactionFunctions

    Private gCon As New Clsappconfiguration

#Region "Functions"

    Public Sub gGridClear(ByRef grdView As DataGridView)
        Dim xCol, xRow As Integer
        With grdView
            For xCol = 0 To .Columns.Count - 2
                For xRow = 0 To .Rows.Count - 1
                    .Item(xCol, xRow).Value = Nothing
                Next
            Next
        End With
    End Sub

    Public Sub gClearFormTxt(ByVal stForm As Form)
        Dim xCtrl As Control
        For Each xCtrl In stForm.Controls
            If (TypeOf xCtrl Is TextBox) Or (TypeOf xCtrl Is ComboBox) Then
                xCtrl.Text = ""
            End If
        Next
    End Sub

    Public Function gmkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer, _
                                       ByRef grdView As DataGridView)
        Return IIf(IsDBNull(grdView.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdView.Rows(irow).Cells(iCol).Value)
    End Function

    Public Sub gClearGrpTxt(ByVal stGrpBox As GroupBox)
        Dim xCtrl As Control
        For Each xCtrl In stGrpBox.Controls
            If (TypeOf xCtrl Is TextBox) Or (TypeOf xCtrl Is ComboBox) Or (TypeOf xCtrl Is MaskedTextBox) Then
                xCtrl.Text = ""
            End If
        Next
    End Sub

    Public Sub gDisableFormTxt(ByVal stForm As Form)
        Dim xCtrl As Control
        For Each xCtrl In stForm.Controls
            If (TypeOf xCtrl Is TextBox) Or (TypeOf xCtrl Is ComboBox) Or (TypeOf xCtrl Is MaskedTextBox) Then
                xCtrl.Enabled = False
            End If
        Next
    End Sub

    Public Sub gEnableFormTxt(ByVal stForm As Form)
        Dim xCtrl As Control
        For Each xCtrl In stForm.Controls
            If (TypeOf xCtrl Is TextBox) Or (TypeOf xCtrl Is ComboBox) Or (TypeOf xCtrl Is MaskedTextBox) Then
                xCtrl.Enabled = True
            End If
        Next
    End Sub

    Public Sub gCreateTextbox(ByRef stForm As Form, ByVal xPoint As Integer)
        Dim xCnt As Integer = 0
        Dim yPnt As Integer = 0
        Dim text As TextBox
        Dim sSQL As String = "Select seg_charsize from dbo.mSegment where co_id = '" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    text = New TextBox
                    stForm.Controls.Add(text)
                    text.Visible = True
                    text.Name = "text" & CStr(xCnt)
                    text.Location = New Point(xPoint + (3 * yPnt), 152)
                    text.Size = New Size(60, 21)
                    text.MaxLength = rd.Item(0)
                    xCnt += 1
                    yPnt += 22
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub

#End Region

#Region "Transactions"

    Public Function gGetSegmentSeparator(ByVal gSequence As Integer) As String
        Dim stVal As String = ""
        Dim sVal As String

        Dim sSQL As String = "Select seg_separator from dbo.mSegment where co_id = '" & gCompanyID() & "' and seg_sequence = '" & gSequence & "' "
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
            While rd.Read
                stVal = rd.Item(0).ToString
            End While
        End Using

        sVal = stVal
        Return sVal
    End Function

    Public Function getSegment(ByRef sForm As Form, ByVal xVal As Integer) As String
        Dim xCnt As Integer
        Dim xSeq As Integer = 0
        Dim xSeg As String = ""
        Dim sSeg As String = ""
        For xCnt = xVal To sForm.Controls.Count - 1
            xSeq += 1
            If xSeq = (sForm.Controls.Count - xVal) Then
                xSeg += sForm.Controls(xCnt).Text
            Else
                xSeg += sForm.Controls(xCnt).Text + gGetSegmentSeparator(xSeq)
            End If
        Next
        Dim iCnt As Integer
        If xSeg(xSeg.Length - 1) = gGetSegmentSeparator(1) Then
            For iCnt = 0 To xSeg.Length - 2
                sSeg += xSeg(iCnt)
            Next
        Else
            For iCnt = 0 To xSeg.Length - 1
                sSeg += xSeg(iCnt)
            Next
        End If

        If sSeg.Substring(0, 1) = gGetSegmentSeparator(1) Then
            sSeg = sSeg.Substring(1, sSeg.Length - 1)
        End If

        Return sSeg
    End Function

    Public Function gSegmentValue(ByVal psAcntType As String) As String

        Dim sVal As String
        Dim stVal As String = ""
        Dim sSQL As String = "select seg_value from dbo.mAccountType where acnt_type ='" & psAcntType & "' and co_id = '" & gCompanyID() & "' "

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
            While rd.Read
                stVal = rd.Item(0).ToString
            End While
        End Using

        sVal = stVal
        Return sVal
    End Function

    Public Function gSegmentFromAccounts(ByVal psAccount As String) As String

        Dim sVal As String
        Dim stVal As String = ""
        Dim sSQL As String = "select acnt_code from dbo.mAccounts where acnt_desc='" & psAccount & "' and co_id ='" & gCompanyID() & "' and acnt_deleted= 0"
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
            While rd.Read
                stVal = rd.Item(0).ToString
            End While
        End Using

        sVal = stVal
        Return sVal
    End Function

    Public Sub gCalculateEndingBalance(ByVal psNewEndBal As String, _
                                                ByVal psBank As String, _
                                                ByRef oForm As Form)
        Dim pdDate As Date = Microsoft.VisualBasic.FormatDateTime(Now.Date, DateFormat.ShortDate)
        Try
            Dim sSQLCmd As String = "UPDATE mAccounts SET acnt_balance_less ='" & psNewEndBal & "' "
            sSQLCmd &= ",updated_by='" & gUserName & "' "
            sSQLCmd &= ",date_updated=' " & pdDate & "' "
            sSQLCmd &= " where acnt_name ='" & psBank & "' and co_id='" & gCompanyID() & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub

    Public Function gSaveCheck(ByVal psChkID As String, _
                                ByVal psBankID As String, _
                                ByVal psSupCustID As String, _
                                ByVal pdChkdate As Date, _
                                ByVal psChkAmtF As String, _
                                ByVal psChkAmtW As String, _
                                ByVal psAddress As String, _
                                ByVal psPayOrder As String, _
                                ByVal psMemo As String) As String
        Dim sMsg As String = ""
        Try
            Dim sSQLCmd As String = "usp_t_writecheck_save "
            sSQLCmd &= "@coid='" & gCompanyID() & "' "
            sSQLCmd &= ",@checkid=" & IIf(psChkID = "", "NULL", "'" & psChkID & "'") & " "
            sSQLCmd &= ",@acntid=" & IIf(psBankID = "", "NULL", "'" & psBankID & "'") & " "
            sSQLCmd &= ",@supcustid=" & IIf(psSupCustID = "", "NULL", "'" & psSupCustID & "'") & " "
            sSQLCmd &= ",@chkdate='" & pdChkdate & "' "
            sSQLCmd &= ",@chkamtf='" & psChkAmtF & "' "
            sSQLCmd &= ",@chkamtw='" & psChkAmtW & "' "
            sSQLCmd &= ",@supcustadd='" & psAddress & "' "
            sSQLCmd &= ",@chkmemo='" & psMemo & "' "
            sSQLCmd &= ",@createdby=' " & gUserName & "' "
            sSQLCmd &= ",@payorder='" & psPayOrder & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            sMsg = "Save Successful...."
        Catch ex As Exception
            sMsg = Err.Description
        End Try
        Return sMsg
    End Function

    Public Sub gSaveCheckExpenses(ByVal psChkID As String, _
                                    ByVal psAcntID As String, _
                                    ByVal psAmount As Long, _
                                    ByVal psMemo As String, _
                                    ByVal psCustID As String, _
                                    ByVal pbBillable As Integer, _
                                    ByRef oForm As Form)
        Try
            Dim sSQLCmd As String = "usp_t_cwexpenses_save "
            sSQLCmd &= "@coid='" & gCompanyID() & "' "
            sSQLCmd &= ",@checkid=" & IIf(psChkID = "", "NULL", "'" & psChkID & "'") & " "
            sSQLCmd &= ",@acntid=" & IIf(psAcntID = "", "NULL", "'" & psAcntID & "'") & " "
            sSQLCmd &= ",@amount='" & psAmount & "' "
            sSQLCmd &= ",@memo='" & psMemo & "' "
            sSQLCmd &= ",@custID=" & IIf(psCustID = "", "NULL", "'" & psCustID & "'") & " "
            sSQLCmd &= ",@billable='" & pbBillable & "' "
            sSQLCmd &= ",@createdby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub

    Public Sub gSaveCheckItems(ByVal psChkID As String, _
                                        ByVal psItemID As String, _
                                        ByVal psDescription As String, _
                                        ByVal psUnit As String, _
                                        ByVal psQuantity As Integer, _
                                        ByVal psCost As Long, _
                                        ByVal psAmount As Long, _
                                        ByVal psCustomerID As String, _
                                        ByVal pbBillable As Integer, _
                                        ByVal psPOID As String, _
                                        ByRef oForm As Form)
        Try
            Dim sSQLCmd As String = "usp_t_wcitems_save "
            sSQLCmd &= "@coid='" & gCompanyID() & "' "
            sSQLCmd &= ",@checkid=" & IIf(psChkID = "", "NULL", "'" & psChkID & "'") & " "
            sSQLCmd &= ",@itemid=" & IIf(psItemID = "", "NULL", "'" & psItemID & "'") & " "
            sSQLCmd &= ",@description='" & psDescription & "' "
            sSQLCmd &= ",@itemunit='" & psUnit & "' "
            sSQLCmd &= ",@itemqty='" & psQuantity & "' "
            sSQLCmd &= ",@itemcost='" & psCost & "' "
            sSQLCmd &= ",@itemamnt='" & psAmount & "' "
            sSQLCmd &= ",@custid=" & IIf(psCustomerID = "", "NULL", "'" & psCustomerID & "'") & " "
            sSQLCmd &= ",@billable='" & pbBillable & "' "
            sSQLCmd &= ",@poid =" & IIf(psPOID = "", "NULL", "'" & psPOID & "'") & " "
            sSQLCmd &= ",@createdby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub

    Public Function gSaveBillExpenses(ByVal psChkID As String, _
                                    ByVal psExpID As String, _
                                     ByVal psAcntID As String, _
                                     ByVal psDebit As Decimal, _
                                     ByVal psCredit As Decimal, _
                                     ByVal psMemo As String, _
                                     ByVal psCustID As String, _
                                     ByRef oForm As Form) As String
        Try
            Dim msg As String = ""
            Dim sSQLCmd As String = "usp_t_billexpenses_save "
            sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
            sSQLCmd &= ",@fxKeyBill='" & psChkID & "' "
            sSQLCmd &= ",@fxKeyExpense =" & IIf(psExpID = "", "NULL", "'" & psExpID & "'") & " "
            sSQLCmd &= ",@fxKeyAccount=" & IIf(psAcntID = "", "NULL", "'" & psAcntID & "'") & " "
            sSQLCmd &= ",@fdDebit='" & psDebit & "' "
            sSQLCmd &= ",@fdCredit='" & psCredit & "' "
            sSQLCmd &= ",@fcMemo='" & psMemo & "' "
            sSQLCmd &= ",@fxKeyCustomer=" & IIf(psCustID = "", "NULL", "'" & psCustID & "'") & " "
            sSQLCmd &= ",@fuCreatedby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            msg = "save successful..."
            Return msg
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
            Return Nothing
        End Try
    End Function

    Public Sub gSaveBillItems(ByVal psChkID As String, _
                                       ByVal psItemID As String, _
                                       ByVal psDescription As String, _
                                       ByVal psUnit As String, _
                                       ByVal psQuantity As Integer, _
                                       ByVal psCost As Long, _
                                       ByVal psAmount As Long, _
                                       ByVal psCustomerID As String, _
                                       ByVal pbBillable As Integer, _
                                       ByVal psPOID As String, _
                                       ByRef oForm As Form)
        Try
            Dim sSQLCmd As String = "usp_t_billitems_save "
            sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
            sSQLCmd &= ",@fxKeyBill=" & IIf(psChkID = "", "NULL", "'" & psChkID & "'") & " "
            sSQLCmd &= ",@fxKeyItem=" & IIf(psItemID = "", "NULL", "'" & psItemID & "'") & " "
            sSQLCmd &= ",@description='" & psDescription & "' "
            sSQLCmd &= ",@itemunit='" & psUnit & "' "
            sSQLCmd &= ",@itemqty='" & psQuantity & "' "
            sSQLCmd &= ",@itemcost='" & psCost & "' "
            sSQLCmd &= ",@itemamnt='" & psAmount & "' "
            sSQLCmd &= ",@custid=" & IIf(psCustomerID = "", "NULL", "'" & psCustomerID & "'") & " "
            sSQLCmd &= ",@billable='" & pbBillable & "' "
            sSQLCmd &= ",@poid =" & IIf(psPOID = "", "NULL", "'" & psPOID & "'") & " "
            sSQLCmd &= ",@createdby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub

    Public Function gSaveBillEntry(ByVal psKeyBill As String, _
                                           ByVal psKeySupplier As String, _
                                           ByVal psKeyTerms As String, _
                                           ByVal psMemo As String, _
                                           ByVal pdTransDate As Date, _
                                           ByVal pnAmountDue As Decimal, _
                                           ByVal pdDueDate As Date, _
                                           ByVal psRefNo As String, _
                                           ByVal pbBillReceived As Integer, _
                                           ByVal pbBillType As Integer, _
                                           ByVal pdDiscDate As Date, _
                                           ByVal Type As String, _
                                           ByVal psCenterCode As String, _
                                           ByVal psReferenceNo As Integer)
        'Dim psAPV As String = CStr(Format(Now.Year, "0000")) + "-" + CStr(Format(Now.Month, "00")) + +CStr(Format(Now.Hour, "00")) + CStr(Format(Now.Millisecond, "00"))
        Dim psAPV As String = getBillAPVNo()
        Dim sMsg As String = ""
        Dim Classrefno As String = frm_vend_EnterBills.ClassRefno
        Try
            Dim sSQLCmd As String = "usp_t_tbillentry_save "
            sSQLCmd &= " @fxKeyBill='" & psKeyBill & "' "
            sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
            sSQLCmd &= ",@fxKeySupplier=" & IIf(psKeySupplier = "", "NULL", "'" & psKeySupplier & "'") & " "
            sSQLCmd &= ",@fxKeyTerms=" & IIf(psKeyTerms = "", "NULL", "'" & psKeyTerms & "'") & " "
            sSQLCmd &= ",@fcMemo='" & psMemo & "' "
            sSQLCmd &= ",@fdTransDate='" & pdTransDate & "' "
            sSQLCmd &= ",@fnAmountDue='" & pnAmountDue & "' "
            sSQLCmd &= ",@fdDueDate='" & pdDueDate & "' "
            sSQLCmd &= ",@fcRefNo='" & psRefNo & "' "
            sSQLCmd &= ",@fbBillReceived='" & pbBillReceived & "' "
            sSQLCmd &= ",@fbBillType='" & pbBillType & "' "
            sSQLCmd &= ",@fuCreatedby='" & gUserName & "' "
            sSQLCmd &= ",@fdDiscDate='" & pdDiscDate & "' "
            sSQLCmd &= ",@fcType='" & Type & "' "
            sSQLCmd &= ",@fcAPVno='" & psAPV & "' "
            sSQLCmd &= ",@fcCenterCode='" & psCenterCode & "' "
            sSQLCmd &= ",@fcReferenceNo='" & psReferenceNo & "' "
            sSQLCmd &= ", @fxClassRefNo ='" & Classrefno & "'"
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
            sMsg = "Save Successful...."
        Catch ex As Exception
            sMsg = Err.Description
        End Try
        Return sMsg
    End Function

    Public Sub gSaveDepositDetails(ByVal psDepID As String, _
                                        ByVal psDepToID As String, _
                                        ByVal pdDepDate As Date, _
                                        ByVal psDepMemo As String, _
                                        ByVal psNameID As String, _
                                        ByVal psAccountID As String, _
                                        ByVal psMemo As String, _
                                        ByVal psChkNo As String, _
                                        ByVal psPmtMeth As String, _
                                        ByVal pdAmount As Long, _
                                        ByRef oForm As Form)
        Try
            Dim sSQLCmd As String = "usp_t_makedeposit_save "
            sSQLCmd &= "@coid='" & gCompanyID() & "' "
            sSQLCmd &= ",@depositID=" & IIf(psDepID = "", "NULL", "'" & psDepID & "'") & " "
            sSQLCmd &= ",@depositto=" & IIf(psDepToID = "", "NULL", "'" & psDepToID & "'") & " "
            sSQLCmd &= ",@datedeposit='" & pdDepDate & "' "
            sSQLCmd &= ",@depositmemo='" & psDepMemo & "' "
            sSQLCmd &= ",@recievefrom=" & IIf(psNameID = "", "NULL", "'" & psNameID & "'") & " "
            sSQLCmd &= ",@fromaccount=" & IIf(psAccountID = "", "NULL", "'" & psAccountID & "'") & " "
            sSQLCmd &= ",@memo='" & psMemo & "' "
            sSQLCmd &= ",@checkno='" & psChkNo & "' "
            sSQLCmd &= ",@memo='" & psPmtMeth & "' "
            sSQLCmd &= ",@amount='" & pdAmount & "' "
            sSQLCmd &= ",@amount='" & pdAmount & "' "
            sSQLCmd &= ",@createdby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub


    Public Sub gUpdateAccountBalanceAmount(ByVal psAccountID As String, _
                                            ByVal psBalAmount As Decimal, _
                                            ByVal oForm As Form)
        Try
            Dim sSQLCmd As String = "usp_t_accountbalanceamount_update "
            sSQLCmd &= "@accountid =" & IIf(psAccountID = "", "NULL", "'" & psAccountID & "'") & " "
            sSQLCmd &= ",@amount='" & psBalAmount & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub

    Public Sub gSaveCashBack(ByVal psDepID As String, _
                                ByVal psAccountID As String, _
                                      ByVal psMemo As String, _
                                       ByVal pdAmount As Long, _
                                            ByVal oForm As Form)
        Try
            Dim sSQLCmd As String = "usp_t_cashback_save "
            sSQLCmd &= "@coid='" & gCompanyID() & "' "
            sSQLCmd &= ",@depositID=" & IIf(psDepID = "", "NULL", "'" & psDepID & "'") & " "
            sSQLCmd &= ",@accountid =" & IIf(psAccountID = "", "NULL", "'" & psAccountID & "'") & " "
            sSQLCmd &= ",@memo='" & psMemo & "' "
            sSQLCmd &= ",@amount='" & pdAmount & "' "
            sSQLCmd &= ",@createdby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub

    Public Sub gSaveTransferFunds(ByVal psTransferFrom As String, _
                                  ByVal psTransferTo As String, _
                                  ByVal pdTransferAmt As Long, _
                                  ByVal psTransferMemo As String, _
                                  ByVal pdTransferDate As Date, _
                                  ByRef oForm As Form)
        Try
            Dim sSQLCmd As String = "usp_t_transferfunds_save "
            sSQLCmd &= "@coid='" & gCompanyID() & "' "
            sSQLCmd &= ",@transferfrom=" & IIf(psTransferFrom = "", "NULL", "'" & psTransferFrom & "'") & " "
            sSQLCmd &= ",@transferto =" & IIf(psTransferTo = "", "NULL", "'" & psTransferTo & "'") & " "
            sSQLCmd &= ",@transferamt='" & pdTransferAmt & "' "
            sSQLCmd &= ",@transfermemo='" & psTransferMemo & "' "
            sSQLCmd &= ",@transferdate='" & pdTransferDate & "' "
            sSQLCmd &= ",@createdby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub

    Public Sub gSaveClosingDate(ByVal psClosingDate As Date, _
                                 ByVal psPassword As String, _
                                  ByRef oForm As Form)
        Try
            Dim sSQLCmd As String = " "
            sSQLCmd &= "@coid='" & gCompanyID() & "' "
            sSQLCmd &= ",@date ='" & psClosingDate & "' "
            sSQLCmd &= ",@password ='" & psPassword & "' "
            sSQLCmd &= ",@createdby='" & gUserName & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub

    Public Sub gSaveReconcilation(ByVal psReconcileID As String, _
                                  ByVal psAccountID As String, _
                                  ByVal pdDatePeriod As Date, _
                                  ByVal pdBeginBal As Long, _
                                  ByVal pdEndBal As Long, _
                                  ByVal pdSrvcChrg As Long, _
                                  ByVal pdSrvcChrgDate As Date, _
                                  ByVal psSrvcAcntID As String, _
                                  ByVal pdIntEarned As Long, _
                                  ByVal pdIntEarnedDate As Date, _
                                  ByVal psIntAcntID As String, _
                                  ByVal pdDepCredits As Long, _
                                  ByVal pdChkPayments As Long, _
                                  ByVal pdDiffAmount As Long, _
                                  ByVal pbReconciled As Integer, _
                                  ByRef oForm As Form)
        Try
            Dim sSQLCmd As String = "usp_t_reconcile_save "
            sSQLCmd &= "@fxreconcileID='" & psReconcileID & "' "
            sSQLCmd &= ",@fxCoID='" & gCompanyID() & "' "
            sSQLCmd &= ",@fxaccountID='" & psAccountID & "' "
            sSQLCmd &= ",@fdDatePeriod='" & pdDatePeriod & "' "
            sSQLCmd &= ",@fdBeginBal='" & pdBeginBal & "' "
            sSQLCmd &= ",@fdEndBal='" & pdEndBal & "' "
            sSQLCmd &= ",@fdSrvcChrg='" & pdSrvcChrg & "' "
            sSQLCmd &= ",@fdSrvcChrgDate='" & pdSrvcChrgDate & "' "
            sSQLCmd &= ",@fxSrvcAccount=" & IIf(psSrvcAcntID = "", "NULL", "'" & psSrvcAcntID & "'") & " "
            sSQLCmd &= ",@fdIntEarned='" & pdIntEarned & "' "
            sSQLCmd &= ",@fdIntEarnedDate='" & pdIntEarnedDate & "' "
            sSQLCmd &= ",@fxIntAccount=" & IIf(psIntAcntID = "", "NULL", "'" & psIntAcntID & "'") & " "
            sSQLCmd &= ",@fdDepCredits='" & pdDepCredits & "' "
            sSQLCmd &= ",@fdChkPayments ='" & pdChkPayments & "' "
            sSQLCmd &= ",@fdDiffAmount='" & pdDiffAmount & "' "
            sSQLCmd &= ",@fbReconciled='" & pbReconciled & "' "
            sSQLCmd &= ",@fucreatedby='" & gUserName & "' "

            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)

        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE@Accounting :" + oForm.Text)
        End Try
    End Sub

    Public Function gVerifyCompanyUser(ByVal psPassword As String, ByRef oForm As Form) As String
        Try
            Dim sVal As String = ""
            Dim sPassword As String
            Dim sSQLCmd As String = "select co_password, co_legalname, co_taxid, co_street, co_city, co_zip, co_phone, co_fax, co_emailadd, co_website, co_Classing from dbo.mCompany "
            sSQLCmd &= "where co_password ='" & psPassword & "' and co_id='" & gCompanyID() & "' and co_deleted='" & 0 & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    sVal = rd.Item(0).ToString
                    If sVal <> "" Then
                        gLegalName = rd.Item(1).ToString
                        gTaxId = rd.Item(2).ToString
                        gStAdd = rd.Item(3).ToString
                        gCityAdd = rd.Item(4).ToString
                        gZipAdd = rd.Item(5).ToString
                        gPhone = rd.Item(6).ToString
                        gFax = rd.Item(7).ToString
                        gEmail = rd.Item(8).ToString
                        gWebSite = rd.Item(9).ToString
                        gClassingStatus = rd.Item(10).ToString
                    End If
                End While
            End Using

            If sVal = psPassword Then
                sPassword = "Load Successful."
            Else
                sPassword = "Incorrect Password."
            End If
            Return sPassword
        Catch ex As Exception
            Return Err.Description
        End Try
    End Function

    Public Function getBillReferenceNo() As String
        'Dim cboTemp As New ComboBox
        'Dim xValidate() As String
        'Dim Result As String = ""
        'Dim sSQLCmd As String = "select fcRefNo from dbo.tBills WHERE fxKeyCompany ='" & gCompanyID() & "' order by fcRefNo desc"

        'Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)

        '    While rd.Read
        '        cboTemp.Items.Add(rd.Item(0).ToString)
        '    End While

        '    rd.Close()

        '    If cboTemp.Items.Count > 0 Then
        '        xValidate = Split(cboTemp.Items(0).ToString, "-")
        '        If CInt(xValidate(0)) = CInt(Now.Year) Then
        '            If CInt(xValidate(1)) = CInt(Now.Month) Then
        '                Result = CStr(Now.Year) + "-" + CStr(Format(Now.Month, "00")) + "-" + CStr(Format(CInt(xValidate(2)) + 1, "00000"))
        '            ElseIf CInt(xValidate(1)) < CStr(Format(Now.Month, "00")) Then
        '                Result = CStr(Now.Year) + "-" + CStr(Format(Now.Month, "00")) + "-" + CStr(Format(1, "00000"))
        '            End If
        '        ElseIf xValidate(0) < CStr(Now.Year) Then
        '            Result = CStr(Now.Year) + "-" + CStr(Format(Now.Month, "00")) + "-" + CStr(Format(1, "00000"))
        '        End If
        '    Else
        '        Result = CStr(Now.Year) + "-" + CStr(Format(Now.Month, "00")) + "-" + CStr(Format(1, "00000"))
        '    End If

        'End Using

        'Return Result

        Dim Ctr As Integer = 1
        Dim xRefNo As String = ""
        Dim xAvaible As Boolean = False
        Dim sSqlCmd As String = ""
        Dim xFxKeyCompany As String = gCompanyID()
        Do While xAvaible = False
            xRefNo = "APV-" & Format(Ctr, "00000000")
            Try
                sSqlCmd = "usp_t_GenerateAPVRefNo '" & xRefNo & "','" & xFxKeyCompany & "'"
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        xAvaible = True
                    End If
                End Using
            Catch ex As Exception
                MsgBox("Error at getBillReferenceNo in clsTransactionFunctions module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
            End Try
            Ctr += 1
        Loop
        Return xRefNo
    End Function

    Public Function getBillAPVNo() As String
        Dim cboTemp As New ComboBox
        Dim xValidate() As String
        Dim Result As String = ""
        Dim sSQLCmd As String = "select fcAPVno from dbo.tBills order by fcAPVno desc"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
        If cboTemp.Items.Count > 0 Then
            xValidate = Split(cboTemp.Items(0).ToString, "-")
            If CInt(xValidate(1)) = CInt(Now.Year) Then
                If CInt(xValidate(2)) = CInt(Now.Month) Then
                    Result = "APV-" + CStr(Now.Year) + "-" + CStr(Format(Now.Month, "00")) + "-" + CStr(Format(CInt(xValidate(3)) + 1, "00000"))
                ElseIf CInt(xValidate(2)) < CStr(Format(Now.Month, "00")) Then
                    Result = "APV-" + CStr(Now.Year) + "-" + CStr(Format(Now.Month, "00")) + "-" + CStr(Format(1, "00000"))
                End If
            ElseIf xValidate(1) < CStr(Now.Year) Then
                Result = "APV-" + CStr(Now.Year) + "-" + CStr(Format(Now.Month, "00")) + "-" + CStr(Format(1, "00000"))
            End If
        Else
            Result = "APV-" + CStr(Now.Year) + "-" + CStr(Format(Now.Month, "00")) + "-" + CStr(Format(1, "00000"))
        End If

        Return Result
    End Function

    Public Sub getTermsDetails(ByVal psTermID As String, _
                                 ByRef lbltemp As Label, _
                                  ByRef lblDiscDate As Label, _
                                   ByVal transdate As Date, _
                                     ByRef dtePicker As DateTimePicker, _
                                      ByRef sForm As Form, _
                                        ByRef sTerms As String)
        Dim NoDayDue As Integer
        Dim DiscPercent As Decimal
        Dim DiscDay As Integer
        Dim NextDue As Integer

        Dim sSQLCmd As String = "usp_m_mterms_load "
        sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fxKeyTerms='" & psTermID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)

                While rd.Read
                    NoDayDue = rd.Item(0)
                    DiscPercent = rd.Item(1)
                    DiscDay = rd.Item(2)
                    NextDue = rd.Item(3)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        

        dtePicker.Value = DateAdd(DateInterval.Day, NoDayDue, transdate)

        If InStr(sTerms, "Due on Receipt", CompareMethod.Binary) > 0 Then
            gDateBillDue = transdate
        Else
            gDateBillDue = DateAdd(DateInterval.Day, NoDayDue, transdate)
        End If


        If DiscDay > 0 Then
            lbltemp.Visible = True
            lbltemp.Text = "Discount Date"
            lblDiscDate.Visible = True
            lblDiscDate.Text = DateAdd(DateInterval.Day, DiscDay, transdate)
            gDiscDate = DateAdd(DateInterval.Day, DiscDay, transdate)
        Else
            lbltemp.Text = ""
            lblDiscDate.Text = ""
            lbltemp.Visible = False
            lblDiscDate.Visible = False
        End If

    End Sub

    Public Sub getTermsDetailsrecurring(ByVal psTermID As String)
        Dim NoDayDue As Integer
        Dim DiscPercent As Decimal
        Dim DiscDay As Integer
        Dim NextDue As Integer
        Dim sSQLCmd As String = "usp_m_mterms_load "
        sSQLCmd &= "@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fxKeyTerms='" & psTermID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    NoDayDue = rd.Item(0)
                    DiscPercent = rd.Item(1)
                    DiscDay = rd.Item(2)
                    NextDue = rd.Item(3)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
        gDateBillDue = DateAdd(DateInterval.Day, NoDayDue, gDate)
        If DiscDay > 0 Then
            gDiscDate = DateAdd(DateInterval.Day, DiscDay, gDate)
        End If
    End Sub

    Public Function makelink_server(ByVal psPath As String) As String
        Dim msg As String
        Try
            Dim sSQLCmd As String = "usp_t_linkserver_create "
            sSQLCmd &= " @psPath='" & psPath & "' "
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSQLCmd)
            msg = "Successful..."
            Return msg
        Catch ex As Exception
            Call removelink_server()
            msg = "Failed..."
            Return msg
        End Try
    End Function

    Public Sub removelink_server()
        Try
            Dim sSQLCmd As String = "exec sp_dropserver 'TSAPATH', 'droplogins'"
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Public Function removelink_servercheck() As Boolean
        Try
            Dim sSQLCmd As String = "exec sp_dropserver 'TSAPATH', 'droplogins'"
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSQLCmd)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function getDatafromExcel(ByVal psQuerry As String, ByRef grdView As DataGridView) As String
        Dim msg As String
        Try
            Dim sSQLCmd As String = psQuerry

            Dim sSQLCmd2 As String = "exec sp_addlinkedsrvlogin 'TSAPATH', false, NULL, 'admin', NULL "
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSQLCmd2)

            With grdView
                .Columns.Clear()
                .DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd).Tables(0)
            End With
            msg = "Successful..."
            Return msg
        Catch ex As Exception
            Call removelink_server()
            msg = "Failed..."
            Return msg
        End Try
    End Function



#End Region


End Class
